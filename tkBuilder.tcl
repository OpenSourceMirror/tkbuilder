# -------------------------------------------------------------
# tkBuilder.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
# history: v1.0.2a:04/29/00
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------

# (line added for version v1.0.2b - presently commented out - lib inchanged between v1.0.2a and v1.0.2b)
# I added this command on 10/16/04
# tk::unsupported::ExposePrivateCommand *


# determine home directory of tkBuilder
set dir [file dirname [info script]]
switch [file pathtype $dir] {
    absolute {}
    relative {
	set dir [file join [pwd] $dir]
    }
    volumerelative {
	set dirCurrent [pwd]
	cd $dir
	set dir [file join [pwd] [file dirname [file join \
						    [lrange [file split $dir] 1 end]]]]
	cd $dirCurrent
    }
}
if { ![file isdirectory $dir] } {
    set dir [pwd]
}
lappend auto_path $dir
lappend auto_path [file join $dir lib]

# initialize and set system variables
Main::InitializeSystem
Main::SetSystem dirHome $dir
Main::SetSystem dirLib [file join $dir lib]
Main::SetSystem dirIcons [file join $dir icons]

# auto_mkindex [file join $dir lib] *.tcl ; # comment out for production

# register app-tkBuilder to allow tclkit build
package provide app-tkBuilder 1.0

# register the bwidget toolkit
package require BWidget

# register the Tcl message catalog and load directory
package require msgcat
namespace import msgcat::*
mcload [file join $dir dict]

# set main system bindings
Main::SetSystemBindings

# withdraw the main window while its being built
wm withdraw .

option add *Button.padY 0
option add *Button.padX 0
option add *Radiobutton.padY 0
option add *Radiobutton.padX 0
option add *Checkbutton.padY 0
option add *Checkbutton.padX 0
option add *Label.padY  0
option add *Entry.padY  0
option add *Combobox.padX 0

# needed for unix
if { $tcl_platform(platform) == "unix" } {
    # colours
    option add *foreground black
    option add *Entry.background white
    option add *Text.background white
    option add *Listbox.background white
    option add *Combobox.background white
    
    option add *font {helvetica 9 normal}
    option add *highlightThickness 0
    option add *Entry.highlightThickness 1
    option add *Text.highlightThickness 1
    option add *Listbox.highlightThickness 1
    option add *Combobox.highlightThickness 1
}


# open the splash screen; get the ok button to enable it when finished setup
# set butOK [Main::OpenAboutWindow 1]
# update

Utility::SetBackgroundColour
Build::InitializeBuild
State::InitializeState
Project::InitializeProject
Option::InitializeOptions              ; # call before ConfigureWidgetAttributeVariables
Canvas::ConfigureCanvasObjectVariables ; # call before ConfigureWidgetAttributeVariables
MenuEntry::ConfigureMenuEntryVariables ; # call before ConfigureWidgetAttributeVariables
Widget::InitializeWidgetVariables      ; # call before ConfigureWidgetAttributeVariables
Attribute::ConfigureWidgetAttributeVariables
Layout::ConfigureLayoutAttributeVariables
General::ConfigureGeneralVariables

Font::InitializeFonts ; # call after InitializeOptions
option add *Text.font textFont

Icon::MakeIconImages

Main::CreateMainWindow
Tree::CreateTree

# make the project node
Tree::CreateNode 0 [list <untitled> -1 {} 1 project]
Project::SetProjectFileName "<untitled>"
Properties::InitializeWidgetProperties 0
Tree::SetCurrentNode 0
Tree::SetNextNodeNumber 0

# add the main toplevel window, if option is set
if { [Option::GetOption addMainWindowToNewProject] } {
    Project::AddMainWindow
}

# create a frame for the widget tree and the property forms frames
set fraMainTop [frame .fraMainTop -bd 0]
pack $fraMainTop -fill both -expand 1

# make the main toolbar
set fraMainToolbar [frame .fraMainToolbar]
Toolbar::CreateMainToolbar $fraMainToolbar $fraMainTop
Toolbar::ShowMainToolbar

# make the status bar
set fraStatusBar [frame .fraStatusBar -bd 1 -relief raised]
Status::CreateStatusBar $fraStatusBar
Status::ShowStatusBar

# frame to hold name entry dialog and widget tree
set fraLeftPane [frame $fraMainTop.fraLeftPane -bd 0 -highlightthickness 0]
pack $fraLeftPane -side left -fill y -expand 0

# frame for name entry dialog widgets
set fraInput [frame $fraLeftPane.fraInput -bd 2 -relief ridge]
pack $fraInput -side top -fill x -expand 0 -padx 1 -pady 1
Dialog::CreateNameEntry $fraInput

# frame to hold widget tree and its scroll bars
set fraTree [frame $fraLeftPane.fraTree -bd 1 -relief ridge -highlightthickness 0]
pack $fraTree -side left -fill y -expand 0

# frame for the widget toolbar
set fraToolbar [frame $fraLeftPane.fraToolbar -borderwidth 1 -relief ridge]
if { [Option::GetOption widgetToolbarShow ] } {
    pack $fraToolbar -side left -anchor nw -before $fraTree
}
Toolbar::CreateToolbar $fraToolbar $fraTree
Toolbar::UpdateRelation CHILD

# create the widget tree
WidgetTree::CreateWidgetTreeCanvas $fraTree
WidgetTree::CreateNodeLabel 0 ; # wait till now because canvas is parent of label
WidgetTree::RedrawWidgetTree

# this must be called after the tree is created (as this will
# call on root node)
Menu::InitializeMenus

# root node must be initialized before this is called (as this will
# initialize forms display to root node - node number 0)
WidgetForms::CreateWidgetFormsFrame $fraMainTop

Status::UpdateStatusBar

# put a trace on the 'modified' State variable to update the title 
# bar whenever required
trace variable State::_state_(modified) w Status::UpdateTitleBar

# enable the ok button on the splash screen
# $butOK configure -state normal

# wait here until the about window is destroyed
# tkwait variable Main::_waitVariable_

# destroy $Main::_window_
wm deiconify .
# image delete splashImage
# focus -force .

# update the project form
WidgetForms::UpdateAllForms [Project::GetProjectNodeNumber] 1

# for some reason, need the update and notebook command to get things looking
# ok; don't know why, but its late and I'm tired ... onwards!!!!
update
$WidgetForms::_notebook_ itemconfigure attributes -state disabled

# bring focus to project node
focus -force [WidgetTree::GetLabelFromNodeNumber [Project::GetProjectNodeNumber]]

