Combobox version 2.0b2
Copyright (c) 1998-1999, Bryan Douglas Oakley
All Rights Reserved.

http://purl.oclc.org/net/oakley/tcl/combobox/index.html
mailto:oakley@channelpoint.com

This software is provided AS-IS with no waranty expressed or
implied. This software may be used free of charge, though I would
appreciate it if you give credit where credit is due and mention my
name when you use it.

To use: place combobox.tcl where your programs can get at it either
via autoloading or from a "source" command. Then, use
"combobox::combobox" to create a combobox, or do "import
::combobox::*" to use the combobox command without the namespace qualifier.

To see a quick example, start up a wish process and cd to the directory
where combobox.tcl is located. Then do "source example.tcl".

To run a simple test proc, source "test.tcl".

Comments are appreciated.

#####################################################################
