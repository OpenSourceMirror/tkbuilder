# some test code for combobox.tcl

source combobox.tcl
package require combobox 2.0
catch {namespace import combobox::*}

# a simple-minded test routine... it just creates a few comboboxes
# in varying configurations. Ideally it ought to do more by exercising
# some of the options (like setting values then querying them, that
# sort of thing)

proc test {} {
    global someValue  ;# attached to a couple of the combo boxes

    # need a bogus image of some sort to test out the -image
    # option...
    image create bitmap foo -data {
	#define open_width 9
	#define open_height 9
	static unsigned char open_bits[] = {
	    0xff, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x7d, 
	    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0xff, 0x01
	}
    }

    # one of them will use this variable as its -textvariable...
    set ::someValue "some initial value"

    # here are the various combinations we want to try
    array set configurations {
	0 {-editable 1 -state normal -borderwidth 2 \
		-relief sunken -width 20 -command test-command}
	1 {-editable 1 -state normal -borderwidth 2 \
	   -relief sunken -width 30 -selectborderwidth 2 \
	   -selectbackground \#00FF00 -selectforeground \#000000 \
	   -height 4 -highlightthickness 1}
	2 {-editable 1 -state normal -borderwidth 1 -relief sunken \
	   -font fixed -cursor right_ptr \
	   -height 0 -maxheight 6}
	3 {-editable true -state normal -borderwidth 2 -relief raised \
	   -textvariable someValue -command test-command \
	   -height 0 -maxheight 0 -highlightthickness 1 -highlightcolor red}
	4 {-editable false -state normal -borderwidth 2 -relief sunken \
	   -foreground \#ff0000 -highlightthickness 1}
	5 {-editable 0 -state normal -image foo -background \#add8e6 \
	   -value foo}
	6 {-editable 0 -state disabled -textvariable someValue}
    }
	
    catch {trace vdelete  ::someValue w localVariableTrace}
    trace variable someValue w localVariableTrace

    proc localVariableTrace {args} {
	global someValue
	puts -nonewline "from test.tcl; the associated -textvariable"
	puts " has changed: $someValue"
#	for {set level [info level]} {$level > 0} {incr level -1} {
#	    puts "level $level: [info level $level]"
#	}
    }

    # make it so!
    set counter 0
    set width [winfo width .]
    if {$width < 300} {set width 300}
    foreach config [lsort [array names configurations]]  {
	destroy .l$config
	destroy .cb$config
	label .l$config \
		-text $configurations($config) \
		-anchor w \
		-justify l \
		-wraplength $width
	eval combobox .cb$config $configurations($config)
	pack .l$config -side top -fill x -expand n
	pack .cb$config -side top -fill none -expand n -anchor w

	# for testing porpoises... populate each list with all known
	# commands..
	foreach command [lsort [info commands]] {
	    .cb$config list insert end $command
	}
    }

    # the last combobox will have a horizontal scrollbar, to test
    # the -xscrollcommand option
    destroy .f1
    frame .f1 -bd 0
    pack .f1 -side top -fill none -expand n -anchor w
    label .f1.l1 -text "-xscrollcommand {.hsb set} -editable 1"
    combobox .f1.cb1 -xscrollcommand {.f1.hsb set} -editable 1
    scrollbar .f1.hsb \
	    -orient horizontal \
	    -command [list .f1.cb1 xview]
    pack .f1.l1 -side top -fill x -expand n
    pack .f1.cb1 -side top -fill x -expand n
    pack .f1.hsb -side top -fill x -anchor w
    .f1.cb1 configure -value \
	    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

    # for one combobox let's delete all the items and replace them
    # with a short list to make sure the default height works as
    # expected (it should "shrink to fit")
    .cb0 list delete 0 end
    .cb0 list insert end "Hello, World"
    .cb0 list insert end "Thanks for all the fish."
    .cb0 list insert end "challenging text \{ \# \" \["
    
    .cb0 configure -value "Hello, World"

}

# a support routine for the test proc..
proc test-command {args} {
    puts "the widget's -command has been called..."
    set count 0
    foreach arg $args {
	puts "    arg $count: $arg"
	incr count
    }
}

# a hack to get keyboard traversal using the tab key to work. Actually,
# this only affects shift-tab... In essense it traps for the special 
# case of children of a combobox and does a little sleight-of-hand
if {![catch {auto_load tk_focusPrev} result]} {
    catch {
	rename tk_focusPrev __tk_focusPrev
	proc tk_focusPrev {w} {
	    # if the widget in question is a child of a combobox, we'll
	    # redefine w to be the combobox frame
	    set parent [winfo parent $w]
	    if {[string length $parent] > 0 && \
		    [winfo class $parent] == "Combobox"} {
		__tk_focusPrev $parent
	    } else {
		__tk_focusPrev $w
	    }
	}
    }
}

test
