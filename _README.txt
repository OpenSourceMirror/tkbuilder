tkBuilder
This version has ben forked by andrasy from version 1.0.2a
Copyright � 1999-2001 Frank Schnekenburger

Version 1.0.2a was originaly built at Sawpit
e-mail: sawpit@yahoo.com
http: scp.on.ca/sawpit

Summary of modifications:
 - tcl 8.4.13
 - BWidget 1.7
 - msgcat calls added for translation
 - combobox from Bryan Oakley replaced by ::ComboBox of BWidget 1.7


Installing tkBuilder

tkBuilder is coded in pure tcl/tk (version 8.4.13) and is distributed as a set of tcl files zipped into the file tkBuilder_for_tcl8.4.zip (for Windows) and the file tkBuilder_for_tcl8.4.tar.gz (for UNIX). The distribution includes the tcl files for the BWidget package. To install tkBuilder, unzip the distribribution file; to run tkBuilder execute the tcl script 'tkBuilder.tcl'.


Manual

The manual for tkBuilder is included in the distribution (./manual/manual.htm).


Contacting Us

We'd appreciate bug reports, and comments and suggestions for improving tkBuilder and its documentation. To contact us, send e-mail to sawpit@yahoo.com (original version 1.0.2a only).

