# -------------------------------------------------------------
# register.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------


namespace eval Register {
    variable _lastRegisteredWidgets_ {}
    # list of widgets registered in the last invocation of the registry
}


# returns the list of attributes available for 'widget'; returns {} if an
# error occurs creating the widget or running its configure command;
# caller must create and delete the interpreter interpRegister
proc Register::GetWidgetAttributes { widget } {

    # create a dummy widget (.x)
    catch {interpRegister eval destroy .x}
    if { [catch {interpRegister eval [list $widget .x]}] } {
	return {}
    } else {
	# run the configure command; get configure list
	if { [catch {interpRegister eval [list .x configure]} configList] } {
	    return {}
	}
    }

    # build the attribute list from the configure list
    set attributeList {}
    foreach config $configList {
	set attribute [lindex $config 1]
	if { [lsearch $attributeList $attribute] == -1 } {
	    lappend attributeList $attribute
	}
    }

    return $attributeList

}


# creates the interpreter 'interpRegister', using the project resource properties;
# if 'showErrorMessage' is  1, gives error message if there is a problem with any of them
proc Register::CreateInterpreter { {showErrorMessage 1} } {

    if { [interp exists interpRegister] } {
	interp delete interpRegister
    }
    interp create interpRegister
    load {} Tk interpRegister
    interpRegister eval wm withdraw .
    set errorFlag 1 ; # assume no error

    # get project resource properties
    set autoPathList  [split [string trim [Project::GetState Auto_path]] "\n,;"]
    set packageList   [split [string trim [Project::GetState Packages]] "\n,;"]
    set namespaceList [split [string trim [Project::GetState Namespaces]] "\n,;"]

    # register the autopath
    foreach autopath $autoPathList {
	interpRegister eval lappend auto_path $autopath
    }

    # loop once for each package
    foreach package $packageList {
	if { [catch {interpRegister eval package require $package}] } {
	    if { $showErrorMessage } {
		Utility::ErrorMessage "[mc {Unable to load package}] '$package'"
	    }
	    set errorFlag 0
	}
    }
    
    # loop once for each namespace import
    foreach namespace $namespaceList {
	if { [catch {interpRegister eval namespace import $namespace}] } {
	    if { $showErrorMessage } {
		Utility::ErrorMessage "[mc {Unable to import namespace}] '$namespace'"
	    }
	    set errorFlag 0
	}
    }

    return $errorFlag
}


# registers the widgets currently defined in the project's resource property form;
# assumes the interpreter 'interpRegister' has been started up by caller
proc Register::RegisterWidgets { {showErrorMessage 1} } {

    # quietly create interpreter
    #	CreateInterpreter 0
    
    # loop once for each widget in the registration list; build a list 
    # of widgets that checkout ok, from those currently in registration listbox
    set okWidgetList {}
    set errorFlag 0 ; # assume no problems
    set widgetList [split [string trim [Project::GetState Widgets]] "\n,;"]
    foreach widget $widgetList {
	set widget [string trim $widget]

	# [lsearch  [Widget::GetWidgetList] $widget] != -1
	
	# check if widget is a tk widget
	if { [List::ItemExists [Widget::GetWidgetList] $widget] } {
	    set errorFlag 1
	    if { $showErrorMessage } {
		Utility::ErrorMessage "'$widget' [mc {is a}] tk widget"
	    }
	}

	# skip duplicates
	if { ![List::ItemExists $okWidgetList $widget] } {
	    # try making the widget
	    catch {interpRegister eval destroy .x}
	    if { ![catch {interpRegister eval [list $widget .x]}] &&
		 ![catch {interpRegister eval [list .x configure]}] } {
		lappend okWidgetList $widget
	    } else {
		set errorFlag 1
		if { $showErrorMessage } {
		    Utility::ErrorMessage "[mc {Unable to register widget}] '$widget'"
		}
	    }
	}
    }

    Register::SetRegisteredWidgets $okWidgetList

    #	DeleteInterpreter

}



# deletes the interpreter 'interpRegister'
proc Register::DeleteInterpreter { } {
    catch { interp delete interpRegister }
}


# sets list of currently registered widgets
proc Register::SetRegisteredWidgets { widgetList } {
    Project::SetState registeredWidgets $widgetList
}


# returns a list of the currently registered widgets
proc Register::GetRegisteredWidgets { } {
    return [Project::GetState registeredWidgets]
    #	return [Option::GetOption registryWidgetList]
}


# returns list of widgets that were registered in the last invocation of the
# registry; called in attribute.tcl
proc Register::GetLastRegisteredWidgets { } {
    variable _lastRegisteredWidgets_
    return $_lastRegisteredWidgets_
}


# sets list of widgets that were registered in the last invocation of the
# registry; called in attribute.tcl
proc Register::SetLastRegisteredWidgets { widgetList } {
    variable _lastRegisteredWidgets_ $widgetList
}



