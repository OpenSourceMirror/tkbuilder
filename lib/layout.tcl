# -------------------------------------------------------------
# layout.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
# -------------------------------------------------------------


namespace eval Layout {

    namespace export ConfigureLayoutAttributeVariables
    namespace export InitializeLayoutAttributes DeleteLayoutAttributes
    namespace export SetLayoutAttribute GetLayoutAttribute
    namespace export GetLayoutAttributeList GetLayoutType
    namespace export GetLayoutAttributeOption
    namespace export CopyLayoutAttributes
    namespace export CopySelectedLayoutAttributes
    namespace export GetLayoutCommand

    variable _layout_
    # array to store the value of each layout option of each node;
    # indexed as follows: _layout_($nodeNumber.$layoutType.$attribute);
    # layout type is one of: pack, grid, place, none
    # e.g. _layout_(32.pack.pady)
    # note special index for layout type: _layout_($nodeNumber.layout)

    variable _layoutAttributes_
    # array to store the list of attributes for each layout type, indexed on
    # layout type (e.g. _layoutAttributes_(pack) {after anchor ...}

    variable _allLayoutAttributes_
    # a list of all possible layout attributes (over all layout types)

    variable _layoutAttributeOptions_
    # array to store the options available (if any) for each layout
    # attribute, indexed on attribute
    # (e.g. _layoutAttributeOptions_(anchor) {n ne e se s...}

}


# initializes the value of all layout attributes of node 'nodeNumber'
# to {}; creates a new set of array elements in _layout_
# (i.e. _layout_($nodeNumber.*.*))
proc Layout::InitializeLayoutAttributes { nodeNumber } {
    variable _layout_
    
    # check whether the widget has layout or not (e.g. menu
    # entries and canvas objects are not layed out)
    if { [WidgetHasLayout [Tree::GetNodeItem widgetType $nodeNumber]] } {
	#		set _layout_($nodeNumber.layout) pack
	set _layout_($nodeNumber.layout) [Option::GetOption geometryManager] ; # 020400
    } else {
	set _layout_($nodeNumber.layout) none
    }
    foreach layoutType {pack grid place} {		
	foreach attribute [GetLayoutAttributeList $layoutType] {
	    SetLayoutAttribute $layoutType $attribute $nodeNumber {}
	}
    }
}


# returns 1 if widget is supposed to be layed out, 0 if not (i.e. layout
# is 'none')
proc Layout::WidgetHasLayout { widget } {
    if { [MenuEntry::WidgetIsMenuEntry $widget] ||
	 [Canvas::WidgetIsCanvasObject $widget] ||
	 $widget == "menu" || 
	 $widget == "toplevel" } {
	return 0
    } else {
	return 1
    }
}


# unsets the array elements in _layout_ and _layoutType_ used for layout 
# attribute values of 'nodeNumber'
proc Layout::DeleteLayoutAttributes { nodeNumber } {
    variable _layout_
    unset _layout_($nodeNumber.layout)
    foreach layoutType {pack grid place} {		
	foreach attribute [GetLayoutAttributeList $layoutType] {
	    unset _layout_($nodeNumber.$layoutType.$attribute)
	}
    }
}


# sets value for the specified 'layoutType', 'attribute' and 'nodeNumber' to
# 'value'
proc Layout::SetLayoutAttribute { layoutType attribute nodeNumber value } {
    variable _layout_
    set _layout_($nodeNumber.$layoutType.$attribute) $value
}

# returns the value of the specified 'layoutType', 'attribute' and 'nodeNumber'
proc Layout::GetLayoutAttribute { layoutType attribute nodeNumber } {
    variable _layout_
    return [string trim $_layout_($nodeNumber.$layoutType.$attribute)]
}

# sets the layout type of the node 'nodeNumber' to 'layoutType'
proc Layout::SetLayoutType { nodeNumber layoutType } {
    variable _layout_
    set _layout_($nodeNumber.layout) $layoutType
}


# returns the layout type of the node 'nodeNumber'
proc Layout::GetLayoutType { nodeNumber } {
    variable _layout_
    return $_layout_($nodeNumber.layout)
}


# initializes the layout attribute lists and options at startup
proc Layout::ConfigureLayoutAttributeVariables { } {
    SetLayoutAttributeLists
    SetAllLayoutAttributesList
    SetLayoutAttributeOptions
}


# sets the attribute list for each of the layout types
proc Layout::SetLayoutAttributeLists { } {
    variable _layoutAttributes_
    
    set _layoutAttributes_(pack) [list\
				      after anchor before expand fill in ipadx ipady padx pady side]
    set _layoutAttributes_(grid) [list\
				      column columnspan in ipadx ipady padx pady row rowspan sticky]
    set _layoutAttributes_(place) [list\
				       anchor bordermode height in relheight relwidth relx rely width x y]
    set _layoutAttributes_(none) {}
    
}


# returns list of the available attributes for the layout type 'layoutType'
proc Layout::GetLayoutAttributeList { layoutType } {
    variable _layoutAttributes_
    return $_layoutAttributes_($layoutType)
}


# builds a list of all possible layout attributes
proc Layout::SetAllLayoutAttributesList { } {
    variable _allLayoutAttributes_
    set _allLayoutAttributes_ {}
    foreach layoutType {pack grid place} {
	foreach attribute [GetLayoutAttributeList $layoutType] {
	    if { [lsearch $_allLayoutAttributes_ $attribute] == -1 } {
		lappend _allLayoutAttributes_ $attribute
	    }
	}
    }
    set _allLayoutAttributes_ [lsort $_allLayoutAttributes_]
}


# returns list of all possible layout attributes
proc Layout::GetAllLayoutAttributesList { } {
    variable _allLayoutAttributes_
    return $_allLayoutAttributes_
}


# sets the options (if any) available for each attribute
proc Layout::SetLayoutAttributeOptions { } {

    variable _layoutAttributeOptions_
    
    # initialize options of each attribute to null
    foreach attribute [GetAllLayoutAttributesList] {
	set _layoutAttributeOptions_($attribute) {}
    }
    
    set _layoutAttributeOptions_(anchor)     {{} n ne e se s sw w nw center}
    set _layoutAttributeOptions_(bordermode) {{} inside outside}
    set _layoutAttributeOptions_(expand)     {{} true false 1 0}
    set _layoutAttributeOptions_(fill)       {{} x y both none}
    set _layoutAttributeOptions_(side)       {{} top bottom left right}

}


# returns the option for 'attribute'
proc Layout::GetLayoutAttributeOption { attribute } {
    variable _layoutAttributeOptions_
    return $_layoutAttributeOptions_($attribute)
}


# copies the layout attribute values of node 'fromNodeNumber' into 
# node 'toNodeNumber'; creates a new set of array elements in _layout_
# if they don't already exist, or overwrites existing ones
proc Layout::CopyLayoutAttributes { fromNodeNumber toNodeNumber } {
    SetLayoutType $toNodeNumber [GetLayoutType $fromNodeNumber]
    foreach layoutType {pack grid place} {
	set attValList {} ; # to build list of option-value pairs
	foreach attribute [GetLayoutAttributeList $layoutType] {
	    set value [GetLayoutAttribute $layoutType $attribute $fromNodeNumber]
	    SetLayoutAttribute $layoutType $attribute $toNodeNumber $value
	    lappend attValList $attribute $value
	}
	# update layout options of running app
	if { $layoutType == [GetLayoutType $fromNodeNumber] } {
	    Build::UpdateAppLayout $toNodeNumber $attValList
	}
    }
}


# for the attributes listed in 'selectedAttributes', copies the attribute values of node 
# 'fromNodeNumber' into node 'toNodeNumber'; does not create new node attributes if they
# don't already exist, since this procedure can copy among two different types of widgets
# where they have common attributes; only copies attributes of current layout type
proc Layout::CopySelectedLayoutAttributes { fromNodeNumber toNodeNumber selectedAttributes } {
    variable _layout_
    
    # check if the layout manager is the same and needs to be
    if { [Option::GetOption copyLayoutIfSameManager] } {
	if { [GetLayoutType $fromNodeNumber] != [GetLayoutType $toNodeNumber] } {
	    return
	}
    }

    set layoutType [GetLayoutType $fromNodeNumber]
    set attValList {} ; # to build list of option-value pairs
    # loop once for each of the selected attributes
    foreach attribute $selectedAttributes {
	# check that the attribute exists in the to-node
	if { [info exists _layout_($toNodeNumber.$layoutType.$attribute)] } {
	    set value [GetLayoutAttribute $layoutType $attribute $fromNodeNumber]
	    SetLayoutAttribute $layoutType $attribute $toNodeNumber $value
	    lappend attValList $attribute $value
	}
    }
    
    # if option set, change the layout manager to that of the from node
    # check that the to node is not a toplevel, which must be 'none'
    if { [Option::GetOption copyLayoutManager] } {
	if { [Tree::GetNodeItem widgetType $toNodeNumber] != "toplevel" } {
	    SetLayoutType $toNodeNumber [GetLayoutType $fromNodeNumber]
	}
    }

    # update layout options of running app
    Build::UpdateAppLayout $toNodeNumber $attValList

}


# for each of the 'selectedAttributes' of 'nodeNumber', sets the value to {}
proc Layout::ClearSelectedLayoutAttributes { nodeNumber selectedAttributes } {
    set layoutType [GetLayoutType $nodeNumber]
    foreach attribute $selectedAttributes {
	SetLayoutAttribute $layoutType $attribute $nodeNumber {}
    }
}


# returns the command for the layout of this node
proc Layout::GetLayoutCommand { nodeNumber nodePathName} {
    variable _layout_
    set layoutType [GetLayoutType $nodeNumber]
    set commandString "$layoutType $nodePathName"
    foreach attribute [GetLayoutAttributeList $layoutType] {
	set value [GetLayoutAttribute $layoutType $attribute $nodeNumber]
	if { $value != {} } {
	    append commandString " -$attribute $value"
	}
    }
    return $commandString
}


proc Layout::GetLayoutArray { } {
    variable _layout_
    return [array get _layout_]
}


proc Layout::SetLayoutArray { array } {
    variable _layout_
    array set _layout_ $array
}


proc Layout::GetNodeArrayElements { nodeNumber } {
    variable _layout_
    return [array get _layout_ $nodeNumber.*]
}


proc Layout::SetNodeArrayElements { newNodeNumber arrayElements } {
    variable _layout_
    foreach { index value } $arrayElements {
	# get index w/o node number (e.g. 325.pack.side yields .pack.side)
	set parameter [string range $index [string first . $index] end]
	set _layout_($newNodeNumber$parameter) $value
    }
}


# search layout options of 'nodeNumber' for string 'searchString'
proc Layout::FindString { nodeNumber searchString matchCase matchWholeWord } {
    set matchList {}
    foreach layoutType {pack grid place} {
	foreach attribute [GetLayoutAttributeList $layoutType] {
	    set value [GetLayoutAttribute $layoutType $attribute $nodeNumber]
	    set matchValueList [Find::FindMatch $value $searchString $matchCase $matchWholeWord]
	    foreach matchValue $matchValueList {
		lappend matchList "$layoutType-$attribute: $matchValue"
	    }
	}
    }
    return $matchList
}

