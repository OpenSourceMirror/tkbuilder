# -------------------------------------------------------------
# namespace.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------



namespace eval Namespace {
    variable _texEval_ ; # textbox to enter eval text
}


proc Namespace::OpenNamespaceForm { fraNamespace nodeNumber } {

    variable _texEval_

    # on first pass, build the namespace frame
    if { ![winfo exists $fraNamespace] } {

	set frame [frame $fraNamespace]
	pack $frame -anchor nw -fill both -expand 1

	grid [label $frame.lblCode -text "[mc {eval}]:"] \
	    -row 0 -column 0 -sticky w -pady 2 -padx 2
	set texEval [Text::CreateTextbox $frame.texEval \
			 -width 40 -height 35 -wrap none]
	bind $texEval <KeyPress> {State::CheckModifiedState %K %A}
	grid [Text::GetTextboxFrame $texEval] -row 1 -column 0 \
	    -sticky ewns -pady 2 -padx 2
	set _texEval_ $texEval
	
	grid columnconfigure $frame 0 -weight 1
	grid rowconfigure $frame 1 -weight 1
	
    } else {
	
	pack $fraNamespace -anchor nw -fill both -expand 1
	
    }

    Text::SetText $_texEval_ [Special::GetSpecialObject namespace eval $nodeNumber]
    bind $_texEval_ <FocusOut> "Special::SetSpecialObject namespace eval $nodeNumber \
		\[Text::GetText $_texEval_]"

}



