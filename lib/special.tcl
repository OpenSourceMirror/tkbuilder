# -------------------------------------------------------------
# special.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------


namespace eval Special {

    variable _object_
    # array of values of special objects, indexed as follows:
    # _object_($nodeNumber.$objectType.$objectParameter)
    # values for objectType and objectParameter will vary with
    # the type of special object
    
    variable _specialObjectList_ { code file namespace proc template }
    # list of the available special objects; note that 'project' is a
    # special object, but not included here as its not in menu
    
    variable _lastObjectType_ {}
    # last object type that was displayed
    
    variable _texCode_ {}
    # text box used to enter code within a code block

}


# returns list of available special objects
proc Special::GetSpecialObjectList { } {
    variable _specialObjectList_
    return $_specialObjectList_
}


# sets the value of 'objectParameter' of 'objectType' of 'nodeNumber' to 'value'
proc Special::SetSpecialObject { objectType objectParameter nodeNumber value } {
    variable _object_
    set _object_($nodeNumber.$objectType.$objectParameter) $value
}


# returns the value of 'objectParameter' of 'objectType' of 'nodeNumber'
proc Special::GetSpecialObject { objectType objectParameter nodeNumber } {
    variable _object_
    return $_object_($nodeNumber.$objectType.$objectParameter)
}


proc Special::InitializeSpecialObject { nodeNumber } {
    variable _object_
    set objectType [Tree::GetNodeItem widgetType $nodeNumber]
    switch $objectType {
	project {
	    # project data is saved in the project state array
	}
	proc {
	    set _object_($nodeNumber.proc.code) {}
	    set _object_($nodeNumber.proc.params) {}
	    set _object_($nodeNumber.proc.args) {}
	}
	template {
	    set _object_($nodeNumber.template.entryList) {}
	}
	namespace {
	    set _object_($nodeNumber.namespace.eval) {}
	}
	code {
	    set _object_($nodeNumber.code.code) {}
	}
	file {
	    set _object_($nodeNumber.file.name) {}
	    set _object_($nodeNumber.file.code) {}
	}
	default {
	    error "[mc {Unknown object type}]: $objectType"
	}
    }
}



proc Special::CopySpecialObject { fromNode toNode } {
    variable _object_
    # using 'array gets' so don't have to pass object type
    set pos [string length $fromNode]
    foreach { index value } [array get _object_ $fromNode.*] {
	set parameter [string range $index $pos end] ; # index w/o number
	set _object_($toNode$parameter) $value
    }
}


# destroys (unsets) all array elements of the special object
# associated with 'nodeNumber'
proc Special::DeleteSpecialObject { nodeNumber } {
    variable _object_
    # using 'array names' so don't have to pass object type
    foreach element [array names _object_ $nodeNumber.*] {
	unset _object_($element)
    }
    return
}


proc Special::AddSpecialObject { object relation } {

    # handle main/root window object separately
    if { $object == [Project::GetRootName] } {
	# check if project already has a main/root window
	if { [Project::RootNodeExists] } {
	    Utility::ErrorMessage "[mc {Project already has}] [Project::GetRootName] [mc {toplevel}]"
	    return
	} else {
	    set rootNode [WidgetTree::AddWidget $object $relation]
	    Project::SetState rootNodeNumber $rootNode
	    Project::SetState rootNodeExists 1
	}
    } else {
	WidgetTree::AddWidget $object $relation
    }
}


# returns 1 if 'widget' is a special object, 0 otherwise
proc Special::WidgetIsSpecialObject { widget } {
    variable _specialObjectList_
    if { $widget == "project" } {
	return 1
    }
    if { [lsearch $_specialObjectList_ $widget] == -1 } {
	return 0
    } else {
	return 1
    }
}


# returns 1 if 'nodeNumber' is a special object, 0 otherwise
proc Special::NodeIsSpecialObject { nodeNumber } {
    set widgetType [Tree::GetNodeItem widgetType $nodeNumber]
    return [WidgetIsSpecialObject $widgetType]
}


# generic procedure to open special forms; opens form in 'frame' for
# node 'nodeNumber'; calls on other procs to do most of the work
proc Special::OpenSpecialForm { frame nodeNumber } {

    variable _lastObjectType_
    
    # get current object type
    set objectType [Tree::GetNodeItem widgetType $nodeNumber]

    # if object type has changed from the last one that was displayed,
    # hide its packing frame
    if { $objectType != $_lastObjectType_ } {
	catch { pack forget $frame.$_lastObjectType_ }
    }
    set _lastObjectType_ $objectType

    switch $objectType {
	project   { OpenProjectForm $frame.project }
	proc      { OpenProcedureForm $frame.proc $nodeNumber }
	template  { Template::OpenTemplateForm $frame.template $nodeNumber }
	namespace { Namespace::OpenNamespaceForm $frame.namespace $nodeNumber }
	code      { OpenCodeForm $frame.code $nodeNumber }
	file      { OpenFileForm $frame.file $nodeNumber }
	default   { error "[mc {Unknown object type}]: $objectType" }
    }

}


proc Special::OpenProjectForm { fraProject } {

    variable _texProject_
    variable _projectItem_
    variable _projectButton_
    variable _projectLabel_
    variable _projectCmbMainTclFile_

    # on first pass, build the project frame
    if { ![winfo exists $fraProject] } {

	set _projectItem_(itemCurrent) {}

	set frame [frame $fraProject]
	pack $frame -anchor nw -fill both -expand 1

	# labels to display project name
	grid [label $frame.lblPrompt -text "[mc {Project}]:"] \
	    -row 0 -column 0 -sticky w -pady 3 -padx 1	
	grid [label $frame.lblFile] \
	    -row 0 -column 1 -sticky w -pady 3 -padx 1
	set _projectItem_(lblFile) $frame.lblFile

	# labels and drop-down list to select main tcl file
	grid [label $frame.lblMainFile -text "[mc {Main Tcl File}]:"] \
	    -row 1 -column 0 -sticky w -pady 3 -padx 1
	# a combobox entry (having two or more choices)
	set cmbTclFileList [ComboBox $frame.cmbTclFileList \
				-editable 0 -state normal \
				-textvariable Project::_project_(mainTclFile)]
	$cmbTclFileList configure -modifycmd "Special::UpdateMainTclFile $cmbTclFileList"
	grid $cmbTclFileList -row 1 -column 1 -sticky ew -pady 3 -padx 1
	set _projectItem_(cmbTclFileList) $cmbTclFileList

	# frame for all resource-related widgets
	set fraRes [frame $frame.fraRes -bd 1 -relief ridge]
	grid $fraRes -row 2 -column 0 -columnspan 2 -sticky new -padx 2
	grid columnconfigure $frame 1 -weight 1
	grid rowconfigure $frame 2 -weight 1
	
	# frame for resource-related buttons
	set fraBut [Frame::MakeListFrame $fraRes.fraBut]
	grid $fraBut -row 1 -column 0 -sticky nw
	grid columnconfigure $fraRes 1 -weight 1

	# buttons
	Frame::SetListFrameItemArgs $fraBut -height 22 -width 75

	set b7 [button $fraBut.b7 -text [mc {Startup}] -width 40 \
		    -command "Special::UpdateProjectItem Startup"]
	Frame::PackInListFrame $fraBut $b7 -pady 2 -padx 3
	set b2 [button $fraBut.b2 -text [mc {Resources}] -width 40 \
		    -command "Special::UpdateProjectItem Resources"]
	Frame::PackInListFrame $fraBut $b2 -pady 2 -padx 3
	set b1 [button $fraBut.b1 -text [mc {Auto_path}] -width 40 \
		    -command "Special::UpdateProjectItem Auto_path"]
	Frame::PackInListFrame $fraBut $b1 -pady 2 -padx 3
	set b3 [button $fraBut.b3 -text [mc {Packages}] -width 40 \
		    -command "Special::UpdateProjectItem Packages"]
	Frame::PackInListFrame $fraBut $b3 -pady 2 -padx 3
	set b4 [button $fraBut.b4 -text [mc {Namespaces}] -width 40 \
		    -command "Special::UpdateProjectItem Namespaces"]
	Frame::PackInListFrame $fraBut $b4 -pady 2 -padx 3
	set b5 [button $fraBut.b5 -text [mc {Widgets}] -width 40 \
		    -command "Special::UpdateProjectItem Widgets"]
	Frame::PackInListFrame $fraBut $b5 -pady 2 -padx 3
	
	set f1 [frame $fraBut.f1 -height 3 -bd 1 -relief sunken]
	pack $f1 -pady 2 -padx 3 -fill x
	
	set b6 [button $fraBut.b6 -text [mc {Update}] -width 40 \
		    -command Special::UpdateProjectResources]

	Frame::PackInListFrame $fraBut $b6 -pady 2 -padx 3

	# make an array of the button widget names keyed on item
	set _projectButton_(Auto_path)  $b1
	set _projectButton_(Resources)  $b2
	set _projectButton_(Packages)   $b3
	set _projectButton_(Namespaces) $b4
	set _projectButton_(Widgets)    $b5
	set _projectButton_(Startup)    $b7
	
	# make an array of text labels for the items
	set _projectLabel_(Auto_path)  "[mc {lappend autopath}]:"
	set _projectLabel_(Resources)  "[mc {option add}]:"
	set _projectLabel_(Packages)   "[mc {package require}]:"
	set _projectLabel_(Namespaces) "[mc {namespace import}]:"
	set _projectLabel_(Widgets)    "[mc {Widget Registry}]"
	set _projectLabel_(Startup)    "[mc {Tcl Startup Code}]"

	# item label above textbox; text will be specified in UpdateProjectItem
	set _projectItem_(labItem) [label $fraRes.labItem]
	grid $_projectItem_(labItem) -row 0 -column 1 -sticky w
	
	# textbox for input
	set _projectItem_(texItem) [Text::CreateTextbox $fraRes.texItem -height 10]
	grid [Text::GetTextboxFrame $_projectItem_(texItem)] \
	    -row 1 -column 1 -stick new -pady 2 -padx 2
	bind $_projectItem_(texItem) <KeyPress> {State::CheckModifiedState %K %A}
	
	Special::UpdateProjectItem Startup

    } else {
	pack $fraProject -anchor nw -fill both -expand 1
    }

    $_projectItem_(lblFile) configure -text [Project::GetState projectFile]
    $_projectItem_(cmbTclFileList) configure -values {}
    $_projectItem_(cmbTclFileList) configure -values [Project::GetState tclFileList]

}


# called when combobox of tcl files is adjusted
proc Special::UpdateMainTclFile { window {value {}} } {

    if {($value == "") && ([winfo exists $window])} {
	set value [$window get]
    }
    Project::SetState mainTclFile $value
}


# updates the project resource display to show the information for item
# 'item' ... e.g. Packages
proc Special::UpdateProjectItem { item } {
    variable _projectItem_
    variable _projectButton_
    variable _projectLabel_

    # raise the button of the current item
    if { $_projectItem_(itemCurrent) != {} } {
	$_projectButton_($_projectItem_(itemCurrent)) configure -relief raised
	Project::SetState $_projectItem_(itemCurrent) [Text::GetText $_projectItem_(texItem)]
    }
    # sink button of new item
    $_projectButton_($item) configure -relief sunken
    set _projectItem_(itemCurrent) $item

    # set label over text box
    $_projectItem_(labItem) configure -text $_projectLabel_($item)

    # get information for current item from project state variable into textbox
    Text::SetText $_projectItem_(texItem) [Project::GetState $item]

    # change binding on textbox so leaving it puts data in project state variable
    # for the new item
    bind $_projectItem_(texItem) <FocusOut> \
	"Project::SetState $item \[Text::GetText $_projectItem_(texItem)]"
    
    # ensure focus is on the textbox; move cursor to beginning
    focus $_projectItem_(texItem)
    $_projectItem_(texItem) mark set insert 1.0

}


# updates the project resource display to show Resources (called when new, open or
# or save as is used)
proc Special::UpdateProjectDisplay { } {
    variable _projectItem_
    variable _projectButton_

    $_projectButton_($_projectItem_(itemCurrent)) configure -relief raised
    set _projectItem_(itemCurrent) {} ; # so as not to use value of textbox

    # update project file name, in case that has changed
    $_projectItem_(lblFile) configure -text [Project::GetState projectFile]

    Special::UpdateProjectItem Startup
}


proc Special::UpdateProjectResources { } {
    variable _projectItem_

    # ensure state variable for current item is up-to-date
    Project::SetState $_projectItem_(itemCurrent) [Text::GetText $_projectItem_(texItem)]
    
    # update the list of registered widgets
    Register::CreateInterpreter
    Register::RegisterWidgets
    Register::DeleteInterpreter
    
    # update the attribute lists for registered widgets and the list
    # of all attributes and the attribute option lists
    Attribute::UpdateRegisteredWidgetAttributeList
    Attribute::UpdateAllAttributesList
    Attribute::UpdateAttributeOptions

    # update the 'More' widgets menu to show currently registered widgets
    Menu::UpdateMoreWidgetsMenu

}

proc Special::OpenFileForm { fraFile nodeNumber } {

    variable _texFileCode_

    # on first pass, build the file frame
    if { ![winfo exists $fraFile] } {
	set frame [frame $fraFile]
	pack $fraFile -anchor nw -fill both -expand 1
	grid [label $fraFile.lblPrompt -text "[mc {Filename}]:"] \
	    -row 0 -column 0 -sticky w -pady 3 -padx 3
	grid [label $fraFile.lblFile] \
	    -row 0 -column 1 -sticky w -pady 3 -padx 3

	grid [label $fraFile.lblCode -text "[mc {Code}]:"] \
	    -row 1 -column 0 -sticky w -pady 2 -padx 2
	set texCode [Text::CreateTextbox $fraFile.texCode \
			 -width 40 -height 35 -wrap none]
	bind $texCode <KeyPress> {State::CheckModifiedState %K %A}
	grid [Text::GetTextboxFrame $texCode] -row 2 -column 0 \
	    -sticky ewns -pady 2 -padx 2 -columnspan 2
	set _texFileCode_ $texCode

	grid columnconfigure $fraFile 1 -weight 1
	grid rowconfigure $fraFile 2 -weight 1

    } else {

	pack $fraFile -anchor nw -fill both -expand 1

    }

    $fraFile.lblFile configure -text \
	[Special::GetSpecialObject file name $nodeNumber]

    Text::SetText $_texFileCode_ [Special::GetSpecialObject file code $nodeNumber]
    bind $_texFileCode_ <FocusOut> "Special::SetSpecialObject file code $nodeNumber \
		\[Text::GetText $_texFileCode_]"

}


# opens a procedure form within frame 'fraProc' for 'nodeNumber'
proc Special::OpenProcedureForm { fraProc nodeNumber } {

    variable _texProcCode_
    variable _entProcParams_
    variable _entProcArgs_

    if { ![winfo exists $fraProc] } {
	set frame [frame $fraProc]
	pack $frame -anchor nw -fill both -expand 1
	
	# entry for parameters
	grid [label $frame.lblParams -text "[mc {Parameters}]:"] \
	    -row 0 -column 0 -sticky e -pady 2 -padx 2
	set _entProcParams_ [entry $frame.entParams -width 30] 
	grid $_entProcParams_ -row 0 -column 1 -sticky ew -pady 2 -padx 2
	bind $_entProcParams_ <KeyPress> {State::CheckModifiedState %K %A}

	# entry for test args
	grid [label $frame.lblArgs -text "[mc {Test Arguments}]:"] \
	    -row 1 -column 0 -sticky e -pady 2 -padx 2
	set _entProcArgs_ [entry $frame.entArgs -width 30] 
	grid $_entProcArgs_ -row 1 -column 1 -stick ew -pady 2 -padx 2
	bind $_entProcArgs_ <KeyPress> {State::CheckModifiedState %K %A}

	# textbox for code
	grid [label $frame.lblCode -text "[mc {Code}]:"] \
	    -row 2 -column 0 -sticky w -columnspan 2 -pady 2 -padx 2
	set _texProcCode_ [Text::CreateTextbox $frame.texCode \
			       -width 40 -height 35 -wrap none]
	bind $_texProcCode_ <KeyPress> {State::CheckModifiedState %K %A}
	grid [Text::GetTextboxFrame $_texProcCode_] -row 3 -column 0 -columnspan 2 \
	    -sticky ewns -pady 2 -padx 2
	grid columnconfigure $frame 1 -weight 1
	grid rowconfigure $frame 3 -weight 1

    } else {
	pack $fraProc -anchor nw -fill both -expand 1
    }

    # link entry and text widget values to current node
    $_entProcParams_ configure -textvariable Special::_object_($nodeNumber.proc.params)
    $_entProcArgs_ configure -textvariable Special::_object_($nodeNumber.proc.args)
    Text::SetText $_texProcCode_ [Special::GetSpecialObject proc code $nodeNumber]
    bind $_texProcCode_ <FocusOut> "Special::SetSpecialObject proc code $nodeNumber \
			\[Text::GetText $_texProcCode_]"

}


# opens a form for entering code of a code block
proc Special::OpenCodeForm { fraCode nodeNumber } {

    variable _texCode_

    # on first pass, build the code frame
    if { ![winfo exists $fraCode] } {
	set frame [frame $fraCode]
	pack $frame -anchor nw -fill both -expand 1

	grid [label $frame.lblCode -text "[mc {Code}]:"] \
	    -row 0 -column 0 -sticky w -pady 2 -padx 2
	set texCode [Text::CreateTextbox $frame.texCode \
			 -width 40 -height 35 -wrap none]
	bind $texCode <KeyPress> {State::CheckModifiedState %K %A}
	grid [Text::GetTextboxFrame $texCode] -row 1 -column 0 \
	    -sticky ewns -pady 2 -padx 2
	set _texCode_ $texCode

	grid columnconfigure $frame 0 -weight 1
	grid rowconfigure $frame 1 -weight 1
	
    } else {
	pack $fraCode -anchor nw -fill both -expand 1
    }

    # link text widget value to current node
    Text::SetText $_texCode_ [Special::GetSpecialObject code code $nodeNumber]
    bind $_texCode_ <FocusOut> "Special::SetSpecialObject code code $nodeNumber \
		\[Text::GetText $_texCode_]"

}


proc Special::GetSpecialArray { } {
    variable _object_
    return [array get _object_]
}


proc Special::SetSpecialArray { array } {
    variable _object_
    array set _object_ $array
}


proc Special::GetNodeArrayElements { nodeNumber } {
    variable _object_
    return [array get _object_ $nodeNumber.*]
}


proc Special::SetNodeArrayElements { newNodeNumber arrayElements } {
    variable _object_
    foreach { index value } $arrayElements {
	# get index w/o node number (e.g. 325.pack.side yields .pack.side)
	set parameter [string range $index [string first . $index] end]
	set _object_($newNodeNumber$parameter) $value
    }
}


proc Special::FindString { nodeNumber searchString matchCase matchWholeWord } {
    variable _object_
    set matchList {}
    set valueLabelList {}
    set objectType [Tree::GetNodeItem widgetType $nodeNumber]
    switch $objectType {
	project {
	    # project data is saved in the project state array;
	    # ignore now for searching
	}
	proc {
	    lappend valueLabelList \
		[GetSpecialObject proc params $nodeNumber] "[mc {Parameters}]"
	    lappend valueLabelList \
		[GetSpecialObject proc args $nodeNumber] "[mc {Arguments}]"
	    lappend valueLabelList \
		[GetSpecialObject proc code $nodeNumber] "[mc {Code}]"
	}
	template {
	    foreach entry [GetSpecialObject template entryList $nodeNumber] {
		lappend valueLabelList \
		    [GetSpecialObject template $entry $nodeNumber] "$entry"
	    }
	}
	namespace {
	    lappend valueLabelList \
		[GetSpecialObject namespace eval $nodeNumber] "[mc {Eval}]"
	}
	code {
	    lappend valueLabelList \
		[GetSpecialObject code code $nodeNumber] "[mc {Code}]"
	}
	file {
	    lappend valueLabelList \
		[GetSpecialObject file name $nodeNumber] "[mc {Filename}]"
	    lappend valueLabelList \
		[GetSpecialObject file code $nodeNumber] "[mc {Code}]"
	}
    }
    
    foreach { value label } $valueLabelList {
	set matchValueList [Find::FindMatch $value $searchString $matchCase $matchWholeWord]
	foreach matchValue $matchValueList {
	    lappend matchList "$label: $matchValue" 
	}
    }

    return $matchList

}



