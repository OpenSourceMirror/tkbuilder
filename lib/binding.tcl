# -------------------------------------------------------------
# binding.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------



namespace eval Binding {

    variable _binding_
    # array to store the value of each binding of each node and event;
    # indexed as follows: _binding_($nodeNumber.$event)
    
    variable _eventList_
    # array to store the list of events available for each
    # node, indexed on node: _eventList_($nodeNumber) {Activate ButtonPress-1 ...}

    namespace export InitializeWidgetBindings
    namespace export DeleteWidgetBindings
    namespace export GetEventList
    namespace export CopySelectedBindingEvents
    namespace export CopyWidgetBindings
    namespace export GetBindingCommand

}


# intializes the value of each binding of 'nodeNumber' to null
proc Binding::InitializeWidgetBindings { nodeNumber } {
    variable _binding_
    variable _eventList_
    set _eventList_($nodeNumber) {}
    foreach binding [GetDefaultEventList] {
	lappend _eventList_($nodeNumber) $binding
	set _binding_($nodeNumber.$binding) {}
    }
}


# returns list of the system default events
proc Binding::GetSystemDefaultEventList { } {
    return [list \
		<Activate> <ButtonPress-1> <ButtonPress-3> <ButtonRelease-1> <B1-Motion> \
		<B3-Motion> <Circulate> <Colormap> <Configure> <Deactivate> \
		<Destroy> <Double-ButtonPress-1> <Double-ButtonRelease-1> <Enter> \
		<Expose> <FocusIn> <FocusOut> <Gravity> <KeyPress> <KeyRelease> \
		<Motion> <Leave> <Map> <Property> <Reparent> <Unmap> <Visibility> \
	       ]
}


# returns list of default events
proc Binding::GetDefaultEventList { } {
    return [Option::GetOption defaultEventList]
}

# sets the default event list to 'eventList'
proc Binding::SetDefaultEventList { eventList } {
    Option::SetOption defaultEventList $eventList
}


# inserts the event given in 'entry' into 'listbox', which contains the default event
# list
proc Binding::InsertDefaultEvent { listbox entry } {
    set errorFlag 0
    set event [string trim [$entry get]]
    if { [Listbox::ItemExists $listbox $event] } {
	Utility::ErrorMessage "[mc {Event}] '$event' [mc {already occurs}]\n\
			[mc {in default event list}]"
	set errorFlag 1
    } elseif { ![TestEvent $event] } {
	set errorFlag 1
    }
    if { $errorFlag } {
	# error: highlight the entry text for easier edit
	$entry select from 0
	$entry select to end
    } else {
	# no problems
	Listbox::InsertEntry $listbox $entry
    }	
}


# returns the list of events available for node 'nodeNumber'
proc Binding::GetEventList { nodeNumber } {
    variable _eventList_
    return $_eventList_($nodeNumber)
}


# adds the event 'event' to the event list of 'nodeNumber' and
# initializes its command to {}; returns 1 if the event is added,
# 0 otherwise
proc Binding::AddEvent { nodeNumber event } {
    variable _binding_
    variable _eventList_
    # add event to this node if it doesn't already exist
    if { [lsearch $_eventList_($nodeNumber) $event] == -1 } {
	lappend _eventList_($nodeNumber) $event
	set _eventList_($nodeNumber) [lsort -dictionary $_eventList_($nodeNumber)]
	set _binding_($nodeNumber.$event) {}
	return 1
    } else {
	return 0
    }
}


# deletes the event 'event' from the event list of 'nodeNumber' and
# unsets the corresonding _binding_ array element; returns 1 if
# event is deleted, 0 otherwise
proc Binding::DeleteEvent { nodeNumber event } {
    variable _binding_
    variable _eventList_
    
    if { [List::RemoveItem _eventList_($nodeNumber) $event] } {
	unset _binding_($nodeNumber.$event)
	return 1
    } else {
	return 0
    }

}


# allows user to specify an event to insert to the event list
# of the current node
proc Binding::InsertSpecifiedEvent { } {

    variable _binding_
    variable _eventList_
    
    set nodeNumber [Tree::GetCurrentNode]
    
    set dialogReply [Dialog::EnableNameEntry "[mc {Event}]:"]
    if { [lindex $dialogReply 0] == "OK" } {
	set event [lindex $dialogReply 1]
	set event [string trim $event]
	
	# check that an event was specified
	if { $event == {} } {
	    Utility::ErrorMessage "[mc {No event specified}]."
	    return 0
	}
	
	# check if the event already exists for this node
	if { [lsearch $_eventList_($nodeNumber) $event] != -1 } {
	    Utility::ErrorMessage "[mc {Event}] '$event' [mc {already exists}]\
	  			[mc {for this widget}]."
	    return 0
	}
	
	# check that the event is valid
	if { ![TestEvent $event $nodeNumber] } {
	    return 0
	}
	
	# add the event to this node
	AddEvent $nodeNumber $event

	# update binding form to show the change in available events (1 = force update)
	WidgetForms::UpdateBindingForm $nodeNumber 1
	State::SetState modified 1

    }
    
}


# tests that the specified event 'event' is valid for the widget of node 'nodeNumber' 
proc Binding::TestEvent { event {nodeNumber {}} } {
    if { $nodeNumber != {} } {
	set widgetType [Tree::GetNodeItem widgetType $nodeNumber]
	$widgetType .x
    } else {
	button .x ; # as a default, make a button
    }
    if { ![string match <*> $event] || [catch {bind .x $event {puts x}}] } {
	Utility::ErrorMessage "[mc {Invalid event}]: $event"
	destroy .x
	return 0
    }
    destroy .x
    return 1
}


# returns 1 if 'nodeNumber' has 1 or more binding commands, 0 if it has no bindings
proc Binding::NodeHasBinding { nodeNumber } {
    # loop once for each event of this node
    foreach event [GetEventList $nodeNumber] {
	# check for binding command of current event
	if { [string trim [GetBinding $nodeNumber $event]] != {} } {
	    return 1
	}
    }
    return 0
}


# builds the command for the bindings of node 'nodeNumber', which has the
# pathname 'nodePathName'; returns -1 if there are errors, an empty string
# if there are no bindings on the widget, or the binding command(s) otherwise
proc Binding::GetBindingCommand { nodeNumber nodePathName} {
    set commandString {} ; # use to build the command
    # loop once for each event of this node
    foreach event [GetEventList $nodeNumber] {
	# get the binding command of the current event
	set command [GetBinding $nodeNumber $event]
	if { $command != {} } {
	    # ensure that the command is only one word is size
	    if { [llength $command] != 1 } {
		Utility::ErrorMessage "[mc {Invalid command}] '$command'\n\
		  			[mc {for binding of}] $nodePathName\n\
		  			[mc {Command must be one word in size}]\n\
		  			(e.g. [mc {use braces}]: {puts testing})."
		return -1
	    }
	    # append the command to the current command string
	    set commandString "$commandString\nbind $nodePathName $event $command"
	}
    }
    set commandString [string trimleft $commandString "\n"]
    return $commandString
}


# builds the command for the bindings of node 'nodeNumber', which 
# is a canvas object and has the name 'nodeName';
# returns -1 if there are errors, an empty string
# if there are no bindings on the widget, or the binding command(s) otherwise
proc Binding::GetCanvasObjectBindingCommand { nodeNumber nodeName \
						  parentPathName setVariableName } {
    set commandString {} ; # use to build the command
    # loop once for each event of this node
    foreach event [GetEventList $nodeNumber] {
	# get the binding command of the current event
	set command [GetBinding $nodeNumber $event]
	if { $command != {} } {
	    # ensure that the command is only one word is size
	    if { [llength $command] != 1 } {
		Utility::ErrorMessage "[mc {Invalid command}] '$command'\n\
		  			[mc {for binding of canvas object}] '$nodeName'\n\
		  			[mc {of canvas}] '$parentPathName'.\n\
		  			[mc {Command must be one word in size}]."
		return -1
	    }
	    # append the command to the current command string
	    set commandString \
		"$commandString\n$parentPathName bind \$$setVariableName $event $command"
	}
    }
    set commandString [string trimleft $commandString "\n"]
    return $commandString
}



# for the current node, deletes all the events that are
# currently selected, if any, on the binding form
proc Binding::DeleteSelectedEvents { } {

    if { [Option::GetOption confirmDeleteEvents] } {
	set response [Utility::ConfirmMessage "[mc {Delete selected event(s)}]?"]
	if { $response == "no" } { return }
    }

    set nodeNumber [Tree::GetCurrentNode]
    foreach event [WidgetForms::GetSelectedAttributes] {
	DeleteEvent $nodeNumber $event
    }

    # update bind form to show the change in available events (1 = force update)
    WidgetForms::UpdateBindingForm $nodeNumber 1
    State::SetState modified 1
    
}


# unsets the array elements in _binding_ and _eventList_ used for bindings
# of node 'nodeNumber'; (i.e. unsets all elements _binding_($nodeNumber.*) 
# and element _elementList_($nodeNumber)); used when node is deleted from tree
proc Binding::DeleteWidgetBindings { nodeNumber } {
    variable _binding_
    variable _eventList_
    foreach event $_eventList_($nodeNumber) {
	unset _binding_($nodeNumber.$event)
    }
    unset _eventList_($nodeNumber)	
}


# returns the binding for event 'event' of node 'nodeNumber';
# generates an error if the node-event aren't defined
proc Binding::GetBinding { nodeNumber event } {
    variable _binding_
    return [string trim $_binding_($nodeNumber.$event)]
}


# sets the binding for event 'event' of node 'nodeNumber' to 'binding'
# creates the necessary array element in _binding_ if it doesn't already exist
proc Binding::SetBinding { nodeNumber event binding } {
    variable _binding_
    set _binding_($nodeNumber.$event) $binding
}



# copies the events and bindings of node 'fromNodeNumber' into node 'toNodeNumber';
# creates new array elements in _binding_ (i.e. _binding_($toNodeNumber.*))
# if they don't already exist, or overwrites existing ones
proc Binding::CopyWidgetBindings { fromNodeNumber toNodeNumber } {
    variable _binding_
    variable _eventList_

    # check if the to-node is new; initialize its event list if so
    if { ![info exists _eventList_($toNodeNumber)] } {
	set _eventList_($toNodeNumber) {}
    }

    # loop once for each event of the from-node
    foreach event $_eventList_($fromNodeNumber) {
	SetBinding $toNodeNumber $event [GetBinding $fromNodeNumber $event]
	# check if the event is already defined for the to-node
	if { [lsearch $_eventList_($toNodeNumber) $event] == -1 } {
	    lappend _eventList_($toNodeNumber) $event
	}
    }
    # sort the event list for the to-node
    set _eventList_($toNodeNumber) [lsort -dictionary $_eventList_($toNodeNumber)]
}




# for the events listed in 'selectedEvents', copies the events and the values of node 
# 'fromNodeNumber' into node 'toNodeNumber'; creates new node events if they
# don't already exist
proc Binding::CopySelectedBindingEvents { fromNodeNumber toNodeNumber selectedEvents } {
    variable _binding_
    variable _eventList_
    # loop once for each of the selected events
    foreach event $selectedEvents {
	SetBinding $toNodeNumber $event [GetBinding $fromNodeNumber $event]
	# check if the event is already defined for the to-node
	if { [lsearch $_eventList_($toNodeNumber) $event] == -1 } {
	    lappend _eventList_($toNodeNumber) $event
	}
    }
    # sort the event list for the to-node
    set _eventList_($toNodeNumber) [lsort -dictionary $_eventList_($toNodeNumber)]
}


# for each of the 'selectedEvents' of 'nodeNumber', sets the value to {}
proc Binding::ClearSelectedBindingEvents { nodeNumber selectedEvents } {
    foreach event $selectedEvents {
	SetBinding $nodeNumber $event {}
    }
}


proc Binding::GetBindingArray { } {
    variable _binding_
    return [array get _binding_]
}

proc Binding::GetEventListArray { } {
    variable _eventList_
    return [array get _eventList_]
}

proc Binding::SetBindingArray { array } {
    variable _binding_
    array set _binding_ $array
}

proc Binding::SetEventListArray { array } {
    variable _eventList_
    array set _eventList_ $array
}

proc Binding::GetNodeArrayElements { nodeNumber } {
    variable _binding_
    variable _eventList_
    if { [info exists _eventList_($nodeNumber)] } {
	return [list $_eventList_($nodeNumber) [array get _binding_ $nodeNumber.*]]
    } else {
	return {}
    }
}

proc Binding::SetNodeArrayElements { newNodeNumber arrayElements } {
    variable _binding_
    variable _eventList_
    set _eventList_($newNodeNumber) [lindex $arrayElements 0]
    foreach { index value } [lindex $arrayElements 1] {
	# get index w/o node number (e.g. 325.pack.side yields .pack.side)
	set parameter [string range $index [string first . $index] end]
	set _binding_($newNodeNumber$parameter) $value
    }
}



# search events of 'nodeNumber' for string 'searchString'
proc Binding::FindString { nodeNumber searchString matchCase matchWholeWord } {
    set matchList {}
    # loop once for each event of this node
    foreach event [Binding::GetEventList $nodeNumber] {
	set value [Binding::GetBinding $nodeNumber $event]
	set matchValueList [Find::FindMatch $value $searchString $matchCase $matchWholeWord]
	foreach matchValue $matchValueList {
	    lappend matchList "$event: $matchValue" 
	}
    }
    return $matchList
}

