# -------------------------------------------------------------
# buildUpdate.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------

namespace eval Build {
    # NB: these variables are cloned in build.tcl and initialized
    # with Build::InitializeBuild
    variable _enableModification_
    variable _inRunMode_
    variable _nodePathname_
}


# links 'nodeNumber' to the full widget path 'widgetPath' in the running app
proc Build::RegisterAppWidget { nodeNumber widgetPath } {
    variable _nodePathname_
    # need to check because bwidget sub-widgets will each call this aswell,
    # overwriting previously registered nodes
    if { ![info exists _nodePathname_($nodeNumber)] } {
	set _nodePathname_($nodeNumber) $widgetPath
    }	
}


# updates the widget attributes of 'nodeNumber' in the running application;
# 'attValList' is list of attribute-value pairs to be updated
proc Build::UpdateAppWidget { nodeNumber attValList } {

    if { [set widgetPath [GetCheckedWidgetPath $nodeNumber]] == {} } {
	return
    }

    # loop once for each attribute-value pair
    foreach { attribute value } $attValList {
	set attribute [string tolower $attribute]
	if { [llength $value] > 1 } {
	    error "[mc {Value more than 1 word in length}]"
	}
	set value [lindex $value 0] ; # to remove braces if needed
	
	# if no value specified, get default from options database
	if { $value == {} } {
	    set widgetType [Tree::GetNodeItem widgetType $nodeNumber]
	    set value [interpBuild eval option get $widgetPath $attribute $widgetType]
	}
	
	# if still no value specified, get default from the config of the widget
	if { $value == {} } {
	    set value [lindex [interpBuild eval $widgetPath configure -$attribute] 3]
	}
	
	catch { interpBuild eval [list $widgetPath configure -$attribute $value] }
    }
    
}


# updates the layout options of widget at 'nodeNumber' in the running application;
# 'attValList' is list of option-value pairs to be updated
proc Build::UpdateAppLayout { nodeNumber attValList } {
    
    if { [set widgetPath [GetCheckedWidgetPath $nodeNumber]] == {} } {
	return
    }

    # get current geometry manager of widget; check that user hasn't
    # changed layout type of the widget after starting app
    set geoManager [interpBuild eval [list winfo manager $widgetPath]]
    if { $geoManager != [Layout::GetLayoutType $nodeNumber] } {
	return
    }

    # loop once for each option-value pair
    foreach { attribute value } $attValList {
	set attribute [string tolower $attribute]
	if { [llength $value] > 1 } {
	    error "[mc {Value more than 1 word in length}]"
	}
	set value [lindex $value 0] ; # to remove braces if needed
	set goodValue 1 ; # set to 0 if can't get a good value for attribute
	if { $value == {} } {
	    switch $geoManager {
		pack {
		    switch $attribute {
			after - before - in { set goodValue 0 }
			anchor { set value center }
			fill { set value none }
			side { set value top }
			default { set value 0 }
		    }
		}
		grid {
		    switch $attribute {
			in { set goodValue 0 }
			columnspan - rowspan { set value 1 }
			column - row { set goodValue 0 }
			sticky { set value {} }
			default { set value 0 }
		    }
		}
		place {
		    switch $attribute {
			in { set goodValue 0 }
			bordermode { set value inside }
			anchor { set value nw }
			width - height - relheight - relwidth { set value {} }
			relx - rely { set value 0.0 }
			default { set value 0 }
		    }
		}
	    }
	}
	if { $goodValue } {
	    catch { interpBuild eval [list $geoManager configure $widgetPath -$attribute $value] }
	}
    }
}


# updates the bindings of 'nodeNumber' in the running application;
# 'attValList' is list of attribute-value pairs to be updated
proc Build::UpdateAppBindings { nodeNumber attValList } {

    if { [set widgetPath [GetCheckedWidgetPath $nodeNumber]] == {} } {
	return
    }

    # loop once for each event-command pair
    foreach { event command } $attValList {	
	# set the new command for this event; if there is a problem, set the
	# command to null, so that the previous command is not in effect
	if [catch { interpBuild eval bind $widgetPath $event $command }] {
	    catch { interpBuild eval bind $widgetPath $event {} }
	}
    }

}


proc Build::GetCheckedWidgetPath { nodeNumber } {
    variable _nodePathname_
    variable _enableModification_
    variable _inRunMode_

    # check if modifications enabled, app is running, node is registered
    if { !$_enableModification_ ||
    	 !$_inRunMode_ ||
    	 ![info exists _nodePathname_($nodeNumber)] } {
    	return {}
    }

    # get the full path of the widget
    set widgetPath $_nodePathname_($nodeNumber)

    # check that the widget still exists in the running app ... it might not,
    # for example, if it is destroyed while the app is running
    if { ![interpBuild eval winfo exists $widgetPath] } {
	unset _nodePathname_($nodeNumber)  
	return {}
    }
    
    return $widgetPath

}


# returns the full path name of node 'nodeNumber' in the running application, or
# an empty string if it doesn't exist
proc Build::GetWidgetPath { nodeNumber } {
    variable _nodePathname_
    if { [info exists _nodePathname_($nodeNumber)] } {
    	return $_nodePathname_($nodeNumber)
    } else {
    	return {}
    }
}


proc Build::DeleteAppWidget { nodeNumber } {
    variable _nodePathname_
    if { ![info exists _nodePathname_($nodeNumber)] } { return }
    set widgetPath $_nodePathname_($nodeNumber)
    catch { interpBuild eval destroy $widgetPath }
    # need catch in case main window is destroyed; that triggers proc that
    # already will unset _nodePathName_
    catch { unset _nodePathname_($nodeNumber) }
}


proc Build::UnsetRegisteredWidgets { } {
    variable _nodePathname_
    catch { unset _nodePathname_ }
}
