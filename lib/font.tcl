# -------------------------------------------------------------
# font.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------



namespace eval Font {
    variable _window_  ; # name of toplevel window
    variable _waitVar_ ; # name of tkwait variable
    variable _font_ ; # array to hold font attributes, indexed on attribute
}


proc Font::OpenFontDialog { {font {}} } {

    variable _window_
    variable _waitVar_
    variable _font_
    
    # if no font specified, use system font
    if { $font == {} } {
	set font [font actual system]
    } else {
	# first set will work with '{System... }' style font spec
	# second will work with '-family System ...' style
	if { [catch {set font [eval font actual $font]}] } {
	    if { [catch {set font [font actual $font]}] } {
		Utility::ErrorMessage "[mc {Unable to use specified font}]"
		set font [font actual system]
	    }
	}
    }

    set _font_(family)     [GetFontAttribute $font family]
    set _font_(size)       [GetFontAttribute $font size]
    set _font_(weight)     [GetFontAttribute $font weight]
    set _font_(slant)      [GetFontAttribute $font slant]
    set _font_(underline)  [GetFontAttribute $font underline]
    set _font_(overstrike) [GetFontAttribute $font overstrike]

    if { ![winfo exists ._window_] } {

	# create a font for the sample text
	eval font create fontSample $font

	set _window_ [toplevel ._window_]  	
	wm withdraw $_window_
	wm title $_window_ Font
	wm iconname $_window_ Font
	wm protocol $_window_ WM_DELETE_WINDOW {set Font::_waitVar_ CANCEL}
	bind $_window_ <Destroy>       {set Font::_waitVar_ CANCEL}
	bind $_window_ <Key-Escape>    {set Font::_waitVar_ CANCEL}
	bind $_window_ <ButtonPress-3> {set Font::_waitVar_ OK}

	# ok and cancel buttons
	set fraButton [frame $_window_.fraButton]
	pack $fraButton -side bottom -anchor e
	Button::MakeButtonSet $fraButton HORIZONTAL ok \
	    ok OK {set Font::_waitVar_ OK} \
	    cancel Cancel {set Font::_waitVar_ CANCEL}
	bind $_window_ <Return>        {set Font::_waitVar_ OK}
	bind $_window_ <ButtonPress-3> {set Font::_waitVar_ OK}

	# build main frame
	set fraMain [frame $_window_.fraMain -bd 1 -relief ridge]
	pack $fraMain -side top -anchor nw -padx 3 -pady 3

	# get list of font families and build the combobox
	set fontFamilies [font families]
	set cmbFamilies [ComboBox $fraMain.cmbFamilies -width 25 \
			     -editable 1 -state normal \
			     -modifycmd Font::UpdateSampleText \
			     -textvariable Font::_font_(family)]
	$cmbFamilies configure -values $fontFamilies
	grid $cmbFamilies -row 0 -column 0 -columnspan 3 -padx 3 -pady 3 -sticky w

	# font size
	set spnSize [label $fraMain.lbSize -text "[mc {Size}]: "]
	grid $spnSize -row 0 -column 3 -padx 3 -pady 3 -sticky e
	set spnSize [SpinBox $fraMain.spnSize \
			 -range {0 36 1} -textvariable Font::_font_(size) \
			 -width 3 -modifycmd Font::UpdateSampleText \
			 -editable 1]
	grid $spnSize -row 0 -column 4 -padx 3 -pady 3 -sticky w
	bind $spnSize <FocusOut> Font::UpdateSampleText

	# weight: normal or bold
	set f $fraMain
	grid [label $f.lblWeight -text "[mc {Weight}]: "] \
	    -row 1 -column 0 -sticky w
	grid [radiobutton $f.radNormal -text [mc {normal}] -value normal \
		  -variable Font::_font_(weight) -command Font::UpdateSampleText] \
	    -row 1 -column 1 -sticky w
	grid [radiobutton $f.radBold -text [mc {bold}] -value bold \
		  -variable Font::_font_(weight) -command Font::UpdateSampleText] \
	    -row 1 -column 2 -sticky w

	# slant: roman or italic
	grid [label $f.lblSlant -text "[mc {Slant}]: "] \
	    -row 2 -column 0 -sticky w
	grid [radiobutton $f.radRoman -text [mc {roman}] -value roman \
		  -variable Font::_font_(slant) -command Font::UpdateSampleText] \
	    -row 2 -column 1 -sticky w
	grid [radiobutton $f.radItalic -text [mc {italic}] -value italic \
		  -variable Font::_font_(slant) -command Font::UpdateSampleText] \
	    -row 2 -column 2 -sticky w
	
	# underline and overstrike
	grid [checkbutton $f.chkUnderline -text "[mc {underline}]" \
		  -variable Font::_font_(underline) -command Font::UpdateSampleText] \
	    -row 3 -column 1 -sticky w
	grid [checkbutton $f.chkOverstrike -text "[mc {overstrike}]" \
		  -variable Font::_font_(overstrike) -command Font::UpdateSampleText] \
	    -row 3 -column 2 -columnspan 2 -sticky w
	
	# sample text to illustrate font
	set f [frame $fraMain.fraSeperator -bd 2 -relief raised -height 2]
	grid $f -row 4 -column 0 -columnspan 5 -sticky ew -padx 5 -pady 5
	set f [frame $fraMain.fraSample]
	pack [label $f.lblSample \
		  -text "1234567890\nabcdefghij\n\ABCDEFGHIJ\n\The quick brown fox." \
		  -font fontSample]
	grid $f -row 5 -column 0 -columnspan 5
	grid columnconfigure $fraMain 4 -weight 1

    } else {
	
	eval font configure fontSample $font

    }

    wm deiconify $_window_
    set callerFocus [focus]
    focus -force $_window_
    catch { tkwait visibility $_window_ }
    catch { grab $_window_ }

    tkwait variable Font::_waitVar_  
    catch { grab release $_window_ } 
    focus $callerFocus
    wm withdraw $_window_

    if { $_waitVar_ == "OK" } {
	UpdateSampleText
	return [list OK [list [GetFontInListFormat [font actual fontSample]]]]
    }
    if { $_waitVar_ == "CANCEL" } {
	return [list CANCEL {}]
    }

}


# returns the value of attribute from the font name-value pair list;
# font is expected in the format '-family times -size 12 ....'
proc Font::GetFontAttribute { font attribute } {
    # find the location of attribute in the font list
    set index [lsearch $font "-$attribute"]
    if { $index == -1 } {
	error "[mc {Unable to find}] $attribute [mc {in}] $font"
    }
    incr index ; # to move from the name to the value
    return [lindex $font $index]
}


# updates the sample text to reflect the current font selection;
# need 'window' and 'item' as the combobox passes those arguments
# by default
proc Font::UpdateSampleText { {window {}} {item {}} } {
    variable _font_
    if { [catch {font configure fontSample \
		     -family $_font_(family) \
		     -size $_font_(size) \
		     -weight $_font_(weight) \
		     -slant $_font_(slant) \
		     -underline $_font_(underline) \
		     -overstrike $_font_(overstrike)}] } {
	Utility::ErrorMessage "[mc {Unable to modify font}]"
	font configure fontSample [font actual System]
    }
}


# sets the user-defined font 'fontname' to the font 'font'
proc Font::SetFont { fontname font } {
    eval font configure $fontname $font
}


# returns 'font' in the list format of '{family size {normal|bold roman|italic}}'
proc Font::GetFontInListFormat { font } {
    set fontList {}
    set styleList {}
    lappend fontList  [GetFontAttribute $font family]
    lappend fontList  [GetFontAttribute $font size]
    lappend styleList [GetFontAttribute $font weight]
    lappend styleList [GetFontAttribute $font slant]
    if { [GetFontAttribute $font underline] } {
	lappend styleList underline
    }
    if { [GetFontAttribute $font overstrike] } {
	lappend styleList overstrike
    }
    lappend fontList $styleList
    return $fontList
}


proc Font::InitializeFonts { } {
    # font to be used in textboxes
    font create textFont
    Font::SetFont textFont [Option::GetOption textFont]
}


# returns a default font; used to initialize default font options (in option.tcl)
proc Font::GetDefaultFont { } {
    global tcl_platform
    if { $tcl_platform(platform) == "windows" } {
	return [font actual {{MS Sans Serif} 8}]
    } else {
	return [font actual system]
    }
}


# allows user to modify the user-specified font 'fontname'
proc Font::ModifyFont { fontname } {
    set fontActual [font actual $fontname]
    set response [Font::OpenFontDialog $fontActual]
    if { [lindex $response 0] == "OK" } {
	set font [lindex [lindex $response 1] 0] ; # lindex 0 to get rid of braces
	Font::SetFont $fontname [font actual $font]
    }
}