# -------------------------------------------------------------
# file.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------


namespace eval File {

    namespace import ::Utility::ErrorMessage
    namespace import ::Utility::ConfirmMessage
    namespace import ::State::GetState
    namespace import ::State::SetState
    namespace import ::Status::UpdateTitleBar

    variable _nodeConversion_ {}

    namespace export OpenFile SaveFile SaveFileAs SaveFileAsTcl

}


proc File::New { } {

    # check if the current information has changed; prompt to save if so
    if { [GetState modified] } {
	set response [tk_messageBox -type yesnocancel -default yes \
			  -title "tkBuilder" -message "[mc {Save current changes}]?" \
			  -icon question]
	if { $response == "yes" } {
	    SaveFile
	} elseif { $response == "cancel" } {
	    return
	} elseif { $response == "no" } {
	    # do nothing here, but continue with procedure
	}
    }
    
    # stop the application if it is running
    Build::StopApplication

    # clear all current project and widget variables
    . configure -cursor watch
    update
    Project::InitializeProject
    Project::SetProjectFileName "[mc {<untitled>}]"
    
    # clear all current widget and attribute variables
    Tree::ClearTree

    # automatically add the main toplevel window, if option is set
    if { [Option::GetOption addMainWindowToNewProject] } {
	Project::AddMainWindow
    }
    WidgetTree::RedrawWidgetTree
    WidgetForms::ResetLastNodeDisplayed
    WidgetForms::UpdateAllForms [Tree::GetCurrentNode] 1
    SetState lastTkbFileName {}
    SetState modified 0
    . configure -cursor arrow
    Toolbar::UpdateToolbar
    Special::UpdateProjectDisplay

    # update the list of registered widgets
    Register::CreateInterpreter
    Register::RegisterWidgets
    Register::DeleteInterpreter

    # update the attribute lists for registered widgets and the list
    # of all attributes and the attribute option lists
    Attribute::UpdateRegisteredWidgetAttributeList
    Attribute::UpdateAllAttributesList
    Attribute::UpdateAttributeOptions

    # update the 'More' widgets menu to show currently registered widgets
    Menu::UpdateMoreWidgetsMenu

    # bring focus to project node (this isn't working yet)
    focus [WidgetTree::GetLabelFromNodeNumber [Project::GetProjectNodeNumber]]

}                            


# opens project file and reads it; 'fileName' is specified only when called from
# recent files menu; otherwise, user is prompted in the procedure for file
proc File::OpenFile { {fileName {}} } {

    # check if the current information has changed; prompt to save if so
    if { [GetState modified] } {
	set response [tk_messageBox -type yesnocancel -default yes \
			  -title "tkBuilder" -message "[mc {Save current changes}]?" \
			  -icon question]
	if { $response == "yes" } {
	    SaveFile
	} elseif { $response == "cancel" } {
	    return
	} elseif { $response == "no" } {
	    # do nothing here, but continue with procedure
	}
    }

    # allow user to specify file name if one not passed to function
    if { $fileName == {} } {
	set typelist [list [list "tkBuilder [mc {Project}]" {".tkb"}]]
	set fileName [tk_getOpenFile -defaultextension tkb -filetypes $typelist \
			  -initialdir [GetState lastTkbDirectory] -title [mc {Open}]]
	if { $fileName == {} } { return	}
    }

    # check that file exists
    if { ![file exists $fileName] } {
	ErrorMessage "[mc {File}] '$fileName' [mc {not found}]"
	RemoveFromRecentFiles $fileName
	return
    }
    
    # check that file is readable
    if { ![file readable $fileName] } {
	ErrorMessage "[mc {File}] '$fileName' [mc {not readable}]"
	RemoveFromRecentFiles $fileName
	return
    }

    # stop the application if it is running
    Build::StopApplication

    # clear all current widget and attribute variables
    Tree::ClearTree

    # open the file and read it
    if { [ReadTkbFile $fileName] } {
	SetState lastTkbFileName $fileName
	SetState lastTkbDirectory [file dirname $fileName]
	SetState modified 0
	Project::SetProjectFileName $fileName
	File::BackupProject $fileName
    }

    UpdateTitleBar
    WidgetTree::RedrawWidgetTree
    WidgetForms::ResetLastNodeDisplayed
    WidgetForms::UpdateAllForms [Tree::GetCurrentNode] 1
    Toolbar::UpdateToolbar
    Special::UpdateProjectDisplay
    
    # update the list of registered widgets
    Register::CreateInterpreter
    Register::RegisterWidgets
    Register::DeleteInterpreter
    
    # update the attribute lists for registered widgets and the list
    # of all attributes and the attribute option lists
    Attribute::UpdateRegisteredWidgetAttributeList
    Attribute::UpdateAllAttributesList
    Attribute::UpdateAttributeOptions

    # update the 'More' widgets menu to show currently registered widgets
    Menu::UpdateMoreWidgetsMenu

    # bring focus to project node
    focus [WidgetTree::GetLabelFromNodeNumber [Project::GetProjectNodeNumber]]

}


# saves the project to file
proc File::SaveFile { } {

    set fileName [GetState lastTkbFileName]
    
    # to fire any pending focus-out events before saving
    set callerFocus [focus]
    catch { focus . }

    # if a file name hasn't been specified, use Save As procedure
    if { $fileName == {} } {
	SaveFileAs
    } else {
	if { [WriteTkbFile $fileName] } {
	    if { [Option::GetOption tclProjectSave] } {
		Project::SaveProjectTclFiles 0
	    }
	    SetState modified 0
	    Project::SetProjectFileName $fileName
	    Special::UpdateProjectDisplay ; # to see new project filename
	    WidgetTree::RedrawWidgetTree ; # to see new project name
	}
    }

    catch { focus -force $callerFocus }

}


# saves project, allowing user to specify project file name
proc File::SaveFileAs { } {
    set fileName [GetState lastTkbFileName]	
    set typelist [list [list "tkBuilder [mc {File}]" {".tkb"}]]
    set fileName [tk_getSaveFile -defaultextension tkb -filetypes $typelist \
		      -initialdir [GetState lastTkbDirectory] -initialfile [file tail $fileName] \
		      -title "[mc {Save As}]"]
    if { $fileName != {} } {
	if { [WriteTkbFile $fileName] } {
	    if { [Option::GetOption tclProjectSave] } {
		Project::SaveProjectTclFiles 0
	    }
	    SetState lastTkbFileName $fileName
	    SetState lastTkbDirectory [file dirname $fileName]
	    SetState modified 0
	    Project::SetProjectFileName $fileName
	    Special::UpdateProjectDisplay ; # to see new project filename
	    WidgetTree::RedrawWidgetTree ; # to see new project name
	    UpdateTitleBar
	    return 1
	} else {
	    return 0
	}
    } else {
	return 0
    }
}


proc File::SaveFileAsTcl { tclCodeVar } {

    upvar $tclCodeVar tclCode
    
    set fileName [State::GetState lastTclFileName]	
    set typelist [list [list "[mc {tcl File}]" {".tcl"}]]
    set fileName [tk_getSaveFile -defaultextension tcl -filetypes $typelist \
		      -initialdir [State::GetState lastTclDirectory] \
		      -initialfile [file tail $fileName] -title "[mc {Save As Tcl}]"]
    if { $fileName != {} } {
	if { [catch {open $fileName "w"} fileID] } {
	    ErrorMessage "[mc {Unable to open file}] '$fileName'"
	    return 0
	}
	puts $fileID $tclCode
	close $fileID
	SetState lastTclFileName $fileName
	SetState lastTclDirectory [file dirname $fileName]
    }
}


# allows user to enter the name of a tcl file, for a file object
proc File::GetTclFileName { {fileName {}} } {
    set typelist [list [list "[mc {tcl File}]" {".tcl"}]]
    set fileName [tk_getSaveFile -defaultextension tcl -filetypes $typelist \
		      -initialdir [State::GetState lastTclDirectory] \
		      -initialfile [file tail $fileName] -title "[mc {Tcl File}]"]
    if { $fileName != {} } {
	SetState lastTclDirectory [file dirname $fileName]
    }
    return $fileName
}


# adds 'fileName' to the list of tcl files used by file objects in this project
proc File::AddTclFileName { fileName } {
    set fileList [Project::GetState tclFileList]
    if { [lsearch $fileList $fileName] == -1 } {
	lappend fileList $fileName
	Project::SetState tclFileList $fileList
    }
}


# removes 'filename' from the list of tcl files used by file objects in this project
proc File::RemoveTclFileName { fileName } {
    set fileList [Project::GetState tclFileList]
    if { [List::RemoveItem fileList $fileName] } {
	Project::SetState tclFileList $fileList
    }
}


# checks if the branch starting at 'nodeNumber' contains a file object;
# a recursive function; returns 1 if so, 0 otherwise
proc File::BranchContainsFile { nodeNumber } {
    if { [Tree::GetNodeItem widgetType $nodeNumber] == "file" } {
	return 1
    } else {
	foreach child [Tree::GetNodeItem childList $nodeNumber] {
	    if { [File::BranchContainsFile $child] } {
		return 1
	    }
	}
	return 0
    }
}


# returns 1 if 'fileName' is already used for a tcl file object
# in this project, 0 otherwise
proc File::TclFileNameExists { fileName } {
    if { [lsearch [Project::GetState tclFileList] $fileName] == -1 } {
	return 0
    } else {
	Utility::ErrorMessage "[mc {Tcl filename}] '$fileName'\n\
			[mc {is used by another file object in this project}]"
	return 1
    }
}


proc File::WriteTkbFile { fileName } {

    if { [catch {open $fileName "w"} fileID] } {
	ErrorMessage "[mc {Unable to open file}] '$fileName'"
	RemoveFromRecentFiles $fileName
	return 0
    }
    fconfigure $fileID -translation binary

    . configure -cursor watch
    update

    WriteItem $fileID [Main::GetSystem version]
    WriteItem $fileID [Project::GetProjectArray]
    WriteItem $fileID [Tree::GetNextNodeNumberNoInc]
    WriteItem $fileID [Tree::GetTreeArray]
    WriteItem $fileID [Attribute::GetAttributeArray]
    WriteItem $fileID [Layout::GetLayoutArray]
    WriteItem $fileID [Binding::GetBindingArray]
    WriteItem $fileID [Binding::GetEventListArray]
    WriteItem $fileID [General::GetGeneralArray]
    WriteItem $fileID [Special::GetSpecialArray]

    close $fileID
    . configure -cursor arrow
    AddToRecentFiles $fileName
    return 1
    
}


proc File::ReadTkbFile { fileName } {

    if { [catch {open $fileName "r"} fileID] } {
	ErrorMessage "[mc {Unable to open file}] '$fileName'"
	RemoveFromRecentFiles $fileName
	return 0
    }
    fconfigure $fileID -translation binary

    . configure -cursor watch
    update

    set version [ReadNextItem $fileID]
    if { $version == "tkBuilder version 1 rev 0" ||
	 $version == "tkBuilder version 1.0.1" } {

	Tree::SetNextNodeNumber      [ReadNextItem]
	Tree::SetTreeArray           [ReadNextItem]
	Attribute::SetAttributeArray [ReadNextItem]
	Layout::SetLayoutArray       [ReadNextItem]
	Binding::SetBindingArray     [ReadNextItem]
	Binding::SetEventListArray   [ReadNextItem]
	General::SetGeneralArray     [ReadNextItem]
	Special::SetSpecialArray     [ReadNextItem]

	File::ConvertFromVersion1.0.1

    } elseif { $version == "tkBuilder 1.0.2" } {

	Project::SetProjectArray     [ReadNextItem]
	Tree::SetNextNodeNumber      [ReadNextItem]
	Tree::SetTreeArray           [ReadNextItem]
	Attribute::SetAttributeArray [ReadNextItem]
	Layout::SetLayoutArray       [ReadNextItem]
	Binding::SetBindingArray     [ReadNextItem]
	Binding::SetEventListArray   [ReadNextItem]
	General::SetGeneralArray     [ReadNextItem]
	Special::SetSpecialArray     [ReadNextItem]

    } else {

	Utility::ErrorMessage "[mc {Unrecognized version}]"
	ClearReadBuffer
	close $fileID
	. configure -cursor arrow
	return 0

    }

    ClearReadBuffer
    close $fileID
    . configure -cursor arrow
    AddToRecentFiles $fileName
    return 1

}


# converts a project read from a file saved in verson 1.0.1 to version
# 1.0.2
proc File::ConvertFromVersion1.0.1 { } {

    # get new number for root and reassign parent number of its children
    set newRootNodeNumber [Tree::GetNextNodeNumber]
    Tree::CreateNode $newRootNodeNumber \
	[list [Project::GetRootName] 0 [Tree::GetNodeItem childList 0] 1 toplevel]
    Tree::ReparentChildren 0 $newRootNodeNumber
    
    # reassign the data arrays for the old root number (0) to the
    # new root number
    Attribute::SetNodeArrayElements $newRootNodeNumber \
	[Attribute::GetNodeArrayElements 0]
    Layout::SetNodeArrayElements $newRootNodeNumber \
	[Layout::GetNodeArrayElements 0]
    Binding::SetNodeArrayElements $newRootNodeNumber \
	[Binding::GetNodeArrayElements 0]
    General::SetNodeArrayElements $newRootNodeNumber \
	[General::GetNodeArrayElements 0]
    Properties::DeleteWidgetProperties 0

    # initialize the project, but set root node info to reflect new root number
    Project::InitializeProject
    Project::SetState rootNodeNumber $newRootNodeNumber
    Project::SetState rootNodeExists 1
    
    # reassign project attributes in the tree 
    set projectNodeNumber [Project::GetProjectNodeNumber]
    Tree::SetNodeItem name $projectNodeNumber <untitled>
    Tree::SetNodeItem childList $projectNodeNumber $newRootNodeNumber
    Tree::SetNodeItem widgetType $projectNodeNumber project

}



proc File::AddToRecentFiles { file } {
    set recentTkbFiles   [State::GetState recentTkbFiles]
    set recentFileNumber [Option::GetOption recentFileNumber]
    if { $recentFileNumber == 0 } {
	set recentTkbFiles {}
    } else {
	# if file is already in list, remove it
	List::RemoveItem recentTkbFiles $file
	# insert file to start of list and get only the number required
	set recentTkbFiles [linsert $recentTkbFiles 0 $file]
	set recentTkbFiles [lrange $recentTkbFiles 0 [expr $recentFileNumber - 1]]
    }
    State::SetState recentTkbFiles $recentTkbFiles
}
proc File::RemoveFromRecentFiles { file } {
    set recentTkbFiles [State::GetState recentTkbFiles]
    List::RemoveItem recentTkbFiles $file
    State::SetState recentTkbFiles $recentTkbFiles
}
proc File::UpdateNumberOfRecentFiles { } {
    set recentTkbFiles   [State::GetState recentTkbFiles]
    set recentFileNumber [Option::GetOption recentFileNumber]
    if { $recentFileNumber == 0 } {
	set recentTkbFiles {}
    } else {
	set recentTkbFiles [lrange $recentTkbFiles 0 [expr $recentFileNumber - 1]]
    }
    State::SetState recentTkbFiles $recentTkbFiles
}


# allows users to save the current branch as a mega-widget
proc File::SaveAsTkwFile { } {

    # check that the branch does not contain a project,
    # main window, or tcl file.
    if { ![File::BranchOkForMegawidget [Tree::GetCurrentNode]] } {
	return
    }
    
    set fileName [GetState lastTkwFileName]
    set typelist [list [list "tkBuilder [mc {Widget File}]" {".tkw"}]]
    set fileName [tk_getSaveFile -defaultextension tkw -filetypes $typelist \
		      -initialdir [GetState lastTkwDirectory] \
		      -initialfile [file tail $fileName] -title "[mc {Save As Megawidget}]"]
    if { $fileName != {} } {
	if { [catch {open $fileName "w"} fileID] } {
	    ErrorMessage "[mc {Unable to open file}] '$fileName'"
	    RemoveFromRecentFiles $fileName
	    return
	}
	WriteTkwFile $fileID
	SetState lastTkwFileName $fileName
	SetState lastTkwDirectory [file dirname $fileName]
	close $fileID
    }	
}
# checks that the branch does not contain a project,
# main window, or tcl file.  Returns true if it doesn't, false otherwise
proc File::BranchOkForMegawidget { node } {
    if { [Project::NodeIsProject $node] } {
	Utility::ErrorMessage "[mc {Project object cannot be part of megawidget}]"
	return 0
    } elseif { [Project::NodeIsRoot $node] } {
	Utility::ErrorMessage "[Project::GetRootName] [mc {cannot be part of megawidget}]"
	return 0
    } elseif { [Project::NodeIsTclFile $node] } {
	Utility::ErrorMessage "[mc {Tcl file object cannot be part of megawidget}]"
	return 0
    }
    foreach child [Tree::GetNodeItem childList $node] {
	if { ![File::BranchOkForMegawidget $child] } {
	    return 0
	}
    }
    return 1
}
proc File::WriteTkwFile { fileID } {
    set nodeNumber [Tree::GetCurrentNode]
    WriteItem $fileID [Main::GetSystem version]
    WriteTkwFileRecurse $fileID $nodeNumber
    WriteItem $fileID "EOF"
}
proc File::WriteTkwFileRecurse { fileID nodeNumber } {
    WriteItem $fileID $nodeNumber
    WriteItem $fileID [Tree::GetNodeItems $nodeNumber]
    if { [Special::NodeIsSpecialObject $nodeNumber] } {
	WriteItem $fileID [Special::GetNodeArrayElements $nodeNumber]
    } else {
	WriteItem $fileID [Attribute::GetNodeArrayElements $nodeNumber]
	WriteItem $fileID [Layout::GetNodeArrayElements $nodeNumber]
	WriteItem $fileID [Binding::GetNodeArrayElements $nodeNumber]
	WriteItem $fileID [General::GetNodeArrayElements $nodeNumber]
    }
    foreach child [Tree::GetNodeItem childList $nodeNumber] {
	WriteTkwFileRecurse $fileID $child
    }
}


proc File::OpenTkwFile { relation } {

    set fileName [State::GetState lastTkwFileName]

    # allow user to specify file name
    set typelist [list [list "tkBuilder [mc {Widget File}]" {".tkw"}]]
    set fileName [tk_getOpenFile -defaultextension tkw -filetypes $typelist \
		      -initialdir [State::GetState lastTkwDirectory] \
		      -initialfile [file tail $fileName] -title [mc {Insert}]]
    if { $fileName == {} } { return	}

    # check that file exists
    if { ![file exists $fileName] } {
	ErrorMessage "[mc {File}] '$fileName' [mc {not found}]"
	return
    }
    
    # check that file is readable
    if { ![file readable $fileName] } {
	ErrorMessage "[mc {File}] '$fileName' [mc {not readable}]"
	return
    }

    # open the tkw file
    if { [catch {open $fileName "r"} fileID] } {
	ErrorMessage "[mc {Unable to open file}] '$fileName'"
	RemoveFromRecentFiles $fileName
	return 0
    }
    fconfigure $fileID -translation binary

    if { [InsertTkwFile $fileID $relation] } {
	WidgetTree::RedrawWidgetTree
	SetState lastTkwFileName $fileName
	SetState lastTkwDirectory [file dirname $fileName]
    }

    ClearReadBuffer
    close $fileID

}
proc File::InsertTkwFile { fileID relation } {

    set currentNode [Tree::GetCurrentNode]
    set firstPass 1

    # initialize the array that tracks node number conversion
    InitializeNodeConversion
    
    # read version; its the first item in the file
    set version [ReadNextItem $fileID]
    
    # loop until "EOF" encountered
    while { 1 } {

	set nodeNumber [ReadNextItem]
	if { $nodeNumber == "EOF" } { break }
	
	# get the list of item values for the current node and extract
	# the individual items
	set itemValues [ReadNextItem]
	set name [Tree::GetItemFromList name $itemValues]
	set parent [Tree::GetItemFromList parentNumber $itemValues]
	set childList [Tree::GetItemFromList childList $itemValues]
	set expanded [Tree::GetItemFromList expanded $itemValues]
	set widgetType [Tree::GetItemFromList widgetType $itemValues]
	
	# if first pass prompt user for name of widget; loops till user enters valid
	# name or presses cancel
	if { $firstPass } {
	    while { 1 } {
		set dialogReply [Dialog::EnableNameEntry "$widgetType:" $name]
		if { [lindex $dialogReply 0] == "OK" } {
		    # ok was selected
		    set nodeName [lindex $dialogReply 1]
		    if { [WidgetTree::CheckWidgetName $currentNode $nodeName $relation] } {
			if { $relation == "CHILD" } {
			    # ensure expanded to see child
			    Tree::SetNodeItem expanded $currentNode 1
			}
			break
		    }
		} else {
		    # cancel was selected
		    return 0
		}
	    }
	}
	
	# if first pass, insert the node into the tree; otherwise, just create
	# new nodes ... hierarchy is managed by converting node numbers
	if { $firstPass } {
	    set newNodeNumber [Tree::InsertNode $currentNode $nodeName $relation \
				   $widgetType $expanded]
	    # currentNode was parentNumber
	    ConvertNodeNumber $nodeNumber $newNodeNumber
	} else {
	    set newNodeNumber [ConvertNodeNumber $nodeNumber]
	    set newParentNumber [ConvertNodeNumber $parent]
	    Tree::CreateNode $newNodeNumber \
		[list $name $newParentNumber {} $expanded $widgetType]
	}

	# convert node numbers of the children and assign to the new node
	set newChildList {}
	foreach child $childList {
	    lappend newChildList [ConvertNodeNumber $child]
	}
	Tree::SetNodeItem childList $newNodeNumber $newChildList

	if { [Special::WidgetIsSpecialObject $widgetType] } {
	    Special::SetNodeArrayElements $newNodeNumber [ReadNextItem]
	} else {
	    Attribute::SetNodeArrayElements $newNodeNumber [ReadNextItem]
	    Layout::SetNodeArrayElements $newNodeNumber [ReadNextItem]
	    Binding::SetNodeArrayElements $newNodeNumber [ReadNextItem]
	    General::SetNodeArrayElements $newNodeNumber [ReadNextItem]
	}

	set firstPass 0

    }

    return 1

}
# given the the node number 'nodeNumber', returns the new number that
# this should be converted to when inserting branch from file; all node
# numbers referenced in the file must be converted to new, unique
# numbers in the current tree; if 'newNodeNumber' is specified, it
# is used as the new number, rather than going to the next available
# node number
proc File::ConvertNodeNumber { nodeNumber {newNodeNumber {}} } {
    variable _nodeConversion_
    if { $newNodeNumber != {} } {
	set _nodeConversion_($nodeNumber) $newNodeNumber
    }
    # if 'nodeNumber' hasn't been encountered yet, its converted to the
    # next available node number of the tree
    if { ![info exists _nodeConversion_($nodeNumber)] } {
	set _nodeConversion_($nodeNumber) [Tree::GetNextNodeNumber]
    }
    return $_nodeConversion_($nodeNumber)
}
proc File::InitializeNodeConversion { } {
    variable _nodeConversion_
    catch { unset _nodeConversion_ }
}


# writes 'item' to file stream 'fileID', using Ctrl-Z as delimiter
proc File::WriteItem { fileID item } {
    # using Ctrl-Z as a delimiter
    puts -nonewline $fileID "$item\x1a"
}


# returns the next item from the current input file buffer; if 'fileID' is
# specified, reloads the buffer (i.e. '_data_') with the contents of the
# file; otherwise, gets next item from '_data_'
proc File::ReadNextItem { {fileID {}} } {
    variable _data_
    variable _itemIndex_
    # on first pass, file id is specified; data is read from that file
    if { $fileID != {} } {
	# read entire file at once; split using Ctrl-Z as delimiter
	set _data_ [split [read $fileID] \x1a]
	set _itemIndex_ -1 ; # first incr will return 0
    }
    return [lindex $_data_ [incr _itemIndex_]]
}
proc File::ClearReadBuffer { } {
    variable _data_
    unset _data_
}


# makes a backup copy of project 'filename'
proc File::BackupProject { filename } {
    if { [Option::GetOption projectBackup] } {
	set backupFile "${filename}.BAK"
	if { [catch {file copy -force $filename $backupFile}] } {
	    Utility::ErrorMessage "[mc {Unable to backup project}]"
	}
    }	
}
