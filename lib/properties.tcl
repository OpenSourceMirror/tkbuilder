# -------------------------------------------------------------
# properties.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
# -------------------------------------------------------------



namespace eval Properties {

}


proc Properties::InitializeWidgetProperties { newNode } {

    if { [Special::NodeIsSpecialObject $newNode] } {
	Special::InitializeSpecialObject $newNode
    } else {
	Attribute::InitializeWidgetAttributes $newNode
	Layout::InitializeLayoutAttributes    $newNode
	Binding::InitializeWidgetBindings     $newNode
	General::InitializeGeneralParameters  $newNode
    }

}


proc Properties::CopyWidgetProperties { fromNode toNode } {
    if { [Special::NodeIsSpecialObject $fromNode] } {
	Special::CopySpecialObject $fromNode $toNode
    } else {
	Attribute::CopyWidgetAttributes $fromNode $toNode
	Layout::CopyLayoutAttributes    $fromNode $toNode
	Binding::CopyWidgetBindings     $fromNode $toNode
	General::CopyGeneralParameters  $fromNode $toNode
    }
}


proc Properties::DeleteWidgetProperties { node } {

    if { [Special::NodeIsSpecialObject $node] } {

	# if file object, remove the file name from tcl file list
	# for this project
	if { [Tree::GetNodeItem widgetType $node] == "file" } {
	    File::RemoveTclFileName [Special::GetSpecialObject file name $node]
	}

	Special::DeleteSpecialObject  $node

    } else {
	Attribute::DeleteWidgetAttributes $node
	Layout::DeleteLayoutAttributes    $node
	Binding::DeleteWidgetBindings     $node
	General::DeleteGeneralParameters  $node
    }

}


proc Properties::NodeOverridesWidgetHierarchy { node } {
    if { [Special::NodeIsSpecialObject $node] } {
	return 0
    } else {
	return [General::NodeOverridesWidgetHierarchy $node]
    }
}


proc Properties::GetSetVariableName { node } {
    if { [Special::NodeIsSpecialObject $node] } {
	return {}
    } else {
	return [General::GetGeneralParameter set $node]
    }
}
