# -------------------------------------------------------------
# attribute.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------


namespace eval Attribute {

    variable _attribute_
    # array to store the value of each attribute of each node;
    # indexed as follows: _attribute_($nodeNumber.$attribute)
    
    variable _attributeList_
    # array of lists indexed on widget type, listing attributes of 
    # each widget type; e.g. _attributeList_(button) {borderWidth relief ...}

    variable _attributeOption_ {}
    # array indexed on attribute to store the options (if any) for each
    # attribute type;
    # e.g. _attributeOption(relief) {{} flat sunken raised groove ridge}

    variable _allAttributesList_
    # list of all possible attributes (over all widgets)

    variable _hiddenAttributesList_ {}
    # list of attributes not displayed in attribute form
    
    namespace export ConfigureWidgetAttributeVariables
    namespace export GetWidgetAttribute SetWidgetAttribute 
    namespace export InitializeWidgetAttributes DeleteWidgetAttributes

    namespace export GetAttributeList GetAttributeOption
    namespace export GetAllAttributesList
    
    namespace export SetHiddenAttributesList GetHiddenAttributesList

    namespace export CopyWidgetAttributes GetWidgetCommand
    namespace export CopySelectedWidgetAttributes

}


# initializes the widget attribute variables at program startup
proc Attribute::ConfigureWidgetAttributeVariables { } {
    SetWidgetTypeAttributes
    UpdateAllAttributesList
    UpdateAttributeOptions
}


# for each widget class (e.g. button) builds a list of available attributes
proc Attribute::SetWidgetTypeAttributes { } {
    variable _attributeList_
    
    # loop once for each widget class
    foreach widgetType [Widget::GetWidgetList] {
	set _attributeList_($widgetType) {}
	$widgetType .x
	# loop once for each attribute descriptor: e.g {-background background
	# Background SystemButtonFace SystemButtonFace}
	foreach attribute [.x configure] {
	    # get the widget class name (second item of descriptor list)
	    set attributeName [lindex $attribute 1]
	    # check for a prefix dash in the name
	    # at version tcl8.3.0; skip it if it has a dash
	    if { [string range $attributeName 0 0] == "-" } {
		continue
	    }
	    # add the attribute name to the list if it isn't there
	    if { ![List::ItemExists $_attributeList_($widgetType) $attributeName] } {
		lappend _attributeList_($widgetType) $attributeName
	    }
	}
	destroy .x
    }

    # include the canvas object attributes
    foreach object [Canvas::GetCanvasObjectList] {
	set _attributeList_($object) {}
	foreach attribute [Canvas::GetCanvasObjectAttributes $object] {
	    lappend _attributeList_($object) $attribute
	}
    }

    # include the menu entry attributes
    foreach entry [MenuEntry::GetMenuEntryList] {
	set _attributeList_($entry) {}
	foreach attribute [MenuEntry::GetMenuEntryAttributes $entry] {
	    lappend _attributeList_($entry) $attribute
	}
    }

}


# updates the attribute lists for the registered widgets; uses procs
# from 'register.tcl' to get registered widgets and their attributes;
# updates the list of currently registered widgets maintained in this
# namespace; this proc manages registered widget separately because
# they can be modified by the user in the property dialog;
# must be called after changes to the registered widgets
proc Attribute::UpdateRegisteredWidgetAttributeList { } {
    variable _attributeList_

    # unset attribute lists of currently registered widget classes
    foreach widgetClass [Register::GetLastRegisteredWidgets] {
	unset _attributeList_($widgetClass)
    }

    # quietly create interpreter for creating widgets to be registered,
    # using the current option settings (rather than the textboxes of the
    # registry dialog, since they may not exist yet)
    Register::CreateInterpreter 0 ; # 021100

    # now include registered widget classes; loop once for each widget class
    foreach widgetClass [Register::GetRegisteredWidgets] {
	set _attributeList_($widgetClass) {}
	# loop once for each attribute of current widget class
	foreach attribute [Register::GetWidgetAttributes $widgetClass] {
	    lappend _attributeList_($widgetClass) $attribute
	}
    }

    Register::SetLastRegisteredWidgets [Register::GetRegisteredWidgets]
    Register::DeleteInterpreter

}


# builds a list of all possible attribute types 
# (for all widgets, canvas objects, menu entries and registered widgets);
# stores in _allAttributesList_; 
# must be called after changes to the registered widgets
proc Attribute::UpdateAllAttributesList { } {
    variable _allAttributesList_
    set _allAttributesList_ {}	
    # loop once for each widget class, canvas object, and menu entry
    foreach widgetType "[Widget::GetWidgetList] [Canvas::GetCanvasObjectList] \
		[MenuEntry::GetMenuEntryList] [Register::GetRegisteredWidgets]" {
	foreach attribute [GetAttributeList $widgetType] {
	    if { ![List::ItemExists $_allAttributesList_ $attribute] } {
		lappend _allAttributesList_ $attribute
	    }
	}
	set _allAttributesList_ [lsort -dictionary $_allAttributesList_]
    }
}


# sets the option (if any) for each attribute type
proc Attribute::UpdateAttributeOptions { } {

    variable _attributeOption_

    # start from scratch each time the proc is called
    unset _attributeOption_

    # initialize option for each attribute to {}
    foreach attribute [GetAllAttributesList] {
	set _attributeOption_($attribute) {}
	
	# check if its a command
    	if { [string first "command" [string tolower $attribute]]  != -1 } {
	    set _attributeOption_($attribute) command
    	}
    	# catch commands in BWidget
    	if { [string first "cmd" [string tolower $attribute]]  != -1 } {
	    set _attributeOption_($attribute) command
    	}
	
    }

    set _attributeOption_(exportSelection) {{} true false 1 0}
    set _attributeOption_(relief)      {{} flat sunken raised groove ridge solid}
    set _attributeOption_(selectMode)  {{} browse single extended multiple}
    set _attributeOption_(setGrid)     {{} true false 1 0}
    set _attributeOption_(state)       {{} normal active disabled readonly}
    set _attributeOption_(default)     {{} normal active disabled}
    set _attributeOption_(justify)     {{} center left right}
    set _attributeOption_(tearOff)     {{} true false 1 0}
    set _attributeOption_(anchor)      {{} n ne e se s sw w nw center}
    set _attributeOption_(orient)      {{} horizontal vertical}
    set _attributeOption_(takeFocus)   {{} 1 0}
    set _attributeOption_(wrap)        {{} none char word}
    set _attributeOption_(overRelief)  {{} raised}
    set _attributeOption_(offRelief)   {{} flat}
    set _attributeOption_(validate)    {{} none focus focusin focusout key all}
    set _attributeOption_(labelAnchor) {{} nw n ne en e es se s sw ws w wn}
    set _attributeOption_(activeStyle) {{} underline dotbox none}
    set _attributeOption_(type)        {{} menubar tearoff normal}
    set _attributeOption_(direction)   {{} above below left right flush}
    set _attributeOption_(indicatorOn) {{} true false 1 0}
    set _attributeOption_(sliderRelief) {{} flat sunken raised groove ridge solid}
    set _attributeOption_(activeRelief) {{} flat sunken raised groove ridge solid}
    set _attributeOption_(sashRelief) {{} flat sunken raised groove ridge solid}
    set _attributeOption_(showHandle) {{} true false}
    set _attributeOption_(opaqueResize) {{} true false}
    set _attributeOption_(cursor) {{} 
X_cursor
arrow
based_arrow_down
based_arrow_up
boat
bogosity
bottom_left_corner
bottom_right_corner
bottom_side
bottom_tee
box_spiral
center_ptr
circle
clock
coffee_mug
cross
cross_reverse
crosshair
diamond_cross
dot
dotbox
double_arrow
draft_large
draft_small
draped_box
exchange
fleur
gobbler
gumby
hand1
hand2
heart
icon
iron_cross
left_ptr
left_side
left_tee
leftbutton
ll_angle
lr_angle
man
middlebutton
mouse
pencil
pirate
plus
question_arrow
right_ptr
right_side
right_tee
rightbutton
rtl_logo
sailboat
sb_down_arrow
sb_h_double_arrow
sb_left_arrow
sb_right_arrow
sb_up_arrow
sb_v_double_arrow
shuttle
sizing
spider
spraycan
star
target
tcross
top_left_arrow
top_left_corner
top_right_corner
top_side
top_tee
trek
ul_angle
umbrella
ur_angle
watch
xterm
}
    set _attributeOption_(sashCursor) $_attributeOption_(cursor)

    set _attributeOption_(background) colour
    set _attributeOption_(foreground) colour
    set _attributeOption_(activeBackground) colour
    set _attributeOption_(activeForeground) colour
    set _attributeOption_(disabledForeground) colour
    set _attributeOption_(highlightBackground) colour
    set _attributeOption_(highlightColor) colour
    set _attributeOption_(insertBackground) colour
    set _attributeOption_(selectBackground) colour
    set _attributeOption_(selectColor) colour
    set _attributeOption_(selectForeground) colour
    set _attributeOption_(troughColor) colour

    set _attributeOption_(font) font

    # canvas object attribute options
    set _attributeOption_(fill)      colour
    set _attributeOption_(outline)   colour
    set _attributeOption_(arrow)     {{} none first last both}
    set _attributeOption_(capstyle)  {{} butt projecting round}
    set _attributeOption_(joinstyle) {{} bevel miter round}
    set _attributeOption_(smooth)    {{} true false 1 0}
    

}


# sets the value of the attribute 'attribute' for node 'nodeNumber' to 'value'
proc Attribute::SetWidgetAttribute { attribute nodeNumber value } {
    variable _attribute_
    set _attribute_($nodeNumber.$attribute) $value
}


# returns the value of the attribute 'attribute' for node 'nodeNumber'
proc Attribute::GetWidgetAttribute { attribute nodeNumber } {
    variable _attribute_
    return $_attribute_($nodeNumber.$attribute)
}


# initializes the value of all attributes of node 'nodeNumber' to {};
# creates a new set of array elements in _attribute_ (i.e. _attribute_($nodeNumber.*))
proc Attribute::InitializeWidgetAttributes { nodeNumber } {
    variable _attribute_
    set widgetType [Tree::GetNodeItem widgetType $nodeNumber]
    foreach attribute [GetAttributeList $widgetType] {
	SetWidgetAttribute $attribute $nodeNumber {}
    }
}


# unsets the array elements in _attribute_ used for attribute values of 'nodeNumber';
# (i.e. unsets all elements _attribute_($nodeNumber.*))
proc Attribute::DeleteWidgetAttributes { nodeNumber } {
    variable _attribute_
    # using 'array names' so don't have to pass widget type
    foreach element [array names _attribute_ $nodeNumber.*] {
	unset _attribute_($element)
    }
}


# returns list of attributes available for the specified widget type;
# if the widgetType does not exist (e.g. unregistered widget), returns null
proc Attribute::GetAttributeList { widgetType } {
    variable _attributeList_
    if { [info exists _attributeList_($widgetType)] } {
	return $_attributeList_($widgetType)
    } else {
	return {}
    }
}


# returns the list of all possible attribute types (over all widget types)
# (this is a unique list of attributes)
proc Attribute::GetAllAttributesList { } {
    variable _allAttributesList_
    return $_allAttributesList_
}


# returns the option of attribute 'attribute'
proc Attribute::GetAttributeOption { attribute } {
    variable _attributeOption_
    return $_attributeOption_($attribute)	
}


#------------------------------------------------------------------------------#
# Procedures to manage hidden attributes
#------------------------------------------------------------------------------#

# sets the list of hidden attributes; interface to the option variable
# 'hiddenWidgetAttributes'
proc Attribute::SetHiddenAttributesList { list } {
    Option::SetOption hiddenWidgetAttributes $list
}

# gets the list of hidden attributes; interface to the option variable
# 'hiddenWidgetAttributes'
proc Attribute::GetHiddenAttributesList { } {
    return [Option::GetOption hiddenWidgetAttributes]
}

# adds each item of list 'attributeList' to the list of hidden
# attributes (for items not already in the list)
proc Attribute::AddToHiddenAttributes { attributeList } {
    set hiddenAttributesList [GetHiddenAttributesList]
    foreach attribute $attributeList {
	if { ![List::ItemExists $hiddenAttributesList $attribute] } {
	    lappend hiddenAttributesList $attribute
	}
    }
    SetHiddenAttributesList [lsort -dictionary $hiddenAttributesList]
}

# removes each item of list 'attributeList' from the list of hidden
# attributes (for items in the list)
proc Attribute::RemoveFromHiddenAttributes { attributeList } {
    set hiddenAttributesList [GetHiddenAttributesList]
    foreach attribute $attributeList {
	List::RemoveItem hiddenAttributesList $attribute
    }
    SetHiddenAttributesList $hiddenAttributesList
}

# returns 1 if attribute 'attribute' is hidden, 0 otherwise
proc Attribute::AttributeIsHidden { attribute } {
    return [List::ItemExists [GetHiddenAttributesList] $attribute]
}

#------------------------------------------------------------------------------#


# copies the attribute values of node 'fromNodeNumber' into node 'toNodeNumber';
# creates a new set of array elements in _attribute_ (i.e. _attribute_($toNodeNumber.*))
# if they don't already exist, or overwrites existing ones
proc Attribute::CopyWidgetAttributes { fromNodeNumber toNodeNumber } {
    set widgetType [Tree::GetNodeItem widgetType $fromNodeNumber]
    set attValList {} ; # to build list of attribute-value pairs
    foreach attribute [GetAttributeList $widgetType] {
	set value [GetWidgetAttribute $attribute $fromNodeNumber]
	SetWidgetAttribute $attribute $toNodeNumber $value
	lappend attValList $attribute $value

    }
    # update widget attributes of running app
    Build::UpdateAppWidget $toNodeNumber $attValList
}


# for the attributes listed in 'selectedAttributes', copies the attribute values of node 
# 'fromNodeNumber' into node 'toNodeNumber'; does not create new node attributes if they
# don't already exist, since this procedure can copy among two different types of widgets
# where they have common attributes
proc Attribute::CopySelectedWidgetAttributes { fromNodeNumber toNodeNumber selectedAttributes } {
    variable _attribute_
    # loop once for each of the selected attributes
    set attValList {} ; # to build list of attribute-value pairs
    foreach attribute $selectedAttributes {
	# check that the attribute exists in the to-node
	if { [info exists _attribute_($toNodeNumber.$attribute)] } {
	    set value [GetWidgetAttribute $attribute $fromNodeNumber]
	    SetWidgetAttribute $attribute $toNodeNumber $value
	    lappend attValList $attribute $value
	}
    }
    # update widget attributes of running app
    Build::UpdateAppWidget $toNodeNumber $attValList
}


# for each of the 'selectedAttributes' of 'nodeNumber', sets the value to {}
proc Attribute::ClearSelectedWidgetAttributes { nodeNumber selectedAttributes } {
    foreach attribute $selectedAttributes {
	SetWidgetAttribute $attribute $nodeNumber {}
    }
}


# returns the command to build the widget at 'nodeNumber';
# returns -1 if there are problems
proc Attribute::GetWidgetCommand { nodeNumber widgetType nodePathName templateVar } {
    upvar $templateVar template
    set commandString "$widgetType $nodePathName"
    set useHiddenAttributes [Option::GetOption buildWithHiddenAttributes]
    foreach attribute [GetAttributeList $widgetType] {
	# check if attribute is hidden
	if { $useHiddenAttributes || ![AttributeIsHidden $attribute] } {
	    set value [GetWidgetAttribute $attribute $nodeNumber]
	    # if the attribute wasn't specified for the widget, but a template
	    # entry exists for this widget-attribute, use it
	    if { $value == {} && [Template::EntryExists template $widgetType $attribute] } {
		set value [Template::GetEntry template $widgetType $attribute]
	    }
	    if { $value != {} } {
		# ensure that the attribute value is only one word is size
		if { [llength $value] != 1 } {
		    Utility::ErrorMessage "[mc {Invalid value}] '$value' [mc {for attribute}]\n\
			  				'$attribute' [mc {of widget}] '$nodePathName'.\n\
			  				[mc {Value must be one word in size}]."
		    return -1
		}
		set attribute [string tolower $attribute]
		append commandString " -$attribute $value"
	    }
	}
    }
    return $commandString
}


# returns the command to configure the root window
proc Attribute::GetRootConfigureCommand { templateVar } {
    upvar $templateVar template
    set nodeNumber [Project::GetRootNodeNumber]
    set commandString {}
    set useHiddenAttributes [Option::GetOption buildWithHiddenAttributes]
    foreach attribute [GetAttributeList toplevel] {
	# check if attribute is hidden
	if { $useHiddenAttributes || ![AttributeIsHidden $attribute] } {
	    set value [GetWidgetAttribute $attribute $nodeNumber]
	    # if the attribute wasn't specified for the widget, but a template
	    # entry exists for this widget-attribute, use it
	    if { $value == {} && [Template::EntryExists template toplevel $attribute] } {
		set value [Template::GetEntry template toplevel $attribute]
	    }
	    if { $value != {} } {
		# ensure that the attribute value is only one word is size
		if { [llength $value] != 1 } {
		    Utility::ErrorMessage "[mc {Invalid value}] '$value' [mc {for attribute}]\n\
			  				'$attribute' [mc {of root}].\n\
			  				[mc {Value must be one word in size}]."
		    return -1
		}
		set attribute [string tolower $attribute]
		append commandString " -$attribute $value"
	    }
	}
    }
    if { $commandString == {} } {
	return {}
    } else {
	return ". configure $commandString"
    }
}


proc Attribute::GetAttributeArray { } {
    variable _attribute_
    return [array get _attribute_]
}


proc Attribute::SetAttributeArray { array } {
    variable _attribute_
    array set _attribute_ $array
}


proc Attribute::GetNodeArrayElements { nodeNumber } {
    variable _attribute_
    return [array get _attribute_ $nodeNumber.*]
}


proc Attribute::SetNodeArrayElements { newNodeNumber arrayElements } {
    variable _attribute_
    foreach { index value } $arrayElements {
	# get index w/o node number (e.g. 325.pack.side yields .pack.side)
	set parameter [string range $index [string first . $index] end]
	set _attribute_($newNodeNumber$parameter) $value
    }
}


# search attributes of 'nodeNumber' for string 'searchString'
proc Attribute::FindString { nodeNumber searchString matchCase matchWholeWord } {
    set matchList {}
    set widgetType [Tree::GetNodeItem widgetType $nodeNumber]
    foreach attribute [Attribute::GetAttributeList $widgetType] {
	set value [Attribute::GetWidgetAttribute $attribute $nodeNumber]
	set matchValueList [Find::FindMatch $value $searchString $matchCase $matchWholeWord]
	foreach matchValue $matchValueList {
	    lappend matchList "$attribute: $matchValue" 
	}
    }
    return $matchList
}

