# -------------------------------------------------------------
# help.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------


namespace eval Help {

}


proc Help::OpenAboutWindow { } {
    set butOK [Main::OpenAboutWindow]
    $butOK configure -state normal
    
    # wait here until the about window is destroyed
    tkwait variable Main::_waitVariable_
    destroy $Main::_window_
    # 	catch { image delete splashImage }

}




proc Help::OpenLicenseWindow { } {

    catch { destroy .winLicense }
    set w [toplevel .winLicense]
    wm title $w "tkBuilder"
    wm resizable $w 0 0
    set f [frame .winLicense.f1 -bd 0]
    pack $f
    set m [message $f.m1 -width 300]
    pack $m
    $m configure -text \
	"tkBuilder
[mc {Forked from}] version 1.0.2 [mc {by}] andrasy
Copyright  (C) 1999, 2000 Frank Schnekenburger

[mc {This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version}].

[mc {This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details}].

[mc {You should have received a copy of the GNU General Public License along with this program in the file}] '_GPL.txt'; [mc {if not, write to the}] Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA"
	
	set b [button $f.b1 -text OK -default active \
		-command "destroy $w"]
	pack $b -pady 3
	bind $b <Key-Return> "destroy $w"
	
	set screenX [winfo screenwidth .]
	set screenY [winfo screenheight .]
	set reqX [winfo reqwidth $w]
	set reqY [winfo reqheight $w]
	set x [expr ($screenX - $reqX) / 2]
	set y [expr ($screenY - $reqY) / 2]
	wm geometry $w +$x+$y

	raise $w
	focus -force $b

}
