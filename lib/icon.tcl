# -------------------------------------------------------------
# icon.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
# -------------------------------------------------------------

namespace eval Icon {	
    variable _icon_
    # array of icon images, indexed on image name (minus '.gif')
}


# makes the icon images from gif files; all gif files in the icons
# directory are made into images
proc Icon::MakeIconImages { } {
    variable _icon_

    set dirIcon [Main::GetSystem dirIcons] ; # directory containing icon images
    
    # loop once for each gif file in icon directory
    foreach file [glob [file join $dirIcon *.gif]] {
	set icon [file tail [file rootname $file]] ; # get icon name
	set _icon_($icon) [image create photo -file $file]
    }
    return

}


proc Icon::GetIconImage { icon } {
    variable _icon_
    return $_icon_($icon)
}


proc Icon::IconExists { icon } {
    variable _icon_
    return [info exists _icon_($icon)]
}