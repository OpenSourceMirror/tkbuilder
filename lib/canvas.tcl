# -------------------------------------------------------------
# canvas.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------


namespace eval Canvas {

    variable _canvasObjectList_
    # simple list of the canvas objects (e.g. arc bitmap ...)
    
    variable _canvasObjectAttributes_
    # array indexed on canvas object, each element a list of the attributes
    # available for that object
    
    variable _numCoord_
    # array indexed on canvas object, indicating the number of coordinates required
    # for the object
    
    variable _numCoordExact_
    # array indexed on canvas object; set to 1 if the number of coordinates
    # must be exactly number in _numCoord_, 0 if it can be equal or greater

}


proc Canvas::ConfigureCanvasObjectVariables { } {

    variable _canvasObjectAttributes_
    variable _canvasObjectList_
    variable _numCoord_
    variable _numCoordExact_
    
    set _canvasObjectList_  [list arc bitmap image line oval polygon rectangle ctext window]
    
    # for each canvas object, set list of available attributes
    set _canvasObjectAttributes_(arc) [list\
					   coordinates extent fill outline outlinestipple start stipple style tags width]
    set _canvasObjectAttributes_(bitmap) [list\
					      coordinates anchor background bitmap foreground tags]
    set _canvasObjectAttributes_(image) [list\
					     coordinates anchor image tags]
    set _canvasObjectAttributes_(line) [list\
					    coordinates arrow arrowshape capstyle fill joinstyle smooth \
					    splinesteps stipple tags width]
    set _canvasObjectAttributes_(oval) [list\
					    coordinates fill outline stipple tags width]
    set _canvasObjectAttributes_(polygon) [list\
					       coordinates fill outline smooth splinesteps stipple tags width]
    set _canvasObjectAttributes_(rectangle) [list\
						 coordinates fill outline stipple tags width]
    set _canvasObjectAttributes_(ctext) [list\
					     coordinates anchor fill font justify stipple tags text width]
    set _canvasObjectAttributes_(window) [list\
					      coordinates anchor height tags width window]

    # for each canvas object, set number of expected values in the coordinates
    # attribute and whether the count must be exact (1) or can be greater (0)
    foreach {object number exact} { arc 4 1 bitmap 2 1 image 2 1 line 4 0 \
					oval 4 1 polygon 6 0 rectangle 4 1 ctext 2 1 \
					window 2 1 } {
	set _numCoord_($object) $number
	set _numCoordExact_($object) $exact
    }

}


# returns list of canvas objects (i.e. arc bitmap image ...)
proc Canvas::GetCanvasObjectList { } {
    variable _canvasObjectList_
    return $_canvasObjectList_
}


# returns list of attributes available for the canvas object 'object'
proc Canvas::GetCanvasObjectAttributes { object } {
    variable _canvasObjectAttributes_
    return $_canvasObjectAttributes_($object)
}


# returns 1 if 'widget' is a canvas object, 0 otherwise
proc Canvas::WidgetIsCanvasObject { widget } {
    variable _canvasObjectList_
    if { [lsearch $_canvasObjectList_ $widget] == -1 } {
	return 0
    } else {
	return 1
    }
}

# returns 1 if 'nodeNumber' is a canvas object, 0 otherwise
proc Canvas::NodeIsCanvasObject { nodeNumber } {
    set widgetType [Tree::GetNodeItem widgetType $nodeNumber]
    return [WidgetIsCanvasObject $widgetType]
}


# adds canvas object 'object' to the widget hierarchy based on
# relation 'relation'
proc Canvas::AddCanvasObject { object relation } {
    WidgetTree::AddWidget $object $relation
}


# returns the command to build this canvas object; returns -1 if there are problems
proc Canvas::GetCanvasObjectCommand { parentPathName nodeNumber objectType } {

    variable _numCoord_
    variable _numCoordExact_

    # initialize command string; if object type is 'ctext', replace
    # with word 'text' ('ctext' is tkBuilder's replacement for 'text'
    # in display)
    if { $objectType == "ctext" } {
	set commandString "$parentPathName create text "
    } else {
	set commandString "$parentPathName create $objectType "
    }

    # get and check number of coordinates
    set coordinates [Attribute::GetWidgetAttribute coordinates $nodeNumber]
    if { [Option::GetOption buildCheckCanvasObjectCoord] } {
	set objectName [Tree::GetNodeItem name $nodeNumber]
	set numCoord [llength $coordinates]
	if { $_numCoordExact_($objectType) } {
	    if { $numCoord != $_numCoord_($objectType) } {
		Utility::ErrorMessage "[mc {Invalid number of coordinates for canvas}]\n\
					[mc {object}] '$objectName' [mc {on canvas}] '$parentPathName'.\n\
					'$objectType' [mc {canvas objects require}]\
					$_numCoord_($objectType) [mc {coordinates}]."
		return -1
	    }
	} else {
	    if { $numCoord < $_numCoord_($objectType) } {
		Utility::ErrorMessage "[mc {Invalid number of coordinates for canvas}]\n\
					[mc {object}] '$objectName' [mc {on canvas}] '$parentPathName'.\n\
					'$objectType' [mc {canvas objects require}]\
					$_numCoord_($objectType) [mc {or more coordinates}]."
		return -1
	    }
	}
	# check for even number of coordinates
	if { ($numCoord/2.0) != ($numCoord/2) } {
	    Utility::ErrorMessage "[mc {Invalid number of coordinates for canvas}]\n\
				[mc {object}] '$objectName' [mc {on canvas}] '$parentPathName'.\n\
				[mc {Even number of coordinates must be specified}]."
	    return -1
	}
    }
    # append the coordinates required for the command at this point
    append commandString $coordinates
    
    # loop once for each attribute of this canvas object
    foreach attribute [Attribute::GetAttributeList $objectType] {
	if { $attribute != "coordinates" } {
	    set value [Attribute::GetWidgetAttribute $attribute $nodeNumber]
	    if { $value != {} } {		
		# ensure that the attribute value is only one word is size
		if { [llength $value] != 1 } {
		    set objectName [Tree::GetNodeItem name $nodeNumber]
		    Utility::ErrorMessage "[mc {Invalid value}] '$value' [mc {for attribute}]\n\
		  				'$attribute' [mc {of object}] '$objectName' [mc {on canvas}] '$parentPathName'.\n\
		  				[mc {Value must be one word in size}]."
		    return -1
		}
		set attribute [string tolower $attribute]
		append commandString " -$attribute $value"
	    }
	}
    }
    return $commandString
}
