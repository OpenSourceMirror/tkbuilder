# -------------------------------------------------------------
# menuentry.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------



namespace eval MenuEntry { } {

    variable _menuEntryList_
    # list of the menu entries (e.g. command, check...)

    variable _menuEntryAttributes_
    # list of attributes available for menu entries

    namespace export ConfigureMenuEntryVariables
    namespace export GetMenuEntryList

}


proc MenuEntry::ConfigureMenuEntryVariables { } {
    variable _menuEntryList_
    variable _menuEntryAttributes_
    set _menuEntryList_ [list cascade check command radio separator]

    # list of attributes common to all menu entries
    set _menuEntryAttributes_ [list\
				   activeBackground\
				   activeForeground\
				   accelerator\
				   background\
				   bitmap\
				   columnBreak\
				   command\
				   font\
				   foreground\
				   hideMargin\
				   image\
				   label\
				   justify\
				   state\
				   underline]

}


# returns 1 if 'widget' is a menu entry, 0 otherwise
proc MenuEntry::WidgetIsMenuEntry { widget } {
    variable _menuEntryList_
    if { [lsearch $_menuEntryList_ $widget] == -1 } {
	return 0
    } else {
	return 1
    }
}


# returns 1 if 'nodeNumber' is a menu entry, 0 otherwise
proc MenuEntry::NodeIsMenuEntry { nodeNumber } {
    set widgetType [Tree::GetNodeItem widgetType $nodeNumber]
    return [WidgetIsMenuEntry $widgetType]
}


# returns the list of menu entries (i.e. cascade, check, ...)
proc MenuEntry::GetMenuEntryList { } {
    variable _menuEntryList_
    return $_menuEntryList_
}


# returns the list of attributes available for menu entry 'entry'
proc MenuEntry::GetMenuEntryAttributes { entry } {
    variable _menuEntryAttributes_
    set attributeList $_menuEntryAttributes_
    switch $entry {
	cascade   {lappend attributeList menu}
	check     {lappend attributeList offValue onValue selectColor variable}
	radio     {lappend attributeList selectColor value variable}
	separator {set attributeList {}}
    }
    return [lsort -dictionary $attributeList]
    #	return $_menuEntryAttributes_
}

proc MenuEntry::AddMenuEntry { entry relation } {
    WidgetTree::AddWidget $entry $relation
}


# returns the command to build this menu entry; returns -1 if there are problems
proc MenuEntry::GetMenuEntryCommand { parentPathName nodeNumber objectType } {

    # initialize the command string
    set commandString "$parentPathName add $objectType "
    
    # loop once for each attribute of this canvas object
    foreach attribute [Attribute::GetAttributeList $objectType] {
	if { $attribute != "coordinates" } {
	    set value [Attribute::GetWidgetAttribute $attribute $nodeNumber]
	    if { $value != {} } {		
		# ensure that the attribute value is only one word is size
		if { [llength $value] != 1 } {
		    set objectName [::Tree::GetNodeItem name $nodeNumber]
		    Utility::ErrorMessage "[mc {Invalid value}] '$value' [mc {for attribute}]\n\
		  					'$attribute' [mc {of entry}] '$objectName' [mc {of menu}] '$parentPathName'.\n\
		  					[mc {Value must be one word in size}]."
		    return -1
		}
		set attribute [string tolower $attribute]
		append commandString " -$attribute $value"
	    }
	}
    }
    return $commandString
}
