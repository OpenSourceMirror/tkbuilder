# -------------------------------------------------------------
# utility.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------


namespace eval Utility {

    variable _background_ {}

    namespace export ErrorMessage
    namespace export ConfirmMessage
    namespace export SetLabelColourNormal
    namespace export SetLabelColourHighlight

}


# sets the value of variable named 'varName2' to 0 if variable named 'varName1'
# is 1; otherwise does nothing; used to toggle the value of a checkbox (varName2) who's
# value is orthogonal to another (varName1)
proc Utility::SetVar2OnVar1 { varName1 varName2 } {
    upvar $varName1 var1
    upvar $varName2 var2
    if { $var1 == 1 } {
	set var2 0
    }
}


# toggles the value of 'variable' from 0 to 1, or 1 to 0
proc Utility::ToggleValue { variable } {
    upvar $variable var
    if { $var == 0 } {
	set var 1
    } else {
	set var 0
    }
}


# general handler for error messages; displays message 'message'
proc Utility::ErrorMessage { message } {
    set focus [focus]
    tk_messageBox -type ok -default ok -title tkBuilder \
	-icon error -message " $message"
    if { $focus != {} } {
	raise [winfo toplevel $focus]
	focus -force $focus
    }
}


# general handler for confirmation messages; displays message 'message';
# defaults default to yes; returns response: yes or no
proc Utility::ConfirmMessage { message {default yes} } {
    set focus [focus]
    set response [tk_messageBox -type yesno -default $default \
		      -title "[mc {Confirm}]" -icon question -message " $message"]
    if { $focus != {} } {
	raise [winfo toplevel $focus]
	focus -force $focus
    }
    return $response
}


# general handler for information messages
proc Utility::InfoMessage { message } {
    set focus [focus]
    tk_messageBox -type ok -default ok \
	-title tkBuilder -icon info -message " $message"
    if { $focus != {} } {
	raise [winfo toplevel $focus]
	focus -force $focus
    }
}


# sets the colours of 'label' to normal default
proc Utility::SetLabelColourNormal { label } {
    $label config -bg [Utility::GetBackgroundColour] -fg black -relief flat
}


# sets the colours of 'label' to highlighted
proc Utility::SetLabelColourHighlight { label } {
    $label config -bg darkblue -fg white
}


# set the colours of 'label' to indicate it has lost focus, though is still current
proc Utility::SetLabelColourNoFocus { label } {
    $label config -bg lightblue -fg black
}


proc Utility::SetBackgroundColour { } {
    variable _background_ [. cget -background]
}
proc Utility::GetBackgroundColour { } {
    variable _background_
    return $_background_
}


# returns 1 if 'word' is a variable name (contains a $), 0 otherwise
proc Utility::WordIsVariable { word } {
    if { [string first "\$" $word] == -1 } {
	return 0
    } else {
	return 1
    }
}



# creates a colour box ('frame') the user can click on to choose a colour;
# binds events to entry 'entry', which will display the choice; creates
# 'frame', but 'entry' must exist
proc Utility::CreateColourBox { frame entry {command {}} } {
    frame $frame -borderwidth 1 -relief ridge \
	-width 16 -height 16 -background [Utility::GetBackgroundColour]
    #	bind $entry <FocusOut> "Utility::SetColourBox $frame $entry 1"
    bind $frame <ButtonPress-1> "Utility::GetColourBox [list $frame $entry $command]"
    return $frame
}


# gets colour value currently stored in 'entry' and sets colour box 'frame'
# to that colour; if 'check' is 1, checks that the colour value is valid
proc Utility::SetColourBox { frame entry {check 0} } {
    set colour [string trim [$entry get]]
    if { $colour == {} || [Utility::WordIsVariable $colour] } {
	$frame configure -background [Utility::GetBackgroundColour]
    } else {
	if { [catch {$frame configure -background $colour}] } {
	    if { $check } {
		Utility::ErrorMessage "[mc {Invalid colour}]: $colour"
		$frame configure -background [Utility::GetBackgroundColour]
	    }
	}
    }
}


# opens a colour selection dialog box to set the colour associated with
# colour box 'frame' and entry 'entry'; 'command' is executed if a
# colour is selected
proc Utility::GetColourBox { frame entry {command {}} } {
    set colour [tk_chooseColor -title "[mc {Colour}]"]
    if { $colour != {} } {
	$entry delete 0 end
	$entry insert 0 $colour
	$frame configure -background $colour
	eval $command
    }
}



# creates a font box ('frame') the user can click on to choose a font;
# binds events to entry 'entry', which will display the choice; creates
# 'frame', but 'entry' must exist;
# 'command' is executed when a font is selected (by the GetFontBox proc)
proc Utility::CreateFontBox { frame entry {command {}} } {
    frame $frame -borderwidth 1 -relief ridge \
	-width 16 -height 16 -background [Utility::GetBackgroundColour]
    bind $frame <ButtonPress-1> "Utility::GetFontBox [list $entry $command]"
    return $frame
}


# opens a font selection dialog box to let user select the font associated with
# entry 'entry'; 'command' is executed if a font is selected
proc Utility::GetFontBox { entry {command {}} } {
    set currentFont [string trim [$entry get]]
    set response [Font::OpenFontDialog $currentFont]
    if { [lindex $response 0] == "CANCEL" } {
	return
    } else {
	set font [lindex $response 1]
	$entry delete 0 end
	$entry insert 0 $font
	eval $command
    }
}
