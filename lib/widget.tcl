# -------------------------------------------------------------
# widget.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
# -------------------------------------------------------------



namespace eval Widget {

    variable _widgetList_
    # list of all possible widgets types
    
    variable _showWidgetList_
    # list of attributes to show (subset of _widgetList_)
    
    variable _hideWidgetList_
    # list of attributes to hide (subset of _widgetList_)

    namespace export GetWidgetList

}



proc Widget::InitializeWidgetVariables { } {

    variable _widgetList_

    if {[package present Tk] >= 8.4} {
	set _widgetList_ [list\
			      button\
			      canvas\
			      checkbutton\
			      entry\
			      frame\
			      label\
			      labelframe\
			      listbox\
			      menu\
			      menubutton\
			      message\
			      panedwindow\
			      radiobutton\
			      scale\
			      scrollbar\
			      spinbox\
			      text\
			      \
			      toplevel\
			     ]
    } else {
	set _widgetList_ [list\
			      button\
			      canvas\
			      checkbutton\
			      entry\
			      frame\
			      label\
			      listbox\
			      menu\
			      menubutton\
			      message\
			      radiobutton\
			      scale\
			      scrollbar\
			      text\
			      \
			      toplevel\
			     ]
    }
}


proc Widget::GetWidgetList { } {
    variable _widgetList_
    return $_widgetList_
}

