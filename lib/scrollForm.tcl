# -------------------------------------------------------------
# scrollForm.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
# -------------------------------------------------------------


namespace eval ScrollForm {

}


proc ScrollForm::CreateScrollForm { frame } {

    # frame to hold canvas and scrollbars
    set fraWidget [frame $frame -bd 2 -relief groove -highlightthickness 0]
    pack $fraWidget -side left -anchor nw -fill both -expand 1
    
    set canWidget [canvas $fraWidget.canWidget  \
		       -xscrollcommand [list ScrollForm::ScrollSet $fraWidget.scrX \
					    [list grid $fraWidget.scrX -row 1 -column 0 -sticky we]] \
		       -yscrollcommand [list ScrollForm::ScrollSet $fraWidget.scrY \
					    [list grid $fraWidget.scrY -row 0 -column 1 -sticky ns]] \
		       -highlightthickness 0]

    set _widgetCanvas_ $canWidget
    scrollbar $fraWidget.scrX -orient horizontal -command "$canWidget xview"
    scrollbar $fraWidget.scrY -orient vertical   -command "$canWidget yview"

    grid $canWidget -row 0 -column 0 -sticky nsew
    grid $fraWidget.scrY -row 0 -column 1 -sticky ns
    grid $fraWidget.scrX -row 1 -column 0 -sticky ew

    grid columnconfigure $fraWidget 0 -weight 1
    grid rowconfigure $fraWidget 0 -weight 1
    
    # frame for putting widgets
    set fraForm [frame $canWidget.fraForm -bd 0]
    set item [$canWidget create window 1 1 -anchor nw -window $fraForm]

    bind $fraForm <Configure> "$canWidget configure -scrollregion \[$canWidget bbox all]"
    
    return $fraForm
    
}


proc ScrollForm::ScrollSet { scrollbar command offset size } {
    if { $offset != 0.0 || $size != 1.0 } {
	eval $command
	$scrollbar set $offset $size
    } else {
	grid forget $scrollbar ; # hide the scroll bar
    }
}

