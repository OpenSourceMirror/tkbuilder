# -------------------------------------------------------------
# listbox.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
# -------------------------------------------------------------


namespace eval Listbox {
}


# creates a listbox with scrollbars; returns name of the listbox,
# which will be '$name.listbox'; creates a frame '$name' within
# which the listbox and scrollbars are packed; caller must pack
# this frame
proc Listbox::CreateListbox { name args } {

    set f [frame $name -bd 0]
    listbox $f.listbox \
	-xscrollcommand [list Listbox::ScrollSet $f.xscroll \
			     [list grid $f.xscroll -row 1 -column 0 -sticky we]] \
	-yscrollcommand [list Listbox::ScrollSet $f.yscroll \
			     [list grid $f.yscroll -row 0 -column 1 -sticky ns]]
    eval {$f.listbox configure} $args
    scrollbar $f.xscroll -orient horizontal \
	-command [list $f.listbox xview]
    scrollbar $f.yscroll -orient vertical \
	-command [list $f.listbox yview]
    grid $f.listbox $f.yscroll -sticky news -padx 0 -pady 0
    grid $f.xscroll -sticky news -padx 0 -pady 0
    grid rowconfigure $f 0 -weight 1
    grid columnconfigure $f 0 -weight 1
    update
    grid propagate $f 0
    return $f.listbox

}


# manages the scrollbars for the listbox created with 'CreateListbox'
proc Listbox::ScrollSet { scrollbar command offset size } {
    if { $offset != 0.0 || $size != 1.0 } {
	eval $command
	$scrollbar set $offset $size
    } else {
	set manager [lindex $command 0]
	$manager forget $scrollbar ; # hide the scroll bar
    }
}


# returns the name of the packing frame used to build the scrolled 
# listbox 'listbox'; 'listbox' must be a scrolled listbox returned 
# by 'CreateListbox' (truncates '.listbox' from $listbox)
proc Listbox::GetListboxFrame { listbox } {
    set index [expr [string length $listbox] - 9]
    return [string range $listbox 0 $index]
}


# returns the contents of 'listbox' as a list
proc Listbox::ListboxToList { listbox } {
    return [$listbox get 0 end]
}


# fills 'listbox' with the items in 'list'
proc Listbox::ListboxFromList { listbox list {clear 1} } {
    if { $clear } {
	$listbox delete 0 end
    }
    eval { $listbox insert end } $list
}


# clears 'listbox'
proc Listbox::ClearListbox { listbox } {
    $listbox delete 0 end
}


# returns 1 if 'item' occurs in 'listbox', 0 otherwise
proc Listbox::ItemExists { listbox item } {
    if { [lsearch [$listbox get 0 end] $item] == -1 } {
	return 0
    } else {
	return 1
    }
}


# moves the selected items from 'fromListbox' to 'toListbox' if they don't already
# exist there; maintains items in 'toListbox' in dictionary sort order
proc Listbox::MoveSelectedItems { fromListbox toListbox } {

    set fromList   [ListboxToList $fromListbox]
    set toList     [ListboxToList $toListbox]
    set selectList [$fromListbox curselection]

    if { $selectList != {} } {
	foreach index $selectList {
	    set selectItem  [$fromListbox get $index]
	    if { [lsearch $toList $selectItem] == -1 } {
		lappend toList $selectItem
		set toList [lsort -dictionary $toList]
		set toIndex [lsearch $toList $selectItem]
		$toListbox insert $toIndex $selectItem
	    }
	}
	set offset 0
	foreach index $selectList {
	    $fromListbox delete [expr $index-$offset]
	    incr offset
	}
    }

}


# deletes currently selected items from listbox 'listbox'
proc Listbox::DeleteListboxItems { listbox } {
    set selectList [$listbox curselection]
    set offset 0
    foreach index $selectList {
	$listbox delete [expr $index-$offset]
	incr offset
    }
}


# inserts the text in entry widget 'entry' as an item into listbox 'listbox';
# clears the entry afterward unless 'clear' is set to 0
proc Listbox::InsertEntry { listbox entry {clear 1} } {
    set entryText [string trim [$entry get]]
    if { $entryText == {} } return
    set list [ListboxToList $listbox]
    lappend list $entryText
    ListboxFromList $listbox [lsort -dictionary $list]
    if { $clear } {
	$entry delete 0 end
    }
}
