# -------------------------------------------------------------
# widgetForms.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------



namespace eval WidgetForms {

    namespace import ::Utility::SetLabelColourNormal
    namespace import ::Utility::SetLabelColourHighlight
    namespace import ::Tree::GetNodeItem
    namespace import ::Attribute::GetWidgetAttribute
    namespace import ::Attribute::GetAttributeList
    namespace import ::Attribute::GetAttributeOption
    namespace import ::Attribute::GetAllAttributesList
    namespace import ::Attribute::GetHiddenAttributesList
    namespace import ::Attribute::CopySelectedWidgetAttributes
    namespace import ::Layout::GetLayoutAttributeList
    namespace import ::Layout::GetLayoutAttributeOption
    namespace import ::Layout::GetLayoutType
    namespace import ::Layout::CopySelectedLayoutAttributes
    namespace import ::Binding::CopySelectedBindingEvents

    variable _widgetFrame_
    variable _notebook_
    variable _page_
    variable _form_

    variable _layoutFrame_
    # array indexed on layout type (pack, grid, place, none) that stores the
    # name of the frame containing the entry items for that layout type

    variable _layoutButtonFrame_
    # name of frame containing the layout buttons (pack, grid, place)

    variable _lastLayoutType_ {}
    # stores the type of the last layout (pack, grid or place) displayed

    variable _lastWidgetType_ {}
    # stores the type of the last widget for which the attribute
    # form was used (e.g. button)

    variable _selectedAttributes_ {}
    # simple list of attributes that have been selected on the current
    # form; stores the names of the label widgets used to display
    # the attribute names (not the attribute names); there is only one
    # list, used for all forms (its cleared anytime a form is raised)

    variable _lastSelectedAttribute_ {}
    # name of last label widget selected

    variable _currentForm_ {}
    # the currently raised form: WIDGET_FORM, LAYOUT_FORM, BINDING_FORM
    # GENERAL_FORM, or SPECIAL_FORM

    variable _lastNodeDisplayed_
    # array indexed on form (WIDGET_FORM, LAYOUT_FORM, BINDING_FORM, GENERAL_FORM)
    # that contains the number of the last node displayed

    namespace export BuildAttributeForm UpdateAttributeForm
    namespace export BuildLayoutForm    UpdateLayoutForm
    namespace export BuildBindingForm   UpdateBindingForm
    namespace export UpdateAllForms
    namespace export ClearSelectedAttributes
    namespace export GetSelectedAttributes
    namespace export GetCurrentForm

}


# sets the name of the currently displayed form; form must be one of:
# WIDGET_FORM, LAYOUT_FORM, BINDING_FORM, GENERAL_FORM, SPECIAL_FORM
proc WidgetForms::SetCurrentForm { form } {
    variable _currentForm_ $form
}


# returns the name of the currently displayed form; one of: WIDGET_FORM, 
# LAYOUT_FORM, BINDING_FORM, GENERAL_FORM, SPECIAL_FORM
proc WidgetForms::GetCurrentForm { } {
    variable _currentForm_
    return $_currentForm_
}


# set 'nodeNumber' as the last node displayed on form of type 'form'
# (WIDGET_FORM, LAYOUT_FORM, BINDING_FORM, GENERAL_FORM, SPECIAL_FORM)
proc WidgetForms::SetLastNodeDisplayed { form nodeNumber } {
    variable _lastNodeDisplayed_
    set _lastNodeDisplayed_($form) $nodeNumber
}


# returns the last node displayed on 'form'
proc WidgetForms::GetLastNodeDisplayed { form } {
    variable _lastNodeDisplayed_
    return  $_lastNodeDisplayed_($form)
}


# resets the array of last node displayed for all forms to {};
# called when new tree is created or opened so that all forms redraw correctly
proc WidgetForms::ResetLastNodeDisplayed { } {
    variable _lastNodeDisplayed_
    foreach form { WIDGET_FORM LAYOUT_FORM BINDING_FORM GENERAL_FORM SPECIAL_FORM } {
	set _lastNodeDisplayed_($form) {}
    }
}


proc WidgetForms::CreateWidgetFormsFrame { fraMain } {

    variable _widgetFrame_
    variable _page_
    variable _notebook_
    variable _form_
    variable _lastNodeDisplayed_

    # main frame to hold all widget property forms
    set fraWidget [frame $fraMain.fraWidgetForms -bd 1 -relief ridge]
    pack $fraWidget -side left -fill both -expand 1
    set _widgetFrame_ $fraWidget
    
    # binding to set colour on widget tree node label to indicate
    # it has lost focus, when property form gets focus
    bind $fraWidget <FocusIn> {
	Utility::SetLabelColourNoFocus [WidgetTree::GetLabelFromNodeNumber [Tree::GetCurrentNode]]
    }

    set _notebook_ [NoteBook $fraWidget.notebook -height 100 -width 340]
    pack $_notebook_ -side top -fill both -expand 1 -padx 0 -pady 3
    
    set _page_(attributes) [$_notebook_ insert end attributes -text [mc {Attributes}] \
				-raisecmd "WidgetForms::ClearSelectedAttributes
		           WidgetForms::SetCurrentForm WIDGET_FORM
		           WidgetForms::UpdateAttributeForm \[Tree::GetCurrentNode]"]
    set _page_(layout)     [$_notebook_ insert end layout -text [mc {Layout}] \
				-raisecmd "WidgetForms::ClearSelectedAttributes
		           WidgetForms::SetCurrentForm LAYOUT_FORM
		           WidgetForms::UpdateLayoutForm \[Tree::GetCurrentNode]"]
    set _page_(bindings)   [$_notebook_ insert end bindings -text [mc {Bindings}] \
				-raisecmd "WidgetForms::ClearSelectedAttributes
		           WidgetForms::SetCurrentForm BINDING_FORM
		           WidgetForms::UpdateBindingForm \[Tree::GetCurrentNode]"]
    set _page_(general)    [$_notebook_ insert end general -text [mc {General}] \
				-raisecmd "WidgetForms::ClearSelectedAttributes
		           WidgetForms::SetCurrentForm GENERAL_FORM
		           WidgetForms::UpdateGeneralForm \[Tree::GetCurrentNode]"]
    # added -state disabled option
    set _page_(special)    [$_notebook_ insert end special -text [mc {Special}] \
				-state disabled \
				-raisecmd "WidgetForms::ClearSelectedAttributes
		           WidgetForms::SetCurrentForm SPECIAL_FORM
		           WidgetForms::UpdateSpecialForm \[Tree::GetCurrentNode]"]
    
    # make scrollable forms for attributes, layout and bindings
    set _form_(attributes) [ScrollForm::CreateScrollForm $_page_(attributes).form]
    set _form_(layout)     [ScrollForm::CreateScrollForm $_page_(layout).form]
    set _form_(bindings)   [ScrollForm::CreateScrollForm $_page_(bindings).form]

    bind $_form_(attributes) <ButtonPress-3> \
	{ Menu::PostPopupMenu [Menu::GetPropertiesMenu] %X %Y }
    bind $_form_(layout)     <ButtonPress-3> \
	{ Menu::PostPopupMenu [Menu::GetPropertiesMenu] %X %Y }
    bind $_form_(bindings)   <ButtonPress-3> \
	{ Menu::PostPopupMenu [Menu::GetPropertiesMenu] %X %Y }

    # make frame for general form
    set _form_(general) [frame $_page_(general).form -bd 2 -relief groove]
    pack $_form_(general) -fill both -expand 1

    # make frame for special form
    set _form_(special) [frame $_page_(special).form -bd 2 -relief groove]
    pack $_form_(special) -fill both -expand 1

    ResetLastNodeDisplayed

    #	UpdateAttributeForm 0
    BuildLayoutForm
    #	UpdateLayoutForm 0
    #	UpdateBindingForm 0
    BuildGeneralForm
    #	UpdateGeneralForm 0
    
    UpdateSpecialForm 0 1

    $_notebook_ itemconfigure attributes -state disabled
    $_notebook_ itemconfigure layout -state disabled
    $_notebook_ itemconfigure bindings -state disabled
    $_notebook_ itemconfigure general -state disabled

    #UpdateAllForms 0 1

    # original:
    #	$_notebook_ itemconfigure special -state disabled
    #	$_notebook_ raise attributes


    bind ItemLabel <Enter> "%W config -relief raised"
    bind ItemLabel <Leave> "%W config -relief flat"
    bind ItemLabel <Control-ButtonPress-1> \
	"WidgetForms::SetSelectedAttributes %W %X %Y 1 CONTROL"
    bind ItemLabel <Shift-ButtonPress-1> \
	"WidgetForms::SetSelectedAttributes %W %X %Y 1 SHIFT"
    bind ItemLabel <ButtonPress-1> \
	"WidgetForms::SetSelectedAttributes %W %X %Y 1 {}"
    bind ItemLabel <ButtonPress-3> \
	"WidgetForms::SetSelectedAttributes %W %X %Y 3 {}"


}


# returns the frame on which to build form 'form'
proc WidgetForms::GetFormFrame { form } {
    variable _form_
    return $_form_($form)
}


#------------------------------------------------------------------------------#
# Procedures to build and update the special form
#------------------------------------------------------------------------------#

proc WidgetForms::UpdateSpecialForm { nodeNumber {force 0} } {

    # don't update if this form is not currently displayed or
    # if it is already displaying the current node, unless forced
    if { !$force } {
	if { [GetCurrentForm] != "SPECIAL_FORM" || 
	     [GetLastNodeDisplayed SPECIAL_FORM] == $nodeNumber } {
	    return
	}
    }
    SetLastNodeDisplayed SPECIAL_FORM $nodeNumber
    SetCurrentForm SPECIAL_FORM
    set fraForm [GetFormFrame special]
    Special::OpenSpecialForm $fraForm $nodeNumber

}


#------------------------------------------------------------------------------#
# Procedures to build and update the layout form
#------------------------------------------------------------------------------#

# builds the layout form; uses a separate frame for entries of each layout type
# (pack, grid, place, none)
proc WidgetForms::BuildLayoutForm { } {

    variable _lastLayoutType_
    variable _layoutFrame_
    variable _layoutButtonFrame_
    
    set fraForm [GetFormFrame layout]

    # build radio buttons to choose layout type
    set fraButton [frame $fraForm.fraButton]
    set _layoutButtonFrame_ $fraButton
    foreach layout {pack grid place none} {
	radiobutton $fraButton.$layout -text $layout -value $layout \
	    -command "WidgetForms::UpdateLayoutFrame $layout
			          WidgetForms::ClearSelectedAttributes
			          WidgetTree::RedrawWidgetTree
			          State::SetState modified 1
			          Status::UpdateStatusBar"
	pack $fraButton.$layout -side left
    }
    pack $fraButton -side top -anchor nw -pady 5

    # build entry form for each type of layout
    foreach layout {pack grid place none} {
	
	set i 1
	set fraLayout [frame $fraForm.$layout]
	set _layoutFrame_($layout) $fraLayout
	
	foreach attribute [GetLayoutAttributeList $layout] {
	    
	    # make the label to display the layout attribute name
	    # (note: the '#' is required to extract the index in other procs)
	    set labItem [label $fraLayout.lab#$i -text "$attribute"]
			 grid $labItem -row $i -column 0 -sticky w -pady 1
			 bindtags $labItem [list $labItem Label ItemLabel . all]
			 #			SetLabelBindings $labItem
			 
			 set attributeOption [GetLayoutAttributeOption $attribute]    	
			 if { $attributeOption == {} } {    	
			     # a plain entry
			     set entItem [entry $fraLayout.ent$i -width 15]
			 } else {    	
			     # a combobox entry
			     set entItem [ComboBox $fraLayout.ent$i -width 15 \
					      -editable 1 -state normal]
			     $entItem configure -values $attributeOption \
				 -modifycmd "WidgetForms::ComboboxLayoutProc $attribute $entItem"
			 }
			 grid $entItem -row $i -column 1 -sticky ew -padx 1
			 bind $entItem <KeyPress> "State::CheckModifiedState %K %A"
			 incr i

			 # bindings to update run-time application
			 bind $entItem <FocusOut> \
			     "WidgetForms::UpdateAppLayout $attribute $entItem"
			 bind $entItem <Return> \
			     "WidgetForms::UpdateAppLayout $attribute $entItem
		    	 $entItem icursor 0"

		     }

	}

	set _lastLayoutType_ pack
	pack $_layoutFrame_(pack) -side top -anchor nw -fill both -expand 1

    }
    # procedure to call update widget layout proc in build.tcl
    proc WidgetForms::UpdateAppLayout { attribute entry } {
	Build::UpdateAppLayout [Tree::GetCurrentNode] [list $attribute [$entry get]]
    }
    proc WidgetForms::ComboboxLayoutProc { attribute {window {}} {item {}} } {

	if {($item == "") && ([winfo exists $window])} {
	    set item [$window get]
	}
	State::SetState modified 1
	Build::UpdateAppLayout [Tree::GetCurrentNode] [list $attribute $item]
    }


    # displays frame for the current layout type
    proc WidgetForms::UpdateLayoutFrame { layoutType } {
	variable _layoutFrame_
	variable _lastLayoutType_
	if { $_lastLayoutType_ != $layoutType } {
	    pack forget $_layoutFrame_($_lastLayoutType_)
	    pack $_layoutFrame_($layoutType)
	    set _lastLayoutType_ $layoutType
	}
    }


    # updates the layout form to display information for node 'nodeNumber'
    proc WidgetForms::UpdateLayoutForm { nodeNumber {force 0} } {

	variable _layoutButtonFrame_
	variable _layoutFrame_
	variable _lastLayoutType_

	# don't update if this form is not currently displayed or
	# if it is already displaying the current node, unless forced
	if { !$force } {
	    if { [GetCurrentForm] != "LAYOUT_FORM" || 
		 [GetLastNodeDisplayed LAYOUT_FORM] == $nodeNumber } {
		return
	    }
	}
	SetLastNodeDisplayed LAYOUT_FORM $nodeNumber

	# re-assign text variables of the layout radio buttons
	# so that selection is stored in layout data array
	# for this node
	# determine whether this node's widget should be laid out; if not
	# disable layout selection buttons
	if { [Layout::WidgetHasLayout [GetNodeItem widgetType $nodeNumber]] } {
	    set state normal
	} else {
	    set state disabled
	}
	foreach layoutType {pack grid place none} {
	    $_layoutButtonFrame_.$layoutType configure \
		-variable Layout::_layout_($nodeNumber.layout) -state $state
	}

	# re-assign text variables for the entries on the form
	# so that they reflect and are stored in the layout data
	# array for this node
	foreach layoutType {pack grid place none} {
	    set i 1
	    set fraLayout $_layoutFrame_($layoutType)
	    foreach attribute [GetLayoutAttributeList $layoutType] {
		$fraLayout.ent$i configure -textvariable \
		    Layout::_layout_($nodeNumber.$layoutType.$attribute)
		incr i
	    }
	}
	
	UpdateLayoutFrame [GetLayoutType $nodeNumber]

    }

    #------------------------------------------------------------------------------#


    #------------------------------------------------------------------------------#
    # Procedures to build and update the attribute form
    #------------------------------------------------------------------------------#

    # builds the form for specifying attribute values for node 'nodeNumber'
    proc WidgetForms::BuildAttributeForm { nodeNumber } {

	set i 0 ; # tracks row number and index on attribute labels

	catch { destroy [GetFormFrame attributes].fraMain }
	set fraForm [frame [GetFormFrame attributes].fraMain]

	# get widget class of node 'nodeNumber'
	set widgetType [GetNodeItem widgetType $nodeNumber]
	# get hidden attribute info
	set hiddenAttributes     [GetHiddenAttributesList]
	set showHiddenAttributes [Option::GetOption showHiddenAttributes]

	# loop once for each attribute of this widget class
	foreach attribute [GetAttributeList $widgetType] {

	    if { $showHiddenAttributes || [lsearch $hiddenAttributes $attribute] == -1 } {

	    	set attributeOption [GetAttributeOption $attribute]
		# make the label to display the attribute name
		# (note: the '#' is required to extract the index in other procs)
		set labItem [label $fraForm.lab#$i -text "$attribute"]
			     grid $labItem -row $i -column 0 -sticky w -pady 1
			     bindtags $labItem [list $labItem Label ItemLabel . all]
			     
			     # SetLabelBindings $labItem

			     if { $attributeOption == {} || $attributeOption == "command" } {
				 # a plain entry
				 set entItem [entry $fraForm.ent$i -width 15 \
						  -textvariable Attribute::_attribute_($nodeNumber.$attribute)]
				 grid $entItem -row $i -column 1 -sticky ew -padx 1 -columnspan 2
				 # if this entry is for a command, bind it to text entry window
				 if { $attributeOption == "command" } {
				     # NB CheckModifiedState requires parameters %K and %A.  These are
				     # given as %% here so that they will reference the binding to the textbox created
				     # in OpenTextEntryWindow (not $entItem)
				     bind $entItem <ButtonPress-1> \
					 "Text::OpenTextEntryWindow $entItem {State::CheckModifiedState %%K %%A} \
						\"WidgetForms::ColourFontCommProc $attribute $entItem\""
				     bind $entItem <Key-Return> \
					 "Text::OpenTextEntryWindow $entItem {State::CheckModifiedState %%K %%A} \
			  			\"WidgetForms::ColourFontCommProc $attribute $entItem\""
				 }	  		
			     } elseif { $attributeOption == "colour" } {
				 # a colour entry; add a colour box
				 set entItem [entry $fraForm.ent$i -width 15 \
						  -textvariable Attribute::_attribute_($nodeNumber.$attribute)]
				 set fraCol [Utility::CreateColourBox $fraForm.col$i $entItem \
						 "WidgetForms::ColourFontCommProc $attribute $entItem"]
				 Utility::SetColourBox $fraCol $entItem
				 grid $entItem -row $i -column 1 -sticky ew -padx 1
				 grid $fraCol -row $i -column 2 -sticky w
			     } elseif { $attributeOption == "font" } {
				 # a font entry; add a font box
				 set entItem [entry $fraForm.ent$i -width 15 \
						  -textvariable Attribute::_attribute_($nodeNumber.$attribute)]
				 set fraCol [Utility::CreateFontBox $fraForm.col$i $entItem \
						 "WidgetForms::ColourFontCommProc $attribute $entItem"]
				 grid $entItem -row $i -column 1 -sticky ew -padx 1
				 grid $fraCol -row $i -column 2 -sticky w
			     } else {
				 # a combobox entry (having two or more choices)
				 set entItem [ComboBox $fraForm.ent$i -width 15 \
						  -editable 1 -state normal \
						  -textvariable Attribute::_attribute_($nodeNumber.$attribute)]
				 $entItem configure -values $attributeOption -modifycmd "WidgetForms::ComboboxProc $attribute $entItem"
				 grid $entItem -row $i -column 1 -sticky ew -padx 1 -columnspan 2
			     }
			     incr i
			     bind $entItem <KeyPress> "State::CheckModifiedState %K %A"

			     # bindings to update widgets of running application
			     # N.B. for the following bindings, using GetCurrentNode (rather than passing 
			     # node number as an arg) so that they don't have to be re-bound 
			     # when moving among widgets of the same class (e.g. buttons) of different nodes
			     # (i.e. UpdateAttributeForm doesn't need to modify these bindings for same widget class)
			     if { $attributeOption == "colour" } {
				 bind $entItem <FocusOut> \
				     "WidgetForms::UpdateAppAttribute $attribute $entItem
			         Utility::SetColourBox $fraCol $entItem 1"
				 bind $entItem <Return> \
				     "WidgetForms::UpdateAppAttribute $attribute $entItem
			         $entItem icursor 0
			         Utility::SetColourBox $fraCol $entItem 1"
			     } elseif { $attributeOption != "command" } {
				 bind $entItem <FocusOut> \
				     "WidgetForms::UpdateAppAttribute $attribute $entItem"
				 bind $entItem <Return> \
				     "WidgetForms::UpdateAppAttribute $attribute $entItem
			    	 $entItem icursor 0"
			     } 

			 }
	    }
	    pack $fraForm -fill both -expand 1
	}


	# N.B. for the following 2 procs, using GetCurrentNode (rather than passing 
	# node number as an arg) so that calls to these procs don't have to be re-bound 
	# when moving among widgets of the same class (e.g. buttons) of different nodes
	# (i.e. UpdateAttributeForm doesn't need to modify these calls for same widget class).
	# procedure that is called when a combobox entry is modified; note that the command
	# specified for the combobox is called with the args 'window' and 'item'
	proc WidgetForms::ComboboxProc { attribute {window {}} {item {}} } {

	    if {($item == "") && ([winfo exists $window])} {
		set item [$window get]
	    }
	    State::SetState modified 1
	    Build::UpdateAppWidget [Tree::GetCurrentNode] [list $attribute $item]
	}
	# procedure that is called when a colour, font or command entry is modified via dialog box
	proc WidgetForms::ColourFontCommProc { attribute entry } {
	    State::SetState modified 1
	    focus -force $entry
	    $entry icursor 0
	    Build::UpdateAppWidget [Tree::GetCurrentNode] [list $attribute [$entry get]]
	}
	# procedure to call update widget attribute proc in build.tcl
	proc WidgetForms::UpdateAppAttribute { attribute entry } {
	    Build::UpdateAppWidget [Tree::GetCurrentNode] [list $attribute [$entry get]]
	}


	# updates the form for specifying attribute values for node 'nodeNumber'
	proc WidgetForms::UpdateAttributeForm { nodeNumber {force 0} } {

	    variable _lastWidgetType_

	    # don't update if this form is not currently displayed or
	    # if it is already displaying the current node, unless forced
	    # (to improve display performance)
	    if { !$force } {
		if { [GetCurrentForm] != "WIDGET_FORM" || 
		     [GetLastNodeDisplayed WIDGET_FORM] == $nodeNumber } {
		    return
		}
	    }
	    SetLastNodeDisplayed WIDGET_FORM $nodeNumber

	    ClearSelectedAttributes

	    set widgetType [Tree::GetNodeItem widgetType $nodeNumber]
	    if { !$force && $widgetType == $_lastWidgetType_ } {
		# re-colour frames to reflect those of current widget
		set i 0
		set fraForm [GetFormFrame attributes].fraMain
		set hiddenAttributes [GetHiddenAttributesList]
		set showHiddenAttributes [Option::GetOption showHiddenAttributes]
		foreach attribute [GetAttributeList $widgetType] {
		    # now checking for hidden attributes
		    if { $showHiddenAttributes || 
			 [lsearch $hiddenAttributes $attribute] == -1 } {
			$fraForm.ent$i configure -textvariable \
			    Attribute::_attribute_($nodeNumber.$attribute)
			if { [GetAttributeOption $attribute] == "colour" } {
			    Utility::SetColourBox $fraForm.col$i $fraForm.ent$i
			}
			incr i
		    }
		}
	    } else {
		BuildAttributeForm $nodeNumber
	    }
	    set _lastWidgetType_ $widgetType
	    
	}


	#------------------------------------------------------------------------------#


	#------------------------------------------------------------------------------#
	# Procedures to build and update the binding form
	#------------------------------------------------------------------------------#

	# builds the form for specifying widget bindings (applies
	# to all widget types)
	proc WidgetForms::BuildBindingForm { nodeNumber } {

	    variable _page_

	    # initialize index to enumerate labels/entries
	    set i 0
	    set fraForm [GetFormFrame bindings]

	    if { [winfo exists $fraForm.fraBinding] } {
		destroy $fraForm.fraBinding
	    }
	    set fraBinding [frame $fraForm.fraBinding]

	    # loop once for each event of this node
	    foreach binding [Binding::GetEventList $nodeNumber] {

		# make the label to display the event name
		# (note: the '#' is required to extract the index i in other procs)
		set labItem [label $fraBinding.lab#$i -text "$binding"]
			     bindtags $labItem [list $labItem Label ItemLabel . all]
			     #		SetLabelBindings $labItem

			     # make the entry to enter the binding command
			     set entItem [entry $fraBinding.ent$i -width 20 \
					      -textvariable Binding::_binding_($nodeNumber.$binding)]
			     # bind entry to floating text entry window
			     
			     bind $entItem <KeyPress> "State::CheckModifiedState %K %A"
			     bind $entItem <ButtonPress-1> \
				 "Text::OpenTextEntryWindow $entItem  {State::CheckModifiedState %%K %%A}"
			     bind $entItem <Key-Return> \
				 "Text::OpenTextEntryWindow $entItem  {State::CheckModifiedState %%K %%A}"

			     # 013100: bindings to update bindings in the running application
			     #  		bind $entItem <FocusOut> \
				 #  			"Build::UpdateAppBindings $nodeNumber \[list $binding \[list \[$entItem get]]]"
			     bind $entItem <FocusIn> \
				 "Build::UpdateAppBindings $nodeNumber \[list $binding \[$entItem get]]"
			     bind $entItem <FocusOut> \
				 "Build::UpdateAppBindings $nodeNumber \[list $binding \[$entItem get]]"

			     grid $labItem -row $i -column 0 -sticky w
			     grid $entItem -row $i -column 1 -sticky w -padx 1
			     incr i

			 }

		# pack the frame of label/entries
		pack $fraBinding -fill both -expand true

		# clear list of previously selected attributes, if any
		ResetSelectedAttributes

	    }


	    # updates the binding form to display bindings for node 'nodeNumber'
	    proc WidgetForms::UpdateBindingForm { nodeNumber {force 0} } {

		variable _currentForm_

		# don't update if this form is not currently displayed or
		# if it is already displaying the current node, unless forced
		if { !$force } {
		    if { [GetCurrentForm] != "BINDING_FORM" || 
			 [GetLastNodeDisplayed BINDING_FORM] == $nodeNumber } {
			return
		    }
		}
		SetLastNodeDisplayed BINDING_FORM $nodeNumber

		# updating the binding form really means rebuilding the whole thing	
		BuildBindingForm $nodeNumber

	    }

	    #------------------------------------------------------------------------------#


	    #------------------------------------------------------------------------------#
	    # Procedures to build and update the general form
	    #------------------------------------------------------------------------------#


	    # builds the form for specifying general parameters
	    proc WidgetForms::BuildGeneralForm { } {

		set f [GetFormFrame general]

		# set variable name: $f.ent0
		set lab [label $f.lab0 -text "[mc {Set Variable Name}]:"]
		grid $lab -row 0 -column 0 -sticky w -pady 1
		set ent [entry $f.ent0 -width 15]
		grid $ent -row 0 -column 1 -sticky w -pady 1 -padx 1
		bind $ent <FocusOut> "Status::UpdateStatusBar; WidgetTree::RedrawWidgetTree"
		bind $ent <KeyPress> "State::CheckModifiedState %K %A"

		# use set name as path: $f.chk1
		set chk [checkbutton $f.chk1 -text "[mc {Use set variable name in path of descendants}]" \
			     -command "WidgetTree::RedrawWidgetTree; State::SetState modified 1"]
		grid $chk -row 1 -column 0 -sticky w -pady 1 -columnspan 2
		
		# frame to hold the next two sets of widgets
		set f2 [frame $f.f2 -bd 2 -relief groove]
		grid $f2 -row 3 -column 0 -sticky ew -columnspan 2 -padx 3

		# widget name is full path: $f.chk2
		#	set chk [checkbutton $f.chk2 -text "[mc {Widget name is full path}]" \
		    #		-command "WidgetForms::SetStateOnNameWidgets \[Tree::GetCurrentNode]; \
		    #			WidgetTree::RedrawWidgetTree; State::SetState modified 1"]
		#	grid $chk -in $f2 -row 0 -column 0 -sticky w -pady 1 -columnspan 2
		
		# specify parent name - the check box: $f.chk3
		#	set chk [checkbutton $f.chk3 -text "[mc {Specify parent name}]" \
		    #		-command "WidgetForms::SetStateOnNameWidgets \[Tree::GetCurrentNode]; \
		    #			WidgetTree::RedrawWidgetTree; State::SetState modified 1"]
		#	grid $chk -in $f2 -row 1 -column 0 -sticky w


		# widget name is full path: $f.chk2
		set chk [checkbutton $f.chk2 -text "[mc {Widget name is full path}]" \
			     -command {
				 WidgetForms::SetStateOnNameWidgets [Tree::GetCurrentNode]
				 WidgetTree::RedrawWidgetTree
				 State::SetState modified 1
			     }]
		grid $chk -in $f2 -row 0 -column 0 -sticky w -pady 1 -columnspan 2
		
		# specify parent name - the check box: $f.chk3
		set chk [checkbutton $f.chk3 -text "[mc {Specify parent name}]" \
			     -command {
				 WidgetForms::SetStateOnNameWidgets [Tree::GetCurrentNode]
				 WidgetTree::RedrawWidgetTree
				 State::SetState modified 1
			     }]
		grid $chk -in $f2 -row 1 -column 0 -sticky w

		# specify parent name - the entry: $f.ent3
		set ent [entry $f.ent3 -width 15]
		grid $ent -in $f2 -row 1 -column 1 -sticky ew -padx 3
		grid columnconfigure $f2 1 -weight 1
		bind $ent <KeyPress> "State::CheckModifiedState %K %A"

		# prewidget code: $f.tex4
		set lab [label $f.lab4 -text "[mc {Pre-Widget Code}]:"]
		grid $lab -row 4 -column 0 -sticky w -pady 1 -columnspan 2
		set tex [Text::CreateTextbox $f.tex4 -width 35 -height 5 -wrap none]
		bind $tex <KeyPress> "State::CheckModifiedState %K %A"
		grid [Text::GetTextboxFrame $tex] -row 5 -column 0 -columnspan 2 \
		    -sticky ewns -pady 3 -padx 3
		
		# postwidget code: $f.tex5
		set lab [label $f.lab5 -text "[mc {Post-Widget Code}]:"]
		grid $lab -row 6 -column 0 -sticky w -pady 1 -columnspan 2
		set tex [Text::CreateTextbox $f.tex5 -width 35 -height 5 -wrap none]
		bind $tex <KeyPress> "State::CheckModifiedState %K %A"
		grid [Text::GetTextboxFrame $tex] -row 7 -column 0 -columnspan 2 \
		    -sticky ewns -pady 3 -padx 3

		grid columnconfigure $f 1 -weight 1
		grid rowconfigure $f 5 -weight 1
		grid rowconfigure $f 7 -weight 1

		# pack the frame of label/entries
		pack $f -fill both -expand true

	    }


	    # updates the form for specifying general parameters
	    proc WidgetForms::UpdateGeneralForm { nodeNumber {force 0} } {

		# don't update if this form is not currently displayed or
		# if it is already displaying the current node, unless forced
		# (to improve display performance)
		if { !$force } {
		    if { [GetCurrentForm] != "GENERAL_FORM" || 
			 [GetLastNodeDisplayed GENERAL_FORM] == $nodeNumber } {
			return
		    }
		}
		SetLastNodeDisplayed GENERAL_FORM $nodeNumber

		# DON'T DELETE ... shows relation between widget name and its referencing name
		# f.lab0 "Set Variable Name"
		# f.ent0 set
		# f.chk1 useSetAsParent
		# f.chk2 nameIsPath
		# f.chk3 specifyParentName
		# f.ent3 parent
		# f.tex4 preWidgetCode
		# f.tex5 postWidgetCode

		set f [GetFormFrame general]
		$f.ent0 configure -textvariable General::_general_($nodeNumber.set)
		$f.chk1 configure -variable General::_general_($nodeNumber.useSetAsParent)
		$f.chk2 configure -variable General::_general_($nodeNumber.nameIsPath)
		$f.chk3 configure -variable General::_general_($nodeNumber.specifyParentName)
		$f.ent3 configure -textvariable General::_general_($nodeNumber.parent)

		set tex [Text::GetTextbox $f.tex4]
		Text::SetText $tex [General::GetGeneralParameter preWidgetCode $nodeNumber]
		bind $tex <FocusOut> "General::SetGeneralParameter preWidgetCode $nodeNumber \
		\[Text::GetText $tex]"

		set tex [Text::GetTextbox $f.tex5]
		Text::SetText $tex [General::GetGeneralParameter postWidgetCode $nodeNumber]
		bind $tex <FocusOut> "General::SetGeneralParameter postWidgetCode $nodeNumber \
		\[Text::GetText $tex]"

		SetStateOnNameWidgets $nodeNumber
		
		if { $nodeNumber == [Project::GetRootNodeNumber] } {
		    $f.lab0 configure -foreground [$f.chk1 cget -disabledforeground]
		    $f.ent0 configure -state disabled -bg [Utility::GetBackgroundColour]
		    $f.chk1 configure -state disabled
		    $f.chk2 configure -state disabled
		    $f.chk3 configure -state disabled
		    $f.ent3 configure -state disabled
		} else {
		    $f.lab0 configure -foreground [$f.chk1 cget -foreground]
		    $f.ent0 configure -state normal -bg white 
		    $f.chk1 configure -state normal
		}
		
	    }
	    # adjusts the state of the name widgets
	    proc WidgetForms::SetStateOnNameWidgets { nodeNumber } {
		set f [GetFormFrame general]

		set nameIsPath [General::GetGeneralParameter nameIsPath $nodeNumber]
		if { $nameIsPath } {
		    $f.chk3 configure -state disabled
		    $f.ent3 configure -foreground grey -state disabled
		} else {
		    $f.chk3 configure -state normal
		    $f.ent3 configure -foreground black -state normal
		}

		# added the '-bg white/grey' options
		set specifyParentName [General::GetGeneralParameter specifyParentName $nodeNumber]
		if { $specifyParentName } {
		    $f.chk2 configure -state disabled
		    $f.ent3 configure -foreground black -bg white -state normal
		} else {
		    $f.chk2 configure -state normal
		    $f.ent3 configure -foreground lightgrey -bg [Utility::GetBackgroundColour] -state disabled
		}

	    }



	    #------------------------------------------------------------------------------#
	    # Procedures to manage selection of individual properties
	    #------------------------------------------------------------------------------#

	    # returns the number of label widget 'widget' (used to enumerate label widgets
	    # when they are built)
	    proc WidgetForms::GetLabelWidgetNumber { widget } {
		set index [expr [string last "#" $widget] + 1]
		return [string range $widget $index end]
	    }

	    # returns the root name (w/o trailing index number) of the widget 'widget'
	    proc WidgetForms::GetLabelWidgetRoot { widget } {
		set index [string last "#" $widget]
		return [string range $widget 0 $index]
	    }


	    # manages the selection of attribute labels (names) in the widget and 
	    # layout attribute forms
	    proc WidgetForms::SetSelectedAttributes { W X Y button modifier } {

		variable _selectedAttributes_
		variable _lastSelectedAttribute_
		
		# set focus to this label so bindings on widget tree labels don't inadvertantly
		# fire when user is changing properities (e.g. presses delete key)
		focus $W

		# determine if the selected attribute is already in selected list;
		# get its index in the list
		set position [lsearch $_selectedAttributes_ $W]

		if { $button == 1 } {
		    
		    # highlight or unhighlight current selected attribute label
		    if { $modifier == "CONTROL" } {
			if { $position == -1 } {
			    lappend _selectedAttributes_ $W
			    SetLabelColourHighlight $W
			} else {
			    set _selectedAttributes_ \
				[lreplace $_selectedAttributes_ $position $position]
			    SetLabelColourNormal $W
			}
			
			# highlight attribute labels from the last selected one to the current one
		    } elseif { $modifier == "SHIFT" && $_lastSelectedAttribute_ != {} } {
			set i1   [GetLabelWidgetNumber $_lastSelectedAttribute_]
			set i2   [GetLabelWidgetNumber $W] ; # current widget
			set root [GetLabelWidgetRoot $W] ; # name w/o trailing number
			# ensure that lower number is in first index (i1), 
			# higher is in second (i2)
			if { $i1 > $i2 } { set temp $i1; set i1 $i2; set i2 $temp }
			ClearSelectedAttributes
			for { set i $i1 } { $i <= $i2 } { incr i } {
			    lappend _selectedAttributes_ $root$i
			    SetLabelColourHighlight $root$i
			}
			
			# return highlighted labels to normal, then highlight current one
		    } else {
			foreach label $_selectedAttributes_ {
			    SetLabelColourNormal $label
			}
			set _selectedAttributes_ $W
			SetLabelColourHighlight $W
		    }
		}
		
		if { $button == 3 } {
		    if { $position == -1 } {
			foreach label $_selectedAttributes_ {
			    SetLabelColourNormal $label
			}
			set _selectedAttributes_ $W
			SetLabelColourHighlight $W
		    }
		    # fireup copy attribute popup menu
		    Menu::PostPopupMenu [Menu::GetPropertiesMenu] $X $Y
		}

		set _lastSelectedAttribute_ $W

	    }


	    # sets to null the selected attribute list
	    proc WidgetForms::ResetSelectedAttributes { } {
		variable _selectedAttributes_
		variable _lastSelectedAttribute_
		set _selectedAttributes_ {}
		set _lastSelectedAttribute_ {}
	    }


	    # clears the highlighting of all selected attribute labels (names) on 
	    # the current form and sets to null the selected attribute list
	    proc WidgetForms::ClearSelectedAttributes { } {
		variable _selectedAttributes_
		foreach label $_selectedAttributes_ {
		    SetLabelColourNormal $label
		}
		ResetSelectedAttributes
	    }


	    # returns a list of the names of the attributes selected on the current form
	    # (returns the attribute names, not the label widgets used to display them)
	    proc WidgetForms::GetSelectedAttributes { } {
		variable _selectedAttributes_
		set attributeList {}
		foreach label $_selectedAttributes_ {
		    lappend attributeList [$label cget -text]
		}
		return $attributeList
	    }


	    #------------------------------------------------------------------------------#


	    #------------------------------------------------------------------------------#
	    # Procedures to manage copying of selected properties
	    #------------------------------------------------------------------------------#

	    # copies the values of the selected properties from the current node to
	    # its sibling nodes
	    proc WidgetForms::CopyAttributesToSiblings { } {
		set currentNode [Tree::GetCurrentNode]
		# loop once for each sibling of the current node
		foreach sibling [Tree::GetSiblingList $currentNode] {
		    CopyAttributesMain $currentNode $sibling
		}
	    }


	    # copies values of selected properties from the current node to
	    # all its descendants; calls 'CopyAttributesToDescendantsRecurse' to do
	    # all the work
	    proc WidgetForms::CopyAttributesToDescendants { } {
		set currentNode [Tree::GetCurrentNode]
		# loop once for each child of the current node
		foreach child [GetNodeItem childList $currentNode] {
		    CopyAttributesToDescendantsRecurse $currentNode $child
		}
	    }


	    # copies values of selected properties from the 'currentNode' to the 'toNode';
	    # descends the widget tree recursively; called first by
	    # 'CopyAttributesToDescendants'; 'currentNode' always refers to the original
	    # current node, as set in 'CopyAttributesToDescendants'
	    proc WidgetForms::CopyAttributesToDescendantsRecurse { currentNode toNode } {
		CopyAttributesMain $currentNode $toNode
		# loop once for each child of the to-node (descend tree)
		foreach child [GetNodeItem childList $toNode] {
		    CopyAttributesToDescendantsRecurse $currentNode $child
		}
	    }


	    # procedure that actually copies the widget properties
	    proc WidgetForms::CopyAttributesMain { fromNode toNode } {

		variable _widgetFrame_
		
		# needed so <FocusOut> bindings fire before copying happens
		focus $_widgetFrame_
		update
		
		# check to see if the to-node is a special object; don't copy
		# attributes if so
		if { [Special::NodeIsSpecialObject $toNode] } {
		    return
		}

		# if copying to all widget classes, or the from and to node are of
		# the same class, copy attributes
		if { [Option::GetOption copyAttributesMode] == "ALL_WIDGET_TYPES" || 
		     [GetNodeItem widgetType $fromNode] == [GetNodeItem widgetType $toNode] } {
		    set attributes [GetSelectedAttributes]
		    switch [GetCurrentForm] {
			WIDGET_FORM { 
			    CopySelectedWidgetAttributes $fromNode $toNode $attributes
			}
			LAYOUT_FORM {
			    CopySelectedLayoutAttributes $fromNode $toNode $attributes
			}
			BINDING_FORM {
			    CopySelectedBindingEvents $fromNode $toNode $attributes
			}
		    }
		}
	    }


	    # for the selected attributes of the current node, sets value to null
	    proc WidgetForms::ClearSelectedProperties { } {
		set node [Tree::GetCurrentNode]
		set attributes [GetSelectedAttributes]
		switch [GetCurrentForm] {
		    WIDGET_FORM { 
			Attribute::ClearSelectedWidgetAttributes $node $attributes
		    }
		    LAYOUT_FORM { 
			Layout::ClearSelectedLayoutAttributes $node $attributes
		    }
		    BINDING_FORM {
			Binding::ClearSelectedBindingEvents $node $attributes
		    }
		}
		State::SetState modified 1
	    }


	    #------------------------------------------------------------------------------#


	    # updates all forms
	    proc WidgetForms::UpdateAllForms { nodeNumber {force 0} } {
		variable _currentForm_
		variable _notebook_
		
		ClearSelectedAttributes
		
		if { [Special::NodeIsSpecialObject $nodeNumber] } {
		    $_notebook_ itemconfigure special    -state normal
		    $_notebook_ itemconfigure attributes -state disabled
		    $_notebook_ itemconfigure layout     -state disabled
		    $_notebook_ itemconfigure bindings   -state disabled
		    $_notebook_ itemconfigure general    -state disabled
		    $_notebook_ raise special
		    UpdateSpecialForm $nodeNumber $force
		} else {
		    if { $_currentForm_ == "SPECIAL_FORM" } {
			$_notebook_ itemconfigure special    -state disabled
			$_notebook_ itemconfigure attributes -state normal
			$_notebook_ itemconfigure layout     -state normal
			$_notebook_ itemconfigure bindings   -state normal
			$_notebook_ itemconfigure general    -state normal
			$_notebook_ raise attributes
		    }
		    UpdateAttributeForm  $nodeNumber $force
		    UpdateLayoutForm     $nodeNumber $force
		    UpdateBindingForm    $nodeNumber $force
		    UpdateGeneralForm    $nodeNumber $force
		}
		Status::UpdateStatusBar
	    }


	    proc WidgetForms::RaiseForm { form } {
		variable _notebook_
		$_notebook_ raise $form
	    }


	    # sets the bindings on the label widgets that are used to display attribute
	    # names in the forms
	    #proc WidgetForms::SetLabelBindings { label } {
	    #return
	    #	bind $label <Enter> "%W config -relief raised"
	    #	bind $label <Leave> "%W config -relief flat"
	    #	bind $label <Control-ButtonPress-1> \
		#		"WidgetForms::SetSelectedAttributes %W %X %Y 1 CONTROL"
	    #	bind $label <Shift-ButtonPress-1> \
		#		"WidgetForms::SetSelectedAttributes %W %X %Y 1 SHIFT"
	    #	bind $label <ButtonPress-1> \
		#		"WidgetForms::SetSelectedAttributes %W %X %Y 1 {}"
	    #	bind $label <ButtonPress-3> \
		#		"WidgetForms::SetSelectedAttributes %W %X %Y 3 {}"
	    #}

