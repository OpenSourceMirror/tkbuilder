# -------------------------------------------------------------
# status.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999,2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
# -------------------------------------------------------------



namespace eval Status {

    namespace export UpdateStatusBar
    namespace export UpdateTitleBar
    
    variable _statusList_ [list name type layout set path node] ; list of status items
    variable _entStatus_	; # array naming entry widgets, indexed on status item
    variable _varStatus_	; # array of variable names for entry value, indexed on status item
    variable _width_        ; # array of entry widths, indexed on status item
}	


proc Status::CreateStatusBar { frame } {

    variable _entStatus_
    variable _varStatus_
    variable _statusList_
    variable _width_
    variable _statusBar_
    
    set _width_(name)   15
    set _width_(type)   10
    set _width_(layout)  5
    set _width_(set)    10
    set _width_(path)   15
    set _width_(node)    5
    
    set _statusBar_ $frame

    foreach status $_statusList_ {
	set _entStatus_($status) [entry $frame.$status -width $_width_($status) \
				      -textvariable Status::_varStatus_($status) \
				      -bd 1 -relief sunken -bg [Utility::GetBackgroundColour]]
	pack $frame.$status -side left -padx 2 -pady 2
    }
    #	pack configure $frame.path -fill x -expand 1
    pack configure $frame.path -fill x -expand 1 -before $frame.node
}


proc Status::UpdateStatusBar { } {

    variable _entStatus_
    variable _varStatus_
    variable _statusList_
    variable _width_

    foreach status $_statusList_ {
	$_entStatus_($status) configure -state normal
    }	

    set currentNode [Tree::GetCurrentNode]
    if { ![Special::NodeIsSpecialObject $currentNode] } {
	set _varStatus_(name)   [Tree::GetNodeItem name $currentNode]
	set _varStatus_(type)   [Tree::GetNodeItem widgetType $currentNode]
	set _varStatus_(layout) [Layout::GetLayoutType $currentNode]
	set _varStatus_(set)    [General::GetGeneralParameter set $currentNode]
	set _varStatus_(path)   [Build::GetWidgetPath $currentNode]
	set _varStatus_(node)   $currentNode
    } else {
	set _varStatus_(name)   [Tree::GetNodeItem name $currentNode]
	set _varStatus_(type)   [Tree::GetNodeItem widgetType $currentNode]
	set _varStatus_(layout) {}
	set _varStatus_(set)    {}
	set _varStatus_(path)   {}
	set _varStatus_(node)   $currentNode
    }

    foreach status $_statusList_ {
	$_entStatus_($status) configure -state disabled
    }	

}


# updates the title bar; using 'args' because this function is traced,
# on State 'modified' requiring addition parameters (that aren't used here)
proc Status::UpdateTitleBar { args } {
    set fileName [State::GetState lastTkbFileName]
    if { [State::GetState modified] } {
	set fileModified "*"
    } else {
	set fileModified {}
    }
    if { $fileName == {} } {
	wm title . "tkBuilder $fileModified"
    } else {
	wm title . "tkBuilder - [file nativename $fileName] $fileModified"
    }
}


proc Status::ShowStatusBar { } {
    variable _statusBar_
    if { [Option::GetOption statusBarShow] } {
	pack $_statusBar_ -side bottom -anchor nw -fill x -expand 0 -before .fraMainTop
    } else {
	pack forget $_statusBar_
    }
}
