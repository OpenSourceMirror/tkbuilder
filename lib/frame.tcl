# -------------------------------------------------------------
# frame.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
# -------------------------------------------------------------

namespace eval Frame {
    variable _argsSubFrame_
    variable _numSubFrames_
}

proc Frame::UnsetListFrameData { name } {
    variable _argsSubFrame_
    variable _numSubFrames_
    unset Frame::_argsSubFrame_($name)
    unset Frame::_numSubFrames_($name)
}

# makes the frame 'name' using args 'args'
proc Frame::MakeListFrame { name args } {
    variable _numSubFrames_
    set _numSubFrames_($name) 0 ; # initialize count of sub-frames
    eval frame $name $args
    bind $name <Destroy> "Frame::UnsetListFrameData $name"
    return $name
}

# to set the options that will be used when item frames of list frame 'name' 
# are created; 'args' is of the form '-height 16 -bd 1' etc...;
# each item frame will contain one widget, passed in 'PackInListFrame'
# 'name' must have been created with 'MakeListFrame'
proc Frame::SetListFrameItemArgs { name args } {
    variable _argsSubFrame_
    set _argsSubFrame_($name) $args
    return $name
}

# packs widget 'widgetName' in list frame 'name'; widget will be packed in an item
# frame created by the procedure; item frame is created and packed here using args 'args'
proc Frame::PackInListFrame { name widgetName args } {
    variable _argsSubFrame_
    variable _numSubFrames_
    set i [incr _numSubFrames_($name)] ; # increment and get tally of sub frames
    set f [eval frame $name._sub_frame_$i $_argsSubFrame_($name)] ; # make item frame
    eval pack $f -fill x -anchor w -side top $args
    pack propagate $f 0
    pack $widgetName -in $f -anchor w
    raise $widgetName ; # needed as widget is created before its containing frame
}

