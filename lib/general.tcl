# -------------------------------------------------------------
# general.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------


namespace eval General {

    variable _general_
    variable _parameterList_

    namespace export ConfigureGeneralVariables
    namespace export InitializeGeneralParameters DeleteGeneralParameters
    namespace export SetGeneralParameter GetGeneralParameter
    namespace export CopyGeneralParameters
    #	namespace export CopySelectedGeneralParameters

}


proc General::ConfigureGeneralVariables { } {
    variable _parameterList_
    variable _parameterNames_
    variable _parameterTypes_
    variable _defaultParameterValues_
    set _parameterList_ [list\
			     set\
			     useSetAsParent\
			     nameIsPath\
			     specifyParentName\
			     parent\
			     preWidgetCode\
			     postWidgetCode
			]
    set _parameterNames_ [list\
			      "[mc {Set Variable Name}]:"\
			      "[mc {Use set variable name in path of descendants}]"\
			      "[mc {Widget name is full widget path}]"\
			      "[mc {Specify parent name}]"\
			      "[mc {Parent Name}]:"\
			      "[mc {Pre-Widget Code}]:"\
			      "[mc {Post-Widget Code}]:"
			 ]
    set _parameterTypes_ [list\
			      entry\
			      checkbox\
			      checkbox\
			      checkbox\
			      entry\
			      text\
			      text\
			     ]
    set _defaultParameterValues_ [list\
				      {}\
				      0\
				      0\
				      0\
				      {}\
				      {}\
				      {}\
				     ]
    
    # also for toplevel:
    # 	window: aspect, geometry, grid, maxsize, minsize, 
    #           positionfrom, resizable, sizefrom, title

}



# returns 1 if node 'nodeNumber' overrides widget hierarchy, i.e., has
# an explicit parent name or the node name is a widget path
proc General::NodeOverridesWidgetHierarchy { nodeNumber } {
    variable _general_
    if { $_general_($nodeNumber.specifyParentName) || 
	 $_general_($nodeNumber.nameIsPath) } {
	return 1
    } else {
	return 0
    }
}


# gets the full list of available parameters
proc General::GetParameterList { } {
    variable _parameterList_
    return $_parameterList_
}

# gets the name used for prompts to describe 'parameter'
proc General::GetParameterName { parameter } {
    variable _parameterList_
    variable _parameterNames_
    set index [lsearch $_parameterList_ $parameter]
    return [lindex $_parameterNames_ $index]
}

# gets the parameter array index (e.g. 'parent') used to store data
# data for parameter 'parameterName (e.g. 'Parent name:')
# (does opposite of GetParameterName)
proc General::GetParameter { parameterName } {
    variable _parameterList_
    variable _parameterNames_
    set index [lsearch $_parameterNames_ $parameterName]
    return [lindex $_parameterList_ $index]
}

# gets the type of widget used for input of 'parameter'
proc General::GetParameterType { parameter } {
    variable _parameterList_
    variable _parameterTypes_
    set index [lsearch $_parameterList_ $parameter]
    return [lindex $_parameterTypes_ $index]
}

# initializes the value of general attributes of node 'nodeNumber' to default
# parameter values;
# creates a new set of array elements in _general_ (i.e. _general_($nodeNumber.*))
proc General::InitializeGeneralParameters { nodeNumber } {
    variable _general_
    variable _defaultParameterValues_
    set index 0
    foreach parameter [GetParameterList] {
	set _general_($nodeNumber.$parameter) [lindex $_defaultParameterValues_ $index]
	incr index
    }
}

# unsets the array elements in _general_ used for parameter values of 'nodeNumber';
# (i.e. unsets all elements _general_($nodeNumber.*))
proc General::DeleteGeneralParameters { nodeNumber } {
    variable _general_
    foreach parameter [GetParameterList] {
	unset _general_($nodeNumber.$parameter)
    }
}


# sets the value of the parameter 'parameter' for node 'nodeNumber' to 'value'
proc General::SetGeneralParameter { parameter nodeNumber value } {
    variable _general_
    #	set _general_($nodeNumber.$parameter) [string trim $value]
    set _general_($nodeNumber.$parameter) $value
}


# returns the value of the parameter 'parameter' for node 'nodeNumber'
proc General::GetGeneralParameter { parameter nodeNumber } {
    variable _general_
    return $_general_($nodeNumber.$parameter)
}


# copies the parameter values of node 'fromNodeNumber' into node 'toNodeNumber';
# creates a new set of array elements in _general_ (i.e. _general_($toNodeNumber.*))
# if they don't already exist, or overwrites existing ones
proc General::CopyGeneralParameters { fromNodeNumber toNodeNumber } {
    variable _general_
    foreach parameter [GetParameterList] {
	SetGeneralParameter $parameter $toNodeNumber \
	    [GetGeneralParameter $parameter $fromNodeNumber]
    }
}


proc General::GetGeneralArray { } {
    variable _general_
    return [array get _general_]
}


proc General::SetGeneralArray { array } {
    variable _general_
    array set _general_ $array
}


proc General::GetNodeArrayElements { nodeNumber } {
    variable _general_
    return [array get _general_ $nodeNumber.*]
}


proc General::SetNodeArrayElements { newNodeNumber arrayElements } {
    variable _general_
    foreach { index value } $arrayElements {
	# get index w/o node number (e.g. 325.pack.side yields .pack.side)
	set parameter [string range $index [string first . $index] end]
	set _general_($newNodeNumber$parameter) $value
    }
}


# search general items of 'nodeNumber' for string 'searchString'
proc General::FindString { nodeNumber searchString matchCase matchWholeWord } {
    set matchList {}
    foreach { parameter label } {
	set "[mc {Set Variable Name}]" \
	    parent "[mc {Parent Name}]" \
	    preWidgetCode "[mc {Pre-Widget Code}]" \
	    postWidgetCode "[mc {PostWidget Code}]" } {
	
	set value [General::GetGeneralParameter $parameter $nodeNumber]
	set matchValueList [Find::FindMatch $value $searchString $matchCase $matchWholeWord]
	foreach matchValue $matchValueList {
	    lappend matchList "$label: $matchValue" 
	}
    }
    return $matchList
}

