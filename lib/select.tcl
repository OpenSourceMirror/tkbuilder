# -------------------------------------------------------------
# select.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------

namespace eval Select {
    variable _menuSelect_ {}
    variable _class_
    variable _clippedText_ {} ; # cut or copied text available for pasting
}


proc Select::PostSelectMenu { widget X Y } {
    variable _menuSelect_

    variable _class_
    variable _range_
    
    variable _widget_ $widget
    variable _clippedText_
    variable _selectedText_
    
    # create menu to display select options if it doesn't exist
    if { $_menuSelect_ == {} } {
	set _menuSelect_ [menu .menuSelect -tearoff 0]
	$_menuSelect_ add command -label "[mc {Cut}]" \
	    -command Select::CutSelectedText \
	    -underline 2 -accelerator "Ctrl+X"
	$_menuSelect_ add command -label "[mc {Copy}]" \
	    -command Select::CopySelectedText \
	    -underline 0 -accelerator "Ctrl+C"
	$_menuSelect_ add command -label "[mc {Paste}]" \
	    -command Select::PasteSelectedText \
	    -underline 0 -accelerator "Ctrl+V"
	$_menuSelect_ add separator
	$_menuSelect_ add command -label "[mc {Cancel}]"
    }
    
    # check that caller is either a text or entry
    set _class_ [winfo class $widget]
    if { $_class_ != "Text" && $_class_ != "Entry" } {
	error "[mc {Invalid widget class}]"
    }

    # get the text currently selected in the text or entry widget
    set _selectedText_ {}
    switch [winfo class $widget] { 
	"Text"  {
	    set range [$widget tag nextrange sel 1.0] 
	    if { $range != {} } {
		set i1 [lindex $range 0]
		set i2 [lindex $range 1]
		set _selectedText_ [$_widget_ get $i1 $i2]
	    }
	}
	"Entry" {
	    if { [$widget selection present] } {
		set i1 [$widget index sel.first]
		set i2 [$widget index sel.last]
		set _selectedText_ [string range [$widget get] $i1 $i2]
	    }
	}
	default { error "[mc {Invalid widget class}]" }
    }	

    # get the selected text from the clipboard
    # on UNIX, might need -selection PRIMARY
    set _clippedText_ {}
    if [catch { set _clippedText_ [selection get -selection CLIPBOARD] } ] {
        if [catch { set _clippedText_ [selection get -selection PRIMARY] } ] {
	    set _clippedText_ {}
        }
    }

    UpdateSelectMenu
    #	Menu::PostPopupMenu $Select::_menuSelect_ [expr $X-35] $Y
    #	Menu::PostPopupMenu $Select::_menuSelect_ [expr $X-20] $Y
    Menu::PostPopupMenu $Select::_menuSelect_ $X $Y
}


proc Select::UpdateSelectMenu { } {
    variable _menuSelect_
    variable _clippedText_
    variable _selectedText_
    if { $_clippedText_ == {} } {
	$_menuSelect_ entryconfigure "[mc {Paste}]" -state disabled
    } else {
	$_menuSelect_ entryconfigure "[mc {Paste}]" -state normal
    }
    if { $_selectedText_ == {} } {
	$_menuSelect_ entryconfigure "[mc {Cut}]" -state disabled
	$_menuSelect_ entryconfigure "[mc {Copy}]" -state disabled
    } else {
	$_menuSelect_ entryconfigure "[mc {Cut}]" -state normal
	$_menuSelect_ entryconfigure "[mc {Copy}]" -state normal
    }
}


# copies the selected text to the clipboard
proc Select::CopySelectedText { } {
    variable _selectedText_
    clipboard clear
    clipboard append $_selectedText_
}


# copies the selected text to the clipboard, deleting
# it from the widget
proc Select::CutSelectedText { } {
    variable _widget_
    variable _class_
    variable _selectedText_

    # copy the text
    clipboard clear
    clipboard append $_selectedText_
    
    # delete the text from the widget
    Select::DeleteSelectedText
    State::SetState modified 1
}


proc Select::PasteSelectedText { } {
    variable _widget_
    variable _clippedText_

    # delete any selected text before pasting
    Select::DeleteSelectedText
    
    # same insertion command for Text and Entry
    $_widget_ insert insert $_clippedText_
    State::SetState modified 1
}


proc Select::DeleteSelectedText { } {
    variable _class_
    variable _widget_
    switch $_class_ {
	"Text" {
	    set range [$_widget_ tag nextrange sel 1.0]
	    if { $range != {} } { 
		set i1 [lindex $range 0]
		set i2 [lindex $range 1]
		$_widget_ delete $i1 $i2
	    }
	}
	"Entry" {
	    if { [$_widget_ selection present] } {
		$_widget_ delete sel.first sel.last
	    }
	}			
    }
}


