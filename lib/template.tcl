# -------------------------------------------------------------
# template.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------


namespace eval Template {
	variable _templateDelete_ {}
	variable _templateClass_ {}
	variable _templateAttribute_ {}
	variable _lastNode_ {}
	variable _menuTemplate_ {}
	variable _cmbAttribute_
}


proc Template::OpenTemplateForm { fraTemplate nodeNumber } {
	variable _templateDelete_ {}
	variable _templateClass_ {}
	variable _templateAttribute_ {}
	variable _lastNode_
	variable _menuTemplate_
	variable _cmbAttribute_

	# on first pass, build the template frame and combobox widgets
	# for the class and attribute lists; these are built just once
	# and used for all template forms
	if { ![winfo exists $fraTemplate] } {

		set fraTemplate [frame $fraTemplate]
		pack $fraTemplate -anchor nw -fill both -expand 1

		# frame to hold the class and attribute comboboxes
		set fraTop [frame $fraTemplate.fraTop -borderwidth 1 -relief groove]
		pack $fraTop -anchor nw -padx 3 -pady 3

		# class label and combobox
		label $fraTop.labClass -text [mc {Class}]
		pack $fraTop.labClass -pady 3 -side left
		set cmbClass [ComboBox $fraTop.cmbClass -width 12 \
			-editable 0 -state normal -takefocus 0 \
			-textvariable Template::_templateClass_]
		$cmbClass configure -values [Widget::GetWidgetList]
	 	pack $cmbClass -side left -padx 3

	 	# attribute label and combobox
		label $fraTop.labAttribute -text [mc {Attribute}]
		pack $fraTop.labAttribute -pady 3 -side left
		set cmbAttribute [ComboBox $fraTop.cmbAttribute -width 17 \
			-editable 0 -state normal -takefocus 0 \
			-textvariable Template::_templateAttribute_]
	 	pack $cmbAttribute -side left -padx 3
	    set _cmbAttribute_ $cmbAttribute

		# binding on class combobox to change attribute list of attribute
		# combobox when former is changed
		$cmbClass configure -modifycmd "Template::UpdateAttributeCombobox $cmbClass $cmbAttribute"

		# small menu to display the delete entry option
		set _menuTemplate_ [menu .menuTemplate -tearoff 0]
		$_menuTemplate_ add command -label "[mc {Delete Entry}]" \
			-command Template::DeleteTemplateEntry
		$_menuTemplate_ add separator
		$_menuTemplate_ add command -label "[mc {Cancel}]"

	} else {
	
		pack $fraTemplate -anchor nw -fill both -expand 1
		
	}
	
	# if node number hasn't changed, don't bother re-doing
	if { $nodeNumber != $_lastNode_ } {
	
		set _lastNode_ $nodeNumber
		
		# destroy the bottom frame if it exists and recreate it with entries
		# of the current node
		catch { destroy $fraTemplate.fraBottom }
	
		# frame for bottom of the box, to hold the entries
		set fraBottom [frame $fraTemplate.fraBottom -borderwidth 0]
		pack $fraBottom -anchor nw -expand true -fill both -padx 3 -pady 3
	
		# scrollable frame in the bottom frame to hold the entries
		set fraEntry [ScrollForm::CreateScrollForm $fraBottom.fraEntry]
	
		# display current list of template entries for this node
		# (don't focus on any entries ... 0 as last parameter)
		foreach entry [Special::GetSpecialObject template entryList $nodeNumber] {
			InsertTemplateEntry $nodeNumber $fraEntry $entry 0
		}

		# now that the entry frame is made, set binding on the attribute combobox
		$_cmbAttribute_ configure \
			-modifycmd "Template::AddTemplateEntry $fraEntry"
			
	}

}


proc Template::AddTemplateEntry { frame {window {}} {item {}} } {
	variable _templateClass_ ; # linked to template class combobox
	variable _templateAttribute_ ; # linked to template attribute combobox

	# ensure both a class and attribute have been selected
	if { $_templateClass_ == {} || $_templateAttribute_ == {} } {
		return
	}
	set nodeNumber [Tree::GetCurrentNode]
	set entry      "${_templateClass_}_${_templateAttribute_}"
	set entryList  [Special::GetSpecialObject template entryList $nodeNumber]

	# check if this entry is already in this template
	if { [lsearch $entryList $entry] != -1 } {
	    Utility::ErrorMessage "[mc {This entry already exists}]\n [mc {in this}] template"
		return
	}
	InsertTemplateEntry $nodeNumber $frame $entry
	Special::SetSpecialObject template entryList $nodeNumber [concat $entryList $entry]
	State::SetState modified 1
}


# delete the currently highlighted template entry (based on the values
# in _templateDelete_)
proc Template::DeleteTemplateEntry { } {
	variable _templateDelete_
	variable _object_
	set nodeNumber [lindex $Template::_templateDelete_ 0]
	set frame      [lindex $Template::_templateDelete_ 1]
	set entry      [lindex $Template::_templateDelete_ 2]
	set nextFrame  [tk_focusPrev $frame] ; # next frame to focus on
	if { $nodeNumber == {} } { return }
	destroy $frame
	set entryList [Special::GetSpecialObject template entryList $nodeNumber]
	set index [lsearch $entryList $entry]
	Special::SetSpecialObject template entryList $nodeNumber \
		[concat [lreplace $entryList $index $index]]
	set _templateDelete_ {}
	unset Special::_object_($nodeNumber.template.$entry)
	State::SetState modified 1
	catch { focus $nextFrame } ; # catch in case this was the last entry-frame left
}


proc Template::InsertTemplateEntry { nodeNumber frame entry {focus 1} } {

	# frame to hold the entry: class name, attribute name, and value
	set f [frame $frame.$entry -highlightcolor blue -highlightthickness 1]
	pack $f -anchor nw -expand 1 -fill x -side bottom
	bind $f <FocusIn> "set Template::_templateDelete_ \[list $nodeNumber $f $entry]"

	# get the class from the entry (e.g. button_borderwidth ... 1st item)
	set class [lindex [split $entry _] 0]
	label $f.labClass -text $class
	grid $f.labClass -column 0 -row 0 -sticky w
	
	# get the attribute from the entry (e.g. button_borderwidth ... 2nd item)
	set attribute [lindex [split $entry _] 1]
	label $f.labAttribute -text $attribute
	grid $f.labAttribute -column 1 -row 0 -sticky w

   	set attributeOption [Attribute::GetAttributeOption $attribute]
	if { $attributeOption == {} || $attributeOption == "command" } {
		# make simple entry and link it to the _object_ data array
		set entItem [entry $f.entValue -width 15 \
			-textvariable Special::_object_($nodeNumber.template.$entry)]
		grid $entItem -row 0 -column 2 -sticky ew -padx 1
	} elseif { $attributeOption == "colour" } {
		# make a simple entry with a colour selection box
		set entItem [entry $f.entValue -width 15 \
			-textvariable Special::_object_($nodeNumber.template.$entry)]
		set fraCol [Utility::CreateColourBox $f.fraCol $entItem \
			"State::SetState modified 1"]
		Utility::SetColourBox $f.fraCol $entItem
		grid $entItem -column 2 -row 0 -sticky ew -padx 1
		grid $fraCol -column 3 -row 0 -sticky w
		
		# 1.0.2:041500
	    bind $entItem <FocusOut> "Utility::SetColourBox $fraCol $entItem 1"
		bind $entItem <Return> "$entItem icursor 0; Utility::SetColourBox $fraCol $entItem 1"		

	} elseif { $attributeOption == "font" } {   	
		# a font entry; add a font box
		set entItem [entry $f.entValue -width 15 \
			-textvariable Special::_object_($nodeNumber.template.$entry)]
		set fraCol [Utility::CreateFontBox $f.fraCol $entItem \
			"State::SetState modified 1"]
		grid $entItem -column 2 -row 0 -sticky ew -padx 1
		grid $fraCol -column 3 -row 0 -sticky w
	} else {
		# make a combobox entry
		set entItem [ComboBox $f.entValue -width 15 \
			-editable 1 -state normal \
			-command Template::ComboboxProc \
			-textvariable Special::_object_($nodeNumber.template.$entry)]
		$entItem configure -values $attributeOption
		grid $entItem -row 0 -column 2 -sticky ew -padx 1

	}
	bind $entItem <KeyPress> {State::CheckModifiedState %K %A}

#	focus -force $f.entValue
	if { $focus } {
		focus -force $f.entValue
	}

	# since stacking order is opposite of display order (inserted at
	# top), 'reverse' bindings on tab to get the desired effect
	# (note: still can't shift-tab to entry preceding combobox)
	bind $f.entValue <Tab> "focus \[tk_focusPrev $f.entValue]; break"
	bind $f.entValue <Shift-Tab> "focus \[tk_focusNext $f.entValue]; break"
	bind $f.entValue <Delete> "Template::DeleteTemplateEntry; break"

	# set bindings on labels to focus on entry and highlight entry frame
	bind $f.labClass <ButtonPress-1> "focus -force $f.entValue"
	bind $f.labAttribute <ButtonPress-1> "focus -force $f.entValue"
	bind $f.labClass <ButtonPress-3> \
		"focus -force $f.entValue
		 Menu::PostPopupMenu $Template::_menuTemplate_ %X %Y"
	bind $f.labAttribute <ButtonPress-3> \
		"focus -force $f.entValue
		 Menu::PostPopupMenu $Template::_menuTemplate_ %X %Y"
	bind $f.entValue <ButtonPress-3> \
		"focus -force $f.entValue
		 Menu::PostPopupMenu $Template::_menuTemplate_ %X %Y; break"
		 # need break so that the cut-copy-paste menu isn't posted too

	grid columnconfigure $f 2 -weight 1
	grid columnconfigure $f 0 -minsize 80
	grid columnconfigure $f 1 -minsize 120

}


# procedure that is called when a combobox entry is modified; note that the command
# specified for the combobox is called with the args 'window' and 'item', though
# they're not required here
proc Template::ComboboxProc { window item } {
	State::SetState modified 1
}


# updates the list of the template attribute combobox; called
# when user changes selection of class widget on the template form
proc Template::UpdateAttributeCombobox { cmbClass cmbAttribute {window {}} {item {}} } {

    # clear and refill the listbox of the attribute combobox
    $cmbAttribute configure -values {}
    set widgetClass [$cmbClass get]
    set attributeList [Attribute::GetAttributeList $widgetClass]
    $cmbAttribute configure -values $attributeList
}


# returns 1 if an entry exists for 'widgetClass' and 'attribute' of the
# template pointed to by 'templateVar', else returns 0
proc Template::EntryExists { templateVar widgetClass attribute } {
    upvar $templateVar template
    if { [info exists template(${widgetClass}_${attribute})] } {
    	return 1
    } else {
    	return 0
	}
}

# sets to 'value' the 'widgetClass' and 'attribute' of 'templateVar'
proc Template::SetEntry { templateVar widgetClass attribute value } {
    upvar $templateVar template
	set template(${widgetClass}_${attribute}) $value
}

# returns value of 'widgetClass' and 'attribute' of 'templateVar'
proc Template::GetEntry { templateVar widgetClass attribute } {
    upvar $templateVar template
	return $template(${widgetClass}_${attribute})
}

