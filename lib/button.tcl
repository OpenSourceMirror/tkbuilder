# -------------------------------------------------------------
# button.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
# -------------------------------------------------------------



namespace eval Button {

    namespace export MakeButtonSet

}



################################################################
#
# MakeButtonSet
#
# Description: Makes a set of buttons and packs them into the
#		frame passed to the function.  Automatically adjusts
#		button width based on parameters.
#
# Parameters:
#   frame	- frame in which to pack the buttons
#   side	- orientation of buttons; "VERTICAL" for
#		  a vertical stack, "HORIZONTAL" for a horizontal row
#   default	- name of the button which is to be the default;
#		  pass a null string {} if there is no default
#   args	- for each button, three words specifying name,
#		  text to display, and command to execute
#
# Returns:	the name of the default button if one was specified,
#		null otherwise
#
# Notes: Code for binding of the default button has to be
#		 specified in the caller after this procedure call.
#		 If name is the constant string "SPACE", then a
#		 a space is inserted between buttons at that point.
#
# Usage:
#
#   MakeButtonSet .frame1 left ok ok "Okay" OkayProcedure \
    #		   cancel "Cancel" CancelProcedure
#
#===============================================================
proc ::Button::MakeButtonSet { frame orientation default args } {

    set i -1     ; # track position in argument list

    set maxTextLen 0
    set buttonDefault {}
    set buttonList {}

    if { $orientation == "HORIZONTAL" } {
	set side left
    } else {
	set side top
    }

    # loop until there are no more names
    while { [set name [lindex $args [incr i]]] != {} } {

	# check if current name actually indicates a space between
	# adjacent buttons; pack a small frame of space if so
	if { $name == "SPACE" } {
	    pack [frame $frame.$i -width 2m -height 2m \
		      -relief flat] -side $side
	    continue
	} 

	# get the text and command for the current button
	set text    [lindex $args [incr i]]
	set command [lindex $args [incr i]]

	# determine length of longest button label
	if { [string length $text] > $maxTextLen } {
	    set maxTextLen [string length $text]
	}

	button $frame.$name -text $text -command $command

	if { $name == $default } {
	    $frame.$name configure -default active
	}
	
	pack $frame.$name -side $side -fill x -padx 3 -pady 3
	lappend buttonList $frame.$name

    }

    # get default button width using first button in the button list
    set defaultButWidth [option get [lindex $buttonList 0] width Button]
    
    # make all buttons the same width              
    foreach button $buttonList {
	$button configure -width $maxTextLen
    }

    return $buttonDefault

}

