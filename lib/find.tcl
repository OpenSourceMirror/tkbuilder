# -------------------------------------------------------------
# find.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------

namespace eval Find {
    variable _window_
    variable _waitVariable_
    variable _cmbFindWhat_ ; # combobox to enter and display search strings
    variable _maxListSize_ 10; # maximum number of search strings remembered
    
    variable _searchString_ {} ; # string to search for
    variable _searchStringList_ {} ; # list of strings that have been found
    variable _startSearchAt_ project
    # where search should begin: one of 'project' or 'branch'
    
    variable _matchCase_ 0 ; # 1 to match case, 0 otherwise
    variable _matchWholeWord_ 0 ; # 1 to match whole word only, 0 otherwise

    variable _matchNodeList_ {} ; # list of node numbers of nodes that have match
    variable _matchNodeIndex_ 0 ; # index within matchNodeList of current goto

    variable _labSummary_ {}; # summary (count) of search results
    variable _texResults_ ; # textbox listing lines that match
}

proc Find::OpenFindDialog { } {
    variable _window_
    variable _waitVariable_
    variable _searchString_
    variable _cmbFindWhat_
    variable _maxListSize_
    variable _texResults_
    variable _butGotoMatchNode_

    if { ![winfo exists ._winFind_] } {
	
    	set _window_ [toplevel ._winFind_]
	wm withdraw $_window_
    	wm title $_window_ "[mc {Find}]"
    	wm iconname $_window_ "[mc {Find}]"
    	wm protocol $_window_ WM_DELETE_WINDOW Find::CloseFindDialog
    	bind $_window_ <Key-Escape> Find::CloseFindDialog
    	
    	set fraTP [frame $_window_.fraTopPane] ; # top pane frame
    	pack $fraTP -anchor nw
    	set fraRP [frame $fraTP.fraRightPane] ; # top right pane (for buttons)
    	pack $fraRP -side right -anchor nw -padx 2
    	set fraLP [frame $fraTP.fraLeftPane] ; # top left pane
    	pack $fraLP -side left -anchor nw -padx 2 -pady 3

	# buttons
	set f [Frame::MakeListFrame $fraRP.fraButtons]
	pack $f -anchor nw -padx 3
	Frame::SetListFrameItemArgs $f -height 22 -width 60
	set c [button $f.butFind -text [mc {Find}] -width 20 -default active -command Find::BeginSearch \
		   -underline 0]
	Frame::PackInListFrame $f $c -pady 3
	set c [button $f.butPrev -text "[mc {Goto Prev}]" -width 20 -command "Find::GotoMatchNode prev" \
		   -underline 5 -state disabled]
	set _butGotoMatchNode_(prev) $c
	Frame::PackInListFrame $f $c -pady 3
	set c [button $f.butNext -text "[mc {Goto Next}]" -width 20 -command "Find::GotoMatchNode next" \
		   -underline 5 -state disabled]
	set _butGotoMatchNode_(next) $c
	Frame::PackInListFrame $f $c -pady 3
	set c [button $f.butCancel -text "[mc {Cancel}]" -width 20 -command Find::CloseFindDialog]
	Frame::PackInListFrame $f $c -pady 3

	bind $_window_ <Alt-Key-f> { Find::BeginSearch }
	bind $_window_ <Alt-Key-p> { Find::GotoMatchNode prev }
	bind $_window_ <Alt-Key-n> { Find::GotoMatchNode next }

    	# frame and widgets for entering the search string
    	set f [frame $fraLP.fraFindWhat]
	pack $f -anchor nw
	
    	grid [label $f.labFindWhat -text "[mc {Find What}]:"] -row 0 -column 0
	grid [ComboBox $f.cmbFindWhat -width 30 -editable 1 -height 0 \
		  -textvariable Find::_searchString_] -row 0 -column 1 -sticky w
	set _cmbFindWhat_ $f.cmbFindWhat
	bind $_cmbFindWhat_ <Key-Return> Find::BeginSearch
	
	# frame and widgets to choose 'project' or 'branch'
	set f [frame $fraLP.fraSearch]
	pack $f -anchor nw
	grid [label $f.labSearch -text "[mc {Search}]:" -pady 0] -row 0 -column 0 -sticky w
	grid [radiobutton $f.radProject -text [mc {project}] -variable Find::_startSearchAt_ \
		  -value project -pady 0] -row 0 -column 1
	grid [radiobutton $f.radBranch -text [mc {branch}] -variable Find::_startSearchAt_ \
		  -value branch -pady 0] -row 0 -column 2
	grid columnconfigure $f 3 -weight 1

	# check boxes
	set f [Frame::MakeListFrame $fraLP.fraGeneral]
	pack $f -side top -fill x -anchor nw
	Frame::SetListFrameItemArgs $f -height 20
	set c [checkbutton $f.chkMatchCase -text "[mc {Match Case}]" -variable Find::_matchCase_ \
		   -underline 6]
	Frame::PackInListFrame $f $c
	set c [checkbutton $f.chkMatchWholeWord -text "[mc {Match Whole Word Only}]" \
		   -underline 6 -variable Find::_matchWholeWord_]
	Frame::PackInListFrame $f $c

	bind $_window_ <Alt-Key-c> { Utility::ToggleValue Find::_matchCase_ }
	bind $_window_ <Alt-Key-w> { Utility::ToggleValue Find::_matchWholeWord_ }

	set f [frame $_window_.fraResults]
	pack $f -side top -fill x -anchor nw
	pack [label $f.labResults -text "[mc {Results}]: "] -side left
	pack [label $f.labSummary -textvariable Find::_labSummary_] -side left

	# textbox for results of search
	set _texResults_ [Text::CreateTextbox $_window_.texResults \
			      -width 35 -height 4 -wrap none -cursor arrow -state disabled]
	pack [Text::GetTextboxFrame $_texResults_] -pady 0 -padx 0 -fill both -expand 1

    }
    wm deiconify $_window_
    wm transient $_window_ .
    focus $_cmbFindWhat_
    raise $_window_
}

proc Find::CloseFindDialog { } {
    variable _window_
    wm withdraw $_window_
}

proc Find::BeginSearch { } {
    variable _searchString_
    variable _startSearchAt_
    variable _labSummary_
    variable _texResults_
    variable _matches_ 0
    variable _matchNodeList_ {}
    variable _matchNodeForm_ {}
    variable _matchNodeIndex_ 0
    variable _butGotoMatchNode_
    variable _window_
    set searchString $_searchString_
    if { $searchString == {} } {
	bell
	set _labSummary_ "[mc {No search string specified}]"
	return
    }
    
    $_texResults_ configure -state normal
    Text::ClearText $_texResults_
    
    if { $_startSearchAt_ == "project" } {
	SearchBranch 0 $searchString
    }
    if { $_startSearchAt_ == "branch" } {
	SearchBranch [Tree::GetCurrentNode] $searchString
    }
    
    switch $_matches_ {
	0 { set result "[mc {No matches found}]" }
	1 { set result "1 [mc {match found}]" }
	default { set result "$_matches_ [mc {matches found}]" }
    }
    
    if { $_matches_ > 0 } {
	AddToSearchList $searchString
	# goto first match
	Build::GetWidgetNumber [lindex $_matchNodeList_ 0]
	WidgetForms::RaiseForm [lindex $_matchNodeForm_ 0]
    }
    if { $_matches_ > 1 } {
	$_butGotoMatchNode_(prev) configure -state normal
	$_butGotoMatchNode_(next) configure -state normal
    } else {
	$_butGotoMatchNode_(prev) configure -state disabled
	$_butGotoMatchNode_(next) configure -state disabled
    }

    set _labSummary_ $result
    $_texResults_ configure -state disabled
    focus $_window_

}


proc Find::SearchBranch { nodeNumber searchString} {
    variable _matches_
    SearchNode $nodeNumber $searchString
    foreach child [Tree::GetNodeItem childList $nodeNumber] {
	SearchBranch $child $searchString
    }
}


proc Find::SearchNode { nodeNumber searchString } {
    variable _matchCase_
    variable _matchWholeWord_
    variable _matchNodeList_
    variable _matchNodeForm_

    set name [Tree::GetNodeItem name $nodeNumber]
    set widgetType [Tree::GetNodeItem widgetType $nodeNumber]
    if { [Special::NodeIsSpecialObject $nodeNumber] } {

	# check special attributes
	set matchList \
	    [Special::FindString $nodeNumber $searchString $_matchCase_ $_matchWholeWord_]
	foreach match $matchList {
	    lappend _matchNodeList_ $nodeNumber
	    lappend _matchNodeForm_ special
	    MakeMatchTextLine "$name <$widgetType> - $match" $nodeNumber layout
	}

    } else {
	
	# check widget attributes
	set matchList \
	    [Attribute::FindString $nodeNumber $searchString $_matchCase_ $_matchWholeWord_]
	foreach match $matchList {
	    lappend _matchNodeList_ $nodeNumber
	    lappend _matchNodeForm_ attributes
	    MakeMatchTextLine "$name <$widgetType> [mc {Attributes}] - $match" $nodeNumber attributes
	}
	
	# check layout options
	set matchList \
	    [Layout::FindString $nodeNumber $searchString $_matchCase_ $_matchWholeWord_]
	foreach match $matchList {
	    lappend _matchNodeList_ $nodeNumber
	    lappend _matchNodeForm_ layout
	    MakeMatchTextLine "$name <$widgetType> [mc {Layout}] - $match" $nodeNumber layout
	}

	# check binding commands
	set matchList \
	    [Binding::FindString $nodeNumber $searchString $_matchCase_ $_matchWholeWord_]
	foreach match $matchList {
	    lappend _matchNodeList_ $nodeNumber
	    lappend _matchNodeForm_ bindings
	    MakeMatchTextLine "$name <$widgetType> [mc {Bindings}] - $match" $nodeNumber bindings
	}

	# check general parameters
	set matchList \
	    [General::FindString $nodeNumber $searchString $_matchCase_ $_matchWholeWord_]
	foreach match $matchList {
	    lappend _matchNodeList_ $nodeNumber
	    lappend _matchNodeForm_ general
	    MakeMatchTextLine "$name <$widgetType> [mc {General}] - $match" $nodeNumber general
	}

    }
}


# adds a text line to the match results text box, with the text 'text'; the
# match is for node 'node' on the form 'form'; form is one of attributes, layout, bindings
# general or special; this proc also tallies the number of matches in _matches_, which
# is used to generate unique tag names for each line
proc Find::MakeMatchTextLine { text node form } {
    variable _texResults_
    variable _matches_
    set t $_texResults_
    set i $_matches_ ; # number of matches to date
    $t insert end $text t$i
    $t insert end "\n"
    $t tag bind t$i <Enter> "$t tag configure t$i -background darkblue -foreground white"
    $t tag bind t$i <Leave> "$t tag configure t$i -background white -foreground black"
    $t tag bind t$i <ButtonPress-1> "
		Build::GetWidgetNumber $node
		WidgetForms::RaiseForm $form
		Find::UpdateMatchNodeIndex $_matches_"
    incr _matches_
}


proc Find::UpdateMatchNodeIndex { node } {
    variable _matchNodeIndex_
    set _matchNodeIndex_ $node
}


# adds 'searchString' to the list of strings that have been found, for inclusion
# into the drop-down listbox
proc Find::AddToSearchList { searchString } {
    variable _searchStringList_
    variable _maxListSize_
    variable _cmbFindWhat_
    # put search string at start of list
    List::RemoveItem _searchStringList_ $searchString
    set _searchStringList_ [linsert $_searchStringList_ 0 $searchString]
    # limit list to maximum size
    set _searchStringList_ [lrange $_searchStringList_ 0 [expr $_maxListSize_-1]]
    # clear and refill find combobox
    $_cmbFindWhat_ configure -values {}
    $_cmbFindWhat_ configure -values $_searchStringList_
}


# searches the string 'value' for the string 'searchString';
# 'value' may contain 2 or more lines; returns a list
# of all lines that match, or null if there are no matches
proc Find::FindMatch { value searchString matchCase matchWholeWord } {
    set matchValue {}
    set valueOriginalCase $value
    set lineList [split $value \n] ; # make list of the lines
    if { !$matchCase } {
	set value [string toupper $value]
	set searchString [string toupper $searchString]
    }
    # if there is only one line in 'value', just check once
    if { [llength $lineList] == 1 } {
	if { $matchWholeWord } {
	    if { [lsearch [split $value] $searchString] != -1 } {
		set matchValue [list '$valueOriginalCase']
	    }
	} else {
	    if { [string first $searchString $value] != -1 } {
		set matchValue [list '$valueOriginalCase']
	    }
	}
    } else {
	# if there are 2 or more lines in 'value', check each line
	set lineNumber 0 ; # current line number
	foreach line $lineList {
	    incr lineNumber
	    set lineOriginalCase $line
	    if { !$matchCase } {
		set line [string toupper $line]
	    }
	    if { $matchWholeWord } {
		if { [lsearch [split $line] $searchString] != -1 } {
		    lappend matchValue "($lineNumber) '$lineOriginalCase'"
		}
	    } else {
		if { [string first $searchString $line] != -1 } {
		    lappend matchValue "($lineNumber) '$lineOriginalCase'"
		}
	    }
	}
    }
    return $matchValue
}


proc Find::GotoMatchNode { direction } {
    variable _matchNodeIndex_
    variable _matchNodeList_
    variable _matchNodeForm_
    variable _butGotoMatchNode_
    variable _window_
    set numMatches [llength $_matchNodeList_]
    if { $numMatches == 0 } {
	return
    }
    if { $direction == "prev" } {
	if { $_matchNodeIndex_ > 0 } {
	    Build::GetWidgetNumber [lindex $_matchNodeList_ [incr _matchNodeIndex_ -1]]
	    WidgetForms::RaiseForm [lindex $_matchNodeForm_ $_matchNodeIndex_]
	    focus $_window_
	} else {
	    bell
	}
    }
    if { $direction == "next" } {
	if { $_matchNodeIndex_ < [expr $numMatches - 1] } {
	    Build::GetWidgetNumber [lindex $_matchNodeList_ [incr _matchNodeIndex_]]
	    WidgetForms::RaiseForm [lindex $_matchNodeForm_ $_matchNodeIndex_]
	    focus $_window_
	} else {
	    bell
	}
    }
}

