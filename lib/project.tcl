# -------------------------------------------------------------
# project.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------



namespace eval Project {
    variable _project_ ; # array of state variables for the project
    
    # - tclFileList: list of tcl files that make up the project
    
    # - tclFileMain: the tcl file that is the main tcl file of the project
    #	(receives the resource and package text); must be one of tclFileList
    # - projectFile: the name of the file (full path) for the project
    # - projectName: the tail of the project file name (shown in the tree)
    # - rootNodeExists: 1 if the root node exists in the project, 0 otherwise
    #	(root node represents the main toplevel widget of the application)

}


proc Project::InitializeProject { } {
    SetState tclFileList <none>
    SetState mainTclFile <none>
    SetState rootNodeExists 0
    SetState rootNodeNumber -1
    SetState projectFile {}
    
    SetState Startup {}
    SetState Auto_path {}
    SetState Resources {}
    SetState Packages {}
    SetState Namespaces {}
    SetState Widgets {} ; # widgets to register (e.g. notebook; combobox, entry)
    SetState registeredWidgets {} ; # proper list of checked, registered widgets
}


proc Project::SetState { item value } {
    variable _project_
    set _project_($item) $value
}


proc Project::GetState { item } {
    variable _project_
    if { ![info exists _project_($item)] } {
	error "[mc {Unrecognized project state item}]: $item"
    }
    return $_project_($item)
}


proc Project::GetProjectArray { } {
    variable _project_
    return [array get _project_]
}


proc Project::SetProjectArray { array } {
    variable _project_
    array set _project_ $array
}


# sets the name of the project node to tail of 'filename', for display in tree
# (project name is always tail of filename used to save project)
proc Project::SetProjectFileName { filename } {
    Project::SetState projectFile $filename
    Tree::SetNodeItem name [Project::GetProjectNodeNumber] [file tail $filename]
}


# returns the full file name of the current project
proc Project::GetProjectFileName { } {
    return [Project::GetState projectFile]
}


# returns 1 if 'nodeNumber' is project node, 0 otherwise
proc Project::NodeIsProject { nodeNumber } {
    return [expr $nodeNumber == 0]
}


# returns the number of the node used for the project object
proc Project::GetProjectNodeNumber { } {
    return 0
}


# adds main window to project; called to automatically add main window to
# new projects
proc Project::AddMainWindow { } {
    set newNode [Tree::InsertNode 0 [Project::GetRootName] CHILD toplevel 0]
    Properties::InitializeWidgetProperties $newNode
    Project::SetState rootNodeNumber $newNode
    Project::SetState rootNodeExists 1
}


# returns 1 if 'nodeNumber' is the main/root toplevel, 0 othewise
proc Project::NodeIsRoot { nodeNumber } {
    return [expr $nodeNumber == [GetState rootNodeNumber]]
}


# returns the name used for root node (presently "Main Window")
proc Project::GetRootName { } {
    return "main window"
}


# returns 1 if 'nodeNumber' is a tcl file object, 0 otherwise
proc Project::NodeIsTclFile { nodeNumber } {
    if { [Tree::GetNodeItem widgetType $nodeNumber] == "file" } {
	return 1
    } else {
	return 0
    }
}


# resets variables used to store info about main/root window
proc Project::FreeRootNode { } {
    Project::SetState rootNodeNumber -1
    Project::SetState rootNodeExists 0
}


# returns the node number used for root
proc Project::GetRootNodeNumber { } {
    return [Project::GetState rootNodeNumber]
}


# returns 1 if the project has a root node, 0 otherwise
proc Project::RootNodeExists { } {
    return [Project::GetState rootNodeExists]
}


# checks if the branch starting at 'nodeNumber' contains the main/root toplevel
# a recursive function; returns 1 if so, 0 otherwise
proc Project::BranchContainsRoot { nodeNumber } {
    if { $nodeNumber == [GetRootNodeNumber] } {
	return 1no longer exists in project
    } else {
	foreach child [Tree::GetNodeItem childList $nodeNumber] {
	    if { [Project::BranchContainsRoot $child] } {
		return 1
	    }
	}
	return 0
    }
}


# saves the project's tcl files; if 'check' is true, first checks that
# the project has atleast one file object
proc Project::SaveProjectTclFiles { {check 1} } {
    
    if { $check } {
	# check if the file list has only '<none>' in it (i.e. no other files)
	if { [Project::GetState tclFileList] == "<none>" } {
	    Utility::ErrorMessage "[mc {Project has no tcl files to save to}]"
	    return
	}
	# check that a main tcl file was specified
	if { [Project::GetState mainTclFile] == "<none>" } {
	    if { [Option::GetOption warnNoMainTclFile] } {
		set response [Utility::ConfirmMessage \
				  "[mc {No main tcl file selected for project}].\n [mc {Continue anyway}]?"]
		if { $response == "no" } {
		    return
		}
	    }
	}
    }
    
    # make sure the main tcl file still exists in the project
    if { [Project::GetState mainTclFile] != "<none>" } {
	if { ![List::ItemExists [GetState tclFileList] [GetState mainTclFile]] } {
	    Utility::ErrorMessage "[mc {The selected main tcl file}]\n [mc {no longer exists in project}]"
	    return
	}
    }
    
    Build::BuildBranch SAVE_PROJECT_TCL

}
