# -------------------------------------------------------------
# menu.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------


# N.B. ALL postcommands are fired when ANY menu is opened
# from the menu bar

# N.B. There were major problems related to 1) using the Widget Edit menu as both
# the drop-down and context menu; 2) having a postcommand in the edit menu; and 3)
# updating the More Widgets sub-menu.  The latter behaved inconsistantly (crash) after
# an update when called using the context menu, but only if the edit menu
# had a post command defined ... not sure why.  The solution was to clone the edit menu
# and call the clone as the context menu ... works, but again, not sure why.


namespace eval Menu {

    variable _menuWidgetEdit_
    variable _menuWidgetEditeClone_

    variable _menuWidgetList_ ; # array of menus, indexed on PARENT, SIBLING, CHILD
    variable _relationList_ { PARENT SIBLING CHILD }
    
    variable _menuCanvasObjects_
    variable _menuMenuEntry_
    variable _menuRecentFiles_
    variable _menuFile_
    variable _menuProperties_
    variable _menuWidgetMove_
    variable _menuBuild_
    
    namespace export InitializeMenus
    namespace export GetWidgetEditMenu
    namespace export GetPropertiesMenu
    namespace export GetWidgetMoveMenu UpdateWidgetMoveMenu
    namespace export PostPopupMenu

}


# creates and initializes some menus
proc Menu::InitializeMenus { } {
    CreateMainMenu
    CreateWidgetMoveMenu
}


# creates the main menu on the main window
proc Menu::CreateMainMenu { } {

    variable _menuBar_

    # create the main menu bar
    set mbar .menuBar
    set _menuBar_ $mbar
    menu $mbar
    . configure -menu $mbar

    # create the main menus that hang off the menubar
    set menFile       [CreateFileMenu $mbar]
    set menView       [CreateViewMenu $mbar]
    set menGeometry   [CreateGeometryMenu $mbar]
    set menWidgetEdit [CreateWidgetEditMenu $mbar]
    set menProperties [CreatePropertiesMenu $mbar]
    set menBuild      [CreateBuildMenu $mbar]
    set menTools      [CreateToolsMenu $mbar]
    set menHelp       [CreateHelpMenu $mbar]

    # add the main menu entries into the menubar
    $mbar add cascade -label [mc {File}] -menu $menFile -underline 0
    $mbar add cascade -label [mc {View}] -menu $menView -underline 0
    $mbar add cascade -label [mc {Geometry}] -menu $menGeometry -underline 0
    $mbar add cascade -label [mc {Widget}] -menu $menWidgetEdit -underline 0
    $mbar add cascade -label [mc {Properties}] -menu $menProperties -underline 0
    $mbar add cascade -label [mc {Build}] -menu $menBuild -underline 0
    $mbar add cascade -label [mc {Tools}] -menu $menTools -underline 0
    $mbar add cascade -label [mc {Help}] -menu $menHelp -underline 0

}


proc Menu::CreateGeometryMenu { parent } {
    set m [menu $parent.menuGeometry -tearoff 0]
    $m add radio -label " [mc {Pack}]" \
	-variable Option::_option_(geometryManager) \
	-value pack -command Toolbar::UpdateGeometryManager
    $m add radio -label " [mc {Grid}]" \
	-variable Option::_option_(geometryManager) \
	-value grid -command Toolbar::UpdateGeometryManager
    $m add radio -label " [mc {Place}]" \
	-variable Option::_option_(geometryManager) \
	-value place -command Toolbar::UpdateGeometryManager
    return $m
}

proc Menu::CreateViewMenu { parent } {
    set m [menu $parent.menuView -tearoff 0]
    $m add check -label " [mc {Widget Icon}]" \
	-variable Option::_option_(treeShowWidgetIcon) \
	-command WidgetTree::RedrawWidgetTree
    $m add check -label " [mc {Geometry Manager}]" \
	-variable Option::_option_(treeShowGeometry) \
	-command WidgetTree::RedrawWidgetTree
    $m add check -label " [mc {Widget Class}]" \
	-variable Option::_option_(treeShowWidgetClass) \
	-command WidgetTree::RedrawWidgetTree
    $m add check -label " [mc {Set Variable}]" \
	-variable Option::_option_(treeShowSetVariable) \
	-command WidgetTree::RedrawWidgetTree
    $m add separator	
    $m add check -label " [mc {Main Toolbar}]" -variable Option::_option_(mainToolbarShow) \
	-command Toolbar::ShowMainToolbar
    $m add check -label " [mc {Widget Toolbar}]" -variable Option::_option_(widgetToolbarShow) \
	-command Toolbar::ShowToolbar
    $m add check -label " [mc {Tool tips}]" -variable Option::_option_(toolTipsShow)
    $m add check -label " [mc {Status Bar}]" -variable Option::_option_(statusBarShow) \
	-command Status::ShowStatusBar

    return $m
}



proc Menu::CreateHelpMenu { parent } {
    set m [menu $parent.menuHelp -tearoff 0]
    $m add command -label "[mc {License}]..." -underline 0 \
	-command Help::OpenLicenseWindow
    $m add separator  
    if {([info exists ::env(DEBUG)]) && ($::env(DEBUG) > 0)} {
	if {![catch {package require twDebugInspector} msg]} {
	    $m add command -label "[mc {Open}] inspector" -underline 0 \
		-command [list twDebug::Inspector .twDebugInspector]
	}
	if { $::tcl_platform(platform) == "windows" } {
	    $m add command -label "[mc {Open}] console" -underline 0 \
		-command [list console show]
	    set manualPath [file join $::dir manual manual.htm]
	    if {[file readable $manualPath]} {
		$m add command -label "[mc {Manual}]" -underline 0 \
		    -command [list exec "c:/Program Files/Internet Explorer/IEXPLORE.EXE" "$manualPath"]
	    }
	}
	$m add separator  
    }
    $m add command -label "[mc {About}] tkBuilder..." -underline 0 \
	-command Help::OpenAboutWindow
    return $m
}


proc Menu::CreateToolsMenu { parent } {
    set m [menu $parent.menuTools -tearoff 0]
    $m add command -label "[mc {Find}]..." -underline 0 \
	-command Find::OpenFindDialog
    $m add command -label "[mc {Options}]..." -underline 0 \
	-command Option::OpenOptionDialog
    return $m
}   	


# creates the File menu; also creates the child Recent Files menu
proc Menu::CreateFileMenu { parent } {
    variable _menuFile_
    set m [menu $parent.menuFile -tearoff 0 \
	       -postcommand "Menu::UpdateFileMenu; Menu::UpdateRecentFilesMenu"]
    set _menuFile_ $m

    CreateRecentFilesMenu $m

    # add sub-items to each main menu item
    $m add command -label "[mc {New Project}]" -underline 0 -command File::New \
	-accelerator "Ctrl+N"
    $m add command -label "[mc {Open Project}]..." -underline 0 -command File::OpenFile \
	-accelerator "Ctrl+O"
    $m add separator  
    $m add command -label "[mc {Save Project}]" -underline 0 -command File::SaveFile \
	-accelerator "Ctrl+S"
    $m add command -label "[mc {Save Project As}]..." -underline 13 -command File::SaveFileAs
    $m add separator

    $m add command -label "[mc {Save Tcl Files}]" -underline 5 -command "Project::SaveProjectTclFiles" \
	-accelerator "Ctrl+T"
    $m add separator

    $m add cascade -label "[mc {Recent Projects}]" -underline 7 -menu [GetRecentFilesMenu]
    $m add separator
    $m add command -label [mc {Exit}] -underline 1 -command Main::MainExitPrompt

    # bindings for accelerators
    bind . <Control-Key-n> File::New
    bind . <Control-Key-o> File::OpenFile
    bind . <Control-Key-s> File::SaveFile
    bind . <Control-Key-t> Project::SaveProjectTclFiles

    return $m
    
}
proc Menu::UpdateFileMenu { } {
    variable _menuFile_
    $_menuFile_ entryconfigure "[mc {Recent Projects}]" -state normal
    if { [State::GetState recentTkbFiles] == {} } {
	$_menuFile_ entryconfigure "[mc {Recent Projects}]" -state disabled
    }
}


# creates the widget edit menu, which contains the operations that can be
# carried out on widget items in the widget tree diagram
proc Menu::CreateWidgetEditMenu {  parent } {
    
    variable _menuWidgetEdit_
    variable _menuWidgetList_
    variable _relationList_
    variable _menuCanvasObjects_
    variable _menuWidgetEditClone_

    # postcommand Menu::UpdateWidgetEditMenu causes error here when called
    # from tk_popup
    set m [menu $parent.menuWidgetEdit -tearoff 0 -postcommand Menu::UpdateWidgetEditMenu]
    set _menuWidgetEdit_ $m

    # create the menus that are children of this one
    CreateWidgetListMenu $m
    CreateCanvasObjectsMenu $m
    CreateMenuEntryMenu $m

    $m add cascade -label "[mc {Insert Parent}]"  -menu [GetWidgetListMenu PARENT]
    $m add cascade -label "[mc {Insert Sibling}]" -menu [GetWidgetListMenu SIBLING]
    $m add cascade -label "[mc {Insert Child}]" -menu [GetWidgetListMenu CHILD]
    $m add separator
    $m add command -label "[mc {Duplicate Widget}]" -accelerator "Ctrl+D" \
	-command ::WidgetTree::DuplicateWidget
    $m add command -label "[mc {Duplicate Branch}]" \
	-command ::WidgetTree::DuplicateBranch
    $m add separator
    $m add command -label "[mc {Copy To Siblings}]" \
	-command ::WidgetTree::CopyWidgetToSiblings
    $m add command -label "[mc {Copy To Descendants}]" \
	-command ::WidgetTree::CopyWidgetToDescendants
    $m add separator
    $m add command -label "[mc {Delete Widget}]" -accelerator "Del" \
	-command ::WidgetTree::DeleteWidgetNode
    $m add command -label "[mc {Delete Branch}]" -accelerator "Ctrl+Del" \
	-command ::WidgetTree::DeleteWidgetBranch
    $m add separator
    $m add command -label "[mc {Rename}]" -underline 0 \
	-command ::WidgetTree::RenameWidget -accelerator "Ctrl+R"

    # now that the widget edit menus are made, cascade them from the canvas object menus
    $_menuCanvasObjects_(CHILD) add separator
    $_menuCanvasObjects_(CHILD) add cascade -label [mc {Widgets}] \
	-menu [GetWidgetListMenu CHILD]
    $_menuCanvasObjects_(SIBLING) add separator
    $_menuCanvasObjects_(SIBLING) add cascade -label [mc {Widgets}] \
	-menu [GetWidgetListMenu SIBLING]

    # make a clone of the menu; the clone is called as the context menu, since using
    # the edit menu as both the drop-down and the context menu causes problems when
    # the More Widgets sub-menu is updated; not sure why this works, but it seems to
    $m clone $m.clone normal
    set _menuWidgetEditClone_ $m.clone

    return $_menuWidgetEdit_	

}
proc Menu::GetWidgetEditMenu { } {
    variable _menuWidgetEdit_        	            
    return $_menuWidgetEdit_
}
proc Menu::GetWidgetEditCloneMenu { } {
    variable _menuWidgetEditClone_
    return $_menuWidgetEditClone_
}


# creates the widget list menus
proc Menu::CreateWidgetListMenu { parent } {

    variable _menuWidgetList_
    variable _relationList_

    # 'command' doesn't work on cascade items (Windows), so need three menus
    # of widget lists, one for parent, sibling and children
    foreach relation $_relationList_ {

	set m [menu $parent.widgetList_$relation  -tearoff 0 \
		   -postcommand "Menu::UpdateWidgetListMenu $relation"]
    	set _menuWidgetList_($relation) $m

    	# loop once for each widget class and add it to the menu
	foreach widget [Widget::GetWidgetList] {                  
	    $m add command -label $widget \
		-command "WidgetTree::AddWidget $widget $relation"
	}

	# create the More Widgets and Special Objects menu, which are
	# children of this one
	CreateSpecialObjectsMenu $m $relation
	CreateMoreWidgetsMenu $m $relation
	
	# add the sub-menu of More Widgets and Special Objects
	$_menuWidgetList_($relation) add separator
	$_menuWidgetList_($relation) add cascade -label [mc {More}] \
	    -menu [Menu::GetMoreWidgetsMenu $relation]
	$_menuWidgetList_($relation) add cascade -label [mc {Special}] \
	    -menu [Menu::GetSpecialObjectsMenu $relation]
	
	# for SIBLING and CHILD menus, add item to add megawidget
	if { $relation != "PARENT" } {
	    $_menuWidgetList_($relation) add command -label "[mc {Megawidget}]..." \
		-command "File::OpenTkwFile $relation"
	}
    }

}
proc Menu::GetWidgetListMenu { relation } {
    variable _menuWidgetList_        	            
    return $_menuWidgetList_($relation)
}
proc Menu::UpdateWidgetListMenu { relation } {
    variable _menuWidgetList_
    if { [Register::GetRegisteredWidgets] == {} } {
	$_menuWidgetList_($relation) entryconfigure "[mc {More}]" -state disabled
    } else {
	$_menuWidgetList_($relation) entryconfigure "[mc {More}]" -state normal
    }
    
    #	$_menuWidgetList_($relation) activate 3 ; # doesn't work
}


# enables/disables certain items of the widget edit menu, depending on current
# status
proc Menu::UpdateWidgetEditMenu { } {

    variable _menuWidgetEdit_
    set m $_menuWidgetEdit_
    
    set nodeNumber  [Tree::GetCurrentNode]		
    set childList   [Tree::GetNodeItem childList $nodeNumber]
    set widgetType  [Tree::GetNodeItem widgetType $nodeNumber]
    set numChildren [llength $childList]
    
    # initially set all entries to normal
    $m entryconfigure "[mc {Insert Parent}]" -state normal
    $m entryconfigure "[mc {Insert Sibling}]" -state normal
    $m entryconfigure "[mc {Insert Child}]" -state normal
    $m entryconfigure "[mc {Duplicate Widget}]" -state normal
    $m entryconfigure "[mc {Duplicate Branch}]" -state normal
    $m entryconfigure "[mc {Copy To Siblings}]" -state normal
    $m entryconfigure "[mc {Copy To Descendants}]" -state normal
    $m entryconfigure "[mc {Delete Widget}]" -state normal
    $m entryconfigure "[mc {Delete Branch}]" -state normal
    $m entryconfigure "[mc {Rename}]" -state normal

    # make adjustments to cascade menus ( 1 and 2)
    # 1. check if widget is canvas or menu
    if { $widgetType == "canvas" } {
	$m entryconfigure "[mc {Insert Child}]" -menu [GetCanvasObjectsMenu CHILD]
    } elseif { $widgetType == "menu" } {
	$m entryconfigure "[mc {Insert Child}]" -menu [GetMenuEntryMenu CHILD]
    } else {
	$m entryconfigure "[mc {Insert Child}]" -menu [GetWidgetListMenu CHILD]
    }

    # 2. check if widget is canvas object or menu entry
    if { [Canvas::WidgetIsCanvasObject $widgetType] } {
	$m entryconfigure "[mc {Insert Sibling}]" -menu [GetCanvasObjectsMenu SIBLING]
    } elseif { [MenuEntry::WidgetIsMenuEntry $widgetType] } {
	$m entryconfigure "[mc {Insert Sibling}]" -menu [GetMenuEntryMenu SIBLING]
    } else {
	$m entryconfigure "[mc {Insert Sibling}]" -menu [GetWidgetListMenu SIBLING]
    }

    # disable entries not available to project node
    if { [Project::NodeIsProject $nodeNumber] } {
	$m entryconfigure "[mc {Insert Parent}]" -state disabled
	$m entryconfigure "[mc {Insert Sibling}]" -state disabled
	$m entryconfigure "[mc {Duplicate Widget}]" -state disabled
	$m entryconfigure "[mc {Duplicate Branch}]" -state disabled
	$m entryconfigure "[mc {Copy To Siblings}]" -state disabled
	$m entryconfigure "[mc {Copy To Descendants}]" -state disabled
	$m entryconfigure "[mc {Delete Widget}]" -state disabled
	$m entryconfigure "[mc {Delete Branch}]" -state disabled
    }

    # can only delete single node if it has 0 or 1 children
    if { $numChildren > 1 } {
	$m entryconfigure "[mc {Delete Widget}]" -state disabled
    }
    
    if { $numChildren == 0 } {
	$m entryconfigure "[mc {Duplicate Branch}]" -state disabled
	$m entryconfigure "[mc {Copy To Descendants}]" -state disabled
    }

    # can only copy to siblings if widget has siblings
    if { [Tree::GetSiblingList $nodeNumber] == {} } {
	$m entryconfigure "[mc {Copy To Siblings}]" -state disabled
    }

    # adjust if widget is canvas object or menu entry
    if { [Canvas::WidgetIsCanvasObject $widgetType] } {
	$m entryconfigure "[mc {Insert Parent}]" -state disabled
	$m entryconfigure "[mc {Insert Child}]" -state disabled
	$m entryconfigure "[mc {Duplicate Branch}]" -state disabled
    }
    if { [MenuEntry::WidgetIsMenuEntry $widgetType] } {
	$m entryconfigure "[mc {Insert Parent}]" -state disabled
	$m entryconfigure "[mc {Insert Child}]" -state disabled
	$m entryconfigure "[mc {Duplicate Branch}]" -state disabled
    }

    if { [Special::NodeIsSpecialObject $nodeNumber] } {
	$m entryconfigure "[mc {Copy To Descendants}]" -state disabled
	$m entryconfigure "[mc {Copy To Siblings}]" -state disabled
	if { $widgetType == "file" } {
	    $m entryconfigure "[mc {Duplicate Widget}]" -state disabled
	    $m entryconfigure "[mc {Duplicate Branch}]" -state disabled
	}
    }

}


proc Menu::CreateCanvasObjectsMenu { parent } {
    variable _menuCanvasObjects_
    foreach relation { SIBLING CHILD } {
	set m [menu $parent.canvasObject_$relation -tearoff 0]
	set _menuCanvasObjects_($relation) $m
	foreach object [Canvas::GetCanvasObjectList] {
	    $m add command -label $object \
		-command "Canvas::AddCanvasObject $object $relation"
	}
    }
}
proc Menu::GetCanvasObjectsMenu { relation } {
    variable _menuCanvasObjects_
    return $_menuCanvasObjects_($relation)
}


proc Menu::CreateMenuEntryMenu { parent } {
    variable _menuMenuEntry_
    foreach relation { SIBLING CHILD } {
	set m [menu $parent.menuEntry_$relation -tearoff 0]
	set _menuMenuEntry_($relation) $m
	foreach entry [MenuEntry::GetMenuEntryList] {
	    $m add command -label $entry \
		-command "MenuEntry::AddMenuEntry $entry $relation"
	}
    }
}
proc Menu::GetMenuEntryMenu { relation } {
    variable _menuMenuEntry_
    return $_menuMenuEntry_($relation)
}


# creates the properties menu
proc Menu::CreatePropertiesMenu { parent } {

    variable _menuProperties_

    # this postcommand appears to be ok
    set m [menu $parent.menuProperties -tearoff 0 \
	       -postcommand Menu::UpdatePropertiesMenu]
    set _menuProperties_ $m
    $m add command -label "[mc {Copy To Siblings}]" \
	-command WidgetForms::CopyAttributesToSiblings
    $m add command -label "[mc {Copy To Descendants}]" \
	-command WidgetForms::CopyAttributesToDescendants

    # make the attribute copy option sub-menu
    set mO [menu $m.menuPropertiesOptions -tearoff 0]
    $mO add radio -label "[mc {to same widget class only}]" \
	-variable Option::_option_(copyAttributesMode) -value SAME_WIDGET_TYPE
    $mO add radio -label "[mc {to all widget classes}]" \
	-variable Option::_option_(copyAttributesMode) -value ALL_WIDGET_TYPES

    #	$mO add separator
    #	$mO add check -label "[mc {copy if geometry manager same (layout)}]" \
	#		-variable Option::_option_(copyLayoutIfSameManager)
    #	$mO add separator
    #	$mO add check -label "[mc {copy geometry manager (layout)}]" \
	#		-variable Option::_option_(copyLayoutManager)
    
    #	$m add separator
    $m add cascade -label "[mc {Copy-To Options}]"  -menu $mO
    
    $m add separator
    $m add command -label "[mc {Clear}]" -command WidgetForms::ClearSelectedProperties

    $m add separator
    $m add check -label "[mc {Show Hidden Attributes}]" \
	-variable Option::_option_(showHiddenAttributes) \
	-command {
	    WidgetForms::UpdateAttributeForm [Tree::GetCurrentNode] 1
	}
    $m add check -label "[mc {Build With Hidden Attributes}]" \
	-variable Option::_option_(buildWithHiddenAttributes) \
	-command {
	    WidgetForms::UpdateAttributeForm [Tree::GetCurrentNode] 1
	}
    $m add command -label "[mc {Add To Hidden List}]" \
	-command {
	    Attribute::AddToHiddenAttributes [WidgetForms::GetSelectedAttributes]
	    WidgetForms::UpdateAttributeForm [Tree::GetCurrentNode] 1
	}
    $m add command -label "[mc {Remove From Hidden List}]" \
	-command {
	    Attribute::RemoveFromHiddenAttributes [WidgetForms::GetSelectedAttributes]
	    WidgetForms::UpdateAttributeForm [Tree::GetCurrentNode] 1
	}

    $m add separator
    $m add command -label "[mc {Insert Event}]" \
	-command ::Binding::InsertSpecifiedEvent
    $m add command -label "[mc {Delete Event}]" \
	-command ::Binding::DeleteSelectedEvents
    
    return $m

}


# updates the state of the attribute copy menu items (based on whether
# current node is root)
proc Menu::UpdatePropertiesMenu { } {
    variable _menuProperties_

    # set all entries to normal by default
    $_menuProperties_ entryconfigure "[mc {Copy To Siblings}]" -state normal
    $_menuProperties_ entryconfigure "[mc {Copy To Descendants}]" -state normal
    $_menuProperties_ entryconfigure "[mc {Clear}]" -state normal
    $_menuProperties_ entryconfigure "[mc {Show Hidden Attributes}]" -state normal
    $_menuProperties_ entryconfigure "[mc {Build With Hidden Attributes}]" -state normal
    $_menuProperties_ entryconfigure "[mc {Add To Hidden List}]" -state normal
    $_menuProperties_ entryconfigure "[mc {Remove From Hidden List}]" -state normal
    $_menuProperties_ entryconfigure "[mc {Insert Event}]" -state normal
    $_menuProperties_ entryconfigure "[mc {Delete Event}]" -state normal

    # if root node, can't copy to siblings
    set currentNode [Tree::GetCurrentNode]
    if { $currentNode == 0 } {
	$_menuProperties_ entryconfigure "[mc {Copy To Siblings}]" -state disabled
    }
    
    # if no children, can't copy to descendants
    if { [Tree::GetNodeItem childList $currentNode] == {} } {
	$_menuProperties_ entryconfigure "[mc {Copy To Descendants}]" -state disabled
    }
    
    # if no siblings, can't copy to siblings
    if { [Tree::GetSiblingList $currentNode] == {} } {
	$_menuProperties_ entryconfigure "[mc {Copy To Siblings}]" -state disabled
    }	
    
    # only the binding widget form allows for insert/delete of events
    if { [WidgetForms::GetCurrentForm] != "BINDING_FORM" } {
	$_menuProperties_ entryconfigure "[mc {Insert Event}]" -state disabled
	$_menuProperties_ entryconfigure "[mc {Delete Event}]" -state disabled
    }
    
    # can only remove hidden widgets if they are shown
    if { ![Option::GetOption showHiddenAttributes] } {
	$_menuProperties_ entryconfigure "[mc {Remove From Hidden List}]" -state disabled
    }	
    
    # only the attribute form allows for managing hidden attributes
    if { [WidgetForms::GetCurrentForm] != "WIDGET_FORM" } {
	$_menuProperties_ entryconfigure "[mc {Show Hidden Attributes}]" -state disabled
	$_menuProperties_ entryconfigure "[mc {Build With Hidden Attributes}]" -state disabled
	$_menuProperties_ entryconfigure "[mc {Add To Hidden List}]" -state disabled
	$_menuProperties_ entryconfigure "[mc {Remove From Hidden List}]" -state disabled
    }
    
    # if no attributes are selected, can't copy, clear, add to hidden, or delete
    if { [WidgetForms::GetSelectedAttributes] == {} } {
	$_menuProperties_ entryconfigure "[mc {Copy To Siblings}]" -state disabled
	$_menuProperties_ entryconfigure "[mc {Copy To Descendants}]" -state disabled
	$_menuProperties_ entryconfigure "[mc {Clear}]" -state disabled
	$_menuProperties_ entryconfigure "[mc {Add To Hidden List}]" -state disabled
	$_menuProperties_ entryconfigure "[mc {Remove From Hidden List}]" -state disabled
	$_menuProperties_ entryconfigure "[mc {Delete Event}]" -state disabled
    }
    
}


# returns name of the attribute copy menu
proc Menu::GetPropertiesMenu { } {
    variable _menuProperties_
    return $_menuProperties_
}


# creates the widget move menu
proc Menu::CreateWidgetMoveMenu { } {
    variable _menuWidgetMove_
    set m [menu .menuWidgetMove -tearoff 0]
    $m add command -label "[mc {Move To Sibling}]" -command "WidgetTree::MoveWidget SIBLING"
    $m add command -label "[mc {Move To Child}]" -command "WidgetTree::MoveWidget CHILD"
    $m add separator
    $m add command -label "[mc {Cancel}]" -command "WidgetTree::MoveWidget CANCEL"
    set _menuWidgetMove_ $m
}
proc Menu::GetWidgetMoveMenu { } {
    variable _menuWidgetMove_
    return $_menuWidgetMove_
}
# updates the state of the widget move menu items, based on the node
# that is being dropped-to
proc Menu::UpdateWidgetMoveMenu { dropToNode } {
    variable _menuWidgetMove_
    if { [Project::NodeIsProject $dropToNode] } {
	$_menuWidgetMove_ entryconfigure "[mc {Move To Sibling}]" -state disabled
    } else {
	$_menuWidgetMove_ entryconfigure "[mc {Move To Sibling}]" -state normal
    }
} 


proc Menu::CreateBuildMenu { parent } {
    variable _menuBuild_

    set m [menu $parent.menuBuild -tearoff 0 -postcommand Menu::UpdateBuildMenu]
    set _menuBuild_ $m
    
    $m add command -label "[mc {Run Project}]" -underline 4 -accelerator F2 \
	-command { Build::BuildBranch RUN_PROJECT }
    $m add command -label "[mc {Run Branch}]" -underline 4 -accelerator F3 \
	-command { Build::BuildBranch RUN_BRANCH }
    $m add command -label "[mc {Run Last}]" -underline 4 -accelerator F4 \
	-command { Build::BuildBranch RUN_LAST }
    $m add command -label "[mc {Stop}]" -underline 0 -accelerator F5 \
	-command { Build::StopApplication }
    $m add separator
    $m add command -label "[mc {View Branch}]" -underline 0 -accelerator F6 \
	-command { Build::BuildBranch VIEW_TCL }
    $m add separator
    $m add command -label "[mc {Save Branch As Tcl}]..." -command { Build::BuildBranch SAVE_AS_TCL }
    $m add separator
    $m add command -label "[mc {Save Branch As Megawidget}]..." -command { File::SaveAsTkwFile }

    bind . <Key-F2> {Build::BuildBranch RUN_PROJECT} ; # run from project node
    bind . <Key-F3> {Build::BuildBranch RUN_BRANCH} ; # run from current node
    bind . <Key-F4> {Build::BuildBranch RUN_LAST} ; # run from last node
    bind . <Key-F5> {Build::StopApplication}  ; # stop running application
    bind . <Key-F6> {Build::BuildBranch VIEW_TCL} ; # view tcl code from current node

    return $m

}
proc Menu::GetBuildMenu { } {
    variable _menuBuild_
    return $_menuBuild_
}
proc Menu::UpdateBuildMenu { } {
    variable _menuBuild_

    set nodeNumber  [Tree::GetCurrentNode]		
    set widgetType  [Tree::GetNodeItem widgetType $nodeNumber]

    $_menuBuild_ entryconfigure "[mc {Run Project}]" -state normal
    $_menuBuild_ entryconfigure "[mc {Run Branch}]" -state normal
    $_menuBuild_ entryconfigure "[mc {Run Last}]" -state normal
    $_menuBuild_ entryconfigure "[mc {Stop}]" -state normal
    $_menuBuild_ entryconfigure "[mc {View Branch}]" -state normal
    $_menuBuild_ entryconfigure "[mc {Save Branch As Tcl}]..." -state normal
    $_menuBuild_ entryconfigure "[mc {Save Branch As Megawidget}]..." -state normal

    if { [Canvas::WidgetIsCanvasObject $widgetType] ||
	 [MenuEntry::WidgetIsMenuEntry $widgetType] } {
	$_menuBuild_ entryconfigure "[mc {Run Branch}]" -state disabled
	$_menuBuild_ entryconfigure "[mc {View Branch}]" -state disabled
	$_menuBuild_ entryconfigure "[mc {Save Branch As Tcl}]..." -state disabled
	$_menuBuild_ entryconfigure "[mc {Save Branch As Megawidget}]..." -state disabled
    }
    
    if { [Build::GetLastBuiltNode] == {} } {
	$_menuBuild_ entryconfigure "[mc {Run Last}]" -state disabled
    }
    
    if { ![Build::GetApplicationIsRunning] } {
	$_menuBuild_ entryconfigure "[mc {Stop}]" -state disabled
    }

}


proc Menu::CreateRecentFilesMenu { parent } {
    variable _menuRecentFiles_
    set m [menu $parent.menuRecentFiles -tearoff 0]
    set _menuRecentFiles_ $m
}
proc Menu::GetRecentFilesMenu { } {
    variable _menuRecentFiles_
    return $_menuRecentFiles_
}
proc Menu::UpdateRecentFilesMenu { } {
    variable _menuRecentFiles_
    $_menuRecentFiles_ delete 0 end
    foreach file [State::GetState recentTkbFiles] {
	$_menuRecentFiles_ add command -label $file -command [list File::OpenFile $file]
    }
}


proc Menu::CreateSpecialObjectsMenu { parent relation } {
    variable _menuSpecialObjects_
    set m [menu $parent.specialObjects_$relation -tearoff 0]
    set _menuSpecialObjects_($relation) $m
    foreach object [Special::GetSpecialObjectList] {
	$m add command -label $object \
	    -command "Special::AddSpecialObject $object $relation"
    }
    # add item for root/main toplevel widget
    $m add separator
    $m add command -label [Project::GetRootName] \
	-command [list Special::AddSpecialObject [Project::GetRootName] $relation]
}
proc Menu::GetSpecialObjectsMenu { relation } {
    variable _menuSpecialObjects_
    return $_menuSpecialObjects_($relation)
}


proc Menu::CreateMoreWidgetsMenu { parent relation } {
    variable _menuMoreWidgets_
    set m [menu $parent.moreWidgets_$relation -tearoff 0]
    set _menuMoreWidgets_($relation) $m
    foreach widget [Register::GetRegisteredWidgets] {
	$m add command -label $widget \
	    -command "WidgetTree::AddWidget $widget $relation"
    }
}
proc Menu::GetMoreWidgetsMenu { relation } {
    variable _menuMoreWidgets_
    return $_menuMoreWidgets_($relation)
}
# called when registered widgets are changed, from other tcl files
proc Menu::UpdateMoreWidgetsMenu { } {
    variable _menuMoreWidgets_
    variable _relationList_
    foreach relation $_relationList_ {
	$_menuMoreWidgets_($relation) delete 0 last
	foreach widget [Register::GetRegisteredWidgets] {
	    $_menuMoreWidgets_($relation) add command -label $widget \
		-command "WidgetTree::AddWidget $widget $relation"
	}
    }
}


# posts the popup men 'menu' at the coordinates 'X' and 'Y'
proc Menu::PostPopupMenu { menu X Y } {
    tk_popup $menu $X $Y
}


# posts a widget list menu in response to a binding
# on Ctrl-i (sibling) or Ctrl-h (child)
proc Menu::PostWidgetListMenu { relation nodeNumber W } {
    if { 0 } {
	variable _menuWidgetList_
	
	set widgetType  [Tree::GetNodeItem widgetType $nodeNumber]
	
	# if child, check if widget is canvas or menu
	if { $relation == "CHILD" } {
	    if { [Canvas::WidgetIsCanvasObject $widgetType] ||
		 [MenuEntry::WidgetIsMenuEntry $widgetType] } {
		Utility::ErrorMessage "[mc {This object cannot have children}]"
		return
	    }
	    if { $widgetType == "canvas" } {
		set menu [GetCanvasObjectsMenu CHILD]
	    } elseif { $widgetType == "menu" } {
		set menu [GetMenuEntryMenu CHILD]
	    } else {
		set menu [GetWidgetListMenu CHILD]
	    }
	}

	# if sibling, check if widget is canvas object or menu entry
	if { $relation == "SIBLING" } {
	    if { $nodeNumber == 0 } {
		Utility::ErrorMessage "[mc {Root cannot have siblings}]"
		return
	    }
	    if { [Canvas::WidgetIsCanvasObject $widgetType] } {
		set menu [GetCanvasObjectsMenu SIBLING]
	    } elseif { [MenuEntry::WidgetIsMenuEntry $widgetType] } {
		set menu [GetMenuEntryMenu SIBLING]
	    } else {
		set menu [GetWidgetListMenu SIBLING]
	    }
	}
	
	Menu::UpdateWidgetListMenu $relation
	tk_popup $menu [winfo rootx $W] [winfo rooty $W]
    }
}

