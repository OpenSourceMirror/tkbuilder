# -------------------------------------------------------------
# list.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
# -------------------------------------------------------------

namespace eval List {
}


# removes 'item' from list pointed to by 'listVar'; returns 1
# if 'item' existed in the list, 0 otherwise; assumes there
# is at most one occurrance of 'item'
proc List::RemoveItem { listVar item } {
    upvar $listVar list
    set index [lsearch $list $item]
    if { $index == -1 } {
	return 0
    } else {
	set list [lreplace $list $index $index]
	return 1
    }
}


# if 'item' occurs in 'list' returns 1, else returns 0
proc List::ItemExists { list item } {
    if { [lsearch $list $item] == -1 } {
	return 0
    } else {
	return 1
    }
}
