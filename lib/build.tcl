# -------------------------------------------------------------
# build.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------


namespace eval Build {

    # NB: these statements are cloned in buildUpdate.tcl
    # Initialized with Build::InitializeBuild (don't initialize variables
    # that are declared in two or more files of same namespace)
    variable _enableModification_
    variable _inRunMode_
    variable _nodePathname_

    variable _lastBuiltNode_ {}
    variable _tclText_     ; # variable to hold text for the viewer
    variable _indentation_ ; # current number of levels of indentation 
    variable _enableWidgetLink_ 0
    variable _applicationRunning_ 0
    variable _currentFileID_ {}
    variable _fileIDList_ {} ; # keep track of open tcl file IDs so can close them on failure

    # variables for viewer
    variable _winView_ 
    variable _waitView_
    variable _textbox_
}

# initialize variables of build namespace that occur in two or more files
proc Build::InitializeBuild { } {
    variable _enableModification_ 0
    variable _inRunMode_ 0
    variable _nodePathname_ {}
}
proc Build::GetLastBuiltNode { } {
    variable _lastBuiltNode_
    return $_lastBuiltNode_
}
proc Build::SetLastBuiltNode { node } {
    variable _lastBuiltNode_ $node
}


# buildMode must be one of RUN_PROJECT, RUN_BRANCH, RUN_LAST, VIEW_TCL, 
# SAVE_AS_TCL, SAVE_AS_TKB, SAVE_PROJECT_TCL
proc Build::BuildBranch { buildMode } {
    global tcl_platform
    variable _tclText_       {}
    variable _inRunMode_
    variable _mode_
    variable _enableWidgetLink_
    variable _enableModification_
    variable _currentFileID_ {}
    variable _fileIDList_ {}

    # to fire any pending <FocusOut> events in the properties forms before building,
    # to ensure data arrays are up-to-date
    focus .
    update

    # determine if building a run
    if { $buildMode == "RUN_BRANCH" || $buildMode == "RUN_LAST" \
	     || $buildMode == "RUN_PROJECT" } {
	set _inRunMode_ 1
    } else {
	set _inRunMode_ 0
    }

    # determine if saving tcl project
    if { $buildMode == "SAVE_PROJECT_TCL" } {
	set _mode_(saveProject) 1
    } else {
	set _mode_(saveProject) 0
    }

    # determine if need to add binding linking widgets to their forms
    set _enableWidgetLink_ [Option::GetOption buildBindToAppWidgets]

    # determine if the application attributes can be changed during a run
    set _enableModification_ [Option::GetOption buildEnableAppModification]

    # unset the array used to register widgets(in case of previous run)
    UnsetRegisteredWidgets

    # determine the node number that this branch is starting from
    set lastBuiltNode [GetLastBuiltNode]
    if { $buildMode == "RUN_LAST" && $lastBuiltNode != {} && \
	     [Tree::NodeExists $lastBuiltNode] } {
	set nodeNumber $lastBuiltNode
    } elseif { $buildMode == "RUN_PROJECT" } {
	set nodeNumber [Project::GetProjectNodeNumber]
    } else {
	set nodeNumber [Tree::GetCurrentNode]
    }

    if { $buildMode == "SAVE_PROJECT_TCL" } {
	set nodeNumber [Project::GetProjectNodeNumber]
    }

    # check if trying to build branch from canvas object or menu entry;
    # need to do this because the run branch tool-button is always enabled
    if { [Canvas::NodeIsCanvasObject $nodeNumber] } {
	Utility::ErrorMessage "[mc {Cannot build branch from canvas object}]"
	return
    } 
    if { [MenuEntry::NodeIsMenuEntry $nodeNumber] } {
	Utility::ErrorMessage "[mc {Cannot build branch from menu entry}]"
	return
    } 

    # if running, but not RUN_LAST, remember this node
    if { $buildMode == "RUN_PROJECT" || $buildMode == "RUN_BRANCH" } {
	SetLastBuiltNode $nodeNumber
    }

    # create interpreter if building a run
    if { $_inRunMode_ } {
	
	if { [interp exists interpBuild] } {
	    interp delete interpBuild
	}
	interp create interpBuild
	load {} Tk interpBuild
	
	# not sure why had stict motif in first place; commented it out; if something craps out
	# this may be why
	#		interpBuild eval [list set tk_strictMotif 1]

	# if first node is toplevel, not root, and withdraw root window option set,
	# withdraw root
	if { [Tree::GetNodeItem widgetType $nodeNumber] == "toplevel" &&
	     ![Project::NodeIsRoot $nodeNumber] &&
	     [Option::GetOption buildWithdrawRoot] } {
	    interpBuild eval [list wm withdraw .]
	}
	if { [Option::GetOption buildOpenConsole] } {
	    if { $tcl_platform(platform) == "windows" } {
		console show
	    }
	}
	
	# set up alias that links the proc _TKB_GetWidgetNumber_ (which will be
	# built in the app) to Build::GetWidgetNumber
	if { $_enableWidgetLink_ } {
	    interp alias interpBuild _TKB_GetWidgetNumber_ {} Build::GetWidgetNumber
	}

	# set up alias that links the proc _TKB_RegisterAppWidget_ (which will be
	# built in the app) to Build::RegisterAppWidget
	if { $_enableModification_ } {
	    interp alias interpBuild _TKB_RegisterAppWidget_ {} Build::RegisterAppWidget 
	}

	# set up alias and dummy widget in the application to determine when it is killed
	interp alias interpBuild _TKB_ApplicationKilled_ {} Build::ApplicationKilled
	interpBuild eval [list label ._TKB_ApplicationKill_]
	interpBuild eval [list bind ._TKB_ApplicationKill_ <Destroy> _TKB_ApplicationKilled_]
	
	# indicate application is running
	SetApplicationIsRunning 1

    }
    
    # initialize the level of indentation in the code
    InitializeIndentation
    
    # initialize the template array with a dummy entry; 'template' is an
    # array indexed on '${widgetClass}_${attribute}'; it is passed by reference
    # to each node of the hierarchy, and modified by template objects along
    # the way 
    set template(dummy) {}

    # write code to append to autopath, load packages and load namespaces
    LoadPackages
    
    # write code to load resource database
    LoadResourceDatabase

    # build widgets and objects for branch starting at 'nodeNumber' (recursive call);
    # check if build has failed
    if { ![BuildCommand $nodeNumber {} template {}] } {
	
	# delete interpreter on failure, if one was created
	catch { interp delete interpBuild }
	
	# on error, if saving project, manually close all potentially open tcl files
	if { $_mode_(saveProject) } {
	    foreach fileID $_fileIDList_ {
		close $fileID
	    }
	}
	
	return
    }

    # if building a run, execute the tcl text
    if { $_inRunMode_ } {
	interpBuild eval $_tclText_

	# check if the first object of branch is a proc; run it if so
	if { [Tree::GetNodeItem widgetType $nodeNumber] == "proc" } {
	    set procName [Tree::GetNodeItem name $nodeNumber]
	    set procArgs [Special::GetSpecialObject proc args $nodeNumber]
	    puts [interpBuild eval $procName $procArgs]
	}

    }

    # if building a view, open the viewer
    if { $buildMode == "VIEW_TCL" } {
	OpenViewWindow [Tree::GetNodeItem name $nodeNumber]
    }
    
    # if building tcl file, save as tcl file
    if { $buildMode == "SAVE_AS_TCL" } {
	File::SaveFileAsTcl _tclText_
    }

    unset template

    # this doesn't work for some reason
    Status::UpdateStatusBar

}


# loads tcl startup script at head of main tcl file
proc Build::LoadStartupScript { } {
    set startupList [split [string trim [Project::GetState Startup]] "\n"]
    set needSpace 0
    foreach startup $startupList {
	ProcessCommand $startup
	set needSpace 1
    }
    if { $needSpace } {
	ProcessCommand BLANK_LINE
    }
}


proc Build::LoadPackages { } {
    set autoPathList  [split [string trim [Project::GetState Auto_path]] "\n,;"]
    set packageList   [split [string trim [Project::GetState Packages]] "\n,;"]
    set namespaceList [split [string trim [Project::GetState Namespaces]] "\n,;"]

    set needSpace 0
    foreach autopath $autoPathList {
	ProcessCommand "lappend auto_path [list [string trim $autopath]]"
	set needSpace 1
    }
    foreach package $packageList {
	ProcessCommand "package require [string trim $package]"
	set needSpace 1
    }
    foreach namespace $namespaceList {
	ProcessCommand "namespace import [string trim $namespace]"
	set needSpace 1
    }
    if { $needSpace } {
	ProcessCommand BLANK_LINE
    }
}


proc Build::LoadResourceDatabase { } {
    set resourceList [split [string trim [Project::GetState Resources]] "\n,;"]
    set needSpace 0
    foreach resource $resourceList {
	ProcessCommand "option add [string trim $resource]"
	set needSpace 1
    }
    if { $needSpace } {
	ProcessCommand BLANK_LINE
    }
}


# aliased to function _TKB_GetWidgetNumber_ defined within the app; receives
# the 'nodeNumber' of the widget clicked on within the app; ensures the branch
# containing the node is expanded with call to ExpandBranch
proc Build::GetWidgetNumber { nodeNumber } {

    # to fire any pending <FocusOut> events in the properties forms before
    # switching to selected widget, to ensure data arrays are up-to-date
    focus .
    update

    if { ![Tree::NodeExists $nodeNumber] } {
	Utility::ErrorMessage "[mc {That widget no longer exists}]\n [mc {in the widget tree}]"
	return
    }
    Tree::SetNodeItem expanded 0 1 ; # always expand root node
    Build::ExpandBranch 0 $nodeNumber
    Tree::SetCurrentNode $nodeNumber
    WidgetTree::RedrawWidgetTree
    update ; # needed to avoid problem of shifting tree
    WidgetTree::MakeNodeVisible $nodeNumber
    WidgetForms::UpdateAllForms $nodeNumber

}
# ensures all nodes of the branch containing 'findNode' are expanded; recursive
# function; 'fromNode' is the current node in the descent of the branch
proc Build::ExpandBranch { fromNode findNode } {
    # loop once for each child of this node
    foreach child [Tree::GetNodeItem childList $fromNode] {
	if { $child == $findNode } { break }
	# if this branch contains the node that was selected,
	# make sure this node is expanded and descend it
	if { [Tree::BranchContainsNode $child $findNode] } {
	    Tree::SetNodeItem expanded $child 1
	    Build::ExpandBranch $child $findNode
	}
    }
}


# builds the command required to construct, layout and bind the node 'nodeNumber';
# 'parentPathName' is the widget path upto this point (i.e., parent of 'nodeNumber');
# on first pass, 'parentPathName' will be {}; a recursive function; return 1
# if successful, 0 otherwise; 'templateVar' is 'template' passed by reference
proc Build::BuildCommand { nodeNumber parentPathName templateVar fileID } {

    variable _mode_
    variable _currentFileID_ $fileID
    variable _fileIDList_
    upvar $templateVar template

    # check that children of this node are not both packed and gridded
    set layoutType {}
    foreach child [Tree::GetNodeItem childList $nodeNumber] {
	if { ![Special::NodeIsSpecialObject $child] } {
	    # remember first grid or pack found among widget children
	    if { $layoutType != "pack" && $layoutType != "grid" } {
		set layoutType [Layout::GetLayoutType $child]
	    } else {
		if { ($layoutType == "grid" && \
			  [Layout::GetLayoutType $child] == "pack") ||
		     ($layoutType == "pack" && \
			  [Layout::GetLayoutType $child] == "grid") } {
		    Utility::ErrorMessage "[mc {Using both pack and grid to layout}]\n\
						[mc {children of}] '[Tree::GetNodeItem name $nodeNumber]'"
		    return 0
		}
	    }
	}
    }

    if { [Special::NodeIsSpecialObject $nodeNumber] } {
	
	# widget hierarchy is unaffected by special objects
	set pathName $parentPathName
	
	set objectType [Tree::GetNodeItem widgetType $nodeNumber]
	switch  $objectType {
	    project {
		# don't do anything
	    }
	    file {
		# check if we're saving the project
		if { $_mode_(saveProject) } {
		    set currentFileID $fileID ; # remember current file ID
		    # open the tcl file specified at this file object, for writing
		    set filename [Special::GetSpecialObject file name $nodeNumber]
		    if { [catch {open $filename "w"} fileID] } {
			Utility::ErrorMessage "[mc {Unable to open tcl file}] '$filename'"
			return 0
		    }
		    lappend _fileIDList_ $fileID
		    set _currentFileID_ $fileID ; # do here so following Process works
		    
		    # if this is the main tcl file, write code to load packages and resources
		    # in that file
		    if { $filename == [Project::GetState mainTclFile] } {
			LoadStartupScript
			LoadPackages
			LoadResourceDatabase
		    }
		}
		ProcessCommand [Special::GetSpecialObject file code $nodeNumber]
	    }
	    proc {
		# insert blank line before proc if option set
		if { [Option::GetOption buildBlankBeforeProc] } {
		    ProcessCommand BLANK_LINE
		}
		BuildProcedure $nodeNumber
	    }
	    template {
		# save current state of 'template', as it may be
		# changed by descendants (as its passed by reference)
		# (currentTemplate is a local var)
		array set currentTemplate [array get template]
		UpdateTemplate template $nodeNumber
	    }
	    namespace {
		# insert blank line before namespace code if option set
		if { [Option::GetOption buildBlankBeforeNamespace] } {
		    ProcessCommand BLANK_LINE
		}
		BuildNamespace $nodeNumber
	    }
	    code {
		# insert blank line before code if option set
		if { [Option::GetOption buildBlankBeforeCode] } {
		    ProcessCommand BLANK_LINE
		}
		ProcessCommand [Special::GetSpecialObject code code $nodeNumber]
	    }
	    default {
		error "[mc {Unknown special object}]: $objectType"
	    }
	}

    } elseif { [Project::NodeIsProject $nodeNumber] } {

	# widget hierarchy is unaffected by project
	set pathName $parentPathName

    } else {

	# insert blank line before the widget code if option set
	if { [Option::GetOption buildBlankBeforeWidget] } {
	    ProcessCommand BLANK_LINE
	}

	# do pre-widget code
	ProcessCommand [General::GetGeneralParameter preWidgetCode $nodeNumber]
	
	# builds the widget for the current node; 'pathName' is the path name
	# of the widget that is built
	set pathName [BuildWidget $nodeNumber $parentPathName template]
	if { $pathName == 0 } { return 0 }

	# do post-widget code
	ProcessCommand [General::GetGeneralParameter postWidgetCode $nodeNumber]

    }

    # traverse each child of this node and build it
    foreach childNumber [Tree::GetNodeItem childList $nodeNumber] {
	if { [BuildCommand $childNumber $pathName template $fileID] == 0 } { return 0 }
    }

    # post-object code
    if { [Special::NodeIsSpecialObject $nodeNumber] } {
	switch  $objectType {
	    project {
		# don't do anything
	    }
	    file {
		if { $_mode_(saveProject) } {
		    close $fileID ; # close the file
		    List::RemoveItem _fileIDList_ $fileID
		    set fileID $currentFileID ; # restore the current file ID
		}
	    }
	    proc {
		DecrementIndentation
		ProcessCommand "\}" ; # closing brace for proc
	    }
	    template {
		# return 'template' to its pre-descendant state
		unset template
		array set template [array get currentTemplate]
		unset currentTemplate
	    }
	    namespace {
		DecrementIndentation
		ProcessCommand "\}"
	    }
	    code {
		# don't do anything
	    }
	}
    }
    
    return 1

}


proc Build::ProcessCommand { command } {
    variable _tclText_
    variable _currentFileID_
    if { $_currentFileID_ == {} } {
	if { $command == "BLANK_LINE" } {
	    append _tclText_ \n
	    return
	}
	if { $command != {} } {
	    foreach line [split $command \n] {
		append _tclText_ [GetIndentation] $line \n
	    }
	}
    } else {
	if { $command == "BLANK_LINE" } {
	    puts $_currentFileID_ ""
	    return
	}
	if { $command != {} } {
	    foreach line [split $command \n] {
		puts $_currentFileID_ "[GetIndentation]$line"
	    }
	}
    }
}


# updates the template array variable 'templateVar' to include the attributes
# specified in the template at 'nodeNumber'
proc Build::UpdateTemplate { templateVar nodeNumber } {
    upvar $templateVar template
    # loop once for each template entry of this node
    foreach entry [Special::GetSpecialObject template entryList $nodeNumber] {
	set value [Special::GetSpecialObject template $entry $nodeNumber]
	set template($entry) $value
    }
}


# builds the eval command associated with 'namespace'; the trailing '}'
# must be added in the caller, after descendents are built
proc Build::BuildNamespace { nodeNumber } {
    set namespace [Tree::GetNodeItem name $nodeNumber]
    set eval [Special::GetSpecialObject namespace eval $nodeNumber]
    ProcessCommand "namespace eval $namespace \{"
    IncrementIndentation
    ProcessCommand $eval
}


# builds the code associated with a procedure; note that the trailing '}'
# must be added in the caller, after the descendants of this proc are built
proc Build::BuildProcedure { nodeNumber } {

    set procName   [Tree::GetNodeItem name $nodeNumber]
    set parameters [Special::GetSpecialObject proc params $nodeNumber]

    # if there is more than one list item of arguments, user did not use
    # braces, so add them
    if { [llength $parameters] != 1 } {
	set parameters "\{ $parameters \}"
    }

    # NB assumes user has not put braces around code block
    ProcessCommand "proc $procName $parameters \{"

    # incement indentation here so 'proc ...' isn't indented
    IncrementIndentation

    # process the code block
    set code [Special::GetSpecialObject proc code $nodeNumber]
    ProcessCommand $code

}


# builds the widget for 'nodeNumber' using 'parentPathName' as its
# parent; returns the pathname of this widget if successful, 0 otherwise
proc Build::BuildWidget { nodeNumber parentPathName templateVar } {

    variable _inRunMode_
    variable _enableWidgetLink_
    variable _enableModification_

    upvar $templateVar template

    # if this is the root node, handle differently
    if { [Project::NodeIsRoot $nodeNumber] } {

	set pathName {}

	# build the root window configure command
	set rootConfigureCommand [Attribute::GetRootConfigureCommand template]
	if { $rootConfigureCommand == "-1" } { return 0 }
	if { $rootConfigureCommand != {} } {
	    ProcessCommand $rootConfigureCommand
	}
	
	if { $_inRunMode_ && $_enableModification_ } {
	    ProcessCommand "bind . <Map> \
				\"_TKB_RegisterAppWidget_ $nodeNumber \%W; break\""
	}

	# build the root bindings
	set widgetBindingCommand [Binding::GetBindingCommand [Project::GetRootNodeNumber] .]
	if { $widgetBindingCommand == "-1" } {
	    return 0
	} elseif { $widgetBindingCommand != {} } {
	    ProcessCommand $widgetBindingCommand
	}

    } else {

	# get name and type of current widget
	set name       [Tree::GetNodeItem name $nodeNumber]
	set widgetType [Tree::GetNodeItem widgetType $nodeNumber]
	
	# check if name-is-path or parent specified
	set nameIsPath [General::GetGeneralParameter nameIsPath $nodeNumber]
	set specifyParentName [General::GetGeneralParameter specifyParentName $nodeNumber]
	set parent [string trim [General::GetGeneralParameter parent $nodeNumber]]

	if { $nameIsPath && $specifyParentName } {
	    error "[mc {Name is path and parent name specified for}] '$name'"
	}

	if { $specifyParentName && $parent == {} } {
	    Utility::ErrorMessage "[mc {Parent name not specified for widget}] '$name'"
	    return 0
	}
	
	# check if the widget name is a path (e.g. .myframe.mybutton)
	if { $nameIsPath } {
	    set pathName $name
	} else {
	    # check if user specified parent name for this widget
	    if { $specifyParentName } {
		# if so, parent name overrides the widget tree
		if { $parent == "." } {
		    set pathName ".$name"
		} else {
		    set pathName "$parent.$name"
		}
	    } else {
		# if not, path name is based on the widget tree
		set pathName "$parentPathName.$name"
	    }
	}

	# get the set variable if any (i.e., variable to be assigned widget name)
	set setVariable [string trim [General::GetGeneralParameter set $nodeNumber]]

	# check if the widget is a canvas object
	if { [Canvas::WidgetIsCanvasObject $widgetType] } {
	    
	    set canvasObjectCommand [Canvas::GetCanvasObjectCommand $parentPathName \
					 $nodeNumber $widgetType]
	    if { $canvasObjectCommand == "-1" } { return 0 }
	    
	    # if the object has bindings, but no set variable name, give it one
	    if { $setVariable == {} && [Binding::NodeHasBinding $nodeNumber] } {
		set setVariable _tkb_canvasObject_
	    }
	    
	    # if building run and widget to form link enabled, also need the set variable
	    if { $setVariable == {} && $_inRunMode_ && $_enableWidgetLink_ } {
		set setVariable _tkb_canvasObject_
	    }			

	    # add set variable name to the command if specified
	    if { $setVariable != {} } {
		set canvasObjectCommand "set $setVariable \[$canvasObjectCommand]"
	    }

	    ProcessCommand $canvasObjectCommand

	    # build the binding command(s), but only if set variable specified
	    # because its required in the binding (e.g. $c bind $setVariable event
	    # command) ... ((but now the set variable is created if needed 022000,
	    # so consider getting rid of check for setVariable name here))
	    if { $setVariable != {} } {
		set objectBindingCommand \
		    [Binding::GetCanvasObjectBindingCommand $nodeNumber $name \
			 $parentPathName $setVariable]
		if { $objectBindingCommand == "-1" } {
		    return 0
		} elseif { $objectBindingCommand != {} } {
		    ProcessCommand $objectBindingCommand
		}
	    }
	    
	    # registers binding with the object that returns its node number;
	    # used to bring up the properties form for the node when
	    # <Ctr-Shft-B1> event fires in the app
	    if { $_inRunMode_ && $_enableWidgetLink_ } {
		ProcessCommand "$parentPathName bind \$$setVariable <Control-Shift-ButtonPress-1> \
					\"_TKB_GetWidgetNumber_ $nodeNumber ; break\""
	    }

	    # check if widget is a menu entry
	} elseif { [MenuEntry::WidgetIsMenuEntry $widgetType] } {
	    
	    set menuEntryCommand [MenuEntry::GetMenuEntryCommand $parentPathName \
				      $nodeNumber $widgetType]
	    if { $menuEntryCommand == "-1" } { return 0	}
	    ProcessCommand $menuEntryCommand
	    
	} else {

	    # build the widget
	    set widgetAttributeCommand \
		[Attribute::GetWidgetCommand $nodeNumber $widgetType $pathName template]
	    if { $widgetAttributeCommand == "-1" } { return 0 }
	    if { $setVariable != {} } {
		set widgetAttributeCommand \
		    "set $setVariable \[$widgetAttributeCommand]"
	    }
	    ProcessCommand $widgetAttributeCommand

	    # check if set variable should be used as widget name in layout and binding
	    # commands (rather than the path name)
	    # (only applicable if this widget has set variable)
	    if { $setVariable != {} && [Option::GetOption buildSetVariableAsPath] } {
		set widgetPath "\$$setVariable"
	    } else {
		set widgetPath $pathName
	    }

	    # lay out the widget (unless layout type is 'none')
	    if { [Layout::GetLayoutType $nodeNumber] != "none" } {
		set widgetLayoutCommand \
		    [Layout::GetLayoutCommand $nodeNumber $widgetPath]
		ProcessCommand $widgetLayoutCommand
	    }

	    # registers binding with the widget that returns its node number;
	    # used to bring up the properties form for the node when
	    # <Ctr-Shft-B1> event fires in the app
	    if { $_inRunMode_ && $_enableWidgetLink_ } {
		if { $widgetType != "toplevel" } {
		    ProcessCommand "bind $widgetPath <Control-Shift-ButtonPress-1> \
						\"_TKB_GetWidgetNumber_ $nodeNumber ; break\""
		}
	    }

	    # registers binding with the widget that returns the node number
	    # and its full path, when the widget is mapped; this provides a link between
	    # the node number and the widget in the running app, which is used to modify
	    # the widget attributes during run-time
	    if { $_inRunMode_ && $_enableModification_ } {
		ProcessCommand "bind $widgetPath <Map> \
					\"_TKB_RegisterAppWidget_ $nodeNumber \%W; break\""
	    }

	    # build the user specified bindings
	    set widgetBindingCommand \
		[Binding::GetBindingCommand $nodeNumber $widgetPath]
	    if { $widgetBindingCommand == "-1" } {
		return 0
	    } elseif { $widgetBindingCommand != {} } {
		ProcessCommand $widgetBindingCommand
	    }

	    # if set variable specified, check if it should be used as the
	    # parent name in path of descendants of this widget (e.g. $f.descendant)
	    if { $setVariable != {} && \
		     [General::GetGeneralParameter useSetAsParent $nodeNumber] } {
		set pathName "\$$setVariable"
	    }
	    
	}
	
    }

    return $pathName

}




# creates and opens the toplevel window used to view the tcl code
proc Build::OpenViewWindow { branch } {

    variable _winView_ 
    variable _waitView_
    variable _textbox_
    variable _tclText_

    if { ![winfo exists ._winView_] } {
    	set _winView_ [toplevel ._winView_]  	
	wm withdraw $_winView_
    	wm iconname $_winView_ "View"
    	wm protocol $_winView_ WM_DELETE_WINDOW {wm withdraw ._winView_}
    	bind $_winView_ <Key-Escape>            {wm withdraw ._winView_}

	set fraMain [frame $_winView_.fraMain]
	set _textbox_ [Text::CreateTextbox $fraMain.fraView \
			   -width 40 -height 20 -wrap none]
	pack [Text::GetTextboxFrame $_textbox_] -fill both -expand true

    	set fraButton [frame $_winView_.fraButton]
	pack $fraButton -side bottom -anchor e -side bottom
	Button::MakeButtonSet $fraButton HORIZONTAL close \
	    close Close {wm withdraw ._winView_}
	
	pack $fraMain -fill both -expand true -side top

	bind $_winView_ <Return> {wm withdraw ._winView_}
	wm deiconify $_winView_
    } elseif { ![winfo ismapped ._winView_] } {
	wm deiconify $_winView_
    }
    
    wm title $_winView_ "[mc {View}] - [mc {From}]: $branch"
    raise $_winView_
    $_textbox_ delete 1.0 end
    $_textbox_ insert 1.0 $_tclText_

}

proc Build::InitializeIndentation { } {
    variable _indentation_ 0
    variable _indentString_ {}
}
proc Build::IncrementIndentation { } {
    variable _indentation_
    incr _indentation_ 1
    SetIndentationString $_indentation_
}
proc Build::DecrementIndentation { } {
    variable _indentation_
    incr _indentation_ -1
    SetIndentationString $_indentation_
}
proc Build::SetIndentationString { indentation } {
    variable _indentString_ {}
    if { [Option::GetOption buildIndentUseSpaces] } {
	set numSpaces [Option::GetOption buildIndentNumSpaces]
	for {set i 0} {$i < $indentation} {incr i} \
	    { append _indentString_ [format "%${numSpaces}s" ""] }
    } else {
	for {set i 0} {$i < $indentation} {incr i} \
	    { append _indentString_ \t }
    }
}
proc Build::GetIndentation { } {
    variable _indentString_
    return $_indentString_
}


# called by the application (through alias _TKB_ApplicationKilled_) when the
# application is killed
proc Build::ApplicationKilled { } {
    variable _nodePathname_
    
    # free array used to link node numbers with widget path names
    catch { unset _nodePathname_ }
    
    # indicate application no longer running
    SetApplicationIsRunning 0

    Status::UpdateStatusBar

}
proc Build::SetApplicationIsRunning { boolean } {
    variable _applicationRunning_ $boolean
}
proc Build::GetApplicationIsRunning { } {
    variable _applicationRunning_
    return $_applicationRunning_
}
proc Build::StopApplication { } {
    variable _nodePathname_
    catch { interpBuild eval destroy . }
    catch { unset _nodePathname_ }
    SetApplicationIsRunning 0
    Status::UpdateStatusBar
}