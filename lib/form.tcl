# -------------------------------------------------------------
# form.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
# history: v1.0.2a:04/29/00
# -------------------------------------------------------------


namespace eval Form {

    proc MakeForm {  } {
	set c [canvas .form1]
	pack .form1
	
	set attributeList { 
	    activeForeground
	    activeBackground
	    anchor
	    background
	    borderwidth
	    foreground
	    relief  }
	
	set dy 17
	set x 10
	set y 10
	set x1 7
	set x2 200
	set yFirst [set y1 2]
	set y2 [expr $y1 + $dy]
	set xMid 120
	set xValue 130
	
	proc SelectAttribute { canvas label x y } {
	    puts "$canvas $label"
	    #	puts [$canvas itemcget $label -text]
	    puts [$canvas coords $label]
	    #	$canvas coords $label [incr x 1] [incr y 1]
	    $canvas raise $label
	    $canvas itemconfigure $label -fill green
	    
	}
	
	foreach attribute $attributeList {
	    # attribute name text
	    set a [$c create text $x $y -text $attribute -anchor w]
	    #	$c bind $a <ButtonPress> "SelectLabel $c $a $x $y"
	    incr y $dy
	    
	    # line
	    #	$c create line $x1 $y1 $x2 $y1
	    #	set p [$c create line $x1 $y1 $x2 $y1 $x2 $y2 $x1 $y2 $x1 $y1 \
		#	-fill lightgrey]
	    set p [$c create line $x1 $y1 $x2 $y1 $x2 $y2 \
		       -fill lightgrey]
	    $c bind $a <ButtonPress> "SelectAttribute $c $p $x $y"
	    incr y1 $dy
	    incr y2 $dy
	}
	$c create line $x1 $y1 $x2 $y1
	$c create line $x1 $yFirst $x1 $y1 -fill lightgrey
	$c create line $xMid $yFirst $xMid $y1 -width 2
	
	set value 0
	set y 10
	foreach attribute $attributeList {
	    incr value
	    set a [$c create text $xValue $y -text $value -fill blue -anchor w]
	    incr y $dy
	}
	
	
	
	button .fdas
	pack .fdas
    }
}
