# -------------------------------------------------------------
# main.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------

namespace eval Main {
    variable _waitVariable_
    variable _system_
    # array to keep track of system variables, indexed on variable names
}

# sets the value of all system variables
proc Main::InitializeSystem { } {

    SetSystem dirHome {} ; # home directory for tkBuilder
    SetSystem dirLib {} ; # directory containing library
    SetSystem dirIcons {} ; # directory containing system icons
    SetSystem version "tkBuilder 1.0.2" ; # this version of tkbuilder

}

# sets the value of the system variable 'item' to 'value'
proc Main::SetSystem { item value } {
    variable _system_
    set _system_($item) $value
}

# gets the value of the system variable 'item'
proc Main::GetSystem { item } {
    variable _system_
    return $_system_($item)
}


# sets the main system-wide bindings
proc Main::SetSystemBindings { } {

    # bindings to mimic X-like cut and paste in text boxes
    bind Text <Control-ButtonRelease-1> { 
	catch { 
	    %W insert insert [%W get sel.first sel.last]
	}
    }
    bind Text <Control-Shift-ButtonRelease-1> {
	catch {
	    %W insert insert [%W get sel.first sel.last]
	    %W delete sel.first sel.last
	}
    }

    # bindings to post edit-cut-copy-past menu
    bind Text <ButtonPress-3> "Select::PostSelectMenu %W %X %Y"
    bind Entry <ButtonPress-3> "Select::PostSelectMenu %W %X %Y"

    # used to indicate if a modifier key is pending, so that state
    # modified is not set when a menu or short-cut is accessed through keyboard
    bind . <KeyPress> "State::SetPendingModifier %K"
    bind . <KeyRelease> "State::UnsetPendingModifier %K"

    # bind . <Escape> "focus \[WidgetTree::GetNodeLabelWidget \[Tree::GetCurrentNode]]"
    # bind . <Escape> {focus [WidgetTree::GetNodeLabelWidget [Tree::GetCurrentNode]]}
    bind . <Escape> {focus [WidgetTree::GetLabelFromNodeNumber [Tree::GetCurrentNode]]}

}


# creates the main window of tkBuilder
proc Main::CreateMainWindow { } {            
    wm title    . "tkBuilder"
    wm iconname . "tkBuilder" 
    wm protocol . WM_DELETE_WINDOW Main::MainExitPrompt
    focus -force .
}


proc Main::MainExitPrompt { } {             
    if { [State::GetState modified] } {
	set response [tk_messageBox -type yesnocancel -default yes \
			  -title "tkBuilder" -message "[mc {Save changes before quitting}]?" \
			  -icon question]
	if { $response == "yes" } {
	    if { [File::SaveFileAs] } {
		MainExit
	    }
	} elseif { $response == "no" } {
	    MainExit
	} elseif { $response == "cancel"} {
	    # do nothing
	}           
    } else {
	if { [Option::GetOption confirmExitProgram] } {
	    set response [tk_messageBox -type yesno -default yes \
			      -title "tkBuilder" -message "[mc {Exit}] tkBuilder?" -icon question]
	    if { $response == "no" } {
		return
	    }
	}
	MainExit
    }
}  


proc Main::MainExit { } {

    # save options to file
    Option::WriteOptionDatabaseFile
    
    # save state to file
    State::WriteStateDatabaseFile
    
    # delete the build interpreter if it exists
    catch { interp delete interpBuild }

    destroy .

}                   


proc Main::OpenAboutWindow { {overrideredirect 0} } {

    global argv0
    variable _window_

    catch { destroy .winAbout }
    set w [toplevel .winAbout]
    set _window_ $w
    wm protocol $w WM_DELETE_WINDOW { set Main::_waitVariable_ 1}

    wm title $w "tkBuilder"
    wm overrideredirect $w $overrideredirect
    wm resizable $w 0 0

    set f [frame .winAbout.f -borderwidth 20 -relief raised -background #5474FF]
	   pack $f
	   label $f.l1 -background #5474FF -font {System 10 {bold roman}} -foreground white -text "tkBuilder"
	   pack $f.l1
	   label $f.l2 -background #5474FF -foreground white -text \
	       "[mc {Forked from}] v1.0.2a\n [mc {for}] tcl/tk 8.4\n [mc {by}] andrasy\n"
	   pack $f.l2
	   label $f.l3 -background #5474FF -foreground white -justify left -text \
	       "Copyright � 1999, 2000 Frank Schnekenburger\n[mc {Built at}] Sawpit\nsawpit@yahoo.com\nhttp://scp.on.ca/sawpit\n[mc {See 'Help-License' for terms of license}]\n\n\n"
	   pack $f.l3
	   set b [button $f.b1 -state disabled -text [mc {OK}] -default active \
		      -command { set Main::_waitVariable_ 1 }]
	   pack $b

	   # 	set f [frame .winAbout.f1 -background white -bd 0]
	   # 	pack $f
	   # 	set c [canvas $f.c -background #7895c9 -height 268 -width 312]
	   # 	pack $c

	   # 	image create photo splashImage \
	       # 		-file [file join [Main::GetSystem dirHome] splash.gif]
	   # 	$c create image 0 0 -image splashImage -anchor nw
	   # 	set b [button $c.b1 -state disabled -text [mc {OK}] -default active \
	       # 		-command { set Main::_waitVariable_ 1 }]

	   bind $b <Key-Return> { set Main::_waitVariable_ 1 }

	   # 	$c create window 158 215 -anchor center -window $b

	   wm geometry $w +30+30
	   
	   raise $w
	   focus -force $b
	   return $b

       }


    proc Main::GetDefaultRelation { objectType } {
	if { [Special::WidgetIsSpecialObject $objectType] } {
	    set relation CHILD
	} else {
	    switch $objectType {
		frame - toplevel - canvas - menu { set relation CHILD }
		default { set relation SIBLING }
	    }
	}
	return $relation
    }
