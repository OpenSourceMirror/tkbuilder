# -------------------------------------------------------------
# widgetTree.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------


namespace eval WidgetTree {

    namespace import ::Utility::SetLabelColourNormal
    namespace import ::Utility::SetLabelColourHighlight
    namespace import ::Utility::ConfirmMessage
    namespace import ::Utility::ErrorMessage
    namespace import ::Tree::*
    namespace import ::Menu::GetWidgetEditMenu
    namespace import ::Menu::GetWidgetMoveMenu
    namespace import ::Menu::UpdateWidgetMoveMenu
    namespace import ::Option::GetOption
    namespace import ::WidgetForms::ClearSelectedAttributes
    namespace import ::WidgetForms::UpdateAllForms

    variable _widgetCanvas_    {}
    variable _lastWidgetLabel_ {}

    variable _dragLabel_       {}
    variable _lastLabel_       {}
    variable _dragging_	       0
    variable _dragFromNode_    {}
    variable _dropToNode_      {}
    
    variable _option_
    # to store some option settings locally, so GetOption has to be called only once per
    # redraw

    namespace export CreateWidgetTreeCanvas RedrawWidgetTree
    namespace export WidgetBoxSelection
    namespace export DestroyWidgetLabel

}


# creates the canvas on which the widget tree is displayed;
# called only once during a session; same canvas is used for all
# trees that might be displayed during a session
proc WidgetTree::CreateWidgetTreeCanvas { fraTree } {

    variable _widgetCanvas_

    set canWidget [canvas $fraTree.canWidget -width 5c -height 10c \
		       -xscrollcommand [list WidgetTree::ScrollSet $fraTree.scrX \
					    [list grid $fraTree.scrX -row 1 -column 0 -sticky we]] \
		       -yscrollcommand [list WidgetTree::ScrollSet $fraTree.scrY \
					    [list grid $fraTree.scrY -row 0 -column 1 -sticky ns]] \
		       -highlightthickness 0]

    set _widgetCanvas_ $canWidget
    scrollbar $fraTree.scrX -orient horizontal -command "$canWidget xview"
    scrollbar $fraTree.scrY -orient vertical   -command "$canWidget yview"

    grid $canWidget -row 0 -column 0 -sticky nsew
    grid $fraTree.scrY -row 0 -column 1 -sticky ns
    grid $fraTree.scrX -row 1 -column 0 -sticky ew
    grid columnconfigure $fraTree 0 -weight 1
    grid rowconfigure $fraTree 0 -weight 1
    
    # build the grab frame ... used to change width of the canvas
    set fraGrab [frame $fraTree.fraGrab -width 4 -bd 1 -relief raised \
		     -cursor sb_h_double_arrow -bg blue]
    grid $fraGrab -row 0 -column 2 -rowspan 2 -sticky ns
    bind $fraGrab <ButtonRelease-1> \
	"if \[winfo ismapped $fraTree.scrY] { set ::scrollWidth 16 } \
		 else { set ::scrollWidth 0 }
		 $canWidget config -width \[expr %X - \[winfo rootx $canWidget] - \$::scrollWidth]
		 $fraGrab config -cursor sb_h_double_arrow"
    bind $fraGrab <B1-Motion> "$fraGrab config -cursor sb_v_double_arrow"
    
    # binding to properly colour widget node label in tree when it
    # receives focus (say when coming from property form)
    bind $canWidget <FocusIn> {
	Utility::SetLabelColourHighlight [WidgetTree::GetLabelFromNodeNumber [Tree::GetCurrentNode]]
    }
    bind $canWidget <ButtonPress-3> {
	# the 'if' is to ensure we're not over an icon
	if { [llength [%W find withtag current]] == 0 } {
	    focus [WidgetTree::GetLabelFromNodeNumber [Tree::GetCurrentNode]]
	    Menu::PostPopupMenu [Menu::GetBuildMenu] %X %Y
	}
    }
    bind $canWidget <ButtonPress-1> {
	focus [WidgetTree::GetLabelFromNodeNumber [Tree::GetCurrentNode]]
    }

    # bindings defined for all node labels, using the NodeLabel binding tag; only includes
    # bindings not needing node number; those bindings are defined for each label separately
    # to avoid overhead of determing node number from label name (bin there, done that)
    bind NodeLabel <Enter> {%W config -relief raised}
    bind NodeLabel <Leave> {%W config -relief flat}
    bind NodeLabel <Key-Delete> {WidgetTree::DeleteWidgetNode}
    bind NodeLabel <Control-Key-Delete> {WidgetTree::DeleteWidgetBranch}
    bind NodeLabel <Control-Key-r> {WidgetTree::RenameWidget}
    bind NodeLabel <Control-Key-d> {WidgetTree::DuplicateWidget}

}


proc WidgetTree::ScrollSet { scrollbar command offset size } {
    if { $offset != 0.0 || $size != 1.0 } {
	eval $command
	$scrollbar set $offset $size
    } else {
	set manager [lindex $command 0]
	$manager forget $scrollbar ; # hide the scroll bar
    }
}


proc WidgetTree::RedrawWidgetTree { } {

    variable _widgetCanvas_
    variable _lastWidgetLabel_
    variable _option_

    # variables to manage keyboard traversal
    variable _prev_
    variable _next_
    catch { unset _prev_ }
    catch { unset _next_ }
    variable _lastDrawnNode_ -1

    # variables to manage making node visible
    variable _xpos_
    variable _ypos_
    variable _maxYPos_ 0
    catch { unset _xpos_ }
    catch { unset _ypos_ }
    
    # call some options here so only have to call once per redraw,
    # rather than with every recursive call of DrawWidgetTree
    set _option_(treeShowWidgetIcon)  [Option::GetOption treeShowWidgetIcon]
    set _option_(treeShowWidgetClass) [Option::GetOption treeShowWidgetClass]
    set _option_(treeShowSetVariable) [Option::GetOption treeShowSetVariable]
    set _option_(treeShowGeometry)    [Option::GetOption treeShowGeometry]

    # delete all current canvas objects; redraw is from scratch
    $_widgetCanvas_ delete all

    # restore the currently selected widget name to normal text;
    # it will be re-highlighted below
    if { $_lastWidgetLabel_ != {} && [winfo exists $_lastWidgetLabel_] } {
	SetLabelColourNormal $_lastWidgetLabel_
    }

    # draw the tree (a recursive call)
    DrawWidgetTree
    $_widgetCanvas_ configure -scrollregion [$_widgetCanvas_ bbox all]

    # highlight the current node
    set currentNode [Tree::GetCurrentNode]
    set nodeLabelWidget [GetLabelFromNodeNumber $currentNode]
    SetLabelColourHighlight $nodeLabelWidget
    set _lastWidgetLabel_ $nodeLabelWidget

    # need to do this in case the redraw is for a brand new tree,
    # in which root focus has to be established
    focus $nodeLabelWidget
    
}



proc WidgetTree::DrawWidgetTree { {nodeNumber 0} {x 10} {y 20} {xpos 0} } {

    variable _widgetCanvas_
    variable _lastWidgetLabel_

    variable _option_

    variable _prev_
    variable _next_
    variable _lastDrawnNode_

    variable _xpos_
    variable _ypos_
    variable _maxYPos_
    set _ypos_($nodeNumber) $_maxYPos_
    incr _maxYPos_
    set _xpos_($nodeNumber) $xpos
    
    set canvas $_widgetCanvas_
    incr x 15
    set dx [expr $x-2]

    # draw horizontal branch line (for all but project node)
    if { $nodeNumber != 0 } {
    	# check if node has a specified parent name or widget name is path
    	# that overrides widget hierarchy
    	if { [Properties::NodeOverridesWidgetHierarchy $nodeNumber] } {
	    set lineColour red ; # indicates start of new naming in tree
    	} else {
	    set lineColour black ; # indicates name based on tree hierarchy
    	}
    	$canvas create line [expr $x-15] $y [expr $x-3] $y -tags line -fill $lineColour
    }

    # select appropriate node box, based on whether it has children
    # and whether its expanded or not
    if { [GetNodeItem childList $nodeNumber] != {} } {
    	if { [GetNodeItem expanded $nodeNumber] } {
	    set boxImage boxDash
    	} else {
	    set boxImage boxCross
    	}
	# make the box (dash or cross) for this node                
	set box [$canvas create image $x $y -anchor center -image \
		     $Icon::_icon_($boxImage) -tags box]
	$canvas bind $box <ButtonPress-1> \
	    "WidgetTree::WidgetBoxSelection $canvas $nodeNumber $canvas.$nodeNumber"
	incr dx 10
    }

    if { $_option_(treeShowWidgetIcon) } {
	set widgetType [Tree::GetNodeItem widgetType $nodeNumber]
	if { [Icon::IconExists $widgetType] } {
	    set icon [$canvas create image $dx $y -anchor w -image \
			  $Icon::_icon_($widgetType) -tags icon]
	    $canvas bind $icon <ButtonPress-1> "WidgetTree::WidgetNameSelection 1 $nodeNumber \
				$canvas.$nodeNumber %X %Y"
	    incr dx 16
	}
    }

    if { $_option_(treeShowGeometry) && ![Special::NodeIsSpecialObject $nodeNumber] } {
	if { [Layout::GetLayoutType $nodeNumber] != "none" } {
	    set icon [$canvas create image $dx $y -anchor w -image \
			  $Icon::_icon_([Layout::GetLayoutType $nodeNumber]) -tags icon]
	    $canvas bind $icon <ButtonPress-1> "WidgetTree::WidgetNameSelection 1 $nodeNumber \
				$canvas.$nodeNumber %X %Y"
	    incr dx 16
	}
    }

    # get the text for the node name label
    set text [GetNodeItem name $nodeNumber]

    # check if the widget class should be displayed in tree
    if { $_option_(treeShowWidgetClass) } {
    	set text "$text <[GetNodeItem widgetType $nodeNumber]>"
    }
    # check if the set variable should be displayed in tree
    if { $_option_(treeShowSetVariable) } {
    	set setVariable [Properties::GetSetVariableName $nodeNumber]
    	if { $setVariable != {} } {
	    set text "$text \[$setVariable]"
    	}
    }
    
    # if the label doesn't exist yet (i.e. have just loaded a new file),
    # create it; it exists for the life of the session of this project, unless
    # widget is deleted
    if { ![winfo exists $canvas.$nodeNumber] } {
    	WidgetTree::CreateNodeLabel $nodeNumber
    }

    # set the text for label of this node
    $canvas.$nodeNumber configure -text $text

    # create window on canvas for the label
    $canvas create window $dx $y -anchor w -window $canvas.$nodeNumber \
    	-tags name

    # start and stop y coord for vertical line of this node									  
    set yBegin $y
    set yEnd   $y

    set _prev_($nodeNumber) $_lastDrawnNode_
    set _next_($_lastDrawnNode_) $nodeNumber
    set _lastDrawnNode_ $nodeNumber

    # loop once for each child of this node and draw its tree
    incr xpos
    foreach child_number [GetNodeItem childList $nodeNumber] {
	if { [GetNodeItem expanded $nodeNumber] } {  
	    set yInfo	[DrawWidgetTree $child_number $x [expr $y+15] $xpos]
	    set y		[lindex $yInfo 0]
	    set yEnd	[lindex $yInfo 1]
	}
    }

    # draw vertical branch line from this node
    if { $yBegin != $yEnd } {
	$canvas create line $x [expr $yBegin + 5] $x $yEnd -tags line
    }
    
    return [list $y $yBegin]

}

# scrolls the canvas so that widget 'nodeNumber' becomes visible
proc WidgetTree::MakeNodeVisible { nodeNumber } {
    variable _widgetCanvas_
    variable _xpos_
    variable _ypos_
    variable _maxYPos_

    # try adjusting yview first
    if { ![winfo ismapped [GetLabelFromNodeNumber $nodeNumber]] } {
	set y0 [lindex [$_widgetCanvas_ yview] 0]
	set y1 [lindex [$_widgetCanvas_ yview] 1]
	set dy [expr ($y1 - $y0) / 2.0] ; # half of proportion of canvas displayed
	$_widgetCanvas_ yview moveto [expr ($_ypos_($nodeNumber).0 / $_maxYPos_) - $dy]
	update
    }

    # if still not visible, adjust xview too
    if { ![winfo ismapped [GetLabelFromNodeNumber $nodeNumber]] } {
	set xposPixels [expr $_xpos_($nodeNumber) * 15.0 - 10.0]
	set bbox [$_widgetCanvas_ bbox all]
	set bboxWidth [expr [lindex $bbox 2] - [lindex $bbox 0]]
	$_widgetCanvas_ xview moveto [expr $xposPixels / $bboxWidth]
    }

}

proc WidgetTree::GotoNextNode { nodeNumber } {
    variable _next_
    variable _widgetCanvas_

    # if the current node isn't mapped (off display), try to make node visible
    if { ![winfo ismapped [GetLabelFromNodeNumber $nodeNumber]] } {
	MakeNodeVisible $nodeNumber
	return
    }

    if { [info exists _next_($nodeNumber)] } {
	set nextNode $_next_($nodeNumber)
	set nextLabel [GetLabelFromNodeNumber $nextNode]
	Utility::SetLabelColourNormal [GetLabelFromNodeNumber $nodeNumber]
	Utility::SetLabelColourHighlight $nextLabel
	focus $nextLabel
	SetLastWidgetLabel $nextLabel
	SetLabelColourHighlight $nextLabel
	SetCurrentNode $nextNode
    } else {
	return
    }

    set c $_widgetCanvas_
    set labY [winfo rooty $nextLabel]
    set canBottom [expr [winfo rooty $c] + [winfo height $c] - 20]

    if { $labY > $canBottom || $labY == 0 } {
	$_widgetCanvas_ yview scroll 1 units
    }

    set canRight [expr [winfo rootx $c] + [winfo width $c] - 25]
    set canLeft [expr [winfo rootx $c] + 25]
    set labX [winfo rootx $nextLabel]
    if { $labX > $canRight } {
	$c xview scroll 2 units
    } elseif { $labX < $canLeft } {
	$c xview scroll -2 units
    }

}


proc WidgetTree::GotoPrevNode { nodeNumber } {
    variable _prev_
    variable _widgetCanvas_

    # if the current node isn't mapped (off display), try to make node visible
    if { ![winfo ismapped [GetLabelFromNodeNumber $nodeNumber]] } {
	MakeNodeVisible $nodeNumber
	return
    }
    
    # check if we're at the top-most node
    if { $_prev_($nodeNumber) != -1 } {
	set prevNode $_prev_($nodeNumber)
	set prevLabel [GetLabelFromNodeNumber $prevNode]
	Utility::SetLabelColourNormal [GetLabelFromNodeNumber $nodeNumber]
	Utility::SetLabelColourHighlight $prevLabel
	focus $prevLabel
	SetLastWidgetLabel $prevLabel
	SetLabelColourHighlight $prevLabel
	SetCurrentNode $prevNode
    } else {
	return
    }

    set c $_widgetCanvas_
    set labY [winfo rooty $prevLabel]
    set canTop [expr [winfo rooty $c] + 20]
    if { $labY < $canTop  || $labY == 0 } {
	if { [lindex [$_widgetCanvas_ yview] 0] != 0 } {
	    $_widgetCanvas_ yview scroll -1 units
	}
    }

    set canRight [expr [winfo rootx $c] + [winfo width $c] - 25]
    set canLeft [expr [winfo rootx $c] + 25]
    update idletasks
    set labX [winfo rootx $prevLabel]
    if { $labX < $canLeft } {
	$c xview scroll -1 units
    } elseif { $labX > $canRight } {
	# when going to previous, label could be far right (off screen)
	set units [expr int(($labX - $canRight) / 2)]
	$c xview scroll $units units
    }
    
}


proc WidgetTree::ToggleNodeExpansion { nodeNumber } {
    if { [Tree::GetNodeItem childList $nodeNumber] == {} } {
	return
    }
    if { [Tree::GetNodeItem expanded $nodeNumber] == 0 } {
	Tree::SetNodeItem expanded $nodeNumber 1
    } else {
	Tree::SetNodeItem expanded $nodeNumber 0
    }
    RedrawWidgetTree
}


# returns the node number of the node being displayed with 'label'
# (label name is of form $canvas.NODENUMBER)
proc WidgetTree::GetNodeNumberFromLabel { label } {
    return [string trim [file extension $label] .]
}


# returns the name of the node label widget of 'nodeNumber'
proc WidgetTree::GetLabelFromNodeNumber { nodeNumber } {
    variable _widgetCanvas_
    return $_widgetCanvas_.$nodeNumber
}


# creates the label widget used to display the name of node 'nodeNumber'
# in the widget tree; bindings that do not require node number are managed
# through the NodeLabel binding tag and defined when the canvas is created
proc WidgetTree::CreateNodeLabel { nodeNumber } {
    variable _widgetCanvas_
    
    set lab [label $_widgetCanvas_.$nodeNumber -padx 1 -pady 0 -bd 1 \
		 -highlightthickness 0]
    bindtags $_widgetCanvas_.$nodeNumber \
	[list $_widgetCanvas_.$nodeNumber Label NodeLabel . all]

    bind $lab <ButtonPress-1>   "WidgetTree::WidgetNameSelection 1 $nodeNumber %W %X %Y"
    bind $lab <Double-ButtonPress-1> "WidgetTree::ToggleNodeExpansion $nodeNumber"
    bind $lab <ButtonPress-3>   "WidgetTree::WidgetNameSelection 3 $nodeNumber %W %X %Y"
    bind $lab <B1-Motion>       "WidgetTree::DragSelection $nodeNumber %W %X %Y"
    bind $lab <ButtonRelease-1> "WidgetTree::DropSelection $nodeNumber %W %X %Y"

    # bindings to manage keyboard traversal
    bind $lab <Key-Up> "WidgetTree::GotoPrevNode $nodeNumber"
    bind $lab <KeyRelease-Up> "WidgetForms::UpdateAllForms $nodeNumber
		Toolbar::UpdateToolbar"
    bind $lab <Key-Down> "WidgetTree::GotoNextNode $nodeNumber"
    bind $lab <KeyRelease-Down> "WidgetForms::UpdateAllForms $nodeNumber
		Toolbar::UpdateToolbar"
    bind $lab <Key-Return> "WidgetTree::ToggleNodeExpansion $nodeNumber"

    # bindings used to move widgets among their siblings
    bind $lab <Control-Key-Up> "Tree::SwapSiblings $nodeNumber prev
		WidgetTree::RedrawWidgetTree
		State::SetState modified 1"
    bind $lab <Control-Key-Down> "Tree::SwapSiblings $nodeNumber next
		WidgetTree::RedrawWidgetTree
		State::SetState modified 1"

}


# destroys the label widget used to display 'nodeNumber' in the tree
proc WidgetTree::DestroyNodeLabel { nodeNumber } {
    variable _widgetCanvas_
    destroy $_widgetCanvas_.$nodeNumber
}


proc WidgetTree::DragSelection { nodeNumber label X Y } {

    variable _dragging_
    variable _draggedLabel_
    variable _widgetCanvas_
    variable _lastLabel_
    
    # can't move root node
    if { $nodeNumber == 0 } { return }

    # if not yet dragging, initialize drag variables
    if { !$_dragging_ } {
	set _dragging_ 1
	set _draggedLabel_ $label
	set _lastLabel_    $label
	return
    }

    # get name of widget currently under mouse
    set currentLabel [winfo containing $X $Y]

    # check if the current widget is on the widget tree canvas
    if { $currentLabel == "" || [winfo parent $currentLabel] != $_widgetCanvas_ } {
	$_widgetCanvas_ config -cursor pirate
	if { $_lastLabel_ != $_draggedLabel_ } {
	    SetLabelColourNormal $_lastLabel_
	}
	return
    }
    $_widgetCanvas_ config -cursor cross

    # set the colours for the last and current label properly (note
    # the last and current may be the same ... no matter)
    if { $_lastLabel_ != $_draggedLabel_ } {
	SetLabelColourNormal $_lastLabel_
    }	
    if { $currentLabel != $_draggedLabel_ } {
	$currentLabel config -bg red -fg white -relief raised
    }
    set _lastLabel_ $currentLabel

}



proc WidgetTree::DropSelection { dragFromNode label X Y } {

    variable _dragging_
    variable _draggedLabel_
    variable _widgetCanvas_
    variable _lastLabel_
    variable _dragFromNode_
    variable _dropToNode_

    # check if in dragging mode
    if { !$_dragging_ } { return }

    # get name of widget currently under mouse
    set currentLabel [winfo containing $X $Y]

    # reset drag variables
    set _dragging_ 0
    $_widgetCanvas_ configure -cursor arrow

    # check if the drop occured on other than a widget tree label
    if { $currentLabel == "" || [winfo parent $currentLabel] != $_widgetCanvas_ } {
	# return the last selection to its normal display
	if { $_lastLabel_ != $_draggedLabel_ } {
	    SetLabelColourNormal $_lastLabel_
	}
	return
    }
    
    # get node number from current label name
    set dropToNode [WidgetTree::GetNodeNumberFromLabel $currentLabel]
    
    # if its the dragged node, don't do anything
    if { $dragFromNode == $dropToNode } { return }
    
    # if the drop to node is on branch of dragged from node, error message
    if { [BranchContainsNode $dragFromNode $dropToNode] } {
	ErrorMessage "[mc {Cannot move widget to one of its descendants}]"
	if { $_lastLabel_ != $_draggedLabel_ } {
	    SetLabelColourNormal $_lastLabel_
	}
	return
    }

    set _dragFromNode_ $dragFromNode
    set _dropToNode_   $dropToNode
    if { ![Option::GetOption moveUsingDefaultRelation] } {
	# post menu that will call 'WidgetTree::MoveWidget' to get user's choice
	# of sibling or child relation
	UpdateWidgetMoveMenu $dropToNode
	Menu::PostPopupMenu [Menu::GetWidgetMoveMenu] $X $Y
    } else {
	WidgetTree::MoveWidget [Main::GetDefaultRelation [Tree::GetNodeItem widgetType $dropToNode]]
    }
    SetLabelColourNormal $_lastLabel_

}


# moves the widget at _dragFromNode_ to _dropToNode_ based on the relation 'relation';
# called by the 'Menu::GetWidgetMoveMenu' menu
proc WidgetTree::MoveWidget { relation } {

    variable _dragFromNode_
    variable _dropToNode_

    if { $relation == {} || $relation == "CANCEL" } { return 0 }

    if { $relation == "SIBLING" } {

	# if drag-from node is a file, ensure parent will be project
	if { [Tree::GetNodeItem widgetType $_dragFromNode_] == "file" } {
	    if { ![ParentWouldBeProject $_dropToNode_ $relation] } {
		return 0
	    }
	}

	# if drag-from and drop-to nodes aren't already siblings, check for 
	# duplicate names (allows for shuffling of siblings)
	if { [lsearch [GetSiblingList $_dragFromNode_] $_dropToNode_] == -1 } {
	    if { ![CheckWidgetName $_dropToNode_ \
		       [GetNodeItem name $_dragFromNode_] $relation] } {
		return 0
	    }
	}
	# if moved widget is a canvas object, parent of its sibling-to-be must 
	# be a canvas widget
	if { [Canvas::NodeIsCanvasObject $_dragFromNode_] } {
	    set parentNumber [GetNodeItem parentNumber $_dropToNode_]
	    if { [Tree::GetNodeItem widgetType $parentNumber] != "canvas" } {
		Utility::ErrorMessage "[mc {Parent of canvas object must be a canvas widget}]"
		return 0
	    }
	}
	# if moved widget is a menu entry, parent of its sibling-to-be must 
	# be a menu widget
	if { [MenuEntry::NodeIsMenuEntry $_dragFromNode_] } {
	    set parentNumber [GetNodeItem parentNumber $_dropToNode_]
	    if { [Tree::GetNodeItem widgetType $parentNumber] != "menu" } {
		Utility::ErrorMessage "[mc {Parent of menu entry must be a menu widget}]"
		return 0
	    }
	}
	# DON'T MESS WITH THE ORDER OF THESE CALLS!!!!
	ReplaceChildNumber $_dragFromNode_
	set parentNumber [GetNodeItem parentNumber $_dropToNode_]
	set childList    [GetNodeItem childList $parentNumber]
	SetNodeItem parentNumber $_dragFromNode_ $parentNumber
	set index [expr [lsearch $childList $_dropToNode_] + 1]
	SetNodeItem childList $parentNumber [linsert $childList $index $_dragFromNode_]
    }

    if { $relation == "CHILD" } {

	# if drag-from node is a file, ensure parent will be project
	if { [Tree::GetNodeItem widgetType $_dragFromNode_] == "file" } {
	    if { ![ParentWouldBeProject $_dropToNode_ $relation] } {
		return 0
	    }
	}

	# if drag-from node isn't already child of drop-to node, check for 
	# duplicate names (allows shuffling of children)
	if { [lsearch [GetNodeItem childList $_dropToNode_] $_dragFromNode_] == -1 } {
	    if { ![CheckWidgetName $_dropToNode_ \
		       [GetNodeItem name $_dragFromNode_] $relation] } {
		return 0
	    }
	}
	# if moved widget is a canvas object, its parent-to-be must be a canvas widget
	if { [Canvas::NodeIsCanvasObject $_dragFromNode_] } {
	    if { [Tree::GetNodeItem widgetType $_dropToNode_] != "canvas" } {
		Utility::ErrorMessage "[mc {Parent of canvas object must be a canvas widget}]"
		return 0
	    }
	}
	# parent-to-be cannot be a canvas object (canvas objects cannot be parents)
	if { [Canvas::NodeIsCanvasObject $_dropToNode_] } {
	    Utility::ErrorMessage "[mc {Canvas objects cannot be parents}]"
	    return 0
	}
	# if moved widget is a menu entry, its parent-to-be must be a menu widget
	if { [MenuEntry::NodeIsMenuEntry $_dragFromNode_] } {
	    if { [Tree::GetNodeItem widgetType $_dropToNode_] != "menu" } {
		Utility::ErrorMessage "[mc {Parent of menu entry must be a menu widget}]"
		return 0
	    }
	}
	# parent-to-be cannot be a menu entry (menu entries cannot be parents)
	if { [MenuEntry::NodeIsMenuEntry $_dropToNode_] } {
	    Utility::ErrorMessage "[mc {Menu entries cannot be parents}]"
	    return 0
	}
	# DON'T MESS WITH THE ORDER OF THESE CALLS!!!!
	ReplaceChildNumber $_dragFromNode_
	SetNodeItem parentNumber $_dragFromNode_ $_dropToNode_
	set childList [GetNodeItem childList $_dropToNode_]
	lappend childList $_dragFromNode_
	SetNodeItem childList $_dropToNode_ $childList
	SetNodeItem expanded $_dropToNode_ 1
    }

    RedrawWidgetTree
    State::SetState modified 1
    return 1

}



proc WidgetTree::AddWidget { widgetType relation } {

    set currentNode [GetCurrentNode]

    if { [Project::NodeIsProject $currentNode] && $relation == "SIBLING" } {
	Utility::ErrorMessage "[mc {Project cannot have siblings}]"
	return -1
    }

    # check if this is to be the root/main toplevel node
    if { $widgetType == [Project::GetRootName] } {
	if { $relation == "CHILD" } {
	    SetNodeItem expanded $currentNode 1 ; # ensure expanded to see child
	}
	set newNode [InsertNode $currentNode [Project::GetRootName] $relation toplevel 0]
	#		CreateNodeLabel $newNode
	SetCurrentNode $newNode
	Properties::InitializeWidgetProperties $newNode
	RedrawWidgetTree
	State::SetState modified 1
	UpdateAllForms $newNode
	Toolbar::UpdateToolbar
	focus [GetLabelFromNodeNumber $newNode]
	return $newNode
    }
    
    if { $widgetType == "file" } {
	if { [ParentWouldBeProject $currentNode $relation] } {
	    if { [State::GetState displayTclFileWarning] } {
		Utility::InfoMessage \
		    "[mc {Note}]: [mc {A tcl file object lets you name a tcl file that will}]\n\
					[mc {be created by tkBuilder to store tcl code for the file's}]\n\
					[mc {descendants. If the file already exists, it will be over-}]\n\
					[mc {written. Tcl file objects do not import existing tcl code}]."
		State::SetState displayTclFileWarning 0
	    }
	    set filename [File::GetTclFileName]
	    if { $filename != {} } {
		if { ![File::TclFileNameExists $filename] } {
		    File::AddTclFileName $filename
		    set widgetName [file tail $filename] ; # put the tail name in tree
		    if { $relation == "CHILD" } {
			SetNodeItem expanded $currentNode 1 ; # ensure expanded to see child
		    }
		    set newNode [InsertNode $currentNode $widgetName $relation $widgetType 0]
		    #					CreateNodeLabel $newNode
		    SetCurrentNode $newNode
		    Properties::InitializeWidgetProperties $newNode
		    RedrawWidgetTree
		    State::SetState modified 1
		    Special::SetSpecialObject file name $newNode $filename
		    UpdateAllForms $newNode
		    Toolbar::UpdateToolbar
		    focus [GetLabelFromNodeNumber $newNode]
		    return $newNode
		}
	    }
	}
	return -1
    }

    # prompt user for name of widget
    if { ![ChildWouldBeFile $currentNode $relation] } {
	set dialogReply [Dialog::EnableNameEntry "$widgetType:"]
	if { [lindex $dialogReply 0] == "OK" } {
	    set widgetName [lindex $dialogReply 1]
	    if { [CheckWidgetName $currentNode $widgetName $relation] } {
		if { $relation == "CHILD" } {
		    SetNodeItem expanded $currentNode 1 ; # ensure expanded to see child
		}
		set newNode [InsertNode $currentNode $widgetName $relation $widgetType 0]
		#				CreateNodeLabel $newNode
		SetCurrentNode $newNode
		Properties::InitializeWidgetProperties $newNode
		RedrawWidgetTree
		State::SetState modified 1
		UpdateAllForms $newNode
		Toolbar::UpdateToolbar
		focus [GetLabelFromNodeNumber $newNode]
		return $newNode
	    }
	}
    }
    return -1	
}


proc WidgetTree::ParentWouldBeProject { currentNode relation } {
    switch $relation {
	PARENT - SIBLING {
	    set currentParent [Tree::GetNodeItem parentNumber $currentNode]
	    if { [Project::NodeIsProject $currentParent] } {
		return 1
	    }
	}
	CHILD {
	    if { [Project::NodeIsProject $currentNode] } {
		return 1
	    }
	}
    }
    Utility::ErrorMessage "[mc {Tcl file objects must be}]\n\
							[mc {children of the project}]"
    return 0
}


proc WidgetTree::ChildWouldBeFile { currentNode relation } {
    set widgetType [Tree::GetNodeItem widgetType $currentNode]
    if { $relation == "PARENT" && $widgetType == "file" } {
	Utility::ErrorMessage "[mc {Parent of tcl file object}]\n\
								[mc {must be project}]"
	return 1
    } else {
	return 0
    }
}


# allows user to rename the current widget	
proc WidgetTree::RenameWidget { } {
    
    set currentNode [GetCurrentNode]

    # to rename project node, select new file name
    if { [Project::NodeIsProject $currentNode] } {
	File::SaveFileAs
	return
    }
    
    # cannot rename root/main widget
    if { [Project::NodeIsRoot $currentNode] } {
	Utility::ErrorMessage "[mc {Cannot rename}] [Project::GetRootName] toplevel"
	return
    }

    set nodeName    [GetNodeItem name $currentNode]
    set widgetType  [GetNodeItem widgetType $currentNode]

    if { $widgetType == "file" } {
	set oldFilename [Special::GetSpecialObject file name $currentNode]
	set filename [File::GetTclFileName $oldFilename]
	if { $filename != {} } {
	    if { ![File::TclFileNameExists $filename] } {
		File::AddTclFileName $filename
		File::RemoveTclFileName $oldFilename
		Special::SetSpecialObject file name $currentNode $filename
		set widgetName [file tail $filename] ; # put the tail name in tree
		SetNodeItem name $currentNode $widgetName
		# change the text attribute of the node label to the new name
		[GetLabelFromNodeNumber $currentNode] configure -text $widgetName
		RedrawWidgetTree
		Status::UpdateStatusBar
		State::SetState modified 1
		UpdateAllForms $currentNode 1
		focus -force [GetLabelFromNodeNumber $currentNode]
	    }
	}
	return
    }

    set dialogReply [Dialog::EnableNameEntry "Rename:" $nodeName]
    if { [lindex $dialogReply 0] == "OK" } {
	set widgetName [lindex $dialogReply 1]
	if { [CheckWidgetName $currentNode $widgetName SIBLING] } {
	    SetNodeItem name $currentNode $widgetName
	    # change the text attribute of the node label to the new name
	    [GetLabelFromNodeNumber $currentNode] configure -text $widgetName
	    RedrawWidgetTree
	    Status::UpdateStatusBar
	    State::SetState modified 1
	}
    }

}


# creates a new widget, prompting for a new name, and duplicating the state 
# of the current widget
proc WidgetTree::DuplicateWidget { } {

    set currentNode [GetCurrentNode]
    set nodeName    [GetNodeItem name $currentNode]
    set widgetType  [GetNodeItem widgetType $currentNode]

    # can't duplicate project
    if { [Project::NodeIsProject $currentNode] } {
	Utility::ErrorMessage "[mc {Cannot duplicate project}]"
	return
    }

    # can't duplicate root/main toplevel
    if { [Project::NodeIsRoot $currentNode] } {
	Utility::ErrorMessage "[mc {Cannot duplicate}] [Project::GetRootName] toplevel"
	return
    }

    # can't duplicate file objects
    if { $widgetType == "file" } {
	Utility::ErrorMessage "[mc {Cannot duplicate tcl file objects}]"
	return
    }

    # prompt user for name of the new widget
    set dialogReply [Dialog::EnableNameEntry "$widgetType:" $nodeName]
    if { [lindex $dialogReply 0] == "OK" } {
	set widgetName [lindex $dialogReply 1]
	if { [CheckWidgetName $currentNode $widgetName SIBLING] } {
	    set newNode [Tree::InsertNode $currentNode $widgetName SIBLING $widgetType 0]
	    Tree::SetCurrentNode $newNode
	    Properties::CopyWidgetProperties $currentNode $newNode
	    RedrawWidgetTree
	    State::SetState modified 1
	    UpdateAllForms $newNode
	    focus -force [GetLabelFromNodeNumber $newNode]
	}
    }
}


# duplicates a branch of the tree, beginning at the current node
proc WidgetTree::DuplicateBranch { } {
    
    set currentNode [GetCurrentNode]
    set nodeName    [GetNodeItem name       $currentNode]
    set widgetType  [GetNodeItem widgetType $currentNode]
    set expanded    [GetNodeItem expanded   $currentNode]

    # can't duplicate branch if it starts at project
    if { [Project::NodeIsProject $currentNode] } {
	Utility::ErrorMessage "[mc {Cannot duplicate project}]"
	return
    }

    # can't duplicate branches that have files
    if { [File::BranchContainsFile $currentNode] } {
	Utility::ErrorMessage "[mc {Cannot duplicate branch that}]\n\
			[mc {contains tcl file object}]"
	return
    }

    # can't duplicate branch that has root/main toplevel
    if { [Project::RootNodeExists] } {
	if { [Project::BranchContainsRoot $currentNode] } {
	    Utility::ErrorMessage "[mc {Cannot duplicate branch that}]\n\
				[mc {contains}] [Project::GetRootName] toplevel"
	    return
	}
    }

    set dialogReply [Dialog::EnableNameEntry "$widgetType:" $nodeName]
    if { [lindex $dialogReply 0] == "OK" } {
	set widgetName [lindex $dialogReply 1]
	if { [CheckWidgetName $currentNode $widgetName SIBLING] } {
	    set newNode [InsertNode $currentNode $widgetName SIBLING $widgetType $expanded]
	    Properties::CopyWidgetProperties $currentNode $newNode
	    DuplicateBranchRecurse $currentNode $newNode
	    Tree::SetCurrentNode $newNode
	    RedrawWidgetTree
	    State::SetState modified 1
	    UpdateAllForms $newNode
	    focus [GetLabelFromNodeNumber $newNode]
	}
    }
}


# the recursive function used for duplicating branches, initially called
# by DuplicateBranch
proc WidgetTree::DuplicateBranchRecurse { fromNode toNode } {

    # loop once for each child of the from-node
    foreach child [GetNodeItem childList $fromNode] {
	
	# get from-node attributes
	set name       [GetNodeItem name       $child]
	set widgetType [GetNodeItem widgetType $child]
	set expanded   [GetNodeItem expanded   $child]
	
	# create a to-node and copy attributes from from-node
	set newNode [InsertNode $toNode $name CHILD $widgetType $expanded]
	Properties::CopyWidgetProperties $child $newNode
	DuplicateBranchRecurse  $child $newNode
	
    }
}


proc WidgetTree::CopyWidgetToSiblings { } {
    set currentNode [GetCurrentNode]
    set widgetType  [GetNodeItem widgetType $currentNode]
    if { [GetOption confirmCopyToSiblings] } {
	set response [ConfirmMessage "[mc {Copy the properties of this widget}]\n\
 	 			[mc {to all its sibling}] $widgetType widgets?"]
	if { $response == "no" } { return }
    }
    foreach sibling [GetSiblingList $currentNode] {
	if { [GetNodeItem widgetType $sibling] == $widgetType } {
	    Properties::CopyWidgetProperties $currentNode $sibling
	}
    }
    # need to redraw in case copy modifies geometry manager,
    # while the geometry icons are displayed
    WidgetTree::RedrawWidgetTree
    State::SetState modified 1
}



proc WidgetTree::CopyWidgetToDescendants { } {
    set currentNode [GetCurrentNode]
    set widgetType  [GetNodeItem widgetType $currentNode]
    if { [GetOption confirmCopyToDescendants] } {
	set response [ConfirmMessage "Copy the properties of this widget\n\
 	 			[mc {to all its descendant}] $widgetType widgets?"]
	if { $response == "no" } { return }
    }
    CopyWidgetRecurse $currentNode $currentNode $widgetType
    # need to redraw in case copy modifies geometry manager,
    # while the geometry icons are displayed
    WidgetTree::RedrawWidgetTree
    State::SetState modified 1
}


# to start from root (i.e. copy to all widgets), set toNode to 0 in first call
proc WidgetTree::CopyWidgetRecurse { fromNode toNode widgetType } {
    if { $fromNode != $toNode } {
	set toWidgetType [GetNodeItem widgetType $toNode]
	if { $toWidgetType == $widgetType } {
	    Properties::CopyWidgetProperties $fromNode $toNode
	}
    }
    foreach child [GetNodeItem childList $toNode] {
	CopyWidgetRecurse $fromNode $child $widgetType
    }
}


# deletes a single widget, the current node; should be used only
# if the current node has 0 or 1 children (if it has more than
# than 1 child, we don't know which one to make the new parent)
# ::Tree::DeleteNode checks to ensure this is the case
proc WidgetTree::DeleteWidgetNode { } {

    set currentNode [GetCurrentNode]
    set childList   [GetNodeItem childList $currentNode]

    # can't delete project
    if { [Project::NodeIsProject $currentNode] } {
	Utility::ErrorMessage "[mc {Cannot delete project}]"
	return
    }

    # can't delete node with more than 1 child
    if { [llength $childList] > 1 } {
	Utility::ErrorMessage "[mc {Cannot delete a widget}]\n\
			[mc {that has more than one child}]\n\
			[mc {(use Delete Branch)}]"
	return
    }

    if { [GetOption confirmDeleteNode] } {
	set response [ConfirmMessage "[mc {Delete this widget}]?"]   
	if { $response == "no" } { return }
    }

    # check if this is the root node
    if { [Project::NodeIsRoot $currentNode] } {
	Project::FreeRootNode
    }

    # if node has siblings, use one of them for new node; else if it
    # has one child, use that; else use parent
    if { $childList == {} } {
	# try to set current node as one of the sibs
	set sibNodeNumber [Tree::GetNearestSibling $currentNode]
	if { $sibNodeNumber != {} } {
	    set newNode $sibNodeNumber
	} else {
	    set newNode [GetNodeItem parentNumber $currentNode]
	}
    } else {
	set newNode [GetNodeItem childList $currentNode]
    }

    SetCurrentNode $newNode
    # N.B. DeleteNode calls Properties::DeleteWidgetProperties
    DeleteNode $currentNode
    SetLastWidgetLabel {}
    RedrawWidgetTree
    State::SetState modified 1
    UpdateAllForms $newNode
    Toolbar::UpdateToolbar
    focus [GetLabelFromNodeNumber $newNode]
}


# deletes the current widget and all its descendants
proc WidgetTree::DeleteWidgetBranch { } {

    set currentNode [GetCurrentNode]

    # can't delete project node
    if { [Project::NodeIsProject $currentNode] } {
	Utility::ErrorMessage "[mc {Cannot delete project}]"
	return
    }

    if { [GetOption confirmDeleteBranch] } {
	if { [GetNodeItem childList $currentNode] == {} } {
	    set message "[mc {Delete this widget}]?"
	} else {
	    set message "[mc {Delete this widget and all its descendants}]?"
	}
	set response [ConfirmMessage $message]   
	if { $response == "no" } { return }
    }

    # if node has siblings, use one of them for new node; 
    # else use parent
    set sibNodeNumber [Tree::GetNearestSibling $currentNode]
    if { $sibNodeNumber != {} } {
	set newNode $sibNodeNumber
    } else {
	set newNode [GetNodeItem parentNumber $currentNode]
    }
    
    SetCurrentNode $newNode
    # N.B. DeleteBranch calls Properties::DeleteWidgetProperties
    set numDeleted [DeleteBranch $currentNode 0]
    SetLastWidgetLabel {}
    RedrawWidgetTree
    State::SetState modified 1
    UpdateAllForms $newNode
    Toolbar::UpdateToolbar
    if { [GetOption messageWidgetsDeleted] && $numDeleted > 1 } {
	tk_messageBox -type ok -default ok -title tkBuilder \
	    -icon info -message "$numDeleted widgets [mc {deleted}]"
    }
    focus [GetLabelFromNodeNumber $newNode]
}


proc WidgetTree::CheckWidgetName { current_node widgetName relation } {

    # don't bother check name of main/root toplevel; in that case,
    # this is called only because of move (user can't specify name
    # of main/root toplevel)
    if { $widgetName == [Project::GetRootName] } {
	return 1
    }

    if { $widgetName == {} } {
	Utility::ErrorMessage "[mc {No name specified}]"
	return 0
    }

    # check that the name is exactly one word in size
    if { [llength $widgetName] != 1 } {
	ErrorMessage "[mc {Invalid name}]: '$widgetName'.\n\
			[mc {Must be single word}]."
	return 0
    }

    # check that the name starts with a lower case letter, unless this is a
    # special object
    #	set firstChar [string index $widgetName 0]
    #	if { [string tolower $firstChar] != $firstChar } {
    #		ErrorMessage "[mc {Invalid widget name}]: '$widgetName'.\n\
	#			[mc {First character must be lower case}]."
    #		return 0
    #	}

    # check for an embedded dot
    #	if { [string first "." $widgetName] != -1 } {
    #		ErrorMessage "[mc {Invalid widget name}]: '$widgetName'.\n\
	#  			[mc {Embedded dot (.) not allowed}]."
    #  			return 0
    #	}

    # check that widget name is not already assigned to one of the
    # parent's children (siblings of this widget)
    if { $relation == "PARENT" } {
	# when inserting a parent relation, name of new node can be
	# the same as the current node, since current node becomes
	# child of new node
	if { [GetNodeItem name $current_node] == $widgetName } {
	    return 1
	}
	set parentNumber [GetNodeItem parentNumber $current_node]
	set childNumberList [GetNodeItem childList $parentNumber]
	set relationText sibling
    } elseif { $relation == "SIBLING" } {
	set parentNumber [GetNodeItem parentNumber $current_node]
	set childNumberList [GetNodeItem childList $parentNumber]
	set relationText sibling
    } elseif { $relation == "CHILD" } {
	set childNumberList [GetNodeItem childList $current_node]
	set relationText child
    } else {
	error "[mc {Bad relation}]: $relation"
    }
    foreach childNumber $childNumberList {
	if { [GetNodeItem name $childNumber] == $widgetName } {
	    ErrorMessage "[mc {Widget name}] '$widgetName' [mc {already exists}]\n\
				[mc {for a}] $relationText [mc {of this widget}]."
	    return 0
	}
    }

    return 1

}


# procedure that is run when user clicks on box in the tree
proc WidgetTree::WidgetBoxSelection { canvas nodeNumber labelWidget } {
    
    # if the node has no children, no need to take action           
    if { [GetNodeItem childList $nodeNumber] == {} } {
	return
    }
    
    # toggle the node between expanded and not-expanded                                          
    if { [GetNodeItem expanded $nodeNumber] } {
	SetNodeItem expanded $nodeNumber 0
	# check if the branch from this node contains the current node;
	# if so, set this node as current so a current node can be shown
	# when branch collapses on redraw
	if { [BranchContainsNode $nodeNumber [GetCurrentNode]] } {
	    # necessary so that <Focus-Out> event is triggered on property text
	    # entry windows before the current node number is changed
	    focus $labelWidget
	    update
	    SetCurrentNode $nodeNumber
	    UpdateAllForms $nodeNumber
	    Toolbar::UpdateToolbar
	}
    } else {                
	SetNodeItem expanded $nodeNumber 1      
    }

    RedrawWidgetTree
    
}


# procedure that is run when user clicks on a widget name label in the tree   
proc WidgetTree::WidgetNameSelection { button nodeNumber W X Y} {

    variable _widgetCanvas_

    # 020800: there is a bug that, under some circumstances causes a call to this
    # function when W does not exist; say, at start of new project, create a button, 
    # then open big.tcl (w/o starting new) ... this will catch it, but really need
    # to find source of this bug (this proc shouldn't be called if W doesn't exist)
    if { ![winfo exists $W] } {
	return
    }

    # necessary so that <Focus-Out> event is triggered on property text entry windows
    # before the current node number is changed
    focus $W
    update

    # restore last name to normal text
    # NB had to add the winfo check because of same errors as above
    if { [GetLastWidgetLabel] != {} && [winfo exists [GetLastWidgetLabel]]} {
	SetLabelColourNormal [GetLastWidgetLabel]
    }

    SetLastWidgetLabel $W
    SetLabelColourHighlight $W
    SetCurrentNode $nodeNumber	
    UpdateAllForms $nodeNumber
    
    Toolbar::UpdateToolbar

    # open the widget edit menu if right button clicked
    if { $button == 3 } {
	Menu::PostPopupMenu [Menu::GetWidgetEditCloneMenu] $X $Y
    }

}                                

proc WidgetTree::SetLastWidgetLabel { widget } {
    variable _lastWidgetLabel_
    set _lastWidgetLabel_ $widget
}

proc WidgetTree::GetLastWidgetLabel { } {
    variable _lastWidgetLabel_
    return $_lastWidgetLabel_
}