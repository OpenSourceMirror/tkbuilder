# -------------------------------------------------------------
# option.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------



namespace eval Option {
    
    namespace import ::Button::MakeButtonSet                             
    namespace import ::Attribute::GetAllAttributesList
    namespace import ::Attribute::SetHiddenAttributesList
    
    variable _winOption_  ; # name of toplevel window
    variable _waitOption_ ; # name of tkwait variable
    variable _option_     ; # array of option settings
    variable _revert_     ; # array of current settings in case of cancel
    variable _notebook_   ; # notebook widget of option pages
    variable _page_       ; # array of pages in notebook
    
    variable _lstShownWidgetAttributes_
    variable _lstHiddenWidgetAttributes_
    # names of listboxes containing the hidden and shown widget attributes
    
    variable _lstDefaultEventList_
    # listbox of default events
    
    variable _optionDatabaseFile_ "tkbOptions.txt"
    # name of file in which to store the options
    
    namespace export InitializeOptions OpenOptionDialog GetOption

}


proc Option::InitializeOptions { } {

    variable _option_
    
    SetOption confirmDeleteNode	       1
    SetOption confirmDeleteBranch      1
    SetOption confirmExitProgram       1
    SetOption confirmCopyToSiblings    1
    SetOption confirmCopyToDescendants 1
    SetOption confirmDeleteEvents      1
    SetOption messageWidgetsDeleted    1

    SetOption hiddenWidgetAttributes     {} ; # list of hidden attributes
    SetOption showHiddenAttributes       0
    SetOption buildWithHiddenAttributes  0
    
    SetOption recentFileNumber  4
    
    SetOption defaultEventList  [Binding::GetSystemDefaultEventList]
    
    SetOption treeShowWidgetIcon   1
    SetOption treeShowWidgetClass  1
    SetOption treeShowSetVariable  1
    SetOption treeShowGeometry     1
    # set only via View menu
    
    SetOption statusBarShow 1
    # set only via View menu
    
    SetOption buildOpenConsole       0
    SetOption buildWithdrawRoot      0
    SetOption buildSetVariableAsPath 0
    SetOption buildCheckCanvasObjectCoord 1
    SetOption buildBlankBeforeWidget    0
    SetOption buildBlankBeforeProc      0
    SetOption buildBlankBeforeNamespace 0
    SetOption buildBlankBeforeCode      0
    SetOption buildIndentUseSpaces  0
    SetOption buildIndentNumSpaces  5
    SetOption buildBindToAppWidgets 1
    SetOption buildEnableAppModification  1

    SetOption registryWidgetList {}
    SetOption registryAutoPath   {}
    SetOption registryPackages   {}
    SetOption registryNamespaces {}

    SetOption resourceDatabase {}

    SetOption copyAttributesMode SAME_WIDGET_TYPE ; # SAME_WIDGET_TYPE or ALL_WIDGET_TYPES
    # may also be set in Menu
    
    SetOption copyLayoutManager 1
    # is value of layout manager (e.g. pack) included when copying selected layout attributes?
    # may also be set in Menu

    SetOption copyLayoutIfSameManager 0
    # copy selected layout attributes only if layout manager is the same for both widgets?
    # (to be implemented)
    
    SetOption textFont [Font::GetDefaultFont]
    SetOption textWordWrap 0
    SetOption textTabs {}
    
    SetOption mainToolbarShow 1 ; # set in view menu
    SetOption widgetToolbarShow 1 ; # set in view menu
    
    SetOption tclProjectSave 1 ; # 1 to save project as tcl when saving project
    
    SetOption toolTipsShow 1 ; # set in view menu
    
    SetOption projectBackup 1 ; # 1 to backup project when its opened
    
    SetOption useDefaultRelation 1 ; # 1 to use default relation when inserting with toolbar
    SetOption moveUsingDefaultRelation 0 ; # 1 to use default relation when moving widgets
    
    SetOption geometryManager pack ; # default manager: pack, grid or place; set in menu
    
    SetOption addMainWindowToNewProject 1
    # set to 1 if a main window should be automatically added to new project
    
    SetOption warnNoMainTclFile 1
    # warn user if no main tcl file specified

    ReadOptionDatabaseFile

}


proc Option::OpenOptionDialog { } {

    variable _winOption_ 
    variable _waitOption_
    variable _option_
    variable _revert_

    # to get back to original settings if cancelled
    array set _revert_ [array get _option_]
    
    if { ![winfo exists ._winOption_] } {
    	set _winOption_ [toplevel ._winOption_]  	
	wm withdraw $_winOption_
    	wm title $_winOption_ "Options"
    	wm iconname $_winOption_ "Options"
    	wm protocol $_winOption_ WM_DELETE_WINDOW {set ::Option::_waitOption_ CANCEL}
    	bind $_winOption_ <Key-Escape> {set ::Option::_waitOption_ CANCEL}
    	bind $_winOption_ <Destroy> {set ::Option::_waitOption_ CANCEL}

	# make ok and cancel buttons first, so they remain visible on resize
    	set fraButton [frame $_winOption_.fraButton]
	pack $fraButton -side bottom -anchor e
	MakeButtonSet $fraButton HORIZONTAL ok \
	    ok [mc {OK}] Option::ValidateOptions \
	    cancel [mc {Cancel}] {set ::Option::_waitOption_ CANCEL}
	bind $_winOption_ <Return> Option::ValidateOptions

	# main frame to hold notebook pages
	set fraMain [frame $_winOption_.fraMain]
	pack $fraMain -side top -fill both -expand true
	CreateOptionPages $fraMain
	
	CreateGeneralPage
	CreateEditPage
	CreateAttributesPage
	CreateBindingPage
	CreateBuildPage
	CreateMessagePage
	
	FirstOpening
	
    }

    EachOpening

    wm deiconify $_winOption_
    set callerFocus [focus]
    focus -force $_winOption_
    catch { tkwait visibility $_winOption_ }
    catch { grab $_winOption_ }

    tkwait variable ::Option::_waitOption_
    catch { bind $_winOption_ <Destroy> {} }
    catch { grab release $_winOption_ } 
    focus $callerFocus
    wm withdraw $_winOption_
    
    if { $_waitOption_ == "CANCEL" } {
	EachCancelClosing
    }
    if { $_waitOption_ == "OK" } {
	EachOKClosing
    }

}


# called after widgets are built and after option database file is
# opened and read
proc Option::FirstOpening { } {


}


# this proc is run each time the options dialog is opened
proc Option::EachOpening { } {

    variable _lstShownWidgetAttributes_
    variable _lstHiddenWidgetAttributes_
    variable _lstDefaultEventList_

    # set listboxes of shown and hidden attributes
    set shownWidgetAttributes {}
    set hiddenWidgetAttributes [GetOption hiddenWidgetAttributes]
    foreach attribute [GetAllAttributesList] {
	if { [lsearch $hiddenWidgetAttributes $attribute] == -1 } {
	    lappend shownWidgetAttributes $attribute
	}
    }
    Listbox::ListboxFromList $_lstShownWidgetAttributes_  $shownWidgetAttributes
    Listbox::ListboxFromList $_lstHiddenWidgetAttributes_ $hiddenWidgetAttributes

    # fill the default event listbox
    Listbox::ListboxFromList $_lstDefaultEventList_ [GetOption defaultEventList]

}


# this proc is run each time the options dialog is closed using cancel button
proc Option::EachCancelClosing { } {
    variable _option_
    variable _revert_

    # revert all options back to pre-option-call state
    array set _option_ [array get _revert_]
    
    # return fonts back to original form
    Font::SetFont textFont [GetOption textFont]

    EachClosing

}


# this proc is run each time the options dialog is closed using ok button
proc Option::EachOKClosing { } {

    variable _lstHiddenWidgetAttributes_
    variable _lstDefaultEventList_
    
    # update the hidden attribute list
    SetOption hiddenWidgetAttributes [Listbox::ListboxToList $_lstHiddenWidgetAttributes_]

    # update the default event list
    SetOption defaultEventList [Listbox::ListboxToList $_lstDefaultEventList_]
    
    # update the tree and forms
    WidgetTree::RedrawWidgetTree
    WidgetForms::UpdateAllForms [Tree::GetCurrentNode] 1
    
    # update the number of recent files available
    File::UpdateNumberOfRecentFiles
    
    # update the geometry manager shown in toolbar, incase changed here
    #	Toolbar::UpdateGeometryManager
    
    # update fonts
    SetOption textFont [font actual textFont]
    
    # update text attributes
    Text::SetWordWrap
    Text::SetTabs
    
    EachClosing

}


# this proc is run each time the options dialog is closed regardless of how
proc Option::EachClosing { } {

}

proc Option::ValidateOptions { } {

    # test for valid tab stop
    if { [catch {text .dummy -tabs [Option::GetOption textTabs]} ] } {
	Utility::ErrorMessage "[mc {Bad screen distance for}] \n\
			[mc {tab stops}]: [Option::GetOption textTabs]"
	return
    } else {
	destroy .dummy
    }

    set Option::_waitOption_ OK

}

proc Option::CreateOptionPages { frame } {

    variable _notebook_
    variable _page_

    set _notebook_ [NoteBook $frame.notebook -height 325 -width 400]
    pack $_notebook_ -padx 3 -pady 3

    set _page_(general)    [$_notebook_ insert end general -text [mc {General}]]
    set _page_(edit)       [$_notebook_ insert end edit -text [mc {Edit}]]
    set _page_(attributes) [$_notebook_ insert end attributes -text [mc {Attributes}]]
    set _page_(bindings)   [$_notebook_ insert end bindings -text [mc {Bindings}]]
    set _page_(build)      [$_notebook_ insert end build -text [mc {Build}]]
    set _page_(message)    [$_notebook_ insert end message -text [mc {Messages}]]

    $_notebook_ raise general

}


proc Option::CreateGeneralPage { } {

    variable _option_
    variable _page_
    
    set page $_page_(general)
    
    # make backup copy of the project
    set w1 [checkbutton $page.chkBackupProject \
		-text "[mc {Make backup copy when opening project}]" \
		-variable Option::_option_(projectBackup)]		
    pack $w1 -anchor w -pady 0
    
    # save project as tcl when saving project
    set w1 [checkbutton $page.chkTclProjectSave \
		-text "[mc {Save all tcl files when saving project}]" \
		-variable Option::_option_(tclProjectSave)]		
    pack $w1 -anchor w -pady 0

    # use default relationships when inserting widgets using toolbar
    set w1 [checkbutton $page.chkUseDefaultRelation \
		-text "[mc {Automatically select default relationship in widget toolbar}]" \
		-variable Option::_option_(useDefaultRelation)]		
    pack $w1 -anchor w -pady 0
    
    # use default relationship when moving widgets
    set w1 [checkbutton $page.chkMoveUsingDefaultRelation \
		-text "[mc {Use default relationship when moving widgets}]" \
		-variable Option::_option_(moveUsingDefaultRelation)]		
    pack $w1 -anchor w -pady 0

    # automatically add main toplevel to new project
    set w1 [checkbutton $page.chkAddMainWindowToNewProject \
		-text "[mc {Add main window when starting new project}]" \
		-variable Option::_option_(addMainWindowToNewProject)]		
    pack $w1 -anchor w -pady 0

    #	set f [frame $page.fraGeometryManager]
    #	pack $f -anchor w -pady 0
    #	pack [label $f.labGeo -text "[mc {Default geometry manager}]: "] -side left -anchor center
    #	foreach man {pack grid place} {
    #		pack [radiobutton $f.rad_$man -text $man -value $man \
	#			-variable Option::_option_(geometryManager)] -side left
    #	}

    # number of recent files
    set w1 [label $page.lbFileNumber -text "[mc {Number of recent projects}]: "]
    pack $w1 -side top -anchor nw -pady 3
    set w1 [SpinBox $page.spnFileNumber  \
		-range {0 10 1} -textvariable Option::_option_(recentFileNumber) \
		-width 3]
    pack $w1 -side top -anchor nw -pady 3

}




proc Option::CreateMessagePage { } {

    variable _option_
    variable _page_
    
    set page $_page_(message)

    # confirmation widgets
    TitleFrame $page.fraConfirm -text [mc {Confirm}]
    pack $page.fraConfirm -anchor w -pady 3 -fill x
    set fraConfirm [$page.fraConfirm getframe]

    set w1 [checkbutton $fraConfirm.chkConfirmDeleteNode \
		-text "[mc {Before deleting widget}]" \
		-variable ::Option::_option_(confirmDeleteNode)]		
    pack $w1 -anchor w -pady 0 -padx 0

    set w1 [checkbutton $fraConfirm.chkConfirmDeleteBranch \
		-text "[mc {Before deleting branch}]" \
		-variable ::Option::_option_(confirmDeleteBranch)]		
    pack $w1 -anchor w -pady 0 -padx 0

    set w1 [checkbutton $fraConfirm.chkConfirmCopyToSiblings \
		-text "[mc {Before copying properties to siblings}]" \
		-variable ::Option::_option_(confirmCopyToSiblings)]		
    pack $w1 -anchor w -pady 0 -padx 0

    set w1 [checkbutton $fraConfirm.chkConfirmCopyToDescendants \
		-text "[mc {Before copying properties to descendants}]" \
		-variable ::Option::_option_(confirmCopyToDescendants)]		
    pack $w1 -anchor w -pady 0 -padx 0

    set w1 [checkbutton $fraConfirm.chkConfirmDeleteEvents \
		-text "[mc {Before deleting events}]" \
		-variable ::Option::_option_(confirmDeleteEvents)]		
    pack $w1 -anchor w -pady 0 -padx 0

    set w1 [checkbutton $fraConfirm.chkConfirmExit \
		-text "[mc {Before exiting}] tkBuilder" \
		-variable ::Option::_option_(confirmExitProgram)]		
    pack $w1 -anchor w -pady 0 -pady 0
    
    # report widgets
    TitleFrame $page.fraMessage -text [mc {Report}]
    pack $page.fraMessage -anchor w -pady 3 -fill x
    set fraMessage [$page.fraMessage getframe]

    set w1 [checkbutton $fraMessage.chkMessageWidgetsDeleted \
		-text "[mc {Report count when 2 or more widgets deleted}]" \
		-variable ::Option::_option_(messageWidgetsDeleted)]		
    pack $w1 -anchor w -pady 0 -pady 0

    # warn widgets
    TitleFrame $page.fraWarn -text [mc {Warn}]
    pack $page.fraWarn -anchor w -pady 3 -fill x
    set fraWarn [$page.fraWarn getframe]

    set w1 [checkbutton $fraWarn.chkWarnNoMainTclFile \
		-text "[mc {Warn if no main tcl file specified}]" \
		-variable ::Option::_option_(warnNoMainTclFile)]		
    pack $w1 -anchor w -pady 0 -pady 0

}



proc Option::CreateEditPage { } {

    variable _option_
    variable _page_
    
    set page $_page_(edit)

    # copy selected layout attribute options
    TitleFrame $page.fraCopy -text "[mc {Copying Selected Properties}]"
    pack $page.fraCopy -anchor w -pady 3
    set fraCopy [$page.fraCopy getframe]

    # copy selected attribute mode (same or all)
    set f1 [frame $fraCopy.f1 -bd 0]
    pack $f1 -anchor w -pady 0 -padx 0
    grid [label $f1.labCopy -text "[mc {Copy properties}]"] -row 0 -column 0 -sticky w
    grid [radiobutton $f1.radCopy1 -text "[mc {to same widget class only}]" \
	      -variable Option::_option_(copyAttributesMode) -value SAME_WIDGET_TYPE] \
	-row 0 -column 1 -sticky w
    grid [radiobutton $f1.radCopy2 -text "[mc {to all widget classes}]" \
	      -variable Option::_option_(copyAttributesMode) -value ALL_WIDGET_TYPES] \
	-row 1 -column 1 -sticky w

    set w1 [checkbutton $fraCopy.chkLayoutManager \
		-text [mc "Copy geometry manager to destination when\ncopying layout options"] \
		-justify left \
		-variable Option::_option_(copyLayoutManager) \
		-command { Utility::SetVar2OnVar1 Option::_option_(copyLayoutManager) \
			       Option::_option_(copyLayoutIfSameManager) } ]
    pack $w1 -anchor w -pady 0 -padx 0

    set w1 [checkbutton $fraCopy.chkIfLayoutManagerSame \
		-text [mc "Copy layout options only if destination\nhas same geometry manager"] \
		-justify left \
		-variable Option::_option_(copyLayoutIfSameManager) \
		-command { Utility::SetVar2OnVar1 Option::_option_(copyLayoutIfSameManager) \
			       Option::_option_(copyLayoutManager) } ]
    pack $w1 -anchor w -pady 0 -padx 0
    
    # textbox options
    TitleFrame $page.fraText -text "[mc {Textbox}]"
    pack $page.fraText -anchor w -pady 3
    set fraText [$page.fraText getframe]

    grid [label $fraText.lblTabs -text "[mc {Tab stops}]:"] \
	-row 0 -column 0 -sticky w -pady 2
    grid [entry $fraText.entTabs -width 4 \
	      -textvariable Option::_option_(textTabs)] \
	-row 0 -column 1 -sticky w -padx 4

    grid [button $fraText.textFont -text "[mc {Font}]..." \
	      -command "Font::ModifyFont textFont"] \
	-row 0 -column 2 -columnspan 2 -sticky w -padx 5

    grid [checkbutton $fraText.chkWordWrap \
	      -text "[mc {Word wrap}]" -variable Option::_option_(textWordWrap)] \
	-row 1 -column 0 -columnspan 2 -sticky w
    
}


# builds widgets for the attributes page; does not populate the shown and
# hidden widget attribute listboxes
proc Option::CreateAttributesPage { } {

    variable _option_
    variable _page_
    variable _lstShownWidgetAttributes_
    variable _lstHiddenWidgetAttributes_
    
    set page $_page_(attributes)
    
    # widgets to allow selection of widget attributes
    TitleFrame $page.fraWidget -text "[mc {Widget Attributes}]"
    pack $page.fraWidget -anchor nw -pady 3
    set fraWidget [$page.fraWidget getframe]
    
    grid [label $fraWidget.labShown -text "[mc {Show}]"] -row 1 -column 0
    grid [label $fraWidget.labHidden  -text "[mc {Hide}]"] -row 1 -column 2
    set lstShown  [Listbox::CreateListbox $fraWidget.fraShown \
		       -selectmode extended -height 6 -width 20]
    set _lstShownWidgetAttributes_ $lstShown
    set lstHidden [Listbox::CreateListbox $fraWidget.fraHidden \
		       -selectmode extended -height 6 -width 20]
    set _lstHiddenWidgetAttributes_ $lstHidden
    grid [Listbox::GetListboxFrame $lstShown]  -row 2 -column 0 -sticky n
    grid [Listbox::GetListboxFrame $lstHidden] -row 2 -column 2 -sticky n
    bind $lstShown  <Double-ButtonRelease-1> \
	"Listbox::MoveSelectedItems $lstShown $lstHidden"
    bind $lstHidden <Double-ButtonRelease-1> \
	"Listbox::MoveSelectedItems $lstHidden $lstShown"
    
    set fraButton [frame $fraWidget.fraButton -bd 0]
    MakeButtonSet $fraButton VERITICAL {} \
	add    "[mc {Add ->}]"    "Listbox::MoveSelectedItems $lstShown $lstHidden" \
	remove "[mc {<- Rem}]" "Listbox::MoveSelectedItems $lstHidden $lstShown"
    grid $fraButton -row 2 -column 1 -padx 3

    set w1 [checkbutton $fraWidget.chkShowHiddenAttributes \
		-text "[mc {Show hidden attributes}]" \
		-variable Option::_option_(showHiddenAttributes)]		
    grid $w1 -row 3 -column 0 -columnspan 3 -sticky w -pady 3
    set w1 [checkbutton $fraWidget.chkBuildWithHiddenAttributes \
		-text "[mc {Build with hidden attributes}]" \
		-variable Option::_option_(buildWithHiddenAttributes)]		
    grid $w1 -row 4 -column 0 -columnspan 3 -sticky w -pady 3

}


# builds widgets for the attributes page; does not populate the default event listbox
proc Option::CreateBindingPage { } {

    variable _option_
    variable _page_
    variable _lstDefaultEventList_
    
    set page $_page_(bindings)

    # widgets to allow addition and removal of events from the default event list
    TitleFrame $page.fraEvent -text "[mc {Default Events}]"
    pack $page.fraEvent -anchor nw -pady 3
    set fraEvent [$page.fraEvent getframe]
    
    # labeled entry for event name
    grid [label $fraEvent.labEvent -text "[mc {Event Name}]:"] \
	-row 0 -column 1 -padx 3 -pady 3 -sticky nw
    set entEventName [entry $fraEvent.entEvent -width 20]
    grid $entEventName -row 1 -column 1 -padx 3 -pady 3 -sticky nw

    # listbox of current default events
    set lstDefaultEventList [Listbox::CreateListbox $fraEvent.fraDefault \
				 -selectmode extended -height 10 -width 25]
    set _lstDefaultEventList_ $lstDefaultEventList

    # buttons to add and delete events
    set fraButton [frame $fraEvent.fraButton -bd 0]
    MakeButtonSet $fraButton VERITICAL {} \
	add    "[mc {Add ->}]"  "Binding::InsertDefaultEvent $lstDefaultEventList $entEventName" \
	remove "[mc {Delete}]" "Listbox::DeleteListboxItems $lstDefaultEventList"

    grid $fraButton -row 1 -column 1 -padx 3 -sticky e
    grid [label $fraEvent.labList -text "[mc {Default Event List}]"] \
	-row 0 -column 2 -padx 3 -pady 3
    grid [Listbox::GetListboxFrame $lstDefaultEventList] \
	-row 1 -column 2 -sticky nw -padx 3 -pady 2

}


proc Option::CreateBuildPage { } {

    variable _option_
    variable _page_
    set page $_page_(build)

    set fraBuild $_page_(build)

    set w1 [checkbutton $fraBuild.chkBuildSetVariableAsPath \
		-text "[mc {Use set variable name as path in layout and binding commands}]" \
		-variable ::Option::_option_(buildSetVariableAsPath)]		
    pack $w1 -anchor w -pady 0 -pady 0

    # checkbutton to check number of coordinates in canvas
    set w1 [checkbutton $fraBuild.chkBuildCheckCanvasObjectCoord \
		-text "[mc {Check number of coordinates in canvas objects}]" \
		-variable ::Option::_option_(buildCheckCanvasObjectCoord)]		
    pack $w1 -anchor w -pady 0 -pady 0

    # widgets to specify run options
    TitleFrame $fraBuild.fraRun -text "[mc {Run Settings}]"
    pack $fraBuild.fraRun -anchor nw -pady 6
    set fraRun [$fraBuild.fraRun getframe]

    set w1 [checkbutton $fraRun.chkBuildOpenConsole \
		-text "[mc {Open console window when building run}]" \
		-variable ::Option::_option_(buildOpenConsole)]		
    pack $w1 -anchor w -pady 0 -pady 0

    set w1 [checkbutton $fraRun.chkBuildWithdrawRoot \
		-text "[mc {Withdraw root window when building run from toplevel widget}]" \
		-variable ::Option::_option_(buildWithdrawRoot)]		
    pack $w1 -anchor w -pady 0 -pady 0

    set w1 [checkbutton $fraRun.chkBuildBindToAppWidgets \
		-text "[mc {Enable selection of widget forms from application}]" \
		-variable ::Option::_option_(buildBindToAppWidgets)]		
    pack $w1 -anchor w -pady 0 -pady 0

    set w1 [checkbutton $fraRun.chkBuildBindToRunApp \
		-text "[mc {Enable run-time modification of application properties}]" \
		-variable ::Option::_option_(buildEnableAppModification)]		
    pack $w1 -anchor w -pady 0 -pady 0

    # widgets to specify blank lines in built code
    TitleFrame $fraBuild.fraBlank -text "[mc {Generated Tcl Code}]"
    pack $fraBuild.fraBlank -anchor nw -pady 0
    set fraBlank [$fraBuild.fraBlank getframe]

    # code for widgets and proc that manage indent tabs/spaces
    set f [frame $fraBlank.fraSpaces -bd 0]
    grid $f -row 0 -column 0 -pady 0 -sticky w -columnspan 3
    #	pack $f -anchor w -pady 0 -pady 0
    set w1 [checkbutton $f.chkBuildUseSpaces \
		-text "[mc {Use spaces instead of tabs for indenting code}]" \
		-variable ::Option::_option_(buildIndentUseSpaces)]
    pack $w1 -side left
    set w2 [label $f.lbNumSpaces -text "  "]
    pack $w2 -side left
    set w2 [SpinBox $f.spnNumSpaces \
		-range {1 20 1} -textvariable ::Option::_option_(buildIndentNumSpaces) \
		-width 3]
    pack $w2 -side left
    $w1 configure -command "Option::SetStateOnNumSpaces $w2"
    proc SetStateOnNumSpaces { spinBox } {
	if { [Option::GetOption buildIndentUseSpaces] } {
	    $spinBox configure -state normal
	} else {
	    $spinBox configure -state disabled
	}
    }
    SetStateOnNumSpaces $w2

    set l1 [label $fraBlank.lblBuildBlank -text "[mc {Insert a blank line before}]: "]
    grid $l1 -row 1 -column 0 -padx 0 -pady 0 -sticky w

    set w1 [checkbutton $fraBlank.chkBuildBlankBeforeWidget \
		-text "[mc {widgets}]" -variable ::Option::_option_(buildBlankBeforeWidget)]		
    grid $w1 -row 1 -column 1 -padx 0 -pady 0 -sticky w

    set w1 [checkbutton $fraBlank.chkBuildBlankBeforeProc \
		-text "[mc {procs}]" -variable ::Option::_option_(buildBlankBeforeProc)]		
    grid $w1 -row 1 -column 2 -padx 0 -pady 0 -sticky w

    set w1 [checkbutton $fraBlank.chkBuildBlankBeforeNamespace \
		-text "[mc {namespaces}]" -variable ::Option::_option_(buildBlankBeforeNamespace)]		
    grid $w1 -row 2 -column 1 -padx 0 -pady 0 -sticky w

    set w1 [checkbutton $fraBlank.chkBuildBlankBeforeCode \
		-text "[mc {code}]" -variable ::Option::_option_(buildBlankBeforeCode)]		
    grid $w1 -row 2 -column 2 -padx 0 -pady 0 -sticky w

}


proc Option::WriteOptionDatabaseFile { } {
    variable _option_
    variable _optionDatabaseFile_
    if [catch {open $_optionDatabaseFile_ "w"} fileID] {
	return 0
    }
    puts $fileID "[Main::GetSystem version] options"
    puts $fileID [array get _option_]
    close $fileID
}


proc Option::ReadOptionDatabaseFile { } {
    variable _option_
    variable _optionDatabaseFile_
    if [catch {open $_optionDatabaseFile_ "r"} fileID] {
	return 0
    }
    gets $fileID line ; # version ... skip it
    gets $fileID line ; # array of options
    array set _option_ $line
    close $fileID
}


proc Option::SetOption { option value } {
    variable _option_
    set _option_($option) $value
}


proc Option::GetOption { option } {
    variable _option_
    if { ![info exists _option_($option)] } {
	error "[mc {Getting unset option}] \"$option\"" ; # shouldn't happen
    }
    return $_option_($option)
}
