# -------------------------------------------------------------
# tree.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------


namespace eval Tree {

    namespace import ::Attribute::DeleteWidgetAttributes
    namespace import ::Layout::DeleteLayoutAttributes
    namespace import ::Binding::DeleteWidgetBindings
    namespace import ::General::DeleteGeneralParameters
    
    variable _currentNode_ 0 
    variable _nextNodeNumber_ 0
    variable gNode ; # array of node data, each element a list of data for one node
    variable index ; # array of indices, one for each field, giving its position 

    namespace export CreateTree ClearTree SetCurrentNode GetCurrentNode
    namespace export InsertNode DeleteNode DeleteBranch
    namespace export SetNodeItem GetNodeItem BranchContainsNode
    namespace export ReplaceChildNumber GetSiblingList

}	


proc Tree::CreateTree {  } {
    variable gNode
    variable index
    variable _numItems_
    variable _nextNodeNumber_

    set _numItems_		5 ; # number of items stored at each node
    set index(name) 		0 ; # required
    set index(parentNumber)	1 ; # required
    set index(childList)	2 ; # required
    set index(expanded) 	3 ; # required for display of widget tree
    set index(widgetType)	4

}


# deletes all nodes of the tree and all attributes, except root (node 0)
proc Tree::ClearTree { } {
    variable _currentNode_
    variable _nextNodeNumber_

    # loop once for each child of root node
    foreach childNumber [GetNodeItem childList 0] {
	DeleteBranch $childNumber 0
    }
    set _currentNode_ 0
    set _nextNodeNumber_ 0
    
    # reset all properties of the root node
    Properties::InitializeWidgetProperties 0
}


# creates and initializes node 'nodeNumber' to values given in 'itemValues' list
proc Tree::CreateNode { nodeNumber itemValues } {
    variable gNode
    variable _numItems_
    if { [info exists gNode($nodeNumber)] } {
	error "[mc {Trying to create existing node}] '$nodeNumber'"
    }
    if { [llength $itemValues] != $_numItems_ } {
	error "[mc {Wrong number of items in}] '$itemValues' [mc {for creating node}] '$nodeNumber'"
    }
    set gNode($nodeNumber) $itemValues	
}


# returns the value for 'item' from the given list of 'itemValues'; used
# in other modules (e.g. file.tcl) for extracting items during file reads
proc Tree::GetItemFromList { item itemValues } {
    variable index
    return [lindex $itemValues $index($item)]
}


# sets the item 'item' (e.g. 'name') of node 'nodeNumber' to the value 'value'
proc Tree::SetNodeItem { item nodeNumber value } {
    variable gNode
    variable index
    if { ![info exists index($item)] } {
	error "[mc {Unrecognized node item}]: $item"
    }
    set gNode($nodeNumber) [lreplace $gNode($nodeNumber) \
				$index($item) $index($item) $value]
}


# gets the value of item 'item' of node 'nodeNumber'
proc Tree::GetNodeItem { item nodeNumber } {
    variable gNode
    variable index
    if { ![info exists index($item)] } {
	error "[mc {Unrecognized node item}]: $item"
    }
    return [lindex $gNode($nodeNumber) $index($item)]
}


proc Tree::NodeExists { nodeNumber } {
    variable gNode
    if { [info exists gNode($nodeNumber)] } {
	return 1
    } else {
	return 0
    }
}


proc Tree::GetTreeArray { } {
    variable gNode
    return [array get gNode]
}


proc Tree::SetTreeArray { array } {
    variable gNode
    array set gNode $array
}


proc Tree::GetNodeItems { nodeNumber } {
    variable gNode
    return $gNode($nodeNumber)
}


proc Tree::SetNodeItems { nodeNumber items } {
    variable gNode
    set $gNode($nodeNumber) $items
}


proc Tree::TreeTraverse { {node_number 0} } {

    # loop once for each child of this node, traversing its branch
    foreach childNumber [GetNodeItem childList $node_number] {
	TreeTraverse $childNumber
    }

}


proc Tree::InsertNode { currentNodeNumber newNodeName relation \
			    widgetType {expanded 0} } {

    variable gNode
    
    # check for proper relation; must be PARENT, CHILD or SIBLING
    if { $relation != "CHILD" && $relation != "SIBLING" && $relation != "PARENT" } {
    	error "[mc {Relation must be}] PARENT, CHILD or SIBLING"
    }
    
    # in this case, the new node becomes a child of the current node's
    # parent, and the current node becomes a child of the new
    # node
    if { $relation == "PARENT" } {
	if { $currentNodeNumber == 0 } {
	    error "[mc {Can't insert parent at root node}]"
	}
	set newNodeNumber [GetNextNodeNumber]
	set parentNumber [GetParentNumber $currentNodeNumber]

	# create the new node; make it expanded
	set gNode($newNodeNumber) [list $newNodeName $parentNumber \
				       $currentNodeNumber 1 $widgetType]

	# replace the current child with the new one in parent's child list
	ReplaceChildNumber $currentNodeNumber $newNodeNumber
	
	# update the parent number of the current node (to the new node number)
	SetParentNumber $currentNodeNumber $newNodeNumber
	
    }

    # in this case, the current node becomes the parent of the new
    # node
    if { $relation == "CHILD" } {
	set newNodeNumber [GetNextNodeNumber]
	set parentNumber $currentNodeNumber
	
	# create the new node
	set gNode($newNodeNumber) [list $newNodeName $parentNumber {} $expanded $widgetType]
	
	# update the child list of the parent node
	set childList [GetChildList $parentNumber]
	## insert new node number at the end of child list for the parent
	set childList [linsert $childList end $newNodeNumber]
	SetChildList $parentNumber $childList
	
    }
    
    # in this case, the parent of the current node becomes the parent
    # of the new node
    if { $relation == "SIBLING" } {	
	if { $currentNodeNumber == 0 } {
	    error "[mc {Can't insert sibling for root node}]"
	}
	set newNodeNumber [GetNextNodeNumber]
	set parentNumber [GetParentNumber $currentNodeNumber]

	# create the new node
	set gNode($newNodeNumber) [list $newNodeName $parentNumber {} $expanded $widgetType]
	
	# update the child list of the parent node
	## find position of current node in the child list of its parent
	set childList [GetChildList $parentNumber]
	set sibPosition [lsearch $childList $currentNodeNumber]
	if { $sibPosition == -1 } {
	    error "[mc {Sibling not found in child list of parent}]" ; # shouldn't happen
	}
	## insert the new node just after current node in the child list
	set childList [linsert $childList [incr sibPosition] $newNodeNumber]
	SetChildList $parentNumber $childList		
	
    }

    return $newNodeNumber

}


# sets and returns the next unique node number (for creating a new node)
proc Tree::GetNextNodeNumber { } {
    variable _nextNodeNumber_
    return [incr _nextNodeNumber_]
}

# returns the next node number without incrementing it; used when saving
# tree to the file
proc Tree::GetNextNodeNumberNoInc { } {
    variable _nextNodeNumber_
    return $_nextNodeNumber_
}

# set next node number to the specified value 'nodeNumber'; used when loading
# an existing tree from file, so subsequent calls to Tree::GetNextNodeNumber
# don't duplicate existing node numbers
proc Tree::SetNextNodeNumber { nodeNumber } {
    variable _nextNodeNumber_
    set _nextNodeNumber_ $nodeNumber
}


proc Tree::SetNodeName { node_number node_name } {
    variable gNode
    set gNode($node_number) [lreplace $gNode($node_number) 0 0 $node_name]
}


proc Tree::GetNodeName { node_number } {
    variable gNode
    return [lindex $gNode($node_number) 0]
}


proc Tree::SetParentNumber { node_number parent_number } {
    variable gNode
    set gNode($node_number) [lreplace $gNode($node_number) 1 1 $parent_number]
}

proc Tree::GetParentNumber { node_number } {
    variable gNode
    return [lindex $gNode($node_number) 1]
}


proc Tree::SetChildList { node_number child_list } {
    variable gNode
    set gNode($node_number) [lreplace $gNode($node_number) 2 2 $child_list]
}


proc Tree::GetChildList { node_number } {
    variable gNode
    return [lindex $gNode($node_number) 2]
}


# returns a list of the siblings of node 'nodeNumber'; the list
# does not include 'nodeNumber'
proc Tree::GetSiblingList { nodeNumber } {
    #	variable gNode
    # root node has no siblings
    if { $nodeNumber == 0 } { return {} }
    set parentNumber [GetParentNumber $nodeNumber]
    set childList [GetChildList $parentNumber]
    set i [lsearch $childList $nodeNumber]
    set siblingList [lreplace $childList $i $i] ; # remove current node from list
    return $siblingList
}


# in the sibling list of 'nodeNumber' swaps position of 'nodeNumber' with
# the sibling before or after it, depending on value given to 'direction', which
# must be one of "prev" or "next", respectively
proc Tree::SwapSiblings { nodeNumber direction } {
    set parentNumber [GetParentNumber $nodeNumber]
    set childList [GetChildList $parentNumber]
    set numChildren [llength $childList]
    set i [lsearch $childList $nodeNumber]
    if { $direction == "prev" } {
	if { $i != 0 } {
	    set childList [lreplace $childList $i $i]
	    set childList [linsert $childList [incr i -1] $nodeNumber]
	    SetNodeItem childList $parentNumber $childList
	}
    }
    if { $direction == "next" } {
	if { $i != [expr $numChildren - 1] } {
	    set childList [lreplace $childList $i $i]
	    set childList [linsert $childList [incr i +1] $nodeNumber]
	    SetNodeItem childList $parentNumber $childList
	}
    }
}


# returns the number of the node nearest to 'nodeNumber' in the
# list of its siblings, with preference to the node immediately following
# it; returns null if there isn't one
proc Tree::GetNearestSibling { nodeNumber } {

    # root node has no siblings
    if { $nodeNumber == 0 } { return {} }
    
    # get list of all siblings, including this node and count them
    set sibList [GetChildList [GetParentNumber $nodeNumber]]
    set sibCount [llength $sibList]
    if { $sibCount == 1 } { return {} } ; # this node the only sib
    set nodePos [lsearch $sibList $nodeNumber] ; # position of this node
    if { ($nodePos + 1) == $sibCount } {
	# this node is last in sib list
	set sibNode [lindex $sibList [expr $nodePos - 1]]
    } else {
	set sibNode [lindex $sibList [expr $nodePos + 1]]
    }
    return $sibNode
}


# deletes single node 'nodeNumber' from the tree; works
# only on nodes that have 0 or 1 children (to avoid ambiguous parentage)
# N.B.: makes a call to 'Properties::DeleteWidgetProperties'
# for each node deleted
proc Tree::DeleteNode { nodeNumber } {

    variable gNode                     
    
    # can't delete root node                     
    if { $nodeNumber == 0 } {
	error "[mc {Cannot delete root node}]"
    }
    
    set childList [GetChildList $nodeNumber] 
    if { [llength $childList] > 1 } {
	error "[mc {Cannot delete a node that has more than one child}]"
    }

    # adjust the parent's child list
    if { $childList == {} } {
	ReplaceChildNumber $nodeNumber

	# remove the node from running application
	Build::DeleteAppWidget $nodeNumber

    } else {
	# in this case, child list is a single number; adjust its parentage
	SetParentNumber $childList [GetParentNumber $nodeNumber]
	ReplaceChildNumber $nodeNumber $childList
    }

    # unset the array elements storing the property values
    Properties::DeleteWidgetProperties $nodeNumber

    WidgetTree::DestroyNodeLabel $nodeNumber ; # destroy label in widget tree

    # delete this node
    unset gNode($nodeNumber)

}                                 


# deletes node 'nodeNumber' and all its descendants from the tree;
# returns the number of nodes deleted; initial call should be with
# 'numDeleted' set 0
# N.B.: makes a call to 'Properties::DeleteWidgetProperties'
# for each node deleted
proc Tree::DeleteBranch { nodeNumber numDeleted } {

    variable gNode                     
    
    # can't delete root node                     
    if { $nodeNumber == 0 } { error "[mc {Cannot delete root node}]" }
    
    incr numDeleted

    # delete this node from its parent's child list
    ReplaceChildNumber $nodeNumber

    # loop once for each child of this node, traversing its branch
    foreach child_number [GetChildList $nodeNumber] {
	set numDeleted [DeleteBranch $child_number $numDeleted]
    }

    # remove the node from running application
    Build::DeleteAppWidget $nodeNumber

    # check if this is the root node; free lock on it if so
    if { [Project::NodeIsRoot $nodeNumber] } {
	Project::FreeRootNode
    }

    # unset the array elements storing the property values
    Properties::DeleteWidgetProperties $nodeNumber

    # destroy label in widget tree
    WidgetTree::DestroyNodeLabel $nodeNumber

    # delete this node
    unset gNode($nodeNumber)
    
    return $numDeleted

}                                 


# deletes the node 'nodeNumber' from it's parent's child list;
# replaces it with 'newChildNumber' if specified
proc Tree::ReplaceChildNumber { nodeNumber {newChildNumber {}} } {

    set parentNodeNumber [GetParentNumber $nodeNumber]
    set childList [GetChildList $parentNodeNumber]
    set childPosition [lsearch $childList $nodeNumber]
    if { $childPosition == -1 } {
	error "[mc {Child not found in child list of parent}]"
    }
    if { $newChildNumber == {} } {
	set childList [lreplace $childList $childPosition $childPosition]
    } else {
	set childList [lreplace $childList $childPosition $childPosition \
			   $newChildNumber]
    }
    SetChildList $parentNodeNumber $childList

}


# makes node 'nodeNumber' the current node of the tree
proc Tree::SetCurrentNode { nodeNumber } {
    variable _currentNode_ $nodeNumber
    #	set _currentNode_ $nodeNumber
}


# returns the number of the current node of the tree
proc Tree::GetCurrentNode { } {
    variable _currentNode_
    return $_currentNode_
}



# returns 1 if the branch from 'fromNode' contains the node 'findNode';
# 0 otherwise
proc Tree::BranchContainsNode { fromNode findNode } {
    if { $fromNode == $findNode } { return 1 }
    # loop once for each child of the from node, traversing its branch
    foreach childNumber [GetNodeItem childList $fromNode] {
	if { [BranchContainsNode $childNumber $findNode] } { return 1 }
    }
    return 0
}


# for each child of parent 'oldParentNumber' assigns it the
# new parent number 'newParentNumber'
proc Tree::ReparentChildren { oldParentNumber newParentNumber } {
    # loop once for each child of parent
    foreach childNumber [GetNodeItem childList $oldParentNumber] {
	SetNodeItem parentNumber $childNumber $newParentNumber
    }
}

