# -------------------------------------------------------------
# state.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
#
# Modified by andrasy on 10/22/06 for :
# - tcl 8.4.13
# - BWidget 1.7
# - msgcat calls added for translation
# - combobox from Bryan Oakley replaced by ::ComboBox of BWidget
# -------------------------------------------------------------



namespace eval State {

    variable _state_ ; # array of state variables
    
    variable _stateDatabaseFile_ "tkbState.txt"
    # name of file in which to store the state
    
    namespace export IntializeState GetState SetState

    variable _pendingModifier_ 0
    # used to indicate that a modifier key (Alt or Control) has been pressed
    # and is pending, waiting for the next key (don't modify state on next key
    # stroke if so)

}


proc State::InitializeState { } {
    
    #	SetState version            "tkBuilder version 1.0.2"
    #	SetState modified			0  ; # boolean; have changes been made since last save to Tkb
    #	SetState lastTkbFileName	{} ; # full Tkb file path used in last open/save file
    SetState lastTkbDirectory	{} ; # directory used in last open/save of Tkb file
    #	SetState lastTclFileName	{} ; # full Tcl file path used in last open/save file
    SetState lastTclDirectory	{} ; # directory used in last open/save of Tcl file
    SetState lastTkwFileName    {} ; # full Tkw file path used in last open/save file
    SetState lastTkwDirectory   {} ; # directory used in last open/save of Tkw file
    SetState recentTkbFiles     {} ; # list of recent tkb files
    SetState displayTclFileWarning 1 ; # boolean; set to 0 first time warning is displayed

    ReadStateDatabaseFile

    # reset some states to initial condition
    #	SetState version  "tkBuilder version 1.0.2"
    SetState modified 0; # boolean; have changes been made since last save to Tkb
    SetState lastTkbFileName {}; # full Tkb file path used in last open/save file
    SetState lastTclFileName {}; # full Tcl file path used in last open/save file

}


proc State::SetState { item value } {
    variable _state_
    set _state_($item) $value
}


proc State::GetState { item } {
    variable _state_
    if { ![info exists _state_($item)] } {
	error "[mc {Unrecognized state item}]: $item"
    }
    return $_state_($item)
}


proc State::WriteStateDatabaseFile { } {
    variable _state_
    variable _stateDatabaseFile_
    if [catch {open $_stateDatabaseFile_ "w"} fileID] {
	return 0
    }
    puts $fileID "[Main::GetSystem version] state"
    puts $fileID [array get _state_]
    close $fileID
}

proc State::ReadStateDatabaseFile { } {
    variable _state_
    variable _stateDatabaseFile_
    if [catch {open $_stateDatabaseFile_ "r"} fileID] {
	return 0
    }
    gets $fileID line ; # version ... skip it
    gets $fileID line ; # array of states
    array set _state_ $line
    close $fileID	
}


# checks if key press is just a modifier (in which case
# modified state is not changed)
proc State::CheckModifiedState { key modifier } {
    variable _pendingModifier_

    # set modified only if an Alt or Control key stroke is not pending
    if { !$_pendingModifier_ } {
	if { $modifier != {} } {
	    switch $key {
		Tab - Up - Down - Left - Right - Escape - Shift {}
		default {State::SetState modified 1}
	    }
	}
	# do seperately, as modifier is null for Delete
	if { $key == "Delete" } {State::SetState modified 1}
    }
    set _pendingModifier_ 0
}
# bound to KeyPress event on . (root window)
proc State::SetPendingModifier { key } {
    variable _pendingModifier_
    switch $key {
	Alt_L - Alt_R - Control_L - Control_R {
	    set _pendingModifier_ 1
	}
    }
}
# bound to KeyRelease event on . (root window)
proc State::UnsetPendingModifier { key } {
    variable _pendingModifier
    switch $key {
	Alt_L - Alt_R - Control_L - Control_R {
	    set _pendingModifier_ 0
	}
    }
}
