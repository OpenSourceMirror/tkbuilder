# -------------------------------------------------------------
# dialog.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999, 2000 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
# -------------------------------------------------------------



namespace eval Dialog {
    variable _waitDialog_ ; # name of tkwait variable
    variable _name_
}


proc Dialog::CreateNameEntry { frame } {

    variable _name_
    set _name_(frame) $frame
    set _name_(prompt) [label $frame.lblPrompt -text {} -foreground lightgrey]
    pack $_name_(prompt) -side left -padx 0
    set _name_(entry) [entry $frame.entInput -width 10 -state disabled \
			   -background [Utility::GetBackgroundColour]]
    pack $_name_(entry) -side left -fill x -expand 1 -padx 0
    
    set fraCancel [frame $frame.fraCancel -bd 0 -height 18 -width 20]
    pack $fraCancel -side right -pady 1 -padx 1
    pack propagate $fraCancel 0
    set fraOK [frame $frame.fraOK -bd 0 -height 18 -width 20]
    pack $fraOK -side right -pady 1 -padx 1
    pack propagate $fraOK 0
    
    image create photo ok -file [file join [Main::GetSystem dirIcons] ok.gif]
    image create photo cancel -file [file join [Main::GetSystem dirIcons] cancel.gif]

    set _name_(butOK) [button $fraOK.butOK -image ok -state disabled \
			   -command {set Dialog::_waitDialog_ OK}]
    set _name_(butCancel) [button $fraCancel.butCancel -image cancel -state disabled \
			       -command {set Dialog::_waitDialog_ CANCEL}]
    pack $_name_(butCancel)
    pack $_name_(butOK)

    bind $_name_(entry) <Escape> {set Dialog::_waitDialog_ CANCEL}
    bind $_name_(entry) <FocusOut> {set Dialog::_waitDialog_ CANCEL}
    bind $_name_(entry) <Return> {set Dialog::_waitDialog_ OK}
    
}


proc Dialog::EnableNameEntry { {prompt {}} {value {}} } {
    variable _name_
    variable _waitDialog_
    
    $_name_(prompt) config -foreground black
    $_name_(prompt) config -text $prompt
    $_name_(entry) config -state normal
    $_name_(entry) config -background white
    $_name_(entry) delete 0 end
    $_name_(entry) insert 0 $value
    #	$_name_(entry) selection from 0
    #	$_name_(entry) selection to end
    $_name_(entry) selection range 0 end ; # 012800

    $_name_(butOK) config -state normal
    $_name_(butCancel) config -state normal
    
    set focusCaller [focus]
    focus $_name_(entry)
    tkwait variable Dialog::_waitDialog_
    set entryText [string trim [$_name_(entry) get]]
    Dialog::DisableNameEntry
    if { $_waitDialog_ == "OK" } {
	return [list "OK" $entryText]
    } else {
	catch { focus $focusCaller }
	return [list "CANCEL" {}]
    }
}


proc Dialog::DisableNameEntry { } {
    variable _name_
    $_name_(prompt) config -text {}
    $_name_(entry) delete 0 end
    $_name_(entry) config -state disabled
    $_name_(entry) config -background [Utility::GetBackgroundColour]
    $_name_(butOK) config -state disabled
    $_name_(butCancel) config -state disabled
    $_name_(prompt) config -text {}
}
