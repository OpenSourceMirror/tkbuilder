# -------------------------------------------------------------
# text.tcl
# This file is part of the tkBuilder distribution
# Copyright (C) 1999 Frank Schnekenburger
# Distributed under the terms of the GNU General Public License
# -------------------------------------------------------------


namespace eval Text {
    variable _text_
    variable _entry_
    variable _textList_ {}; # list of all textboxes used within tkBuilder
}


# opens a floating text entry window that is bound to entry 'entry', at
# coordinates 'X' and 'Y'; text entered into the textbox will be written
# to 'entry' when the box is closed; creates a toplevel window 
# '.textEntryWindow' the exists for remainder of session;
# 'keyBinding' is the optional binding command for a KeyPress event
# 'command' is executed when the textbox is closed
proc Text::OpenTextEntryWindow { entry {keyBinding {}} {command {}} } {
    variable _entry_
    variable _text_
    
    set X [winfo rootx $entry]
    set Y [winfo rooty $entry]

    if { ![winfo exists .textEntryWindow] } {
	set t [toplevel .textEntryWindow]
	wm overrideredirect $t 1
	wm geometry .textEntryWindow +$X+$Y

	set f [frame $t.frame -bd 2 -relief ridge]
	pack $f

	set text [text $f.txtBox -height 4 -width 20 \
		      -tabs [Text::GetTabs] \
		      -wrap [Text::GetWordWrap]]
	set _text_ $text
	grid $text -row 0 -column 0 -sticky nsew
	bind $text <FocusOut> "Text::CloseTextEntryWindow [list $command]"
	bind $text <Key-Escape> "Text::CloseTextEntryWindow [list $command]"
	bind $text <ButtonPress-2> "Text::CloseTextEntryWindow [list $command]"
	bind $text <KeyPress> $keyBinding

	# get character pixel height and width
	set charHeight [expr [winfo reqheight $text] / 4] ; # 4 is initial height
	set charWidth  [expr [winfo reqwidth $text] / 20] ; # 20 is initial width

	# build the grab frame and bindings for changing width
	set fraGrabV [frame $f.fraGrabV -width 4 -bd 1 -relief raised \
			  -cursor sb_h_double_arrow -bg blue]
	grid $fraGrabV -row 0 -column 1 -sticky ns
	bind $fraGrabV <ButtonRelease-1> \
	    "$text config -width \[expr (%X - \[winfo rootx $text]-4)/$charWidth]
			 $fraGrabV config -cursor sb_h_double_arrow"
	bind $fraGrabV <B1-Motion> "$fraGrabV config -cursor sb_v_double_arrow"

	# build the grab frame and bindings for changing height
	set fraGrabH [frame $f.fraGrabH -height 4 -bd 1 -relief raised \
			  -cursor sb_v_double_arrow -bg blue]
	grid $fraGrabH -row 1 -column 0 -sticky ew
	bind $fraGrabH <ButtonRelease-1> \
	    "$text config -height \[expr ceil((%Y - \[winfo rooty $text])/$charHeight)]
			 $fraGrabH config -cursor sb_v_double_arrow"
	bind $fraGrabH <B1-Motion> "$fraGrabH config -cursor sb_h_double_arrow"

    } elseif { [winfo ismapped .textEntryWindow] } {
	CloseTextEntryWindow
	update
	wm deiconify .textEntryWindow
	wm geometry .textEntryWindow +$X+$Y
    } else {
	update
	wm deiconify .textEntryWindow
	wm geometry .textEntryWindow +$X+$Y
    }
    #	raise .textEntryWindow ; # needed on unix box
    focus -force $_text_
    $_text_ delete 1.0 end
    $_text_ insert 1.0 [$entry get]
    set _entry_ $entry
}


# for previously opened text entry window, copies the text from the textbox 
# to the entry and closes textbox
proc Text::CloseTextEntryWindow { {commandOnClose {}} } {
    variable _entry_
    variable _text_
    $_entry_ delete 0 end
    set command [$_text_ get 1.0 "end -1 chars"]
    # trim trailing newline chars that might occur in commands,
    # as they'll bung up the command string when its built
    set command [string trim $command \n]
    $_entry_ insert 0 $command
    wm withdraw .textEntryWindow
    eval $commandOnClose
}



# creates a textbox with scrollbars; returns name of the textbox,
# which will be '$name.textbox'; creates a frame '$name' within
# which the textbox and scrollbars are packed; caller must pack
# this frame
proc Text::CreateTextbox { name args } {

    variable _textList_

    set f [frame $name -bd 0]
    text $f.textbox -tabs [Text::GetTabs] \
	-xscrollcommand [list Text::ScrollSet $f.xscroll \
			     [list grid $f.xscroll -row 1 -column 0 -sticky we]] \
	-yscrollcommand [list Text::ScrollSet $f.yscroll \
			     [list grid $f.yscroll -row 0 -column 1 -sticky ns]]
    eval {$f.textbox configure} $args
    scrollbar $f.xscroll -orient horizontal \
	-command [list $f.textbox xview]
    scrollbar $f.yscroll -orient vertical \
	-command [list $f.textbox yview]
    grid $f.textbox $f.yscroll -sticky news
    grid $f.xscroll -sticky news
    grid rowconfigure $f 0 -weight 1
    grid columnconfigure $f 0 -weight 1
    update
    grid propagate $f 0

    # add this textbox to the list of current textboxes
    if { [lsearch $_textList_ $f.textbox] == -1 } {
	lappend _textList_ $f.textbox
    }

    return $f.textbox

}


# manages the scrollbars for the textbox created with 'CreateTextbox'
proc Text::ScrollSet { scrollbar command offset size } {
    if { $offset != 0.0 || $size != 1.0 } {
	eval $command
	$scrollbar set $offset $size
    } else {
	set manager [lindex $command 0]
	$manager forget $scrollbar ; # hide the scroll bar
    }
}


# returns the name of the packing frame used to build the
# textbox 'textbox'; 'textbox' must be a scrolled textbox returned 
# by 'CreateTextbox' (truncates '.textbox' from $textbox)
proc Text::GetTextboxFrame { textbox } {
    set index [expr [string length $textbox] - 9]
    return [string range $textbox 0 $index]
}


# returns the name of the textbox widget, given 'name', which was
# used when the textbox was created with 'CreateTextbox'
proc Text::GetTextbox { name } {
    return "$name.textbox"
}


# returns the text in 'textbox' as a string
proc Text::GetText { textbox } {
    return [$textbox get 1.0 "end -1 chars"]
}


# sets the text in 'textbox' to 'text'; by default, clears existing text
# first
proc Text::SetText { textbox text {clear 1} } {
    if { $clear } {
	$textbox delete 1.0 end
    }
    $textbox insert 1.0 $text
}


proc Text::ClearText { textbox } {
    $textbox delete 1.0 end
}


proc Text::InsertText { textbox text } {
    $textbox insert end $text
}


# returns a list that contains an item for each line of text in 'textbox'
proc Text::TextboxToList { textbox } {
    set textString [$textbox get 1.0 "end -1 chars"]
    set textString [string trim $textString " \n"]
    return [split $textString \n]
}


# fills 'textbox' with the items in 'list'
proc Text::TextboxFromList { textbox list {clear 1} } {
    if { $clear } {
	$textbox delete 1.0 end
    }
    foreach line $list {
	$textbox insert end $line\n
    }
}


proc Text::SetWordWrap { } {
    variable _text_
    catch { $_text_ configure -wrap [Text::GetWordWrap] }
}

proc Text::SetTabs { } {
    variable _text_
    variable _textList_
    catch { $_text_ configure -tabs [Text::GetTabs] }
    foreach text $_textList_ {
	catch { $text configure -tabs [Text::GetTabs] }
    }
}

proc Text::GetWordWrap { } {
    if { [Option::GetOption textWordWrap] } {
	return word
    } else {
	return none
    }
}

proc Text::GetTabs { } {
    return [Option::GetOption textTabs]
}
